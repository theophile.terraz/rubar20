! modif de avril 2014 : 
! -- passage en arguments du nom de l'étude pour permettre une
!    éxécution automatique (chemin complet de l'étude)
! -- le fichier xyh se retrouve là où se trouvait l'étude
! -- en train de modifier les temps pour une éxécution automatique
! -- dans notre cas on ne regarde que le temps final sur nos cas tests suppression
! -- pour l'instant des autres cas de figures. 
! -- réécriture des messages à l'écran


! modif pour lire tpc
! modif du27/11/000 pour choisir le nombre de decimales en sortie
! modif en accord avec tpsmto4 pour format de xyh et avec une decimale sur x,y
! ecriture d'un fichier x,y,h,zf,z,vx,vy,fr
!        implicit logical (a-z)
        integer nemax,naemax,nnemax,nevmx,namax,nnamx,&
     &   nevamx,nnmax,namax1,nw,ndes
!       iik= nombre maximal de temps lus dans etude.tps
        parameter (nemax  =300000,&
     &             naemax = 4,&
     &      nnemax = 4,&
     &      namax  = 900000,&
     &       nnamx  = 2,&
     &      nnmax  = 360000,&
     &             nw    =   5000,&
     &      namax1 = 15000,ndes=100)
        integer ne,ia0,&
     &   na,nna(namax),nne(nemax),&
     &   ina(namax,nnamx),ine(nemax,nnemax),&
     &   iae(namax),nrefa(namax),nx,ny,irep,ire,ire2,ire3,&
     &   nn,ifdm,ie,ia,ja,jn,je,in,s,naf,iaf,i,j,n,ir1,ifro&
     &,idec1,idec2,ifsor
        double precision xn(nnmax),yn(nnmax),xf(namax1),yf(namax1)&
     &   ,co(nnmax)
        double precision xe(nemax),ye(nemax),zf(nemax),he(nemax),rad
        double precision ze(nemax),ve(nemax),ue(nemax),th(nemax)&
        &,tu(nemax)
        double precision xmin,xmax,ymin,ymax,zmin,zmax,tv(nemax)&
     &,qve(nemax),que(nemax),t,tn,fre(nemax),grav,hint(ndes,ndes)
        double precision xf2(namax1),yf2(namax1),vint(ndes,ndes)
        double precision zfmax,xk,eps
        character etude*255,don*15,uni*10,donnee*35,chain*6,rep*1&
     &,nomfic*40
        double precision sommehe,sommezf,sommez,sommev,sommefr
        character(len=32) :: arg_temp
       eps=0.0001
!       ouverture fichier donnees maillage
      ifdm=47
      ifro=46
	  ifsor=18
	  open(ifsor,file='toto.txt',status='unknown')
!
!new
	call get_command_argument(1, etude)
	
	!1st choice
!	call get_command_argument(2, arg_temp)
!	read (arg_temp,'(i10)') irep
	
	
	

!endnew	
!      write (*,*)'donnez le nom de l''etude'
!      read (*,'(a6)')etude
      open(ifdm,file=trim(etude)//'.dat',status='old')
      write(*,*)'-------lancement de trxyh---------'
      write(*,*)' lecture fichier données maillage'

!       write(*,*)' entrer le zfmax'
!       read(*,*)zfmax
      read (ifdm,'(i6)')ne
      do 100 ie = 1,ne
         read (ifdm,'(5i6)') n,(iae(j),j=1,n)
         read (ifdm,'(5i6)') nne(ie),(ine(ie,jn),jn=1,nne(ie))
         read (ifdm,'(5i6)') n,(iae(j),j=1,n)
 100  continue
      read (ifdm,'(i6)') na
      do 110 ia = 1,na
         read (ifdm,'(5i6)') nna(ia),(ina(ia,jn),jn=1,nna(ia))
       read (ifdm,'(6i6)')nrefa(ia),n,(iae(j),j=1,n)
 110  continue
      read (ifdm,'(i6)') nn
!      read(ifdm,'(10f8.1)')(xn(in),in=1,nn)
!      read(ifdm,'(10f8.1)')(yn(in),in=1,nn)
!      read(ifdm,'(10f8.3)',end=121)(co(in),in=1,nn)
! 121  close(ifdm)

      open(ifro,file=trim(etude)//'.frt',status='old',err=999)
      go to 998
 999  write(*,*)'pas de fichier de frottement ',trim(etude)//'.frt'
      stop
 998  read(ifro,'(10f8.3)')(fre(ie),ie=1,ne)
      close(ifro) 



       ! write(*,*) 'si vous voulez en sortie 1 decimale pour x et y'
       ! write(*,*) 'et 3 decimales pour h taper sur la touche entree'
       ! write(*,*) 'sinon donner successivement les deux chiffres'
       ! read (*,'(a1)')rep
       ! if (rep.eq.' ')then
       !    idec1=1
       !    idec2=3
       ! else
       	!2nd choice
	!call get_command_argument(2, arg_temp)
	!read (arg_temp,'(i10)') idec1
	
	!call get_command_argument(3, arg_temp)
	!read (arg_temp,'(i10)') idec2
	
	!!Définition des décimales que l'on souhaite :
	idec1 = 3
	idec2 = 5
	
	
        !   read(rep,'(i1)')idec1
        !   read(*,*)idec2
           if(idec1.gt.4.or.idec2.gt.5)then
             write (*,*) 'trop de decimales'
             stop
           elseif(idec1.lt.1.or.idec2.lt.3)then
             write (*,*) 'trop peu de decimales'
             stop
           endif
       ! endif
        if(idec1.eq.1)then
      read(ifdm,'(10f8.1)')(xn(in),in=1,nn)
      read(ifdm,'(10f8.1)')(yn(in),in=1,nn)
      read(ifdm,'(10f8.3)',end=121)(co(in),in=1,nn)
       else
      read(ifdm,'(10f8.4)')(xn(in),in=1,nn)
      read(ifdm,'(10f8.4)')(yn(in),in=1,nn)
      read(ifdm,'(10f8.3)',end=121)(co(in),in=1,nn)
        endif
 121  close(ifdm)
        do 125 ie=1,ne
           xe(ie)=xn(ine(ie,1))
           do 225 i=2,nne(ie)
              xe(ie)=xe(ie)+xn(ine(ie,i))
 225       continue
           xe(ie)=xe(ie)/nne(ie)
           ye(ie)=yn(ine(ie,1))
           do 325 i=2,nne(ie)
              ye(ie)=ye(ie)+yn(ine(ie,i))
 325       continue
           ye(ie)=ye(ie)/nne(ie)
           zf(ie)=co(ine(ie,1))
           do 425 i=2,nne(ie)
              zf(ie)=zf(ie)+co(ine(ie,i))
 425       continue
           zf(ie)=zf(ie)/nne(ie)
 125    continue

!          endif
!        endif
!        endif
! 700    write(*,*) ' voulez vous  ?'
!        write(*,*) '        les maxima     ----->   1'
!        write(*,*) '        un temps donne ----->   2'
!        write(*,*) 'une concentration a t donne->   3'
!        write(*,*) '        rien           ----->   0'
!new        read(*,*) irep

	write(*,*) '---récupération des résultats au temps final---' 
! ajout de statistiques le 31/01/2012
           sommehe=0.
		   sommezf=0.
		   sommez=0.
		   sommev=0
		   sommefr=0
 write(*,*) 'Ecriture des concentrations' 
  open(47,file=trim(etude)//'.tpc',status='old')
        !lecture du dernier temps de l'étude  
          open(12,file=trim(etude)//'.par',status='unknown')
          read(12,*)
          read(12,*)
          read(12,*)
          read(12,*)
       	read(12,*) arg_temp,arg_temp,arg_temp,arg_temp,arg_temp,tn


! fichier concentrations	
		open(48,file=trim(etude)//'.xyc2',status='unknown')
    write(48,*)'x y c zf0 zf dzf vfrot frot'


  3     read(47,'(f15.3)',end=4)t
!        write(*,*) t
        read(47,'(8f10.5)')(he(ie),ie=1,ne)
        read(47,'(8f10.5)')(que(ie),ie=1,ne)
        read(47,'(8f10.5)',end=5)(qve(ie),ie=1,ne)
        if (abs(tn-t).lt..1)go to 6
         go to 3
 5      if (abs(tn-t).lt..1)go to 6
 4      write(*,*)'le temps donne ne correspond '
        write(*,*)' a aucune ligne d''eau stockee'
        write(*,*)'derniere ligne d''eau = ',t
        close(47)
        !go to 700
 6      close(47)
! write(50,'(f15.6)')que(2)
!  write(50,*)que(2)
 do ie=1,ne
        
          write(48,'(2f15.3,f15.5,2f15.3,f15.4,f15.1,f15.3)')&
     & xe(ie),ye(ie),he(ie),zf(ie),que(ie)+zf(ie),&
     & que(ie),qve(ie),fre(ie)
    
! ajout de statistiques
           sommehe=sommehe+he(ie)
		   sommezf=sommezf+zf(ie)
		   sommez=sommez+he(ie)+zf(ie)
		   if(he(ie).gt.eps)then
		   sommev=sommev+sqrt(que(ie)**2+qve(ie)**2)/he(ie)
		   endif
		   sommefr=sommefr+fre(ie)
enddo
        close(48)
        write(ifsor,*)"h  zf    z  v   fr"
		write(ifsor,'(5f15.3)')sommehe/float(ne),sommezf/float(ne),&
     &sommez/float(ne),sommev/float(ne),sommefr/float(ne)
     
write(*,*) 'Fin de l''écriture sortie du programme...'     
     end
