C modif de avril 2014 : 
C -- passage en arguments du nom de l'étude pour permettre une
C    éxécution automatique (chemin complet de l'étude)
C -- le fichier xyh se retrouve là où se trouvait l'étude
C -- en train de modifier les temps pour une éxécution automatique
C -- Dans notre cas on ne regarde que le temps final sur nos cas tests suppression
C -- pour l'instant des autres cas de figures. 
C -- Réécriture des messages à l'écran


C modif pour lire tpc
C modif du27/11/000 pour choisir le nombre de decimales en sortie
C modif en accord avec tpsmto4 pour format de xyh et avec une decimale sur x,y
C ecriture d'un fichier x,y,h,zf,z,vx,vy,fr
C        IMPLICIT LOGICAL (A-Z)
        INTEGER NEMAX,NAEMAX,NNEMAX,NEVMX,NAMAX,NNAMX,
     :   NEVAMX,NNMAX,NAMAX1,NW,NDES
C       IIK= NOMBRE MAXIMAL DE TEMPS LUS DANS ETUDE.TPS
        PARAMETER (NEMAX  =300000,
     :             NAEMAX = 4,
     :      NNEMAX = 4,
     :      NAMAX  = 900000,
     :       NNAMX  = 2,
     :      NNMAX  = 360000,
     :             NW    =   5000,
     :      NAMAX1 = 15000,NDES=100)
        INTEGER NE,IA0,
     :   NA,NNA(NAMAX),NNE(NEMAX),
     :   INA(NAMAX,NNAMX),INE(NEMAX,NNEMAX),
     :   IAE(NAMAX),NREFA(NAMAX),NX,NY,IREP,IRE,IRE2,IRE3,
     :   NN,IFDM,IE,IA,JA,JN,JE,IN,S,NAF,IAF,I,J,N,IR1,IFRO
     :,idec1,idec2,ifsor
        DOUBLE PRECISION XN(NNMAX),YN(NNMAX),XF(NAMAX1),YF(NAMAX1)
     :   ,CO(NNMAX)
        DOUBLE PRECISION XE(NEMAX),YE(NEMAX),ZF(NEMAX),HE(NEMAX),RAD
        DOUBLE PRECISION ZE(NEMAX),VE(NEMAX),UE(NEMAX),TH(NEMAX)
     :   ,TU(NEMAX)
        DOUBLE PRECISION XMIN,XMAX,YMIN,YMAX,ZMIN,ZMAX,TV(NEMAX)
     :,QVE(NEMAX),QUE(NEMAX),T,TN,FRE(NEMAX),GRAV,HINT(NDES,NDES)
        DOUBLE PRECISION XF2(NAMAX1),YF2(NAMAX1),VINT(NDES,NDES)
        DOUBLE PRECISION ZFMAX,XK,EPS
        CHARACTER ETUDE*255,DON*15,UNI*10,DONNEE*35,CHAIN*6,REP*1
     :,nomfic*40
        double precision sommehe,sommezf,sommez,sommev,sommefr
        character(len=32) :: arg_temp
       EPS=0.0001
C       OUVERTURE FICHIER DONNEES MAILLAGE
      IFDM=47
      IFRO=46
	  ifsor=18
	  open(ifsor,file='toto.txt',status='unknown')
C
!NEW
	CALL get_command_argument(1, ETUDE)
	
	!1st choice
	CALL get_command_argument(2, arg_temp)
	read (arg_temp,'(I10)') IREP
	
	
	

!ENDNEW	
!      WRITE (*,*)'DONNEZ LE NOM DE L''ETUDE'
!      READ (*,'(A6)')ETUDE
      OPEN(IFDM,FILE=trim(ETUDE)//'.dat',STATUS='OLD')
      WRITE(*,*)'-------Lancement de Trxyh---------'
      WRITE(*,*)' Lecture fichier données maillage'

C       WRITE(*,*)' ENTRER LE ZFMAX'
C       READ(*,*)ZFMAX
      READ (IFDM,'(I6)')NE
      DO 100 IE = 1,NE
         READ (IFDM,'(5I6)') N,(IAE(J),J=1,N)
         READ (IFDM,'(5I6)') NNE(IE),(INE(IE,JN),JN=1,NNE(IE))
         READ (IFDM,'(5I6)') N,(IAE(J),J=1,N)
 100  CONTINUE
      READ (IFDM,'(I6)') NA
      DO 110 IA = 1,NA
         READ (IFDM,'(5I6)') NNA(IA),(INA(IA,JN),JN=1,NNA(IA))
       READ (IFDM,'(6I6)')NREFA(IA),N,(IAE(J),J=1,N)
 110  CONTINUE
      READ (IFDM,'(I6)') NN
C      READ(IFDM,'(10F8.1)')(XN(IN),IN=1,NN)
C      READ(IFDM,'(10F8.1)')(YN(IN),IN=1,NN)
C      READ(IFDM,'(10F8.3)',END=121)(CO(IN),IN=1,NN)
C 121  CLOSE(IFDM)

      OPEN(IFRO,FILE=trim(ETUDE)//'.frt',STATUS='OLD',ERR=999)
      GO TO 998
 999  Write(*,*)'pas de fichier de frottement ',trim(ETUDE)//'.frt'
      STOP
 998  READ(IFRO,'(10F8.3)')(FRE(IE),IE=1,NE)
      CLOSE(IFRO) 



       ! WRITE(*,*) 'si vous voulez en sortie 1 decimale pour x et y'
       ! WRITE(*,*) 'et 3 decimales pour h taper sur la touche entree'
       ! Write(*,*) 'sinon donner successivement les deux chiffres'
       ! read (*,'(a1)')rep
       ! if (rep.eq.' ')then
       !    idec1=1
       !    idec2=3
       ! else
       	!2nd choice
	CALL get_command_argument(2, arg_temp)
	read (arg_temp,'(I10)') idec1
	
	CALL get_command_argument(3, arg_temp)
	read (arg_temp,'(I10)') idec2
        !   read(rep,'(I1)')idec1
        !   read(*,*)Idec2
           If(idec1.gt.4.or.idec2.gt.5)then
             write (*,*) 'trop de decimales'
             stop
           elseif(idec1.lt.1.or.idec2.lt.3)then
             write (*,*) 'trop peu de decimales'
             stop
           endif
       ! endif
        If(idec1.eq.1)then
      READ(IFDM,'(10F8.1)')(XN(IN),IN=1,NN)
      READ(IFDM,'(10F8.1)')(YN(IN),IN=1,NN)
      READ(IFDM,'(10F8.3)',END=121)(CO(IN),IN=1,NN)
       else
      READ(IFDM,'(10F8.4)')(XN(IN),IN=1,NN)
      READ(IFDM,'(10F8.4)')(YN(IN),IN=1,NN)
      READ(IFDM,'(10F8.3)',END=121)(CO(IN),IN=1,NN)
        endif
 121  CLOSE(IFDM)
        DO 125 IE=1,NE
           XE(IE)=XN(INE(IE,1))
           DO 225 I=2,NNE(IE)
              XE(IE)=XE(IE)+XN(INE(IE,I))
 225       CONTINUE
           XE(IE)=XE(IE)/NNE(IE)
           YE(IE)=YN(INE(IE,1))
           DO 325 I=2,NNE(IE)
              YE(IE)=YE(IE)+YN(INE(IE,I))
 325       CONTINUE
           YE(IE)=YE(IE)/NNE(IE)
           ZF(IE)=CO(INE(IE,1))
           DO 425 I=2,NNE(IE)
              ZF(IE)=ZF(IE)+CO(INE(IE,I))
 425       CONTINUE
           ZF(IE)=ZF(IE)/NNE(IE)
 125    CONTINUE

C          if(idec2.eq.3)then
 257       FORMAT(2F9.1,3F8.3,2F8.4,F8.3)
C          elseif(idec2.eq.4)then
 258       FORMAT(2F9.1,F8.4,2F8.3,2F8.4,F8.3)
C          elseif(idec2.eq.5)then
 259       FORMAT(2F9.1,F8.5,2F8.3,2F8.4,F8.3)
C          endif
c          If(idec1.eq.2)then
c          if(idec2.eq.3)then
 2257       FORMAT(2F9.2,3F8.3,2F8.4,F8.3)
c          elseif(idec2.eq.4)then
 2258       FORMAT(2F9.2,F8.4,2F8.3,2F8.4,F8.3)
c          elseif(idec2.eq.5)then
 2259       FORMAT(2F9.2,F8.5,2F8.3,2F8.4,F8.3)
c          endif
c        elseIf(idec1.eq.3)then
c          if(idec2.eq.3)then
 3257       FORMAT(2F9.3,3F8.3,2F8.4,F8.3)
c          elseif(idec2.eq.4)then
 3258       FORMAT(2F9.3,F8.4,2F8.3,2F8.4,F8.3)
c          elseif(idec2.eq.5)then
 3259       FORMAT(2F9.3,F8.5,2F8.3,2F8.4,F8.3)
c          endif
c        elseIf(idec1.eq.4)then
c          if(idec2.eq.3)then
 4257       FORMAT(2F9.4,3F8.3,2F8.4,F8.3)
c          elseif(idec2.eq.4)then
 4258       FORMAT(2F9.4,F8.4,2F8.3,2F8.4,F8.3)
c          elseif(idec2.eq.5)then
 4259       FORMAT(2F9.4,F8.5,2F8.3,2F8.4,F8.3)
C          if(idec2.eq.3)then
10257      FORMAT(2F9.1,F8.6,2F8.3,F8.4,F8.1,F8.3)
C          elseif(idec2.eq.4)then
10258      FORMAT(2F9.1,F8.4,2F8.3,F8.4,F8.1,F8.3)
C          elseif(idec2.eq.5)then
10259      FORMAT(2F9.1,F8.5,2F8.3,F8.4,F8.1,F8.3)
C          endif
c          If(idec1.eq.2)then
c          if(idec2.eq.3)then
12257       FORMAT(2F9.2,F8.6,2F8.3,F8.4,F8.1,F8.3)
c          elseif(idec2.eq.4)then
12258       FORMAT(2F9.2,F8.4,2F8.3,F8.4,F8.1,F8.3)
c          elseif(idec2.eq.5)then
12259       FORMAT(2F9.2,F8.5,2F8.3,F8.4,F8.1,F8.3)
c          endif
c        elseIf(idec1.eq.3)then
c          if(idec2.eq.3)then
13257       FORMAT(2F9.3,F8.6,2F8.3,F8.4,F8.1,F8.3)
c          elseif(idec2.eq.4)then
13258       FORMAT(2F9.3,F8.4,2F8.3,F8.4,F8.1,F8.3)
c          elseif(idec2.eq.5)then
13259       FORMAT(2F9.3,F8.5,2F8.3,F8.4,F8.1,F8.3)
c          endif
c        elseIf(idec1.eq.4)then
c          if(idec2.eq.3)then
14257       FORMAT(2F9.4,F8.6,2F8.3,F8.4,F8.1,F8.3)
c          elseif(idec2.eq.4)then
14258       FORMAT(2F9.4,F8.4,2F8.3,F8.4,F8.1,F8.3)
c          elseif(idec2.eq.5)then
14259       FORMAT(2F9.4,F8.5,2F8.3,F8.4,F8.1,F8.3)
c          endif
c        endif
c        endif
! 700    WRITE(*,*) ' VOULEZ VOUS  ?'
!        WRITE(*,*) '        LES MAXIMA     ----->   1'
!        WRITE(*,*) '        UN TEMPS DONNE ----->   2'
!        write(*,*) 'UNE CONCENTRATION a T donne->   3'
!        WRITE(*,*) '        RIEN           ----->   0'
!NEW        READ(*,*) IREP

	write(*,*) '---Récupération des résultats au temps final---' 
C ajout de statistiques le 31/01/2012
           sommehe=0.
		   sommezf=0.
		   sommez=0.
		   sommev=0
		   sommefr=0
        
          OPEN(47,FILE=trim(ETUDE)//'.tps',STATUS='OLD')
        !Lecture du dernier temps de l'étude  
          OPEN(12,FILE=trim(ETUDE)//'.par',STATUS='UNKNOWN')
          READ(12,*)
          READ(12,*)
          READ(12,*)
          READ(12,*)
       	READ(12,*) arg_temp,arg_temp,arg_temp,arg_temp,arg_temp,TN
        OPEN(48,FILE=trim(ETUDE)//'.xyh',STATUS='UNKNOWN')
	WRITE(48,*)'x y h zf z vx vy frot'
  3     READ(47,'(F15.3)',END=4)T
        WRITE(*,*) T
        READ(47,'(8F10.5)')(HE(IE),IE=1,NE)
        READ(47,'(8F10.5)')(QUE(IE),IE=1,NE)
        READ(47,'(8F10.5)',END=5)(QVE(IE),IE=1,NE)
        IF (ABS(TN-T).LT..1)GO TO 6
         GO TO 3
 5      IF (ABS(TN-T).LT..1)GO TO 6
 4      WRITE(*,*)'LE TEMPS DONNE NE CORRESPOND '
        WRITE(*,*)' A AUCUNE LIGNE D''EAU STOCKEE'
        WRITE(*,*)'DERNIERE LIGNE D''EAU = ',T
        CLOSE(47)
        !GO TO 700
 6      CLOSE(47)
        DO IE=1,NE
        If(idec1.eq.1)then
          if(idec2.eq.3)then
          IF(HE(IE).GT.EPS)THEN
          WRITE(48,257)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     : QUE(IE)/HE(IE),QVE(IE)/HE(IE),FRE(IE)
          ELSE
          WRITE(48,257)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     :0.,0.,FRE(IE)
          ENDIF
          elseif(idec2.eq.4)then
          IF(HE(IE).GT.EPS)THEN
          WRITE(48,258)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     : QUE(IE)/HE(IE),QVE(IE)/HE(IE),FRE(IE)
          ELSE
          WRITE(48,258)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     :0.,0.,FRE(IE)
          ENDIF
          elseif(idec2.eq.5)then
          IF(HE(IE).GT.EPS)THEN
          WRITE(48,259)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     : QUE(IE)/HE(IE),QVE(IE)/HE(IE),FRE(IE)
          ELSE
          WRITE(48,259)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     :0.,0.,FRE(IE)
          ENDIF
          endif
        elseIf(idec1.eq.2)then
          if(idec2.eq.3)then
          IF(HE(IE).GT.EPS)THEN
          WRITE(48,2257)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     : QUE(IE)/HE(IE),QVE(IE)/HE(IE),FRE(IE)
          ELSE
          WRITE(48,2257)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     :0.,0.,FRE(IE)
          ENDIF
          elseif(idec2.eq.4)then
          IF(HE(IE).GT.EPS)THEN
          WRITE(48,2258)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     : QUE(IE)/HE(IE),QVE(IE)/HE(IE),FRE(IE)
          ELSE
          WRITE(48,2258)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     :0.,0.,FRE(IE)
          ENDIF
          elseif(idec2.eq.5)then
          IF(HE(IE).GT.EPS)THEN
          WRITE(48,2259)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     : QUE(IE)/HE(IE),QVE(IE)/HE(IE),FRE(IE)
          ELSE
          WRITE(48,2259)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     :0.,0.,FRE(IE)
          ENDIF
          endif
        elseIf(idec1.eq.3)then
          if(idec2.eq.3)then
          IF(HE(IE).GT.EPS)THEN
          WRITE(48,3257)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     : QUE(IE)/HE(IE),QVE(IE)/HE(IE),FRE(IE)
          ELSE
          WRITE(48,3257)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     :0.,0.,FRE(IE)
          ENDIF
          elseif(idec2.eq.4)then
          IF(HE(IE).GT.EPS)THEN
          WRITE(48,3258)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     : QUE(IE)/HE(IE),QVE(IE)/HE(IE),FRE(IE)
          ELSE
          WRITE(48,3258)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     :0.,0.,FRE(IE)
          ENDIF
          elseif(idec2.eq.5)then
          IF(HE(IE).GT.EPS)THEN
          WRITE(48,3259)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     : QUE(IE)/HE(IE),QVE(IE)/HE(IE),FRE(IE)
          ELSE
          WRITE(48,3259)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     :0.,0.,FRE(IE)
          ENDIF
          endif
        elseIf(idec1.eq.4)then
          if(idec2.eq.3)then
          IF(HE(IE).GT.EPS)THEN
          WRITE(48,4257)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     : QUE(IE)/HE(IE),QVE(IE)/HE(IE),FRE(IE)
          ELSE
          WRITE(48,4257)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     :0.,0.,FRE(IE)
          ENDIF
          elseif(idec2.eq.4)then
          IF(HE(IE).GT.EPS)THEN
          WRITE(48,4258)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     : QUE(IE)/HE(IE),QVE(IE)/HE(IE),FRE(IE)
          ELSE
          WRITE(48,4258)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     :0.,0.,FRE(IE)
          ENDIF
          elseif(idec2.eq.5)then
          IF(HE(IE).GT.EPS)THEN
          WRITE(48,4259)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     : QUE(IE)/HE(IE),QVE(IE)/HE(IE),FRE(IE)
          ELSE
          WRITE(48,4259)
     : XE(IE),YE(IE),HE(IE),ZF(IE),HE(IE)+ZF(IE),
     :0.,0.,FRE(IE)
          ENDIF
          ENDIF
          ENDIF
          END DO
C ajout de statistiques
           sommehe=sommehe+he(ie)
		   sommezf=sommezf+zf(ie)
		   sommez=sommez+He(ie)+zf(ie)
		   if(he(ie).gt.eps)then
		   sommev=sommev+sqrt(que(ie)**2+qve(ie)**2)/he(ie)
		   endif
		   sommefr=sommefr+fre(ie)
		   
        CLOSE(48)
        write(ifsor,*)"h  zf    z  v   fr"
		write(ifsor,'(5f15.3)')sommehe/float(ne),sommezf/float(ne),
     :sommez/float(ne),sommev/float(ne),sommefr/float(ne)
	END
