#!/bin/bash
NUM_PROC=2
echo
printf '\033[;46mCas tests en eau claire' $i
printf '\033[0m'
echo '' > result_test.log
for i in 1 2 3 4 5 6b 7 8 9 10
do
	echo
	echo
	#chemin des études
	STUDY_REF_PATH=../Tests_Reference/trubar20r20011208donnees/T$i/barrag$i
	STUDY_COMP_PATH=../cas_tests/trubar20r20011208donnees/barrag$i

	#Préparation du fichier de sortie
	echo "****************Test unitaire sur le cas $i**********************" >> result_test.log

	#Execution du code Rubar20 qui a été au préalable comoilé dans le Makefile
	printf '\033[;45mExécution du code Rubar20 sur le cas test %s' $i
	printf '\033[0m'
	 mpirun -np $NUM_PROC ../code/rubar20_MPI $STUDY_COMP_PATH >> result_test.log

	#initialisation variables pour TRXYH
	PRES_X=3
	PRES_H=5
	NUM_COLUMNS=8
	CREA_XYH=1
	CHOICE=2

	#Création des fichiers xyh
	if [ -f $STUDY_REF_PATH.xyh ] && [ $CREA_XYH -eq 0 ]; then
		echo
		echo "Utilisation du fichier xyh existant pour les résultats de référence, Test $i"
	else
		echo
		echo "Creation du fichier xyh pour les résultats de référence, Test $i"
		../TRXYH/a.out $STUDY_REF_PATH $CHOICE $PRES_X $PRES_H >> result_test.log
	fi

	echo "Creation du fichier xyh pour les résultats calculés, Test $i"
	../TRXYH/a.out $STUDY_COMP_PATH $CHOICE $PRES_X $PRES_H >> result_test.log

	#initialisation des variables pour la comparaison
	FILENAME_DAT=$STUDY_REF_PATH.dat
	FILENAME_REF=$STUDY_REF_PATH.xyh
	FILENAME_COMP=$STUDY_COMP_PATH.xyh
	read NUM_LINES_TO_COMPARE < $FILENAME_DAT
	TOLERANCE=0.00001
	COLUMN_NUM=7

	#Comparaison des deux fichiers xyh et résultat du test
	echo "Comparaison des deux fichiers xyh"
	echo
	../DataFileComparator/DataFileComparator.sh $FILENAME_REF $FILENAME_COMP $NUM_LINES_TO_COMPARE $NUM_COLUMNS $COLUMN_NUM $TOLERANCE >> result_test.log
	strErrorCode=$?
	echo "Résultat du test :"
	if [ "$strErrorCode" -eq 0 ]; then
		printf '\033[;37;42mTEST %s PASSED' $i
		printf '\033[0m'

	else
		printf '\033[;37;41mTEST %s FAILED' $i
		printf '\033[0m'
	fi

	echo >> result_test.log
done

printf '\033[0m\n'
echo

