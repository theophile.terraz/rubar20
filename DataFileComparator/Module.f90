module Param
	integer NumLines,NumColumns,ColumnNum,NumError
	real(kind=8) Tolerance
	character(len=255) Filename1,Filename2
	real(kind=8), dimension(:,:), allocatable :: Data1
	real(kind=8), dimension(:,:), allocatable :: Data2
end module Param
