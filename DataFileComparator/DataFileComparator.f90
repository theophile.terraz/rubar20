program DataFileComparator
    use Param
    implicit none
    integer :: NumArg
    real(kind=4) :: next_real
    NumArg = command_argument_count()

    NumError = 0

    !Filename1 = "    1.143 2. 3. 4.87196             "

    !print*,adjustl(Filename1),next_real(adjustl(Filename1),' ',NumArg)
    !print*,NumArg
    !print*,next_real(adjustl(Filename1),' ',NumArg)


    if (NumArg/=6) then
        print *,"Le nombre d'arguments n'est pas correct ( 6 attendus)"
        call exit(1)
    endif
    call GetArguments()
    print*,""
    print*,"Paramètres d'entrée :"
    print*,trim(Filename1)
    print*,trim(Filename2)
    print*,NumLines
    print*,Numcolumns
    print*,Tolerance


    call InitData(trim(Filename1),trim(Filename2))

    call DataAreDifferent()

    if (NumError == 0) then
        call exit(NumError)
    else
        call exit(NumError)
    endif

end program DataFileComparator


!*************************************************!
!                                                 !
!****Lectures des paramètres du fichier shell*****!
!*******pour la création des tableaux et la*******!
!************comparaison des fichiers*************!
!                                                 !
!*************************************************!
subroutine GetArguments()
    use Param
    implicit none

    character(len=32) :: arg

    !Filename 1
    CALL get_command_argument(1, Filename1)

    !Filename 2
    CALL get_command_argument(2, Filename2)

    !NumLines to compare
    CALL get_command_argument(3, arg)
    read (arg,'(I10)') NumLines

    !Numcolumns to compare
    CALL get_command_argument(4, arg)
    read (arg,'(I10)') NumColumns

    !Numcolumns to compare
    CALL get_command_argument(5, arg)
    read (arg,'(I10)') ColumnNum

    !Tolerance
    CALL get_command_argument(6, arg)
    read (arg,'(F10.7)') Tolerance

end subroutine GetArguments


!******************************************!
!                                          !
!*******Initialisation des données*********!
!                                          !
!******************************************!
subroutine InitData(filename1_cut,filename2_cut)
    use Param
    implicit none
    character(len=*),intent(in) :: filename1_cut
    character(len=*),intent(in) :: filename2_cut
    integer i,j
    integer v1,v2
    character(len=255) :: temp

    allocate(Data1(NumLines,Numcolumns),Data2(NumLines,Numcolumns))
    open (unit=10,file=filename1_cut,action="read",status="old",form='formatted')
    open (unit=20,file=filename2_cut,action="read",status="old",form='formatted')
    read(unit=10,fmt=*)
    read(unit=20,fmt=*)

    !Read all data
    do i=1,NumLines
        read(unit=10,fmt=*) (Data1(i,j),j=1,ColumnNum)
        !print*,(Data1(i,j),j=1,Numcolumns)
        read(unit=20,fmt=*) (Data2(i,j),j=1,ColumnNum)
    end do

    !read(unit=10,fmt=*) temp
    !print *,"Mon temp ",temp

end subroutine InitData

!******************************************!
!                                          !
!*********Comparaison des données**********!
!                                          !
!******************************************!
subroutine DataAreDifferent()
    use Param
    implicit none
    integer i,j

    do i=1,NumLines
        do j=1,ColumnNum
            if((abs(Data1(i,j)-Data2(i,j)))>Tolerance) then
                !print*,i,Data1(i,ColumnNum),Data2(i,ColumnNum)
                NumError = NumError+1
            endif
        end do
    end do
end subroutine DataAreDifferent


