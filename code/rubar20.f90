! Copyright (c) 2021, Institut National de Recherche pour l'Agriculture, l'Alimentation et l'Environnement (INRAE)
! All rights reserved.
!
! Redistribution and use in source and binary forms, with or without
! modification, are permitted provided that the following conditions are met:
!
! * Redistributions of source code must retain the above copyright notice, this
!   list of conditions and the following disclaimer.
!
! * Redistributions in binary form must reproduce the above copyright notice,
!   this list of conditions and the following disclaimer in the documentation
!   and/or other materials provided with the distribution.
!
! * Neither the name of the copyright holder nor the names of its
!   contributors may be used to endorse or promote products derived from
!   this software without specific prior written permission.
!
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
! DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
! FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
! DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
! SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
! CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
! OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
! OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

       program rubar20

! r2pp3d100murh8h mix� avec r8ng4cwc2
! 25000 ouvrages
! le 11/4/01 valeur indefinie pour h en condition initiale passee a 999.999
! le 26/2/01 introduction de dicho2 au lieu de nwt2 : passage de r193 r193d
! version du 28/7/00 avec introduction pluie
! version corrigee le 27/7/2000 en mettant zfm = 0. (zfm est alors inutile)
! version corrigee le 21 juin 2000 pour faire disparaitre tabulations
! version du 15/2/99 d'apres r191b4e3.f
! modifications du 24/4/92 pour accepter triangles
! le 1/12/92 avec frottement du vent,nouveau format.cin
! le 3/05/2007 gestion de plusieurs ouvrages b
! le 04/06/07 puis le 15/10/07 modification pour couplage avec canoe
! le 16 juillet 2007 : correction secmb1 fluu, etc
! le 29 octobre 2007 ecriture et lecture du nouveau format du fichier envlop
! le 12 novembre 2007 : introduction de la deviation du charriage
! et de la formule de camenen : options 13,14 et 15
! le 3 mars 2008 : introduction de la formule de frottement de darcy weisbach
! le 14 mai 2008 : introduction vitesse de surface et vent fonction temps et espace
! le 29 mai 2008 : modification des echnages avec canoe selon dernier envoi sogreah
! le 19 juin 2008 : coorections pour couplage avec canoe
! le 27 octobre 2008 : augmentation du nombre de chroniques d'apports et
! creation des references arete 8 et 9
! le 18/11/2009 : verificationde la ref>60
! juillet 2012 : achevement introduction plusieurs couches
! juillet 2012 :introduction traitement ouvrage C
! juilet 2012 : on ramene de D a 0.5D la limite de non erosion
! juillet 2012 si nref ouvrage est -1 on autorise nbmail=0 et deux
!  aretes differente (evite de definir des mailles internes qui ne servent pas)
! juilet 2012 : fmax divise par 2
! juilet 2012 : limitation cet a 0.5
! juin 2013 : remplacement de tout les tableaux dont une dimension valait
! nemax,namax ou nnmax par des tableaux dynamiques, ajout des modules
! remplacement de tous les commons par des modules (hors transport solide)
! aout 2013 : parall�lisation MPI du code sans transport solide
!******************************************************************************
!  vf2 : resolution des equations de st venant : ut + f(u)x + g(u)y = 0 avec  *
!  u = (h,hu,hv), f(u) = (hu,hu2 + gh2/2,huv), g(u) = (hv,huv,hv2 + gh2/2),   *
!  en 2d d'espace, par une technique de volumes finis.                        *
!  schema van leer (ordre 2) en  espace.               cemagref  25/03/88     *
!******************************************************************************

       use module_tableaux
       use module_precision
!        use module_init
!        use module_final
!$     use omp_lib
       use module_mpi
#ifdef WITH_MPI
       use mpi
#endif /* WITH_MPI */
#ifndef ENG
! temporaire
#define FRA
#endif

      implicit none
      !include 'rubar20_common.for'

      integer :: vtime1(8), vtime2(8),heure,minute,seconde
      real(wp) :: duree_effective
      real :: t_debut, t_fin

#ifdef WITH_MPI
      ! subroutines mpi:
      call mpi_init(statinfo)
      call mpi_comm_rank(mpi_comm_world,me,statinfo)
      ! me: num�ro du processus courant
      call mpi_comm_size(mpi_comm_world,np,statinfo)
      ! np: nombre de processus
      ! o<=me<=np-1
      ! le process np-1 est d�di� aux i/o

      if(np<=0) then
#if ENG
         print*,'-----------------------------------------'
         print*,'  This program needs at least 1 process  '
         print*,'-----------------------------------------'
#else /* FRA */
         print*,'-----------------------------------------------'
         print*,'  Ce programme necessite au moins 1 processus  '
         print*,'-----------------------------------------------'
#endif /* FRA */
         call mpi_finalize(statinfo)
         stop
      end if
#else /* WITH_MPI */
      me = 0
      np = 1
#endif /* WITH_MPI */

      scribe=np-1

        call getarg(1,etude)

!         if (trim(etude) == '') then
!           if (me==scribe)then
!             print*,''
! #if ENG
!             print*,' Give a study name in the program command line'
!             print*,' exemple:',' ./program STUDY1'
! #else /* FRA */
!             print*,' donnez en 6 caracteres le nom de l''etude au lancement du programme'
!             print*,' exemple:',' ./programme ETUDE1'
! #endif /* FRA */
!             print*,''
!           end if ! me==scribe
! #ifdef WITH_MPI
!           call mpi_finalize(statinfo)
! #endif /* WITH_MPI */
!           stop
!         end if

      call init
      if (me==scribe)call date_and_time(values=vtime1)
      if (me==scribe)call cpu_time(t_debut)
      call calcdt(tmax)
      if (me==scribe)call cpu_time(t_fin)
      if (me==scribe)call date_and_time(values=vtime2)
      call final
      if (me==scribe)then
#if ENG
        write(*,*) 'total CPU time : ',t_fin-t_debut,' seconds'
#else /* FRA */
        write(*,*) 'temps CPU total : ',t_fin-t_debut,' secondes'
#endif /* FRA */
        duree_effective = (vtime2(5)*3600.+vtime2(6)*60.+vtime2(7)+&
       &                   vtime2(8)*0.001)&
       &                  -(vtime1(5)*3600.+vtime1(6)*60.+vtime1(7)+&
       &                   vtime1(8)*0.001)+(vtime2(3)-vtime1(3))*86400.
        heure=floor(duree_effective/3600)
        minute=floor((duree_effective-heure*3600)/60)
        seconde=floor(duree_effective-heure*3600-minute*60)
#if ENG
        write(6,'(a24,f11.3,a9)')' ---> program duration : ',duree_effective,' secondes'
        write(6,'(a7,i3,a2,i2,a7,i2,a1)')'   --> ',heure,'h ',minute,'min ',seconde,'s'
#else /* FRA */
        write(6,'(a24,f11.3,a9)')' ---> duree effective : ',duree_effective,' secondes'
        write(6,'(a7,i3,a2,i2,a7,i2,a1)')'  soit ',heure,'h ',minute,'min ',seconde,'s'
#endif /* FRA */
      end if
      call deallocate_tabs

#if TRANSPORT_SOLIDE
      call deallocate_tabs_ts
#endif


#ifdef WITH_MPI
      call mpi_finalize(statinfo)
#endif /* WITH_MPI */

      end program rubar20


!***********************************************************************

!        use module_tableaux
!        use module_precision
!
!
!
! !$     use omp_lib
!        use module_mpi
! #ifdef WITH_MPI
!        use mpi
! #endif /* WITH_MPI */
!
!       contains

      subroutine init
!=======================================================================
! initialisation : demande et lecture des fichiers de donn�es
!=======================================================================
      use module_tableaux

#if TRANSPORT_SOLIDE
      use module_ts
#endif
      use module_mpi
      use comptage
      use module_lecl, only: na30
#ifdef WITH_MPI
      use module_messages
      use mpi
#endif /* WITH_MPI */

!      implicit none

      !include 'rubar20_common.for'

#if TRANSPORT_SOLIDE
!!!!       real(WP) :: cofr2(ne),zfn2(nn),zfa2(na),zfe2(ne)

#endif

! variables locales
      integer :: ibouv,i,ie,ia,in,ieloc
      real(wp) :: xnb(nlimmax), ynb(nlimmax), cvi
      !integer,dimension(3,1)::ranges
      real(wp) :: volume
#ifdef WITH_MPI
      integer,dimension(mpi_status_size)::status
#endif /* WITH_MPI */
      external volume



! constantes et initialisations
      volo=0.d0


! variable logique impliquant pas de retour en arriere
      tnou = .true.
! variable pour l'ecriture sur .out et grdlp ; mis a faux dans sp grdlp
      debut = .true.
      if (me==scribe)then
        write (*,*)
#if ENG
        write (*,*)'RUBAR20 : 2D FINITE VOLUME SHALLOW WATER EQUATIONS'
#if TRANSPORT_SOLIDE
        Write(*,*)'WITH CONCENTRATION TRANSPORT'
#endif /* TRANSPORT_SOLIDE */
         WRITE (*,*)'October  21st   2021'
         WRITE (*,*)'Irstea, HHLY, France'
#else /* if FRA */
        write (*,*)'RUBAR20 : EQUATIONS ST-VENANT 2D PAR VOLUMES FINIS'
#if TRANSPORT_SOLIDE
        Write(*,*)'AVEC TRANSPORT DE CONCENTRATIONS'
#endif /* TRANSPORT_SOLIDE */
         WRITE (*,*)'VERSION  21 octobre 2021'
         WRITE (*,*)'Irstea, HHLY, France'
#endif /* FRA */
         write (*,*)
         if (trim(etude) == '') then
            print*,''
#if ENG
            print*,' Give a study name'
            print*,' exemple:',' STUDY1'
#else /* FRA */
            print*,' donnez le nom de l''etude'
            print*,' exemple:',' ETUDE1'
#endif /* FRA */
            print*,''
            read (*,'(a6)') etude
          end if
       !write (*,*)'donnez en 6 caracteres le nom de l''etude'

! lecture/edition donnees diverses
#if ENG
      write (*,*) 'reading misc. data'
#else /* if FRA */
      write (*,*) 'lecture donnees diverses'
#endif /* FRA */
      end if ! me==scribe
      call ledd(alpha,nbpt,xnb,ynb,cvi)
#ifdef WITH_MPI
      call mpi_bcast(ifm,1,mpi_integer,scribe,mpi_comm_world,i)
#endif /* WITH_MPI */

! lecture/edition donnees maillage
      if (me==scribe)then
#if ENG
      write (*,*) 'reading mesh data'
#else /* if FRA */
      write (*,*) 'lecture donnees maillage'
#endif /* FRA */
      end if ! me==scribe

      call ledm(ifdm,ifm)

! calcul/edition donnees complementaires elements, aretes
      if (me==scribe)then
#if ENG
      write (*,*) 'computing additional edge data'
#else /* if FRA */
      write (*,*) 'calcul donnees complementaires aretes'
#endif /* FRA */
      end if ! me==scribe
      call cedca(ifm,cvi)

! lecture du fichier ven
! en retour pven vrai si vent lu dans ven, ifve remis a zero sinon
      call leven(ifve)

#if TRANSPORT_SOLIDE
      if (.not.solute) then
#if SEDVAR
        call lecsed
#endif
      endif
#endif

! lecture/edition conditions initiales
      if (me==scribe)then
#if ENG
      write (*,*) 'reading initial conditions'
#else /* if FRA */
      write (*,*) 'lecture conditions initiales'
#endif /* FRA */
      end if ! me==scribe
      call leci(xnb,ynb)

! lecture des donnees des ouvrages
      if (iosmb == 1) then
         call louvr
         do ibouv = 1, nob
! variable logique permettant un calcul plus precis du volume parti
! en cas de rupture progressive avec effondrement du renard
            kappa(ibouv) = .false.
! initialisation du temps de passage en canal rectangulaire pour ouvrage b
            trect(ibouv) = min(tinit,tini(ibouv))
         enddo
      else
         nob = 0
      endif

#if TRANSPORT_SOLIDE
       do ie=1,ne
         zfe0(ie)=zfe(ie)
       enddo
! ecriture des differences de cotes dans dzf si on ne r�actualise pas la topo
         if(.not.solute)then
           if(.not.modifz)then
             opendz=.true.
             open(id_dzf,file=trim(etude)//'.dzf',status='unknown')
           else
             opendz=.false.
           endif
         else
           opendz=.false.
         endif

         lmailczero=.false.

#endif

! calcul du volume initial d'eau
      voli = volume(tinit)
! initialisation des volumes entrant et sortant a zero
      vole = 0.
      vols = 0.

#if TRANSPORT_SOLIDE
!  calcul du volume initial  de sediments (ancienne fonction volsed)
       volai=0.
       do ie = 1,ne
         volai=volai+hcet(ie)*se(ie)
       end do
       volae=0.
       volas=0.
       esdt=0.
#endif
       na30 = 0


! lecture/edition des frottements
      call lefr(ifro,ifrt,iofr)

! lecture/edition conditions aux limites
      if (me==scribe)then
#if ENG
      write (*,*) 'reading boundary conditions'
#else /* FRA */
      write (*,*) 'lecture conditions aux limites'
#endif /* FRA */
      end if ! me==scribe
! le temps de cette premiere lecture n'a aucune importance
      call lecl(tm)

! lecture/edition des apports de pluie
      if (iapp.gt.0) then
        if (me==scribe)then
#if ENG
          write (*,*) 'reading rain input'
#else /* FRA */
          write (*,*) 'lecture apports pluie'
#endif /* FRA */
        end if ! me==scribe

        call leap(iapp,ifap)
      endif


  !---definition des vecteurs n�cessaires aux communications---!
      call charge2(me,ne,np)
      call aretes
#ifdef WITH_MPI
        ! initialisation des communications collectives
      if(me==scribe)then
#if ENG
        print*, 'communication initialization'
#else /* FRA */
        print*, 'initialisation des communications'
#endif /* FRA */
      endif
      call init_gatherv

  !--envoi/r�ception des tableaux de conversion local/global--!
      call mpi_gatherv(mailles_loc(1:ne_loc(me)),ne_loc(me),mpi_integer,mailles_loc_all,nb_mailles_recues&
     &,deplacements,mpi_integer,scribe,mpi_comm_world,statinfo)
      call mpi_gatherv(aretes_loc(1:naloc),naloc,mpi_integer,aretes_loc_all,nb_aretes_recues&
     &,deplacements2,mpi_integer,scribe,mpi_comm_world,statinfo)
      call mpi_gatherv(noeuds_loc(1:nnloc),nnloc,mpi_integer,noeuds_loc_all,nb_noeuds_recus&
     &,deplacements3,mpi_integer,scribe,mpi_comm_world,statinfo)
!       if(nob>0)then
!         call mpi_gatherv(ouvragesb_loc(1:nbouv_b_loc),nbouv_b_loc,mpi_integer,ouvragesb_loc,nb_ouvb_recus&
!        &,deplacements3,mpi_integer,scribe,mpi_comm_world,statinfo)
!       end if

  !---creation du groupe de calcul---!
!       ranges(1,1)=scribe
!       ranges(2,1)=scribe
!       ranges(3,1)=1
      call mpi_comm_group(mpi_comm_world,group_world,statinfo)
!       call mpi_group_range_excl(group_world,1,ranges,group_calcul,statinfo)
      call mpi_comm_create(mpi_comm_world,group_world,comm_calcul,statinfo)

#endif /* WITH_MPI */

      ! procouvb(ibouv) donne le process qui traite l'oubrage ibouv
      if (nob>0) then
        allocate(procouvb(nob))
        do ibouv=1,nob
          procouvb(ibouv)=machine(ie1(ouv(ibouv)))
        end do
      end if
! marque que toutes les lectures se sont bien passees
#if ENG
      if (me==scribe)write (*,*) 'start ', schema, ' scheme computation'
#else /* FRA */
      if (me==scribe)write (*,*) 'debut calculs schema de ', schema
#endif /* FRA */
      dt=dt0

#if TRANSPORT_SOLIDE
        !chzn=.true.
        if(chzn)then
          call semba2
        else
          call semba_loc
! pour g�rer le cas de concentration mises n�gatives dans le fichier de contitions initiales
! cela n'a d'interet que pour van rijn sinon ceq=0
                do ieloc=1,ne_loc(me)
                  ie = mailles_loc(ieloc)
                  if(cet(ie).lt.0.)then
! on rajoute un max au cas ou ceq serait negative
                    cet(ie)=max(ceq(ie),0.d0)
                    hcet(ie)=cet(ie)*het(ie)
                  endif
                enddo
#ifdef WITH_MPI
                call message_group_calcul(cet)
                call message_group_calcul(hcet)
#endif /* WITH_MP */
         endif
#endif

      end subroutine init

      subroutine calcdt(tfin)
!=======================================================================
! routine de calcul principale
!=======================================================================
      use module_tableaux
      use module_precision
      use module_mpi
      use comptage
#ifdef WITH_MPI
      use mpi
      use module_messages
#endif /* WITH_MPI */
      use module_loc,only:th,hem,tu,qum,qvm,hem2,qum2,qvm2

#if TRANSPORT_SOLIDE
      use module_ts
#endif
#if WITH_CGNS
      use cgns
      use cgns_data
#endif

!$    use omp_lib
      implicit none
      ! include 'rubar20_common.for'

! prototype
      real(wp), intent(inout) :: tfin
! variables locales
      integer :: k, i, ibouv, ie, iouv, ieloc
!$    integer :: threads
      real(wp) :: dt1, qu1, qv1, volet, volst!,tpscomm,tpsecr
      !integer :: tpsecr1(8), tpsecr2(8)
#ifdef WITH_MPI
      real(wp),dimension(:),allocatable::mess_qouv1,mess_qouv2
      integer,dimension(mpi_status_size)::status
#endif /* WITH_MPI */
      !real::tgrad1,tgrad2,tgrad
      !real::tflux1,tflux2,tflux

#if TRANSPORT_SOLIDE
      real(wp) :: volaet,volast,delta_esdt_loc
      real(wp) :: ce(ne),maxce,mindhe,maxdhe,maxzfn,minzfn
      integer :: in,ia,ialoc,jn
!      real(WP) :: cofr2(ne),zfn2(nn),zfa2(na),zfe2(ne)
#endif

#if VH
      integer je,iev
      real(wp) :: rcet,cetr
#endif
#if WITH_CGNS
      integer :: ier, isize(3), index_field, index_flow, index_flow2, index_coord, index_motion
      character(LEN=32) :: field_name
#endif

#ifdef WITH_MPI
      allocate(mess_qouv1(na),mess_qouv2(na))
      mess_qouv1(:)=0.
      mess_qouv2(:)=0.
      if(me==scribe)then
#if ENG
      write(6,'(a15,i4,a10)')' execution on ',np,' processus'
      write(6,'(a34,i4)')' results written by processus',scribe
#else /* FRA */
      write(6,'(a15,i4,a10)')' execution sur ',np,' processus'
      write(6,'(a34,i4)')' resultats ecrits par le processus',scribe
#endif /* FRA */
      end if ! me==scribe
      if (me/=scribe)then
        if (ifen > 0) then
          call lenv
        endif
      end if ! me/=scribe

#endif /* WITH_MPI */

      !tflux=0.
      !tpsecr=0.
      !tcomm=0.
      !tgrad=0.
      k=0
!$omp parallel
!$    threads=omp_get_num_threads()
!$omp master
!$    write(6,'(a15,i4,a12)')' execution sur ',threads,' processeurs'
!$omp end master
!$omp end parallel
! boucle sur t : tant que t <= tfin faire ...
      do
      t=tm+dt

!  test d arret sur t par rapport a tfin
      if (t > tfin) then
         if (tm < tfin) then
            t = tfin
            dt = tfin-tm
         else
#ifdef WITH_MPI
           ! print*,'temps de communication:',tpscomm,'process',me
            !if (me==scribe)then

            !print*,'temps d''ecriture:',tpsecr

            !end if ! me==scribe
            !if (me/=scribe)then

            !print*,'temps de calcul des gradients:',tgrad,'process',me
            !print*,'temps de calcul des flux:',tflux,'process',me

            !end if ! me/=scribe
            deallocate(mess_qouv1,mess_qouv2)
#endif /* WITH_MPI */
            return  ! fin des calculs
         endif
      elseif (t+eps2 > tfin) then
         t = tfin
         dt = tfin-tm
      endif
      if (dt < eps2) then
#if ENG
        write(*,*)'time step too small'
#else /* FRA */
        write(*,*)'pas de temps trop petit'
#endif /* FRA */
         stop
      endif

#ifdef WITH_MPI
!    print*,"plop1 proc",me
      call message_gatherv (hae)
!    print*,"plop2 proc",me
#if TRANSPORT_SOLIDE
      call message_gatherv2 (cet)
         call message_gatherv3(zfn)
         call message_gatherv3(dzfn)
!       if(solute)then
!          write(*,*) 'Transmission Dhe'
          call message_gatherv2(dhe)
!         write(*,*) 'Transmission vitesse frottement'
          call message_gatherv2(vitfro)
          call message_gatherv(fhca)
#if SEDVAR
!     else
          if(sedvar)then
!             write(*,*) 'Transmission diam�tre'
            call message_gatherv2(diame)
            call message_gatherv(diama)
!             write(*,*) 'Transmission Etendue'
            call message_gatherv2(sigmae)
            call message_gatherv(sigmaa)
            call message_gatherv_nbcou(nbcoun)
            do i=1,10
              call message_gatherv3(epcoun(:,i))
              call message_gatherv3(diamn(:,i))
              call message_gatherv3(sigman(:,i))
              call message_gatherv3(taummn(:,i))
            enddo
          endif
#endif
!     endif
#endif
#endif /* WITH_MPI */
! gestion des sorties
      if (me==scribe)then
      !call date_and_time(values=tpsecr1)
      if (tm .ge. trc) then
         call etr(ift,tm)

#if TRANSPORT_SOLIDE
! if(avects)then
        call etc(ift,tm)
!endif
#endif


         trc = trc+dtrc
      endif
! calcul avec ou sans ecriture sur le fichier enveloppe
      if (ifen > 0) then
         call lenv
      endif
      if (tm .ge. tr) then
! ecriture des resultats aux limites
         call er(ifr,tm)

#if TRANSPORT_SOLIDE
! if(avects)then
        call erc(ifr, tm)
!endif
#endif

! si ecriture de edm test sur les mailles les plus p�nalisantes
        if (ifm.gt.0)then
          call wrmaillespenal(ifm)
        endif
         if (lcvi) then
            if (lcviv) then
! ecriture des coefficients nu dans etude.nua
               call wrnu(tm)
            endif
         endif
! ecriture de la ligne d'eau partout pour que le fichier tps soit propre
      if(irep.eq.0.or.tm.ne.tinit)then
         do ie = 1,ne
            if (het(ie).gt.9999.999) then
               het(ie) = 9999.999
            endif
         end do
         do ie = 1,ne
            if (quet(ie).gt.9999.999) then
               quet(ie) = 9999.999
            elseif (quet(ie).lt.-999.999) then
               quet(ie) = -999.999
            endif
         end do
         do ie = 1,ne
            if (qvet(ie).gt.9999.999) then
               qvet(ie) = 9999.999
            elseif (qvet(ie).lt.-999.999) then
               qvet(ie) = -999.999
            endif
         enddo
!          do ie = 1,ne
!            if(.not. eau(ie))then
!              het(ie) = 0.0
!              quet(ie) = 0.0
!              qvet(ie) = 0.0
!            endif
!          enddo
         write(id_tps,'(f15.3)')tm
         write(id_tps,'(8f10.5)') (het(ie),ie=1,ne)
!          call write_contrainte(id_tps)
         write(id_tps,'(8f10.5)') (quet(ie),ie=1,ne)
         write(id_tps,'(8f10.5)') (qvet(ie),ie=1,ne)
#if WITH_CGNS
         !!! test cgns !!!
         nb_timesteps_cgns = nb_timesteps_cgns+1
!          call cg_open_f('grid.cgns',CG_MODE_MODIFY,index_file,ier)
!          if (ier .ne. CG_OK) call cg_error_exit_f
!          index_base=1
!          index_zone=1
         write(sol_names_temp,'(a4,i0)')"Cell",nb_timesteps_cgns
         call c32_push_back(sol_names,sol_names_temp)
         write(sol_names_temp,'(a4,i0)')"Vertex",nb_timesteps_cgns
         call c32_push_back(sol_names2,sol_names_temp)
         call d_push_back(times_cgns,tm)
         call cg_sol_write_f(index_file,index_base,index_zone,sol_names%values(nb_timesteps_cgns),CellCenter,index_flow,ier)
         call cg_sol_write_f(index_file,index_base,index_zone,sol_names2%values(nb_timesteps_cgns),Vertex,index_flow2,ier)
         call cg_field_write_f(index_file,index_base,index_zone,index_flow,integer,'proc',machine(1:ne),index_field,ier)
         call cg_field_write_f(index_file,index_base,index_zone,index_flow,RealDouble,'het',het,index_field,ier)
         call cg_field_write_f(index_file,index_base,index_zone,index_flow,RealDouble,'quet',quet,index_field,ier)
         call cg_field_write_f(index_file,index_base,index_zone,index_flow,RealDouble,'qvet',qvet,index_field,ier)
         !!! fin test cgns !!!
#endif

#if TRANSPORT_SOLIDE
! ecriture des concentrations dans tpc
        write(id_tpc,'(f15.3)')tm
        maxce=0.
        do ie=1,ne
          if(conckg)then
            ce(ie)=min(cet(ie)*dens,9999.999d0)
            maxce=max(ce(ie),maxce)
          else
            ce(ie)=min(cet(ie),9999.999d0)
            maxce=max(ce(ie),maxce)
          endif
        enddo
        if(maxce.gt.1000.)then
          write(id_tpc,'(8f10.4)') (ce(ie),ie=1,ne)
        elseif(maxce.gt.100.)then
          write(id_tpc,'(8f10.5)') (ce(ie),ie=1,ne)
        elseif(maxce.gt.10.)then
          write(id_tpc,'(8f10.6)') (ce(ie),ie=1,ne)
        else
          write(id_tpc,'(8f10.7)') (ce(ie),ie=1,ne)
        endif
#if WITH_CGNS
        call cg_field_write_f(index_file,index_base,index_zone,index_flow,RealDouble,'ce',ce,index_field,ier)
#endif

        if(solute)then
! si solute et derive on ecrit dans vitfro et dhe la vitesse de surface fonction du vent
          if(derive)then
            call vitsur
          endif
        write(id_tpc,'(8f10.5)') (dhe(ie),ie=1,ne)
        write(id_tpc,'(8f10.5)') (vitfro(ie),ie=1,ne)
#if WITH_CGNS
        call cg_field_write_f(index_file,index_base,index_zone,index_flow,RealDouble,'dhe',dhe,index_field,ier)
        call cg_field_write_f(index_file,index_base,index_zone,index_flow,RealDouble,'vitfro',vitfro,index_field,ier)
#endif
! si pas des solutes :limites a vitfro et dhe et ecriture zfn
        else
#if SEDVAR
          if(sedvar)then
        write(id_tpc,'(8f10.5)') (diame(ie),ie=1,ne)
        write(id_tpc,'(8f10.5)') (sigmae(ie),ie=1,ne)
#if WITH_CGNS
        call cg_field_write_f(index_file,index_base,index_zone,index_flow,RealDouble,'diame',diame,index_field,ier)
        call cg_field_write_f(index_file,index_base,index_zone,index_flow,RealDouble,'sigmae',sigmae,index_field,ier)
#endif
        call ecrsed(tm)
           else
!end if sedvar
#endif

        maxdhe=0.
        mindhe=0.
        do ie=1,ne
! pour ecrire des valeurs lisibles on met dhe en mm/h au lieu de m/s
          dhe(ie)=3600000.*dhe(ie)
          maxdhe=max(dhe(ie),maxdhe)
          mindhe=min(dhe(ie),mindhe)
          if(dhe(ie).gt.99999999.)then
            dhe(ie)=99999999.
           elseif(dhe(ie).lt.-9999999.)then
            dhe(ie)=-9999999.
          endif
          if(vitfro(ie).gt.9999.99)then
            vitfro(ie)=9999.999
           elseif(vitfro(ie).lt.-999.99)then
            vitfro(ie)=-999.999
           endif
        enddo
        maxdhe=max(maxdhe,-10.*mindhe)
        if(maxdhe.gt.10000000.)then
          write(id_tpc,'(8f10.0)') (dhe(ie),ie=1,ne)
        elseif(maxdhe.gt.1000000.)then
          write(id_tpc,'(8f10.1)') (dhe(ie),ie=1,ne)
        elseif(maxdhe.gt.100000.)then
          write(id_tpc,'(8f10.2)') (dhe(ie),ie=1,ne)
        elseif(maxdhe.gt.10000.)then
          write(id_tpc,'(8f10.3)') (dhe(ie),ie=1,ne)
        elseif(maxdhe.gt.1000.)then
          write(id_tpc,'(8f10.4)') (dhe(ie),ie=1,ne)
        elseif(maxdhe.gt.100.)then
          write(id_tpc,'(8f10.5)') (dhe(ie),ie=1,ne)
        elseif(maxdhe.gt.10.)then
          write(id_tpc,'(8f10.6)') (dhe(ie),ie=1,ne)
        else
          write(id_tpc,'(8f10.7)') (dhe(ie),ie=1,ne)
        endif
        write(id_tpc,'(8f10.5)') (vitfro(ie),ie=1,ne)
#if WITH_CGNS
        call cg_field_write_f(index_file,index_base,index_zone,index_flow,RealDouble,'dhe',dhe,index_field,ier)
        call cg_field_write_f(index_file,index_base,index_zone,index_flow,RealDouble,'vitfro',vitfro,index_field,ier)
#endif

#if SEDVAR
! fin du if sur sedvar
      endif
!end if sedvar
#endif

! ecriture des cotes dans zfn
          write(id_zfn,'(f15.3)')tm
            if(opendz)then
! dzfn ajoute a zfn pour prendre le cas o� on ne r�actualise pas la topo
          minzfn=zfn(1)+dzfn(1)
          maxzfn=zfn(1)+dzfn(1)
         do in=2,nn
           if (minzfn.gt.zfn(in)+dzfn(in))then
            minzfn=zfn(in)+dzfn(in)
           elseif (maxzfn.lt.zfn(in)+dzfn(in))then
            maxzfn=zfn(in)+dzfn(in)
            endif
         enddo

          if(minzfn.gt.-99.999.and.maxzfn.lt.999.999)then
            write(id_zfn,'(10f8.4)')(zfn(in)+dzfn(in),in=1,nn)
          elseif(minzfn.gt.-999.99.and.maxzfn.lt.9999.99)then
            write(id_zfn,'(10f8.3)')(zfn(in)+dzfn(in),in=1,nn)
          elseif(minzfn.gt.-9999.9.and.maxzfn.lt.99999.9)then
            write(id_zfn,'(10f8.2)')(zfn(in)+dzfn(in),in=1,nn)
          endif
#if WITH_CGNS
        call cg_field_write_f(index_file,index_base,index_zone,index_flow2,RealDouble,'dzfn',dzfn,index_field,ier)
#endif
! dzfn ecrit si on ne r�actualise pas la topo
              write(id_dzf,'(f15.3)')tm
              write(id_dzf,'(10f8.5)')&
     &(max(min(dzfn(in),99.9999d0),-9.9999d0),in=1,nn)
              close(id_dzf)
            else
! si on reactualise la topo
          minzfn=zfn(1)
          maxzfn=zfn(1)
         do in=2,nn
           if (minzfn.gt.zfn(in))then
            minzfn=zfn(in)
           elseif (maxzfn.lt.zfn(in))then
            maxzfn=zfn(in)
            endif
         enddo

          if(minzfn.gt.-99.999.and.maxzfn.lt.999.999)then
            write(id_zfn,'(10f8.4)')(zfn(in),in=1,nn)
          elseif(minzfn.gt.-999.99.and.maxzfn.lt.9999.99)then
            write(id_zfn,'(10f8.3)')(zfn(in),in=1,nn)
          elseif(minzfn.gt.-9999.9.and.maxzfn.lt.99999.9)then
            write(id_zfn,'(10f8.2)')(zfn(in),in=1,nn)
          endif

#if WITH_CGNS
          write(sol_names_temp,'(A19,I0)')'ArbitraryGridMotion',nb_timesteps_cgns
          call c32_push_back(gridmotionpointers, sol_names_temp)
!           write(gridmotionpointers(nb_timesteps_cgns),'(A19,I0)')'ArbitraryGridMotion',nb_timesteps_cgns
          call cg_arbitrary_motion_write_f(index_file,index_base,index_zone,&
        &gridmotionpointers%values(nb_timesteps_cgns),DeformingGrid,index_motion,ier)
!           write(gridcoordpointers(nb_timesteps_cgns),'(A14,I0)')'GridCoord',nb_timesteps_cgns
          write(sol_names_temp,'(A9,I0)')'GridCoord',nb_timesteps_cgns
          call c32_push_back(gridcoordpointers, sol_names_temp)
          call cg_grid_write_f(index_file,index_base,index_zone,gridcoordpointers%values(nb_timesteps_cgns),&
        &index_coord,ier)
          write(sol_names_temp,'(2A)')'/Base/Zone1/',trim(gridcoordpointers%values(nb_timesteps_cgns))
          call cg_gopath_f(index_file,sol_names_temp,ier)
          call cg_array_write_f('CoordinateX',RealDouble,1,sizeof(xn),xn,ier)
          call cg_gopath_f(index_file,sol_names_temp,ier)
          call cg_array_write_f('CoordinateY',RealDouble,1,sizeof(yn),yn,ier)
          call cg_gopath_f(index_file,sol_names_temp,ier)
          call cg_array_write_f('CoordinateZ',RealDouble,1,sizeof(zfn),zfn,ier)
#endif
! fin du if sur opendz
            endif
! fin du if sur solute
        endif

#endif
#if WITH_CGNS
        call cg_field_write_f(index_file,index_base,index_zone,index_flow2,RealDouble,'zfn',zfn,index_field,ier)
#endif

!       call cg_close_f(index_file,ier)

! fin du if sur reprise pour ecriture ou pas TPS
      endif

         tr = tr + dtr
! sortie des resultats d'erosion d'une digue
         do ibouv = 1, nob
            i = int((tm-tini(ibouv))/dt2(ibouv)) + 1
            if (i .le. ntrmax) then
               call impri2(tm,i,ibouv)
            endif
         enddo
! fin du if pour stockage regulier resultats
      endif

      k=k+1
      if (k.gt.99) then
#if ENG
          write (*,*) 'computation at time: ',t!,' par',me
#else /* FRA */
          write (*,*) 'calcul au temps: ',t!,' par',me
#endif /* FRA */
          k=0
      endif
! #ifdef WITH_MPI
        !debut=.false.
        !dt=5000.0
        !tnou=.true.
! #endif /* WITH_MPI */
      !call date_and_time(values=tpsecr2)
       !tpsecr = tpsecr +(tpsecr2(5)*3600.+tpsecr2(6)*60.+tpsecr2(7)+tpsecr2(8)*0.001)&
      !&                  -(tpsecr1(5)*3600.+tpsecr1(6)*60.+tpsecr1(7)+&
      !&                   tpsecr1(8)*0.001)+(tpsecr2(3)-tpsecr1(3))*86400.

      end if ! me==scribe

      !if (me/=scribe)then

! calcul des gradients de h,qu,qv avec limitation, sur les elements
        !call cpu_time(tgrad2)
        call grdlp
        !call cpu_time(tgrad1)
        !tgrad=tgrad+tgrad1-tgrad2
! calcul des nouvelles conditions a la limite si dependent de t
      if ((iclvar.eq.1) .or. debglo) then
         call lecl2(t-dt)
      endif

! resolution par le schema choisi de l equation sans sd. membre
! sauf apports
      if (iapp .gt. 0) then
! reporte ici en debut de sp resrmn
         call secmb6(t-0.5*dt)
      endif

      !call cpu_time(tflux2)
! calcul des flux unitaires de h,qu,qv sur les aretes
      if (schema.eq.'vanleer1') then
!        calcul des flux par vanleer 2-espace et 1-temps
         call flvla1(dt)
      else
!        calcul des flux par vanleer 2-espace et 2-temps
         call flvla2(dt,t,iapp)
      endif
      call schem1(dt,volet,volst&
#if TRANSPORT_SOLIDE
      &,volaet,volast&
#endif
      &,iosmb,iapp)
      !call cpu_time(tflux1)
      !tflux=tflux+tflux1-tflux2

#if TRANSPORT_SOLIDE
! on calcule la contribution des termes d'erosion et de sedimentation
!*******************************************************************
! deplace ici pour que dhe soit celui correspondant au meme temps
! redeplace de la ligne1379 a 1646 pour sedvar
! ou diametre doit tenir compte des flux entrant et sortant
!      if(avects)then
       if(chzn)then

         call semba2!(zfn2,zfa2,zfe2,cofr2)
       else

         call semba_loc!(zfn2,zfa2,zfe2,cofr2)
       endif
!      endif
#endif

      do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
         if (eau(ie)) then
! on ajoute le terme de pente et le laplacien  ainsi que le vent(smbu)
            qu1 = que(ie) + smbu(ie)
            qv1 = qve(ie) + smbv(ie)
            if (abs(qu1) .gt. abs(qv1)) then
! on annule le debit s'il y a inversion de sens seulement pour la plus grande cooordonnee
               if (quet(ie)*qu1 .lt. 0.) then
                  que(ie) = 0.
                  qve(ie) = 0.
               else
                  que(ie) = qu1
                  qve(ie) = qv1
               endif
            else
! else sur la plus grande coordonnee
               if (qvet(ie)*qv1.lt.0.) then
                  que(ie) = 0.
                  qve(ie) = 0.
               else
                  que(ie) = qu1
                  qve(ie) = qv1
               endif
! fin du if sur la plus grande coordonnee
            endif
! fin du if sur he=0
         endif
      end do

#ifdef WITH_MPI
      !call cpu_time(tcomm1)
      call message_group_calcul(que)
      call message_group_calcul(qve)
      !call cpu_time(tcomm2)
      !tcomm=tcomm+tcomm2-tcomm1
#endif /* WITH_MPI */

! calcul des frottements a t et des nouvelles valeurs de he,que,qve
      call secmb2(dt)

! calcul de la nouvelle valeur de dt
      dt1=dt
      !end if ! me/=scribe
      call cflmax(icfl)
#ifdef WITH_MPI
      call mpi_bcast(tnou,1,mpi_logical,scribe,mpi_comm_world,statinfo)
      call mpi_bcast(dt,1,mpi_real8,scribe,mpi_comm_world,statinfo)
      call mpi_bcast(tm,1,mpi_real8,scribe,mpi_comm_world,statinfo)
#endif /* WITH_MPI */
      !if(me/=scribe)then
! egalisation de quet,qvet,het a que,qve,he si calculs acceptes, le contraire sinon
      if (tnou) then




#if TRANSPORT_SOLIDE
        if(.not.solute)then
          if(modifz)then
!!!        write(*,*) 'Salut 2'
! modif de la topo effective
             if (iofr.eq.0)then
               do ieloc=1,ne_loc(me)
                 ie = mailles_loc(ieloc)
                 fra(ie)=cofr2(ie)*fra(ie)/cofr(ie)
              enddo
             else
              do ieloc=1,ne_loc(me)
                ie = mailles_loc(ieloc)
                fre(ie)=cofr2(ie)*fre(ie)/cofr(ie)
              enddo
             endif
            do ieloc=1,ne_loc(me)
              ie = mailles_loc(ieloc)
              cofr(ie)=cofr2(ie)
              zfe(ie)=zfe2(ie)
            enddo
            do ialoc=1,naloc
              ia = aretes_loc(ialoc)
              zfa(ia)=zfa2(ia)
            enddo
            do jn=1,nnloc
              in = noeuds_loc(jn)
              zfn(in)=zfn2(in)
            enddo
          else
! si pas modif topo on conserve le dzfn

            do jn=1,nnloc
              in = noeuds_loc(jn)
              dzfn(in)=dzfn(in)+zfn2(in)-zfn(in)
            enddo
! fin du if sur modif topo
         endif
! fin du if sur solute
       endif
! deux boucles sur ie pour ajouter d'abord smbhc2 puis smbhc
! la premiere boucle est inutile car smbhc2 toujours nulle (flh! utilise)
!        do ie=1,ne
!          if(he(ie).gt.paray)then
! on limite le terme de diffusion si la concentration est non nulle
!            if(hce(ie).gt.eps2)then
!              if(abs(smbhc2(ie)).gt.0.5*abs(hce(ie)))then
!                 smbhc2(ie)=0.5*smbhc2(ie)*abs(hce(ie))/abs(smbhc2(ie))
!              endif
!            endif
!            hce(ie)=hce(ie)+smbhc2(ie)
!            if (hce(ie).lt.eps2) then
!              hce(ie)=0.
!            endif
! si he < paray reporte dans deuxieme boucle
!!          else
!!            cet(ie)=0.
!!            hcet(ie)=0.
!!            hce(ie)=0.
!          endif
! fin boucle sur ie
!        enddo
! ajout smbhc
! calcul nouveau diametre et etendue fait avant
! a partir concentration ancienne (hcet/het)

#if SEDVAR
        if(sedvar)then
          call melangecouches
        endif
#endif /* SEDVAR */
        delta_esdt_loc = 0.
        do ieloc=1,ne_loc(me)
          ie = mailles_loc(ieloc)
          if(he(ie).gt.paray)then
            if(smbhc(ie).lt.-hce(ie))then
! possible car flux entrants/sortants deja pris
!              rapsmbhc(ie)=(-hce(ie)/smbhc(ie))*rapsmbhc(ie)
              smbhc(ie)=-hce(ie)
              hce(ie)=0.
            else
!              rapsmbhc(ie)=min(1.d0,1.1*rapsmbhc(ie))
              hce(ie)=hce(ie)+smbhc(ie)
            endif
            if(cet(ie).gt.eps2)then
              hcet(ie)=hce(ie)/he(ie)
              if(abs(hcet(ie)-cet(ie)).lt.eps2)then
! cet reste identique
                hcet(ie)=cet(ie)*he(ie)
                hce(ie)=hcet(ie)
              else
! si la concentration etait inferieure ou egale a 2 fois celle de ses voisines (avant ou apres) elle le reste
#if VH
                rcet=3.
                do je=1,nne(ie)
                  iev=ieve(ie,je)
                  if(iev.gt.0)then
!! on devrait rajouter une condition sur lepassege ntre les deux mailles
!! nrefa non egal a -2 et cote arete non superieure a cote eau
                    if(cet(iev).gt.eps2)then
                      if(cet(ie)/cet(iev).lt.rcet)then
                        rcet=cet(ie)/cet(iev)
                        cetr=cet(iev)
                      endif
                    endif
                  endif
                enddo
                if(rcet.gt.2.)then
                  hcet(ie)=hce(ie)
                  cet(ie)=hce(ie)/he(ie)
                else
!! on corrige si la concentration etait inferieure a 2 fois celle du voisin
                  cet(ie)=min(1.99*cetr, hce(ie)/he(ie))
                  hcet(ie)=cet(ie)*he(ie)
                  hce(ie)=hcet(ie)
                endif
! limite physique moitie eau moitie sediments
               if(cet(ie).gt.0.5)then
                 cet(ie)=0.5
                 hcet(ie)=0.5*he(ie)
               endif
!fin du if sur VH
#endif
!!            else
! dans version non h les deux lignes suivantes remplacent les lignes !vh
#if VNH
                cet(ie)=hcet(ie)
!                hcet(ie)=hce(ie)
! limite physique moitie eau moitie sediments
               if(cet(ie).gt.0.5)then
                 cet(ie)=0.5
                 hcet(ie)=0.5*he(ie)
                 hce(ie)=hcet(ie)
               else
                 hcet(ie)=hce(ie)
               endif
!fin du if sur VNH
#endif
              endif
! si la concentration ancienne etait nulle
           else
             if(hce(ie).lt.eps2)then
               hcet(ie)=0.
               hce(ie)=0.
               cet(ie)=0.
             else
               cet(ie)=hce(ie)/he(ie)
! limite physique moitie eau moitie sediments
               if(cet(ie).gt.0.5)then
                 cet(ie)=0.5
                 hcet(ie)=0.5*he(ie)
               else
                 hcet(ie)=hce(ie)
               endif
             endif
           endif
! si he < paray
          else
            cet(ie)=0.
            hcet(ie)=0.
            hce(ie)=0.
          endif
! evolution en volume sur le pas de temps
          delta_esdt_loc=delta_esdt_loc-smbhc(ie)*se(ie)
!           esdt=esdt-smbhc(ie)*se(ie)
! fin boucle sur ie
        enddo
        if(lmailczero)then
          do i=1,nmailczero
            cet(imailczero(i))=0.
            hcet(imailczero(i))=0.
            hce(imailczero(i))=0.
          enddo
        endif
#endif






        !call cpu_time(tcomm1)

#ifdef WITH_MPI
        call message_group_calcul(he)
        call message_group_calcul(qve)
        call message_group_calcul(que)

#if TRANSPORT_SOLIDE
        call message_group_calcul(zfe)
        call message_group_calcul(hcet)
        call message_group_calcul(cet)
        call message_group_calcul(hce)
        call mpi_allreduce(delta_esdt_loc,delta_esdt_loc,1,mpi_real8,mpi_sum,mpi_comm_world,statinfo)
        esdt = esdt + delta_esdt_loc
#endif
#endif /* WITH_MPI */

        !call message_group_calcul3
        !call cpu_time(tcomm2)
        !tcomm=tcomm+tcomm2-tcomm1

         do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
            if (he(ie) .lt. paray) then
               quet(ie) = 0.
               que(ie) = 0.
               qvet(ie) = 0.
               qve(ie) = 0.
            else
               quet(ie) = que(ie)
               qvet(ie) = qve(ie)
            endif
         het(ie) = he(ie)
         end do
         ! boucle sur les �l�ments voisins
         ! cela �vite d'�changer les valeurs voisines de het,quet et qvet
         do i = 1,nmv
           ie=elt_voisins(i)
            if (he(ie) .lt. paray) then
               quet(ie) = 0.
               que(ie) = 0.
               qvet(ie) = 0.
               qve(ie) = 0.
            else
               quet(ie) = que(ie)
               qvet(ie) = qve(ie)
            endif
            het(ie) = he(ie)
         end do
! si il y validation de la ligne d'eau on valide les volumes entre et sorti
         vole = vole+volet
         vols = vols+volst
         do iouv = 1,nbouv
            volo(iouv) = volo(iouv)+volouv(iouv)*dt1
            if (typouv(iouv,1).eq.'q') then
! suppose que ouvrage elementaire q tout seul
               vole = vole-volouv(iouv)*dt1
            elseif (ia2(iouv).eq.0) then
               vols = vols+volouv(iouv)*dt1
            endif
         end do

#if TRANSPORT_SOLIDE
         volae=volae+volaet
         volas=volas+volast
         do iouv=1,nbouv
           voloa(iouv)=voloa(iouv)+voaouv(iouv)*dt1
           if(ia2(iouv).eq.0)then
             volas=volas+voaouv(iouv)*dt1
           elseif(typouv(iouv,1).eq.'q')then
             volae=volae+voaouv(iouv)*dt1
           endif
         enddo
#endif



! le 3/12/92 modification tpli n'est plus utilise comme anciennes valeurs
      else
         do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
            if (het(ie) .le. paray) then
               eau(ie) = .false.
            else
               eau(ie) = .true.
            endif
         end do
         ! boucle sur les �l�ments voisins
         do i = 1,nmv
           ie=elt_voisins(i)
            if (het(ie) .le. paray) then
               eau(ie) = .false.
            else
               eau(ie) = .true.
            endif
         end do
      endif
#ifdef WITH_MPI

      if(me/=scribe)then

        if (ifen > 0) then
          call lenv
        endif

      end if ! me/=scribe

! communications
      call mpi_bcast(trc,1,mpi_real8,scribe,mpi_comm_world,statinfo)
      call mpi_bcast(tr,1,mpi_real8,scribe,mpi_comm_world,statinfo)

       if(tm >= trc  .or. tm >= tr .or. tm+dt>=tfin)then
         call message_gatherv2(het)
         call message_gatherv2(quet)
         call message_gatherv2(qvet)

       end if
!
       if ((tm.ge.tr .or. tm+dt>=tfin) .and. ifen>0) then
!          call message_gatherv2(th)
         call message_gatherv2(hem)
         call message_gatherv2(hem2)
         call message_gatherv2(tu)
         call message_gatherv2(qum)
         call message_gatherv2(qum2)
         call message_gatherv2(qvm)
         call message_gatherv2(qvm2)
       end if

       if (tm >= tr .or. tm+dt>=tfin)then
         !if (me/=scribe) then
           mess_qouv1=qouv(1:na,1)
           mess_qouv2=qouv(1:na,2)
         !end if ! me/=scribe

         call message_gatherv(fha)
         call message_gatherv(mess_qouv1)
         call message_gatherv(mess_qouv2)
         if (ifm.gt.0)then
           call message_gatherv2(he)
           call message_gatherv_eau(eau)
         endif
         if (lcvi) then
           if (lcviv) then
             call message_gatherv(ncvi)
           endif
         endif

         if (nob>0) then
           do ibouv = 1, nob
             if(me==procouvb(ibouv))then
               call mpi_send(tini(ibouv),1,mpi_real8,scribe,49,mpi_comm_world,statinfo)
               i = int((tm-tini(ibouv))/dt2(ibouv)) + 1
               call mpi_send(i,1,mpi_integer,scribe,50,mpi_comm_world,statinfo)
               if (i.le.ntrmax .and. tm.ge.tini(ibouv)) then
                 call mpi_send(z(i,ibouv),1,mpi_real8,scribe,51,mpi_comm_world,statinfo)
                 call mpi_send(zbr(i,ibouv),1,mpi_real8,scribe,52,mpi_comm_world,statinfo)
                 call mpi_send(dbr(i,ibouv),1,mpi_real8,scribe,53,mpi_comm_world,statinfo)
                 call mpi_send(ql(i,ibouv),1,mpi_real8,scribe,54,mpi_comm_world,statinfo)
                 call mpi_send(qs(i,ibouv),1,mpi_real8,scribe,55,mpi_comm_world,statinfo)
               end if
             end if ! me==procouvb(ibouv)
             if(me==scribe)then
               call mpi_recv(tini(ibouv),1,mpi_real8,procouvb(ibouv),49,mpi_comm_world,status,statinfo)
               call mpi_recv(i,1,mpi_integer,procouvb(ibouv),50,mpi_comm_world,status,statinfo)
               if (i.le.ntrmax .and. tm.ge.tini(ibouv)) then
                 call mpi_recv(z(i,ibouv),1,mpi_real8,procouvb(ibouv),51,mpi_comm_world,status,statinfo)
                 call mpi_recv(zbr(i,ibouv),1,mpi_real8,procouvb(ibouv),52,mpi_comm_world,status,statinfo)
                 call mpi_recv(dbr(i,ibouv),1,mpi_real8,procouvb(ibouv),53,mpi_comm_world,status,statinfo)
                 call mpi_recv(ql(i,ibouv),1,mpi_real8,procouvb(ibouv),54,mpi_comm_world,status,statinfo)
                 call mpi_recv(qs(i,ibouv),1,mpi_real8,procouvb(ibouv),55,mpi_comm_world,status,statinfo)
               end if
             endif ! me==scribe
           end do
         end if
         if (me==scribe) then
           qouv(1:na,1)=mess_qouv1
           qouv(1:na,2)=mess_qouv2
         end if ! me==scribe
       end if
#endif /* WITH_MPI */
! calcul pour un nouveau temps
      enddo
      end subroutine calcdt


      subroutine final
!=======================================================================
! finalisation : �criture des r�sultats, calcul des bilans
!=======================================================================
      use module_tableaux
      use module_mpi
#ifdef WITH_MPI
      use module_messages
      use mpi
#endif /* WITH_MPI */
#if TRANSPORT_SOLIDE
      use module_ts
#endif
#if WITH_CGNS
      use cgns
      use cgns_data
#endif
      implicit none
      !include 'rubar20_common.for'

! variables locales
      integer :: i, ibouv, ie, je, iouv, isor
      real(wp) :: fm1, nu2, q, volf, volp, volt
      real(wp) :: volume
#ifdef WITH_MPI
      integer,dimension(mpi_status_size)::status
#endif /* WITH_MPI */
      external volume

#if TRANSPORT_SOLIDE
      real(WP) :: ce(ne)
      real(WP) :: maxce,maxdhe,mindhe,maxzfn,minzfn,volaf,volde
      integer :: in
#endif
#if WITH_CGNS
      integer :: ier, isize(3), index_flow, index_flow2, index_field, index_coord, index_motion
      character(LEN=32) :: field_name
#endif


#if ENG
        if (me==scribe)write(*,*) 'Gather water data on writer process'
#else /* FRA */
        if (me==scribe)write(*,*) 'Rassemblement des donn�es eau claire sur le processus scribe'
#endif /* FRA */
#ifdef WITH_MPI
!         !-----pour afficher les voisins-----!
!         he(1:ne)=machine(1:ne)
!         do i=1,nmv
!           he(elt_bord(i))=real(machine(elt_voisins(i))+2)
!         end do
!         !-----------------------------------!
        call message_gatherv2(he)
        !he(1:ne)=machine(1:ne) ! pour afficher le d�coupage
        call message_gatherv2(que)
        call message_gatherv2(qve)
        call message_gatherv_eau(eau)


#if TRANSPORT_SOLIDE
#if ENG
        if (me==scribe)write(*,*) 'Gather sediment data on writer process'
#else /* FRA */
        if (me==scribe)write(*,*) 'Rassemblement des donn�es transport solide sur le processus scribe'
#endif /* FRA */
!         if (me==scribe)write(*,*) 'Concentrations'
      call message_gatherv2(cet)
      call message_gatherv (hae)
      call message_gatherv2 (zfe)
      call message_gatherv3(zfn)
      call message_gatherv3(dzfn)

!     if(solute)then
!          write(*,*) 'Transmission Dhe'
      call message_gatherv2(dhe)
!         write(*,*) 'Transmission vitesse frottement'
      call message_gatherv2(vitfro)
      call message_gatherv (fhca)
#if SEDVAR
!     else
          if(sedvar)then
!             write(*,*) 'Transmission diam�tre'
            call message_gatherv2(diame)
            call message_gatherv(diama)
!             write(*,*) 'Transmission Etendue'
            call message_gatherv2(sigmae)
            call message_gatherv(sigmaa)
            call message_gatherv_nbcou(nbcoun)
            do i=1,10
              call message_gatherv3(epcoun(:,i))
              call message_gatherv3(diamn(:,i))
              call message_gatherv3(sigman(:,i))
              call message_gatherv3(taummn(:,i))
            enddo
          endif
#endif
!     endif
#endif

!       call mpi_gatherv(eau(e1loc:enloc),neloc,mpi_logical,eau,nb_elts_recus&
!      &,deplacements,mpi_logical,scribe,mpi_comm_world,statinfo)
!       call mpi_allreduce(vole,vole,1,mpi_real8,mpi_sum,mpi_comm_world,statinfo)
!       call mpi_allreduce(vols,vols,1,mpi_real8,mpi_sum,mpi_comm_world,statinfo)
#if TRANSPORT_SOLIDE
!       call mpi_allreduce(volas,volas,1,mpi_real8,mpi_sum,mpi_comm_world,statinfo)
!       call mpi_allreduce(volae,volae,1,mpi_real8,mpi_sum,mpi_comm_world,statinfo)
!       call mpi_allreduce(esdt,esdt,1,mpi_real8,mpi_sum,mpi_comm_world,statinfo)
#endif
      do iouv = 1,nbouv
        call mpi_allreduce(volo(iouv),volo(iouv),1,mpi_real8,mpi_sum,mpi_comm_world,statinfo)
      end do
         if (nob>0) then
           do ibouv = 1, nob
             call mpi_allreduce(tini(ibouv),tini(ibouv),1,mpi_real8,mpi_min,mpi_comm_world,statinfo)
             !i = int((tm-tini(ibouv))/dt2(ibouv)) + 1!
             !if (i.le.ntrmax .and. tm.ge.tini(ibouv)) then
               if(me==procouvb(ibouv) .and. me .ne. scribe)then
                 call mpi_send(z(:,ibouv),ntrmax,mpi_real8,scribe,51,mpi_comm_world,statinfo)
                 call mpi_send(zbr(:,ibouv),ntrmax,mpi_real8,scribe,52,mpi_comm_world,statinfo)
                 call mpi_send(dbr(:,ibouv),ntrmax,mpi_real8,scribe,53,mpi_comm_world,statinfo)
                 call mpi_send(ql(:,ibouv),ntrmax,mpi_real8,scribe,54,mpi_comm_world,statinfo)
                 call mpi_send(qs(:,ibouv),ntrmax,mpi_real8,scribe,55,mpi_comm_world,statinfo)
                 call mpi_send(trect(ibouv),1,mpi_real8,scribe,56,mpi_comm_world,statinfo)
                 call mpi_send(db(ibouv),1,mpi_real8,scribe,57,mpi_comm_world,statinfo)
                 call mpi_send(phi(ibouv),1,mpi_real8,scribe,58,mpi_comm_world,statinfo)
                 call mpi_send(zb(ibouv),1,mpi_real8,scribe,59,mpi_comm_world,statinfo)
                 call mpi_send(kappa(ibouv),1,mpi_logical,scribe,60,mpi_comm_world,statinfo)
                 call mpi_send(it(ibouv),1,mpi_integer,scribe,61,mpi_comm_world,statinfo)
               end if ! me==0
               if(me .ne. procouvb(ibouv) .and. me==scribe)then
                 call mpi_recv(z(:,ibouv),ntrmax,mpi_real8,procouvb(ibouv),51,mpi_comm_world,status,statinfo)
                 call mpi_recv(zbr(:,ibouv),ntrmax,mpi_real8,procouvb(ibouv),52,mpi_comm_world,status,statinfo)
                 call mpi_recv(dbr(:,ibouv),ntrmax,mpi_real8,procouvb(ibouv),53,mpi_comm_world,status,statinfo)
                 call mpi_recv(ql(:,ibouv),ntrmax,mpi_real8,procouvb(ibouv),54,mpi_comm_world,status,statinfo)
                 call mpi_recv(qs(:,ibouv),ntrmax,mpi_real8,procouvb(ibouv),55,mpi_comm_world,status,statinfo)
                 call mpi_recv(trect(ibouv),1,mpi_real8,procouvb(ibouv),56,mpi_comm_world,status,statinfo)
                 call mpi_recv(db(ibouv),1,mpi_real8,procouvb(ibouv),57,mpi_comm_world,status,statinfo)
                 call mpi_recv(phi(ibouv),1,mpi_real8,procouvb(ibouv),58,mpi_comm_world,status,statinfo)
                 call mpi_recv(zb(ibouv),1,mpi_real8,procouvb(ibouv),59,mpi_comm_world,status,statinfo)
                 call mpi_recv(kappa(ibouv),1,mpi_logical,procouvb(ibouv),60,mpi_comm_world,status,statinfo)
                 call mpi_recv(it(ibouv),1,mpi_integer,procouvb(ibouv),61,mpi_comm_world,status,statinfo)
               endif
             !end if
           end do
         end if
      !call mpi_allreduce(tcomm,tcomm,1,mpi_real8,mpi_max,mpi_comm_world,statinfo)

#endif /* WITH_MPI */

      if (me==scribe) then
! retour depuis debut du calcul a t si t>tmax
#if ENG
      write (*,*) 'end computation ',schema, ' scheme'
#else /* FRA */
      write (*,*) 'fin calculs schema de ',schema
#endif /* FRA */
! edition resultats
! ecriture sur le fichier enveloppe
      !write (*,*) 'temps de communication: ',tcomm
      if (ifen.gt.0) then
         call lenv
      endif
      call er(ifr,tm)
! si ecriture de edm test sur les mailles les plus p�nalisantes
        if (ifm.gt.0)then
          call wrmaillespenal(ifm)
        endif
      if (lcvi) then
         if (lcviv) then
! ecriture des coefficients nu dans etude.nua
            call wrnu(tm)
            close(id_nua)
         endif
      endif
      call etr(ift,tm)
! pour que le fichier tps soit propre
      do ie = 1,ne
         if (he(ie).gt.9999.999) then
            he(ie) = 9999.999
         endif
         if (que(ie).gt.9999.999) then
            que(ie) = 9999.999
         elseif (que(ie).lt.-999.999) then
            que(ie) = -999.999
         endif
         if (qve(ie).gt.9999.999) then
            qve(ie) = 9999.999
         elseif (qve(ie).lt.-999.999) then
            qve(ie) = -999.999
         endif
      enddo
      write(id_tps,'(f15.3)') tm
      write(id_tps,'(8f10.5)') (he(ie),ie=1,ne)
      write(id_tps,'(8f10.5)') (que(ie),ie=1,ne)
      write(id_tps,'(8f10.5)') (qve(ie),ie=1,ne)
      close(id_tps)
#if WITH_CGNS
      !!! test cgns !!!
      nb_timesteps_cgns = nb_timesteps_cgns+1
      index_base=1
      index_zone=1
      write(sol_names_temp,'(a4,i0)')"Cell",nb_timesteps_cgns
      call c32_push_back(sol_names,sol_names_temp)
      write(sol_names_temp,'(a4,i0)')"Vertex",nb_timesteps_cgns
      call c32_push_back(sol_names2,sol_names_temp)
      call d_push_back(times_cgns,tm)
      call cg_sol_write_f(index_file,index_base,index_zone,sol_names%values(nb_timesteps_cgns),CellCenter,index_flow,ier)
      call cg_sol_write_f(index_file,index_base,index_zone,sol_names2%values(nb_timesteps_cgns),Vertex,index_flow2,ier)
!       call cg_field_write_f(index_file,index_base,index_zone,index_flow2,RealDouble,'Z',zfn,index_field,ier)
      call cg_field_write_f(index_file,index_base,index_zone,index_flow,integer,'proc',machine(1:ne),index_field,ier)
      call cg_field_write_f(index_file,index_base,index_zone,index_flow,RealDouble,'het',het,index_field,ier)
      call cg_field_write_f(index_file,index_base,index_zone,index_flow,RealDouble,'quet',quet,index_field,ier)
      call cg_field_write_f(index_file,index_base,index_zone,index_flow,RealDouble,'qvet',qvet,index_field,ier)
#endif


#if TRANSPORT_SOLIDE



!      if(avects)then
! ecriture dans mas
        call erc(ifr,tm)
        close(id_mas)
! ecriture dans hyc
        call etc(ift,tm)
! ecriture des concentrations dans tpc
        write(id_tpc,'(f15.3)')tm
        maxce=0.
        do ie=1,ne
          if(conckg)then
            ce(ie)=min(cet(ie)*dens,9999.999d0)
            maxce=max(ce(ie),maxce)
          else
            ce(ie)=min(cet(ie),9999.999d0)
            maxce=max(ce(ie),maxce)
          endif
        enddo
        if(maxce.gt.1000.)then
          write(id_tpc,'(8f10.4)') (ce(ie),ie=1,ne)
        elseif(maxce.gt.100.)then
          write(id_tpc,'(8f10.5)') (ce(ie),ie=1,ne)
        elseif(maxce.gt.10.)then
          write(id_tpc,'(8f10.6)') (ce(ie),ie=1,ne)
        else
          write(id_tpc,'(8f10.7)') (ce(ie),ie=1,ne)
        endif
#if WITH_CGNS
       call cg_field_write_f(index_file,index_base,index_zone,index_flow,RealDouble,'ce',ce,index_field,ier)
#endif
!        if(conckg)then
!          write(id_tpc,'(8f10.5)')
!     :(min(cet(ie)*dens,9999.999d0),ie=1,ne)
! !          write(id_tpc,'(8f10.6)') (cet(ie)*dens,ie=1,ne)
!        else
!          write(id_tpc,'(8f10.7)')
!     :(min(cet(ie),99.99999d0),ie=1,ne)
!        endif
        if(solute)then
! si solute et derive on ecrit dans vitfro et dhe la vitesse de surface fonction du vent
          if(derive)then
            call vitsur
          endif

        write(id_tpc,'(8f10.5)') (dhe(ie),ie=1,ne)
        write(id_tpc,'(8f10.5)') (vitfro(ie),ie=1,ne)
#if WITH_CGNS
       call cg_field_write_f(index_file,index_base,index_zone,index_flow,RealDouble,'dhe',dhe,index_field,ier)
       call cg_field_write_f(index_file,index_base,index_zone,index_flow,RealDouble,'vitfro',vitfro,index_field,ier)
#endif
! si pas des solutes :limites a vitfro et dhe et ecriture zfn
        else

#if SEDVAR
          if(sedvar)then

        write(id_tpc,'(8f10.5)') (diame(ie),ie=1,ne)
        write(id_tpc,'(8f10.5)') (sigmae(ie),ie=1,ne)
#if WITH_CGNS
       call cg_field_write_f(index_file,index_base,index_zone,index_flow,RealDouble,'diame',diame,index_field,ier)
       call cg_field_write_f(index_file,index_base,index_zone,index_flow,RealDouble,'sigmae',sigmae,index_field,ier)
#endif
        call ecrsed(tm)
        close(37)
           else
!end if sedvar
#endif

        maxdhe=0.
        mindhe=0.
        do ie=1,ne
! pour ecrire des valeurs lisibles on met dhe en mm/h au lieu de m/s
          dhe(ie)=3600000.*dhe(ie)
          maxdhe=max(dhe(ie),maxdhe)
          mindhe=min(dhe(ie),mindhe)
          if(dhe(ie).gt.99999999.)then
            dhe(ie)=99999999.
           elseif(dhe(ie).lt.-9999999.)then
            dhe(ie)=-9999999.
          endif
          if(vitfro(ie).gt.9999.99)then
            vitfro(ie)=9999.999
           elseif(vitfro(ie).lt.-999.99)then
            vitfro(ie)=-999.999
           endif
        enddo
        maxdhe=max(maxdhe,-10.*mindhe)
        if(maxdhe.gt.10000000.)then
          write(id_tpc,'(8f10.0)') (dhe(ie),ie=1,ne)
        elseif(maxdhe.gt.1000000.)then
          write(id_tpc,'(8f10.1)') (dhe(ie),ie=1,ne)
        elseif(maxdhe.gt.100000.)then
          write(id_tpc,'(8f10.2)') (dhe(ie),ie=1,ne)
        elseif(maxdhe.gt.10000.)then
          write(id_tpc,'(8f10.3)') (dhe(ie),ie=1,ne)
        elseif(maxdhe.gt.1000.)then
          write(id_tpc,'(8f10.4)') (dhe(ie),ie=1,ne)
        elseif(maxdhe.gt.100.)then
          write(id_tpc,'(8f10.5)') (dhe(ie),ie=1,ne)
        elseif(maxdhe.gt.10.)then
          write(id_tpc,'(8f10.6)') (dhe(ie),ie=1,ne)
        else
          write(id_tpc,'(8f10.7)') (dhe(ie),ie=1,ne)
        endif
        write(id_tpc,'(8f10.5)') (vitfro(ie),ie=1,ne)
        close(id_tpc)
#if WITH_CGNS
       call cg_field_write_f(index_file,index_base,index_zone,index_flow,RealDouble,'dhe',dhe,index_field,ier)
       call cg_field_write_f(index_file,index_base,index_zone,index_flow,RealDouble,'vitfro',vitfro,index_field,ier)
#endif

#if SEDVAR
! fin du if sur sedvar
        endif
!end if sedvar
#endif

! ecriture des cotes dans zfn
          write(id_zfn,'(f15.3)')tm
            if(opendz)then
! dzfn ajoute a zfn pour prendre le cas o� on ne r�actualise pas la topo
          minzfn=zfn(1)+dzfn(1)
          maxzfn=zfn(1)+dzfn(1)
         do in=2,nn
           if (minzfn.gt.zfn(in)+dzfn(in))then
            minzfn=zfn(in)+dzfn(in)
           elseif (maxzfn.lt.zfn(in)+dzfn(in))then
            maxzfn=zfn(in)+dzfn(in)
            endif
         enddo

          if(minzfn.gt.-99.999.and.maxzfn.lt.999.999)then
            write(id_zfn,'(10f8.4)')(zfn(in)+dzfn(in),in=1,nn)
          elseif(minzfn.gt.-999.99.and.maxzfn.lt.9999.99)then
            write(id_zfn,'(10f8.3)')(zfn(in)+dzfn(in),in=1,nn)
          elseif(minzfn.gt.-9999.9.and.maxzfn.lt.99999.9)then
            write(id_zfn,'(10f8.2)')(zfn(in)+dzfn(in),in=1,nn)
          endif
#if WITH_CGNS
       call cg_field_write_f(index_file,index_base,index_zone,index_flow2,RealDouble,'dzfn',dzfn,index_field,ier)
#endif
! dzfn ecrit si on ne r�actualise pas la topo
              write(id_dzf,'(f15.3)')tm
              write(id_dzf,'(10f8.5)')&
     &(max(min(dzfn(in),99.9999d0),-9.9999d0),in=1,nn)
              close(id_dzf)
            else
! si on reactualise la topo
          minzfn=zfn(1)
          maxzfn=zfn(1)
         do in=2,nn
           if (minzfn.gt.zfn(in))then
            minzfn=zfn(in)
           elseif (maxzfn.lt.zfn(in))then
            maxzfn=zfn(in)
            endif
         enddo

          if(minzfn.gt.-99.999.and.maxzfn.lt.999.999)then
            write(id_zfn,'(10f8.4)')(zfn(in),in=1,nn)
          elseif(minzfn.gt.-999.99.and.maxzfn.lt.9999.99)then
            write(id_zfn,'(10f8.3)')(zfn(in),in=1,nn)
          elseif(minzfn.gt.-9999.9.and.maxzfn.lt.99999.9)then
            write(id_zfn,'(10f8.2)')(zfn(in),in=1,nn)
          endif
#if WITH_CGNS
          write(sol_names_temp,'(A19,I0)')'ArbitraryGridMotion',nb_timesteps_cgns
          call c32_push_back(gridmotionpointers, sol_names_temp)
!           write(gridmotionpointers(nb_timesteps_cgns),'(A19,I0)')'ArbitraryGridMotion',nb_timesteps_cgns
          call cg_arbitrary_motion_write_f(index_file,index_base,index_zone,&
        &gridmotionpointers%values(nb_timesteps_cgns),DeformingGrid,index_motion,ier)
!           write(gridcoordpointers(nb_timesteps_cgns),'(A14,I0)')'GridCoord',nb_timesteps_cgns
          write(sol_names_temp,'(A9,I0)')'GridCoord',nb_timesteps_cgns
          call c32_push_back(gridcoordpointers, sol_names_temp)
          call cg_grid_write_f(index_file,index_base,index_zone,gridcoordpointers%values(nb_timesteps_cgns),&
        &index_coord,ier)
          write(sol_names_temp,'(2A)')'/Base/Zone1/',trim(gridcoordpointers%values(nb_timesteps_cgns))
          call cg_gopath_f(index_file,sol_names_temp,ier)
          call cg_array_write_f('CoordinateX',RealDouble,1,sizeof(xn),xn,ier)
          call cg_gopath_f(index_file,sol_names_temp,ier)
          call cg_array_write_f('CoordinateY',RealDouble,1,sizeof(yn),yn,ier)
          call cg_gopath_f(index_file,sol_names_temp,ier)
          call cg_array_write_f('CoordinateZ',RealDouble,1,sizeof(zfn),zfn,ier)
          call cg_field_write_f(index_file,index_base,index_zone,index_flow2,RealDouble,'zfn',zfn,index_field,ier)
#endif
! fin du if sur opendz
            endif
          close(id_zfn)
! fin du if sur solute
        endif
!      endif
#endif

#if WITH_CGNS
      dim(1)=32
      dim(2)=nb_timesteps_cgns
      call cg_biter_write_f(index_file,index_base,'TimeIterValues',nb_timesteps_cgns,ier)
      call cg_gopath_f(index_file,'/Base/TimeIterValues',ier)
      call cg_array_write_f('TimeValues',RealDouble,1,nb_timesteps_cgns,times_cgns%values,ier)
      call cg_ziter_write_f(index_file,index_base,index_zone,'ZoneIterativeData',ier)
#if TRANSPORT_SOLIDE
      if (.not. solute .and. .not. opendz)then
        call cg_gopath_f(index_file,'/Base/Zone1/ZoneIterativeData',ier)
        call cg_array_write_f('ArbitraryGridMotionPointers',Character,2,&
       &dim,gridmotionpointers%values(:),ier)
        call cg_gopath_f(index_file,'/Base/Zone1/ZoneIterativeData',ier)
        call cg_array_write_f('GridCoordinatesPointers',Character,2,&
       &dim,gridcoordpointers%values(:),ier)
      endif
#endif
      call cg_gopath_f(index_file,'/Base/Zone1/ZoneIterativeData',ier)
      call cg_array_write_f('FlowSolutionPointers',Character,2,dim,sol_names%values,ier)
      call cg_array_write_f('FlowSolutionPointers2',Character,2,dim,sol_names2%values,ier)
      call cg_simulation_type_write_f(index_file,index_base,TimeAccurate,ier)
      call cg_close_f(index_file,ier)
      call d_deallocate(times_cgns)
      call c32_deallocate(sol_names)
      call c32_deallocate(sol_names2)
      call c32_deallocate(gridmotionpointers)
      call c32_deallocate(gridcoordpointers)
      !!! fin test cgns !!!
#endif




! fin du programme
! on recalcule le strickler equivalent et on ecrit dans frx et frt
      if (darcy) then
          nu2 = 0.000001
          do ie = 1,ne
              if (eau(ie)) then
                  q = sqrt(que(ie)*que(ie)+qve(ie)*qve(ie))
                  if(q.gt.eps2)then
                      fm1=4.*(dlog10(kse(ie)/(12.*he(ie))+1.95/(q/nu2)**0.9))**2
                  else
                      fm1=4.*(dlog10(kse(ie)/(12.*he(ie))))**2
                  endif
                  fre(ie) = sqrt(8.*g*fm1*he(ie)**(-0.333333))
              else
                 fre(ie) = 0.
              endif
          enddo
         isor = 15
         open (isor,file = trim(etude)//'.frc',status = 'unknown')
         write(isor,'(10f8.3)')(fre(ie),ie = 1,ne)
         close(isor)
      endif
! sortie des resultats d'erosion d'une digue
      do ibouv = 1,nob
         i = int((tm-tini(ibouv))/dt2(ibouv))+1
         if (i.le.ntrmax) call impri2(tm,i,ibouv)
         ! pour limiter ecriture resultats aux valeurs enregistrees
         if (i.lt.nt(ibouv)) nt(ibouv) = i
         call impri(ibouv)
      enddo

! calcul du volume final d'eau
      volf=volume(tm)
#if ENG
      write(*,*) 'initial volume =',voli,' output volume =',vols
      write(*,*) 'input volume  =',vole,' final volume =',volf
#else /* FRA */
      write(*,*) 'volume initial =',voli,' volume sorti =',vols
      write(*,*) 'volume entre =',vole,' volume final =',volf
#endif /* FRA */
      if (iapp.gt.0) then
         call calvolp(volp)
         volt=voli+vole+volp-vols
#if ENG
         write (*,*)'volume rain inputs =',volp
         write(*,*)' volume vi+ve+vp-vs =',volt,' to compare to vf'
#else /* FRA */
         write (*,*)'volume apports pluie =',volp
         write(*,*)' volume vi+ve+vp-vs =',volt,' a comparer a vf'
#endif /* FRA */
      else
         volt=voli+vole-vols
#if ENG
         write(*,*)' volume vi+ve-vs =',volt,' to compare to vf'
#else /* FRA */
         write(*,*)' volume vi+ve-vs =',volt,' a comparer a vf'
#endif /* FRA */
      endif

#if TRANSPORT_SOLIDE

!      if(avects)then
! calcul du volume final  de sediments
      volaf=0.
      do ie = 1,ne
         volaf=volaf+hcet(ie)*se(ie)
       end do

      if(solute)then
#if ENG
      write(*,*)'the solute quantities are in m3'
      write(*,*)'init. vol. i =',volai,'output s=',volas
      write(*,*) 'input e=',volae
      write(*,*) 'total i+e-s =',volai+volae-volas,
     :' to compare to qf'
      write(*,*)'final volume qf=',volaf
#else /* FRA */
      write(*,*)'les quantites de solutes sont exprimees en m3'
      write(*,*)'quantite init. =',volai,'quantite sortie=',volas
      write(*,*) 'quantite entree=',volae
      write(*,*) 'quantite i+e-s =',volai+volae-volas,&
     &' a comparer a qf'
      write(*,*)'quantite finale =',volaf
#endif /* FRA */
      else
#if ENG
      write(*,*)'the sediment qauntities are in m3'
      write(*,*) 'init. vol. i=',volai,' output s=',volas
      write(*,*) 'input e=',volae,' deposit d = ',esdt
      write(*,*) 'total i+e-s-d =',volai+volae-volas-esdt,
     :'to compare to qf'
#else /* FRA */
      write(*,*)'les quantites de sediments sont exprimees en m3'
      write(*,*) 'quantite init. sed.=',volai,' sed. sorties=',volas
      write(*,*) 'sed. entrees=',volae,' depots = ',esdt
      write(*,*) 'quantite i+e-s-d =',volai+volae-volas-esdt,&
     &'a comparer a qf'
#endif /* FRA */
      if(modifz)then
      volde=0.
      do ie=1,ne
        volde=volde+(zfe(ie)-zfe0(ie))*se(ie)
      enddo
#if ENG
      write(*,*) 'final sed. volume qf =',volaf,' dif. of zf=',volde
#else /* FRA */
      write(*,*) 'quantite finale sed.=',volaf,' dif. de zf=',volde
#endif /* FRA */
      else
#if ENG
      write(*,*) 'final sed. volume qf=',volaf
#else /* FRA */
      write(*,*) 'quantite finale sed.=',volaf
#endif /* FRA */
      endif
! fin du if sur solute
      endif
!      endif



#endif


#if ENG
      if (ifm.gt.0)  write (*,*) 'mesh data in ',trim(etude),'.edm'
      if (ifci .gt. 0) write (*,*) 'ini.cond. in ',trim(etude),'.eci'
      if (ifcl .gt. 0) write (*,*) 'bound.cond. in ',trim(etude),'.ecl'
      if (ifrt .gt. 0) write (*,*)'drag in ',trim(etude),'.efr'
      if (ifap .gt. 0) write (*,*)'rain inputs in ',trim(etude),'.eap'
      if (ifen .gt. 0) write (*,*)'enveloppe in ',trim(etude),'.env'
      if (ifr .gt. 0) then
         write (*,*) 'boundary results in ',trim(etude),'.out'
         write(ifr,*)'initial volume =',voli,' output volume =',vols
         write(ifr,*)'input volume =',vole,' final volume =',volf
         if (iapp .gt. 0) then
            write (ifr,*)'rain input volume =',volp
#else /* FRA */
      if (ifm.gt.0)  write (*,*) 'donnees maillage dans ',trim(etude),'.edm'
      if (ifci .gt. 0) write (*,*) 'cond.ini. dans ',trim(etude),'.eci'
      if (ifcl .gt. 0) write (*,*) 'cond.lim. dans ',trim(etude),'.ecl'
      if (ifrt .gt. 0) write (*,*)'frottements dans ',trim(etude),'.efr'
      if (ifap .gt. 0) write (*,*)'apports pluie dans ',trim(etude),'.eap'
      if (ifen .gt. 0) write (*,*)'enveloppe dans ',trim(etude),'.env'
      if (ifr .gt. 0) then
         write (*,*) 'resultats aux limites dans ',trim(etude),'.out'
         write(ifr,*)'volume initial =',voli,' volume sorti =',vols
         write(ifr,*)'volume entre =',vole,' volume final =',volf
         if (iapp .gt. 0) then
            write (ifr,*)'volume apports pluie =',volp
#endif /* FRA */
            volt=voli+vole+volp-vols
#if ENG
            write(ifr,*)' volume vi+ve+vp-vs =',volt,' to compare to vf'
#else /* FRA */
            write(ifr,*)' volume vi+ve+vp-vs =',volt,' a comparer a vf'
#endif /* FRA */
         else
            volt=voli+vole-vols
#if ENG
            write(ifr,*)' volume vi+ve-vs =',volt,' to compare to vf'
         endif

         do iouv=1,nbouv
            write(ifr,*)'structure ',iouv,' transited volume ',volo(iouv)
#else /* FRA */
            write(ifr,*)' volume vi+ve-vs =',volt,' a comparer a vf'
         endif

         do iouv=1,nbouv
            write(ifr,*)'ouvrage ',iouv,' volume transite ',volo(iouv)
#endif /* FRA */
         end do

#if TRANSPORT_SOLIDE
!      if(avects)then
      if(solute)then
#if ENG
      write(ifr,*)'solutes volumes are written in m3'
      write(ifr,*)'init. volume = ',volai,' output volume = ',volas
      write(ifr,*) 'intput volume= ',volae
      write(ifr,*) 'volume i+e-s = ',volai+volae-volas,&
     &' to compare to qf'
      write(ifr,*)'final volume = ',volaf
      do iouv=1,nbouv
        write(ifr,*)'structure ',iouv,' trans. volume ',voloa(iouv)
      end do
#else /* FRA */
      write(ifr,*)'les quantites de solutes sont exprimees en m3'
      write(ifr,*)'quantite init. = ',volai,'quantite sortie = ',volas
      write(ifr,*) 'quantite entree = ',volae
      write(ifr,*) 'quantite i+e-s =',volai+volae-volas,&
     &' a comparer a qf'
      write(ifr,*)'quantite finale =',volaf
      do iouv=1,nbouv
        write(ifr,*)'ouvrage ',iouv,'quantite trans.',voloa(iouv)
      end do
#endif /* FRA */

      else
#if ENG
      write(ifr,*)'sediment volumes are written in m3'
      write(ifr,*)'init. sed. = ',volai,' output sed. = ',volas
      write(ifr,*)'input sed. =',volae,' deposits = ',esdt
      write(ifr,*)'volume i+e-s-d =',volai+volae-volas-esdt,&
     &'to compare to qf '
      write(ifr,*)'finale sed. volume =',volaf,' zf dif. =',volde
      do iouv=1,nbouv
        write(ifr,*)'structure ',iouv,' trans. sed. vol. ',voloa(iouv)
      end do
#else /* FRA */
      write(ifr,*)'les quantites de sediments sont exprimees en m3'
      write(ifr,*)'quantite init. sed.=',volai,' sed. sorties=',volas
      write(ifr,*) 'sed. entrees=',volae,' depots = ',esdt
      write(ifr,*) 'quantite i+e-s-d =',volai+volae-volas-esdt,&
     &'a comparer a qf '
      write(ifr,*)'quantite finale sed.=',volaf,' dif. de zf=',volde
      do iouv=1,nbouv
        write(ifr,*)'ouvrage ',iouv,'quant.sed. trans.',voloa(iouv)
      end do
#endif /* FRA */
! fin du if sur solute
      endif
!      endif

#endif



         close (ifr)
      endif
      if (ift .gt. 0) then
#if ENG
          write (*,*) 'limnigrams in ',trim(etude),'.trc'
#else /* FRA */
          write (*,*) 'limnigrammes dans ',trim(etude),'.trc'
#endif /* FRA */
          close (ift)

#if TRANSPORT_SOLIDE
!      if(avects)then
#if ENG
          write (*,*) 'and concentrations in ',trim(etude),'.hyc'
#else /* FRA */
          write (*,*) 'et concentrations dans ',trim(etude),'.hyc'
#endif /* FRA */
          close(id_hyc)
!      endif
#endif

      endif
      if (lcvi) then
         if (lcviv) then
! ecriture des coefficients nu dans etude.nua
#if ENG
            write(*,*)'nu coefficients written in ', trim(etude), '.nua'
#else /* FRA */
            write(*,*)'ecriture des coefficients nu dans ', trim(etude), '.nua'
#endif /* FRA */
         endif
      endif
      end if ! me==scribe
      end subroutine final

!******************************************************************************
      subroutine cedca(if,cvi)
!******************************************************************************
!     calcul/edition donnees complementaires aretes
      use module_precision, only: wp
      use module_tableaux, only :xna,yna,xta,yta,xn,yn,&
     &xa,ya,se,la,cvia,ieva,ina,eps1,eps2,paray,ne,na,nn,lcvi,lcviv&
     &,lcvif,etude
      use module_mpi, only: me, scribe

#if TRANSPORT_SOLIDE
      use module_ts,only:diffus,avects
#endif
      implicit none
!      implicit logical (a-z)
      integer if,ia,ie
!      real(wp) xn(*),yn(*),la(*),xa(*),ya(*),xna(*),yna(*)
!     :,xta(*),yta(*),se(*)
      real(wp) sgn,cvi
      integer ifcvi
      real(wp),dimension(:),allocatable::cvie

!
      external sgn

      !common/param/eps1,eps2,paray
      !common/nomb/ne,na,nn
      !common/vare/xna,yna,xta,yta
      !common/coon/xn,yn
      !common/cooa/xa,ya
      !common/dose/se,la
      !common/cvi/lcvi,lcviv,lcvif
      !common/lcvia/cvia
      !common/iela/ieva
      !common/etud/etude
      !common/nina/ina


      allocate(cvie(0:ne))
      ifcvi=29
      cvie(0)=0.

!     calcul donnees complementaires aretes
!     attention : calcul fait avec nna = 2
      do 100 ia = 1,na

!        longueur
         la(ia) = sqrt((xn(ina(ia,1)) - xn(ina(ia,2)))**2&
     &                +(yn(ina(ia,1)) - yn(ina(ia,2)))**2)

!        coordonnees milieu de l'arete
         xa(ia) = (xn(ina(ia,1)) + xn(ina(ia,2))) / 2.0
         ya(ia) = (yn(ina(ia,1)) + yn(ina(ia,2))) / 2.0

!        coordonnees vecteur unitaire tangent a l'arete
!        (orientee  ina(ia,1)) --> ina(ia,2)))
         xta(ia) = (xn(ina(ia,2)) - xn(ina(ia,1))) / la(ia)
         yta(ia) = (yn(ina(ia,2)) - yn(ina(ia,1))) / la(ia)
         if (abs(xta(ia)).lt.eps2) then
           xta(ia)=0.
           yta(ia)=sgn(yta(ia))
         elseif (abs(yta(ia)).lt.eps2) then
           xta(ia)=sgn(xta(ia))
           yta(ia)=0.
         endif
!        coordonnees vecteur unitaire normal a l'arete
!        (tel que (norm,tang) repere orthonorme direct)
         xna(ia) = + yta(ia)
         yna(ia) = - xta(ia)
 100  continue
      if (cvi .gt. eps2) then
         do ia=1,na
           cvia(ia)=cvi
         enddo
         lcvi=.true.
      elseif (cvi.lt.-eps2) then
         lcvi=.true.
         open(ifcvi,file=trim(etude)//'.dif',status='unknown')
      if (me==scribe)then
#if ENG
         write(*,*)'reading diffusion coefficients'
#else /* FRA */
         write(*,*)'lecture des coefficients de diffusion'
#endif /* FRA */
      end if ! me==scribe
         read(ifcvi,'(10f8.5)')(cvie(ie),ie=1,ne)
         close(ifcvi)
         do ia=1,na
           cvia(ia)=max(cvie(ieva(ia,1)),cvie(ieva(ia,2)))
         enddo
      else
#if TRANSPORT_SOLIDE
      if(avects)then
         if(diffus)then
          lcvi=.true.
           do ia=1,na
             cvia(ia)=0.
           enddo
        else
#endif
         lcvi=.false.
#if TRANSPORT_SOLIDE
        endif
      endif
#endif
      endif

!     edition donnees complementaires aretes
      if (me==scribe)then
      if (if.eq.21.or.if.eq.23.or.if.eq.25.or.if.eq.27) then
      write (if,*)
      write (if,*) '***********************************************'
#if ENG
      write (if,*) '    ADDITIONAL EDGE DATA'
#else /* FRA */
      write (if,*) '    DONNEES  COMPLEMENTAIRES  ARETES'
#endif /* FRA */
      write (if,*) '***********************************************'
      write (if,*)
#if ENG
      write (if,*) '   EDGE     LENGTH       XMIL         YMIL',&
#else /* FRA */
      write (if,*) '   ARETE    LONG         XMIL         YMIL',&
#endif /* FRA */
     &               '    XNRM  YNRM   XTNG  YTNG'
      write (if,*) '-------------------------------',&
     &               '---------------------------'
      do 200 ia = 1,na
         write (if,1) ia,la(ia),xa(ia),ya(ia),xna(ia),&
     &                           yna(ia),xta(ia),yta(ia)
   1     format(i9,1x,f7.2,1x,f12.4,1x,f12.4,3x,&
     &              f5.2,1x,f5.2,2x,f5.2,1x,f5.2)
 200  continue
      endif
      end if ! me==scribe
      deallocate(cvie)

!      close(if)
! 300  continue
      end subroutine cedca

!******************************************************************************
      subroutine cedce(if)
!******************************************************************************
!     calcul/edition donnees complementaires elements

      use module_tableaux, only :dxe,dxen,xn,yn,xe,ye,se,nne,ine,iae&
     &,la,xna,yna,xta,yta,zfn,cofr,ne,na,nn,xa,ya
      use module_precision
      use module_mpi,only:ne_loc,me,mailles_loc, scribe

      implicit none
!      implicit logical (a-z)
      integer if,ie,j,ia,ja,ie1,ie2,ie3,ie4,ia1,ia2,ia3
      real(wp) :: dx1,dx2,dx3

!c valeur minimale normale du pas d'espace
!      dx0=1.

!  initialisation de dxen
      do ie1=1,ne
        dxen(ie1)=1000000.
      enddo
!     parcours des elements
      do 100 ie = 1,ne
!  pour ecriture si 3 sommets seulement
          DXE(IE,4)=0.

!        calcul surface horizontale de l'element
          ie1=ine(ie,1)
          ie2=ine(ie,2)
          ie3=ine(ie,3)
          if (nne(ie).eq.4) then
            ie4=ine(ie,4)
            se(ie)=(xn(ie1)-xn(ie3))*(yn(ie2)-&
     &      yn(ie4))-(xn(ie2)-xn(ie4))*&
     &      (yn(ie1)-yn(ie3))
          elseif (nne(ie).eq.3) then
            se(ie)=xn(ie1)*(yn(ie2)-yn(ie3))&
     &      +xn(ie2)*(yn(ie3)-yn(ie1))&
     &      +xn(ie3)*(yn(ie1)-yn(ie2))
          endif

!        calcul surface de l'element
! surface du triangle 123
          cofr(ie)=sqrt(((xn(ie3)-xn(ie2))*(yn(ie1)-yn(ie3))-&
     & (yn(ie3)-yn(ie2))*(xn(ie1)-xn(ie3)))**2+&
     &((xn(ie3)-xn(ie2))*(zfn(ie1)-zfn(ie3))-&
     & (zfn(ie3)-zfn(ie2))*(xn(ie1)-xn(ie3)))**2+&
     &((yn(ie3)-yn(ie2))*(zfn(ie1)-zfn(ie3))-&
     & (zfn(ie3)-zfn(ie2))*(yn(ie1)-yn(ie3)))**2 )
          if (nne(ie).eq.4) then
! on ajoute la surface du triangle 143
            cofr(ie)=cofr(ie)+sqrt(((xn(ie3)-xn(ie4))*&
     & (yn(ie1)-yn(ie3))-(yn(ie3)-yn(ie4))*(xn(ie1)-xn(ie3)))**2+&
     &((xn(ie3)-xn(ie4))*(zfn(ie1)-zfn(ie3))-&
     & (zfn(ie3)-zfn(ie4))*(xn(ie1)-xn(ie3)))**2+&
     &((yn(ie3)-yn(ie4))*(zfn(ie1)-zfn(ie3))-&
     & (zfn(ie3)-zfn(ie4))*(yn(ie1)-yn(ie3)))**2 )
         endif

         cofr(ie) = abs(se(ie)/cofr(ie))
         se(ie) = 0.5*abs(se(ie))

!        calcul coordonnees isobarycentre de l'element
         xe(ie) = xn(ine(ie,1))
         ye(ie) = yn(ine(ie,1))
         do 20 j = 2,nne(ie)
            xe(ie) = xe(ie) + xn(ine(ie,j))
            ye(ie) = ye(ie) + yn(ine(ie,j))
  20     continue
         xe(ie) = xe(ie) / nne(ie)
         ye(ie) = ye(ie) / nne(ie)

! on calcule la distance du centre de l'element
! a chacun des cotes
         if(nne(ie).eq.4)then
         do 30 ja=1,nne(ie)
          ia=iae(ie,ja)
          dxe(ie,ja)=se(ie)/la(ia)
!                ie1=ine(ie,ja)
                 if(dxen(ie).gt.dxe(ie,ja))dxen(ie)=dxe(ie,ja)
!          if (dx0 .gt. dxe(ie,ja)) then
!           in=ina(ia,1)
!c le coeffcient 2 est introduit pour avoir sur un maillage
!c regulier dxe=dx=dy ou on introduit le pas d'espace sur toute la maille
!           dxe(ie,ja)=max(dxe(ie,ja),2.*
!     :       abs((yn(in)-ye(ie))*yna(ia)+(xn(in)-xe(ie))*xna(ia)))
!           if (dxe(ie,ja).lt.dx0) then
!             write(*,*)'pas trop petit :',dxe(ie,ja),' maille',ie,' x='
!     :,xe(ie),' y=',ye(ie)
!           endif
!          endif
 30      continue
! si nne=3
         else
         IA1=IAE(IE,1)
         IA2=IAE(IE,2)
         IA3=IAE(IE,3)
         dx1=sqrt((xa(ia1)-xa(ia2))**2+(ya(ia1)-ya(ia2))**2)
         dx2=sqrt((xa(ia3)-xa(ia2))**2+(ya(ia3)-ya(ia2))**2)
         dx3=sqrt((xa(ia1)-xa(ia3))**2+(ya(ia1)-ya(ia3))**2)
         dxe(ie,1)=min(dx1,dx3)
         dxe(ie,2)=min(dx2,dx1)
         dxe(ie,3)=min(dx2,dx3)
         do 31 ja=1,nne(ie)
           if(dxen(ie).gt.dxe(ie,ja))dxen(ie)=dxe(ie,ja)
 31      continue
! fin du if sur nne
         endif
 100  continue

!     edition donnees complementaires elements
      if (me==scribe)then
      if (if.eq.21.or.if.eq.22.or.if.eq.25.or.if.eq.26) then
      write (if,*)
      write (if,*) '***********************************************'
#if ENG
      write (if,*) '    ADDITIONAL CELL DATA'
#else /* FRA */
      write (if,*) '    DONNEES  COMPLEMENTAIRES  ELEMENTS'
#endif /* FRA */
      write (if,*) '***********************************************'
      write (if,*)
#if ENG
      write (if,*) '   CELL   SURFACE      XBAR     YBAR       DX1',&
#else /* FRA */
      write (if,*) '   ELEMT  SURFACE      XBAR     YBAR       DX1',&
#endif /* FRA */
     & '    DX2     DX3    DX4'
      write (if,*) '--------------------------------------------'
      do 200 ie = 1,ne
         write (if,1) ie,se(ie),xe(ie),ye(ie),dxe(ie,1),dxe(ie,2)&
     &,dxe(ie,3),dxe(ie,4)
   1     format(i9,1x,f15.6,2x,f13.4,1x,f13.4,3x,4f9.4)
 200  continue
      endif
      !if (if.ne.0)close(if)
      end if ! me==scribe
! 300  continue
      end subroutine cedce

!*****************************************************************************
      subroutine cflmax(icfl)
!      subroutine cflmax(icfl,tnou,vole,vols)
!*****************************************************************************
!   calcul de la cfl maximum et ensuite du pas de temps assurant la
!   stabilite du schema (cfl<1)
!   entree :
!          he : hauteur d eau
!          que : debit selon x
!          qve : debit selon y
!          dt0 : pas de temps initial
!   sortie :
!          dt  : pas de temps de calcul
!          cfl : cflmaxi calcule
!*****************************************************************************

      use module_tableaux,only:dxe,dxen,he,que,qve,xna,yna,xta,yta,iae&
     &,nne,nrefa,ieva,hae,fha,fqua,fqva,eau,se,la,ne,na,nn,g,&
     &dt,cfl,dt0,t,tm,tr,tinit,tmax,ift,eps1,eps2,paray,tnou
      use module_precision
      use module_mpi,only:ne_loc,me,mailles_loc,statinfo,scribe
#ifdef WITH_MPI
      use mpi
#endif /* WITH_MPI */
#if TRANSPORT_SOLIDE
!Suppression des lignes suivantes car n'intervient pas dans le calcul du nombre de courant
!      use module_ts,only:smbhc,hce
#endif
!$    use omp_lib

      implicit none

      real(wp) cflc,dt1,c,sgn, cflc2
      integer ie,icfl,id,id2,n,ja,ia,ieloc
      real(wp) ui,ci,uj
!     :,se(nemax),la(namax),vole,vols,r

#if TRANSPORT_SOLIDE
!Suppression des lignes suivantes car n'intervient pas dans le calcul du nombre de courant
!      real(WP) :: cflc_ts
#endif
      external c,sgn

      !common/grav/g
      !common/dxdy/dxe,dxen
      !common/nomb/ne,na,nn
!      common/tlig/het,quet,qvet
      !common/tpli/he,que,qve
      !common/cflr/dt,cfl,dt0,t
      !common/tenv/tm,tr,tinit,tmax
      !common/vare/xna,yna,xta,yta
      !common/nrel/iae
      !common/nvois/nne
      !common/iftr/ift
      !common/param/eps1,eps2,paray
      !common/narf/nrefa
      !common/hali/hae
      !common/flux/fha,fqua,fqva
      !common/preso/eau
      !common/dose/se,la
      !common/iela/ieva
      !common/tsuiva/tnou

      save id,id2
      data id,id2/0,0/

!c mailles trop petites (<dx0) non prises en compte pour calcul cfl
!      dx0=1.
!      tnou=.true.

!  test si trace de calcul o/n
!*****************************
!26          if (ift .gt. 0) then
!26              write (ift,*) 'calcul du nouveau dt'
!26          endif
! calcul de la cfl

              cflc=0.
              cflc2=0.
#if TRANSPORT_SOLIDE
!Suppression des lignes suivantes car n'intervient pas dans le calcul du nombre de courant
!              cflc_ts=0.
#endif


! #ifdef WITH_MPI
      !if (me/=scribe) then
! #endif /* WITH_MPI */
!$omp parallel private(dt1,ie,id,id2,n,ja,ia,ui,ci)
!$omp do schedule(runtime)reduction(max:cflc)
              do ieloc=1,ne_loc(me)
                   ie=mailles_loc(ieloc)
!                  if (eau(ie)) then
!                     u=que(ie)/he(ie)
!                     v=qve(ie)/he(ie)
                   if(he(ie).gt.paray)then
                      ci=c(he(ie))
                      uj=sqrt(que(ie)**2+qve(ie)**2)/he(ie)
                      if(uj.gt.30.)then
                         que(ie)=que(ie)*30./uj
                         qve(ie)=qve(ie)*30./uj
                      endif
                   do ja=1,nne(ie)

                      ia=iae(ie,ja)
                     if (nrefa(ia).eq.-2) then
                       dt1=0.
!                     elseif (dxe(ie,ja).lt.dx0) then
!                        dt1=0.
                     elseif (hae(ia) .gt. paray) then
                      ui=abs(fha(ia)/hae(ia))
! ajout du 26/3/2014 : nouvelle correction plus forte
! en accord avce correction TS a 10m/s
! on corrige la vitesse mais pas le flux ni les hauteurs
! donc pas d'erreur de volume
! novembre 2020 : limite accrue de 10 a 30 m/s pour test barrag

                      if(ui.gt.30.)then
!                          if(he(ie).gt.paray)then
!                             uj = sqrt(que(ie)**2+qve(ie)**2)/he(ie)
!                             if(uj.gt.10)then
                               ui=30.
!                                que(ie) = ui*que(ie)/uj
!                                qve(ie) = ui*qve(ie)/uj
!                             else
!                                ui = uj
!                             endif
!                          else
!                             ui = 0
!                          endif
                      endif
! ajout du 26/7/2012 pour eviter accoups sur dt
!ap                      if (ui .gt. 20.) then
!ap                         ui=20.
! ajout pour rejaustement
!            contribution sur l'element de gauche (meme normale que l'arete)
!ap             iev=ieva(ia,1)
!ap             if (iev .gt. 0) then
!ap                 r = (dt/se(iev)) * la(ia)
!apc on met fha eagl a 20hae et fqua, fqva egaux � z�ro
!ap                 he(iev)  = he(iev)  + r * (fha(ia)- 20.*hae(ia))
!              fque=0.5*g*he*he+que*que/he
!              fqve=que*qvg/he
!ap                 if (he(iev) .gt. paray) then
!ap                   que(iev) = que(iev)
!ap     : + r * (fqua(ia)-0.5*g*hae(ia)**2*xna(ia))
!ap                   qve(iev) = qve(iev)
!ap     : + r * (fqva(ia)-0.5*g*hae(ia)**2*yna(ia))
!ap                 else
!ap                   eau(iev)=.false.
!ap                   he(iev)=0.
!ap                   que(iev)=0.
!ap                   qve(iev)=0.
!ap                 endif
!ap             elseif (fha(ia).lt.0.) then
!ap                 vols=vols+dt*la(ia)*(fha(ia)  - 20.*hae(ia))
!ap             else
!ap                 vole=vole-dt*la(ia)*(fha(ia)  - 20.*hae(ia))
!ap             endif
!apc            contribution sur l'element de droite (normale opposee)
!ap             iev=ieva(ia,2)
!ap             if (iev .gt. 0) then
!ap                 r = (dt/se(iev)) * la(ia)
!ap                 he(iev)  = he(iev)  - r * (fha(ia)-20.*hae(ia))
!ap                 if (he(iev) .gt. paray) then
!ap                   que(iev) = que(iev)
!ap     : - r * (fqua(ia)-0.5*g*hae(ia)**2*xna(ia))
!ap                   qve(iev) = qve(iev)
!ap     : - r * (fqva(ia)-0.5*g*hae(ia)**2*yna(ia))
!ap                 else
!ap                   eau(iev)=.false.
!ap                   he(iev)=0.
!ap                   que(iev)=0.
!ap                   qve(iev)=0.
!ap                 endif
!ap             elseif (fha(ia) .gt. 0.) then
!ap                 vols=vols-dt*la(ia)*(fha(ia)- 20.*hae(ia))
!ap             else
!ap                 vole=vole+dt*la(ia)*(fha(ia)  - 20.*hae(ia))
!ap             endif
! fin du reajustement
! fin du if sur ui>20
!ap                     endif
!                      ui=u*xna(ia)+v*yna(ia)
!c                       ui=max(abs(u),abs(v))
!c                       dt1=(ui+ci)*dt/dxn(ie)
                      dt1=(abs(ui)+ci)/dxe(ie,ja)
!c                   dt1 = sqrt(dt1) + c(he(ie))
!c                   dt1 = dt1 * dt / dxe(ie)
! si hae est nul
                     else
                       dt1=0.
                     endif
                   cflc=max(cflc,dt1)
                   end do
#if TRANSPORT_SOLIDE
! suppression des 5 lignes suivantes le 30/4/13 car reduit trop dt
!                   if(hce(ie).gt.eps1)then
! on tolere 10% d'erreur
!                    if(smbhc(ie).lt.-1.1*hce(ie))then
!                       cflc_ts=max(cflc_ts,abs(SMbhc(ie)/hce(ie)))
!                    endif
!                  endif
#endif

! fin du if sur he=0
                 endif
             end do
!$omp end do
!$omp end parallel
             cflc2=cflc*dt

      !end if ! me/=scribe


#if TRANSPORT_SOLIDE
!Suppression des lignes suivantes car n'intervient pas dans le calcul du nombre de courant
!        if(cflc_ts.gt.1.1)then
!            cflc2=cflc2*cflc_ts
!        endif
#endif
#ifdef WITH_MPI
      call mpi_reduce(cflc2,cflc,1,mpi_real8,mpi_max,scribe,mpi_comm_world,statinfo)
#else /* WITH_MPI */
      cflc=cflc2
#endif /* WITH_MPI */

      if (me==scribe)then

!  test sur la cfl
!*****************
! cas d'un modele vide
             if (cflc.eq.0.)cflc=0.9*cfl
! pour un calcul avec pas de temps constant
! id est l'indice de division de dt0
! id2=1 si au pas de temps precedent il y a eu division ou multiplication de dt
      if (icfl.eq.0) then
         if (cflc .gt. cfl) then
! si la condition de courant n'est pas verifiee on recommence avec dt/2**n
!            dt = dt * 0.9 * (cfl/cflc)
            n=nint(log(cflc/cfl)/log(2.))+1
            dt=(.5)**n*dt
!            tnou=.false.
!            do 30 ie = 1,ne
!             he(ie) = het(ie)
!             que(ie) = quet(ie)
!             qve(ie) = qvet(ie)
! 30         continue
            id = id+n
            id2 = 1
!            goto 100
         elseif (id.eq.0) then
            dt = dt0
            id2 = 0
          elseif (id2.eq.0) then
            dt = 2.*dt
            id2 = 1
            id = id-1
          else
! on garde le meme dt
            id2 = 0
          endif
!          go to 100
!         endif
          tm = t
      else
! pour un calcul avec cfl constante
          if (cflc .gt. 1.05*cfl.and.tnou) then
! on recommence le calcul pour avoir une cflc plus faible mais une fois
! seulement
             dt = dt*max(0.9d0,cfl/cflc)
! on recommence le calcul pour eviter les brusques modifications de cfl
#if ENG
             write (*,*) 'rewind at time: ',t
#else /* FRA */
             write (*,*) 'retour en arriere au temps: ',t
#endif /* FRA */
             tnou=.false.
          else
           tnou=.true.
           tm = t
!           dt = dt * cfl/cflc
           dt = dt * min(1.1d0,max(0.9d0,cfl/cflc))
          endif
      endif
      end if ! me==scribe
      end subroutine cflmax

!******************************************************************************
      subroutine dicho2(qul,y,dy,he,ue,hl,xl,ia)
!******************************************************************************
!     resolution par dichotomie de l'equation en hl : qul = y(he,ue,hl)
!           avec qul et hl lies par une loi de tarage et qul inconnu
!
!     entrees:
!     ia d'ou la loi de tarage
!     he,ue  valeurs de h et u sur l'autre extremite
!     y,dy   fonctions donnees (y notee gama dans vila et dy derivee de y)
!     eps1    eps1ilon machine (common param)
!     prec   precision sur abs(x-y) dans newton (common newton)
!     nitmax nombre maxi d'iterations par newton autorise (common newton)
! xl longueur arete
!
!     sorties:
!     qul hl    valeurs de h et q sur l'extremite ou s'applique la c.l.
!******************************************************************************
!      implicit logical (a-z)
      use module_tableaux,only:zfa,nltmax,nplmax,eps1,eps2,paray,&
     &prec,nitmax,ift,ialt,nblt,iltmax,idl
       use module_precision
      implicit none

      integer ia,i
      integer niter
      !integer idl
      real(wp) qul,he,ue,hl,y,dy,qul0,dq
      real(wp) delta,delta1,delta2,hm,hp,zf,xl,fmul
      external y,dy

      !common/param/eps1,eps2,paray
      !common/newton/prec,nitmax
      !common/iftr/ift
      !common/nbplt/ialt,nblt,iltmax
      !common/zare/zfa

! compte de messages avertissement
!      data idl/0/
!      save idl
      If(he.lt.paray)then
         hl=0.
         QUL=0.
         return
      endif
! on garde dans qul0 le signe de QUL
      qul0=qul
! fmul facteur multiplicatif
      fmul=2.
      zf=0.
      do i=1,iltmax
        if (ia.eq.ialt(i)) then
            zf=zfa(ia)
            go to 2
        endif
      end do
  1   fmul=fmul*5.!     valeurs initiales
      if(fmul.gt.2000.)return
!     valeurs initiales
! le 26/7/12 modif du facteur 2 en facteur 10 pour le cas de faibles hauteur
  2   hp=fmul*he
      hm=he/fmul
! premiere etape : hm valeur ou delta negative
! hp valeur ou delta positive
      hl=he
      qul=qul0
      call calcvl(qul,hl+zf,dq,i)
! calcvl renvoie le debit de la loi de tarage
      delta = qul - y(he,ue,hl,xl)
      if(abs(delta).le.prec)then
        return
      endif
      qul=qul0
      call calcvl(qul,hp+zf,dq,i)
      delta1 = qul - y(he,ue,hp,xl)
      if(abs(delta1).le.prec)then
        hl=hp
        return
      endif
      qul=qul0
      call calcvl(qul,hm+zf,dq,i)
      delta2 = qul - y(he,ue,hm,xl)
      if(abs(delta2).le.prec)then
        hl=hm
        return
      endif
      if(delta1.gt.prec)then
        if(delta.gt.prec)then
          if(delta2.gt.prec)then
!             write(*,*)'non convergence pour la loi de tarage'
!             write(*,*) 'dicho2 : signe constant arete ia =',ia
            qul=qul0
            call nwt2(qul,y,dy,he,ue,hl,xl,ia)
            return
! normalement delta2 negatif
          else
             hp=hl
!            hm=hm
          endif
! normalement delta negatif,delta2 positif ou negatif
        else
          if(delta2.gt.prec)then
             qul=qul0
             call nwt2(qul,y,dy,he,ue,hl,xl,ia)
             return
          else
!            hp=hp
            hm=hl
          endif
        endif
! normalement delta1 negatif,delta2 positif ou negatif
      elseif(delta.gt.prec)then
        if(delta2.gt.prec)then
          hm=hp
          hp=hl
        else
             qul=qul0
             call nwt2(qul,y,dy,he,ue,hl,xl,ia)
             return
        endif
! normalement delta1 negatif,delta negatif
      elseif(delta2.gt.prec)then
        hp=hm
        hm=hl
      else
!            write(*,*)'non convergence pour la loi de tarage'
!            write(*,*) 'dicho2 : signe constant arete ia =',ia
             qul=qul0
             call nwt2(qul,y,dy,he,ue,hl,xl,ia)
             return
      endif
! on commence
      niter = 1
      hl=0.5*(hm+hp)
!     boucle d'iteration dichotomie
 10   niter = niter + 1
      qul=qul0
      call calcvl(qul,hl+zf,dq,i)
      delta = qul - y(he,ue,hl,xl)
      if (abs(delta).lt.prec) then
        return
      elseif (delta .gt. 0.) then
        hp=hl
      else
        hm=hl
      endif
      if (niter .gt. nitmax) then
        ! if (abs(delta) .gt. sqrt(prec)) then
        if (idl.lt.1000) then
          if(fmul.gt.400.)then
#if ENG
          write(*,*)'Rating Curve Not Reliable'
          write(*,*) 'DICHO2 : NITER > NITMAX EDGE IA =',ia
#else
          write(*,*)'NON CONVERGENCE POUR LA LOI DE TARAGE'
          write(*,*) 'DICHO2 : NITER > NITMAX ARETE IA =',ia
#endif
          write(*,*)'DELTA:',delta,' QUL:',qul,' Q:',y(he,ue,hl,xl)
          idl=idl+1
          endif
        endif
        ! endif
! on recommence avec un fmul plus grand
        go to 1
        return
      endif
      hl=0.5*(hm+hp)
      goto 10
      return
      end subroutine dicho2

!*****************************************************************************
      subroutine er(if,t)
!******************************************************************************
!     edition resultats vf2
      use module_tableaux, only:xa,ya,zfa,nrefa,&
     &se,la,hae,fha,fqua,fqva,ieva,houv,qouv,ne,na,nn,zfm,&
     &eps1,eps2,paray,noumax,debut,ie1,ie2,ia1,ia2,j1,j2,nouv,nbouv&
     &,nouveau_format
       use module_precision

      implicit none
!      implicit logical (a-z)
      integer if,ia,iouv
      real(wp) t,fq,q
!      integer if,na,ia,nrefa(*)
!      real(wp) t,hae(*),fha(*),zfa(*),xa(*),ya(*),la(*),fq,zfm
      logical ecrit

      !common/nomb/ne,na,nn
      !common/cooa/xa,ya
      !common/zare/zfa
      !common/narf/nrefa
      !common/zbas/zfm
      !common/dose/se,la
      !common/hali/hae
      !common/flux/fha,fqua,fqva
      !common/iela/ieva
      !common/hqouv/houv,qouv
      !common/param/eps1,eps2,paray
      !common/debcal/debut
      !common/iouvra/ie1,ie2,ia1,ia2,j1,j2,nouv,nbouv

      save ecrit

      if (debut) then
! mis a faux dans grdlp
!        debut=.false.
        if (if .gt. 0) then
           ecrit=.true.
        endif
      else
        if (ecrit) then
!      write (if,*)
!      write (if,*) '************************************************'
!      write (if,*) '   resultats des calculs par volumes finis 2d'
!      write (if,*) '           sorties, entrees et ouvrages'
!      write (if,*) '************************************************'
      write (if,*)
#if ENG
      write (if,*) 'time  : ',t
#else /* FRA */
      write (if,*) 'temps  : ',t
#endif /* FRA */
      write (if,*)
#if ENG
      write (if,*) 'edge   h edge     qn edge     l edge '//&
     &             '     z edge      x edge      y edge   ref'
#else /* FRA */
      write (if,*) 'arete   h arete    qn arete    l arete'//&
     &             '     z arete     x arete     y arete  ref'
#endif /* FRA */
      write (if,*) '--------------------------------------',&
     &             '--------------------------------'
       if(nouveau_format)then
        do ia = 1,na
          IF(NREFA(IA).GT.0)THEN
          IF(NREFA(IA).NE.2&
!cou1d     &.OR.COUPLAGE1D(IA) &
     &)THEN
            if (ieva(ia,1).eq.0) then
              fq=fha(ia)*la(ia)
            else
              fq=-fha(ia)*la(ia)
            endif
            if (fq .gt. 9999999.) then
              fq=9999999.
            elseif (fq.lt.-999999.) then
              fq=-999999.
            endif
!         write(if,1)ia,hae(ia),fq,la(ia),zfa(ia)+zfm,xa(ia),ya(ia),
            if (abs(fq).lt.1.) then
              write(if,4)ia,hae(ia),fq,la(ia),zfa(ia),xa(ia),ya(ia),&
     &              nrefa(ia)
            else
              write(if,3)ia,hae(ia),fq,la(ia),zfa(ia),xa(ia),ya(ia),&
     &              nrefa(ia)
            endif
! fin du if sur nrefa=2
               end if
! fin du if sur nrefa=0
             end if
           enddo
       else !else de nouveau_format
        do ia = 1,na
          IF(NREFA(IA).GT.0)THEN
          IF(NREFA(IA).NE.2&
!cou1d     &.OR.COUPLAGE1D(IA) &
     &)THEN
            if (ieva(ia,1).eq.0) then
              fq=fha(ia)*la(ia)
            else
              fq=-fha(ia)*la(ia)
            endif
            if (fq .gt. 9999999.) then
              fq=9999999.
            elseif (fq.lt.-999999.) then
              fq=-999999.
            endif
!         write(if,1)ia,hae(ia),fq,la(ia),zfa(ia)+zfm,xa(ia),ya(ia),
            if (abs(fq).lt.1.) then
              write(if,2)ia,hae(ia),fq,la(ia),zfa(ia),xa(ia),ya(ia),&
     &              nrefa(ia)
            else
              write(if,1)ia,hae(ia),fq,la(ia),zfa(ia),xa(ia),ya(ia),&
     &              nrefa(ia)
            endif
!       elseif (nrefa(ia).lt.0) then
! debit sur ouvrage modifie en ecriture
!28         if (houv(ia,1) .gt. paray.or.abs(qouv(ia,1)) .gt. eps2) then
!28         if (abs(qouv(ia,1)) .gt. eps2) then
!          if (abs(qouv(ia,1)).ge.abs(qouv(ia,2))) then
!           if (qouv(ia,1) .gt. 9999999.) then
!             qouv(ia,1)=9999999.
!           elseif (qouv(ia,1).lt.-999999.) then
!             qouv(ia,1)=-999999.
!           endif
!           if (abs(qouv(ia,1)).lt.1.) then
!           write(if,2)ia,houv(ia,1),qouv(ia,1),la(ia),zfa(ia)
!     :              ,xa(ia),ya(ia),nrefa(ia)
!           else
!                write(if,1)ia,houv(ia,1),qouv(ia,1),la(ia),zfa(ia)
!     :              ,xa(ia),ya(ia),nrefa(ia)
!           endif
!28         endif
!28         if (houv(ia,2) .gt. paray.or.abs(qouv(ia,2)) .gt. eps2) then
!28         if (abs(qouv(ia,2)) .gt. eps2) then
!28         if (abs(qouv(ia,2)) .gt. abs(qouv(ia,1))) then
!         else
!           if (qouv(ia,2) .gt. 9999999.) then
!             qouv(ia,2)=9999999.
!           elseif (qouv(ia,2).lt.-999999.) then
!             qouv(ia,2)=-999999.
!           endif
!           if (abs(qouv(ia,2)).lt.1.) then
!           write(if,2)ia,houv(ia,2),qouv(ia,2),la(ia),zfa(ia)
!     :              ,xa(ia),ya(ia),nrefa(ia)
!           else
!           write(if,1)ia,houv(ia,2),qouv(ia,2),la(ia),zfa(ia)
!     :              ,xa(ia),ya(ia),nrefa(ia)
!           endif
!         endif
! fin du if sur nrefa=2
               end if
! fin du if sur nrefa=0
             end if
           enddo
       endif !endif de nouveau_format
! on ecrit le debit de l'ouvrage iouv sur arete ia1
! si plusieurs ouvrages sur la meme arete on ecrit pour chaque ouvrage
! la somme des debits de tous les ouvrages elementaires
       if(nouveau_format)then
          do iouv=1,nbouv
          if (nouv(iouv).ne.0) then
            if (j1(iouv).eq.1) then
             q=qouv(ia1(iouv),j1(iouv))
            else
              q=-qouv(ia1(iouv),j1(iouv))
            endif
           ia=ia1(iouv)
           if (q .gt. 9999999.) then
             q=9999999.
           elseif (q.lt.-999999.) then
             q=-999999.
           endif
           if (abs(q).lt.1.) then
             write(if,4)ia,houv(ia,j1(iouv)),q,la(ia),zfa(ia)&
     &              ,xa(ia),ya(ia),nrefa(ia)
           else
             write(if,3)ia,houv(ia,j1(iouv)),q,la(ia),zfa(ia)&
     &              ,xa(ia),ya(ia),nrefa(ia)
           endif
! fin if sur nouv
          endif
! fin boucle sur iouv
          enddo
       else ! else if sur nouveau_format
          do iouv=1,nbouv
          if (nouv(iouv).ne.0) then
            if (j1(iouv).eq.1) then
             q=qouv(ia1(iouv),j1(iouv))
            else
              q=-qouv(ia1(iouv),j1(iouv))
            endif
           ia=ia1(iouv)
           if (q .gt. 9999999.) then
             q=9999999.
           elseif (q.lt.-999999.) then
             q=-999999.
           endif
           if (abs(q).lt.1.) then
             write(if,2)ia,houv(ia,j1(iouv)),q,la(ia),zfa(ia)&
     &              ,xa(ia),ya(ia),nrefa(ia)
           else
             write(if,1)ia,houv(ia,j1(iouv)),q,la(ia),zfa(ia)&
     &              ,xa(ia),ya(ia),nrefa(ia)
           endif
! fin if sur nouv
          endif
! fin boucle sur iouv
          enddo
       endif ! fin if sur nouveau_format
   1  format(i6,6f12.4,i3)
   2  format(i6,f12.4,f12.8,4f12.4,i3)
   3  format(i9,6f12.4,i3)
   4  format(i9,f12.4,f12.8,4f12.4,i3)
!      close(if)
! fin du if sur ecrit
      endif
! fin du if sur debut (on n'ecrit pas la prmiere fois)
      endif
      end subroutine er
!*****************************************************************************
      subroutine etr(if,t)
!******************************************************************************
!     edition resultats en certains points
      use module_tableaux, only: het,quet,qvet,eps1,eps2,paray&
     &,nbpt,ienb
       use module_precision
!$    use omp_lib
      implicit none
!      implicit logical (a-z)
      integer if,i,nlimmax
      parameter (nlimmax=300)
      real(wp) t,h(nlimmax),v(nlimmax),u(nlimmax)

      !common/param/eps1,eps2,paray
      !common/tlig/het,quet,qvet
      !common/nomlim/nbpt,ienb

      if (if .gt. 0) then
!$omp parallel
!$omp do schedule(runtime)
      do i=1,nbpt
       if (het(ienb(i)) .gt. paray) then
         v(i)=qvet(ienb(i))/het(ienb(i))
         u(i)=quet(ienb(i))/het(ienb(i))
         h(i)=het(ienb(i))
       else
         v(i)=0.
         u(i)=0.
         h(i)=0.
       endif
      end do
!$omp end do
!$omp end parallel
      write (if,*) t
      write (if,'(5f15.4)')(v(i),i=1,nbpt)
      write (if,'(5f15.4)')(u(i),i=1,nbpt)
      write (if,'(5f15.4)')(h(i),i=1,nbpt)
      endif
      return
      end subroutine etr

! !******************************************************************************
!       subroutine flxga
! !      subroutine flxga(dt)
! !******************************************************************************
! !     calcul du flux godounov f(h,qu,qv) sur les aretes
! !      implicit none
! !      implicit logical (a-z)
!       integer nemax,naemax,nnemax,nevemx,namax,nnamax,nevamx,nnmax,
!      :        nalmax
!       parameter (nemax  = 300000,naemax = 4,nnemax = 4,nevemx = 4,
!      :           namax  = 900000,nnamax = 2,nevamx = 2,nnmax  = 360000,
!      :           nalmax = 1000)
!       integer na,ift,ia,ne,nn,ieg,ied
! !      integer na,ieva(namax,nevamx),ift,ia,nrefa(*)
!       real(wp) g,
!      :hg,qng,qtg,hd,qnd,qtd,eps1,eps2,paray
! !      real(wp) g,xna(*),yna(*),xta(*),yta(*),he(*),que(*),qve(*),
! !     :   hal(*),qnal(*),qtal(*),fha(*),fqua(*),fqva(*),zfa(*),zfe(*),
! !     :hg,qng,qtg,hd,qnd,qtd,ha,qna,qta,qua,qva,eps1,eps2,paray,hae(*)
!       real(wp) xna(namax),yna(namax),xta(namax),yta(namax)
!       real(wp) zfa(namax),zfe(nemax)
!       integer nrefa(namax),ieva(namax,nevamx)
!       real(wp) hal(namax),qnal(namax),qtal(namax)
!       real(wp) het(nemax),quet(nemax),qvet(nemax)
! !      real(wp) hae(namax),fha(namax),fqua(namax),fqva(namax)
!       common/param/eps1,eps2,paray
!       common/iftr/ift
!       common/grav/g
!       common/nomb/ne,na,nn
!       common/vare/xna,yna,xta,yta
!       common/zare/zfa
!       common/zele/zfe
!       common/narf/nrefa
!       common/clim/hal,qnal,qtal
!       common/tlig/het,quet,qvet
!       common/iela/ieva
! !      common/hali/hae
! !      common/flux/fha,fqua,fqva
! !     parcours des aretes
!       do 10 ia = 1,na
!           ieg = ieva(ia,1)
!           ied = ieva(ia,2)
! !     calcul de h,qn,qt a gauche, en coordonnees locales (n,t)
!       if (ieg.ne.0) then
! !         il y a un element a gauche, on projette ses valeurs de h,qu,qv
!           hg = het(ieg)
! !          hg = he(ieg) + zfe(ieg) - zfa(ia)
!           qng = quet(ieg) * xna(ia) + qvet(ieg) * yna(ia)
!           qtg = quet(ieg) * xta(ia) + qvet(ieg) * yta(ia)
!       else
! !         pas d'element a gauche donc valeurs cl
!           hg = hal(ia)
!           qng = qnal(ia)
!           qtg = qtal(ia)
!       endif
!
! !     calcul de h,qn,qt a droite, en coordonnees locales (n,t)
!       if (ied.ne.0) then
! !         il y a un element a droite, on projette ses valeurs de h,qu,qv
!           hd = het(ied)
!           qnd = quet(ied) * xna(ia) + qvet(ied) * yna(ia)
!           qtd = quet(ied) * xta(ia) + qvet(ied) * yta(ia)
!           if (ieg.ne.0) then
!           if (het(ieg).le.paray) then
!             if (het(ied).le.paray) then
!               hd=0.
!               hg=0.
!               qng=0.
!               qnd=0.
!               qtg=0.
!               qtd=0.
! !            elseif (hd .gt. hg) then
! !              hg=
! !              qng=0.
! !              qtg=0.
!             elseif (hg .gt. hd) then
!               hg=hd
!               qng=qnd
!               qtg=qtd
!             endif
!          elseif (het(ied).le.paray) then
!             if (hd .gt. hg) then
!               hd=hg
!               qnd=qng
!               qtd=qtg
! !            else
! !              hd=0.
! !              qnd=0.
! !              qtd=0.
!             endif
!             endif
! ! si ieg=0 pas d'element a gauche
! !         else
! !            if (he(ied).le.paray) then
! !              hg=0.
! !              hd=0.
! !              qng=0.
! !              qtg=0.
! !              qnd=0.
! !              qtd=0.
! !            endif
!          endif
!       else
! !         pas d'element a droite donc valeurs cl
! !          if (he(ieg).le.paray) then
! !             hg=0.
! !             hd=0.
! !             qng=0.
! !             qnd=0.
! !             qtg=0.
! !             qtd=0.
! !          else
!           hd = hal(ia)
!           qnd = qnal(ia)
!           qtd = qtal(ia)
! !          endif
!       endif
!
! !     resolution du probleme de riemann 1d en coordonnees locales (n,t).
! !     on obtient les valeurs de flux a l'interface (: sur l'arete)
! ! attention appel supprime car schema godunov debranche
! !      call resrmn(ieg,ied,ia,hg,qng,qtg,hd,qnd,qtd,dt)
! !     fin parcours des aretes
!   10  continue
!
!       return
!       end subroutine flxga

!******************************************************************************
      subroutine flvla1(dt)
!     calcul du flux van leer f(h,qu,qv) sur les aretes
!******************************************************************************
      use module_tableaux,only:xna,yna,xta,yta,xe,ye,xa,ya,zfa,zfe,&
     &nrefa,ieva,fra,fre,hal,qnal,qtal,het,quet,qvet,alpha,pxzfe,&
     &pyzfe,pxque,pyque,pxqve,pyqv,pxhe,pyhe,hae,fha,fqua,fqva,&
     &pxze,pyze,se,la,houv,qouv,xae,yae,hg1,qug1,qvg1,eau,&
     &eps1,eps2,paray,ne,na,nn,g
       use module_precision
       use module_mpi,only:naloc,aretes_loc
!$    use omp_lib

#if TRANSPORT_SOLIDE
      use module_ts,only:hcal,hcet,hcg1
#endif


      implicit none

      integer ieg,ied,ia,ie,je,ialoc
      real(wp) hg,qug,qvg,qng,qtg,hd,qud,qvd,qnd,qtd,dt
#if TRANSPORT_SOLIDE
      real(wp) :: hcd,hcg
#endif


!     :,delh,delu,delv,delx,dely
!      real(wp) smbu(nemax),smbv(nemax),smbu2(nemax),smbv2(nemax)
!      real(wp) smbu3(nemax),smbv3(nemax)
!     :,zeg,zed,hmin,hmax,heg,hed
!      integer ind
!      real(wp) delx2,dely2


      !common/param/eps1,eps2,paray
      !common/nomb/ne,na,nn
      !common/vare/xna,yna,xta,yta
      !common/cooe/xe,ye
      !common/cooa/xa,ya
      !common/zare/zfa
      !common/zele/zfe
      !common/grav/g
      !common/narf/nrefa
      !common/frot/fra,fre
      !common/clim/hal,qnal,qtal
      !common/tlig/het,quet,qvet
      !common/grad/alpha,pxzfe,pyzfe,pxque,pyque,pxqve,pyqve
      !common/iela/ieva
      !common/hali/hae
!      common/smbr/smbu,smbv
!      common/smbr2/smbu2,smbv2
!      common/smbr3/smbu3,smbv3
      !common/flux/fha,fqua,fqva
      !common/gradz/pxze,pyze
      !common/gradh/pxhe,pyhe
      !common/dose/se,la
      !common/hqouv/houv,qouv
      !common/xyaa/xae,yae
!      common/fina/final
      !common/hg12/hg1,qug1,qvg1
      !common/preso/eau


! en premier lieu, on calcule au centre h,qu,qv a t+1/2 sans frottements
! hcg1 est une concentration
      call calgrd


! en cas d'ouvrage final signifie qu'on est au pas de temps complet
!      final=.true.
!$omp parallel private(je,hg,qug,qvg,qng,qtg,hd,
!$omp1 qud,qvd,qnd,qtd,ieg,ied,ie)
!$omp do schedule(runtime)
      qud=0.
      qvd=0.
      qug=0.
      qvg=0.
      hd=0.
      hg=0.
      do ialoc=1,naloc
       ia=aretes_loc(ialoc)
! calcul du terme de frottement
       do je=1,2
        ie=ieva(ia,je)
        if (ie.ne.0) then
          if (eau(ie)) then
            hd=hg1(ia,je)
         if (hd .gt. paray) then
            qud=qug1(ia,je)
            qvd=qvg1(ia,je)

#if TRANSPORT_SOLIDE
            hcd=hcg1(ia,je)
#endif


          if (abs(qud/hd).lt.eps2) then
            qud=0.
          endif
          if (abs(qvd/hd).lt.eps2) then
            qvd=0.
          endif


#if TRANSPORT_SOLIDE
          if(hcd.lt.eps2)hcd=0.
#endif

! hd<paray
         else
          hd=0.
          qud=0.
          qvd=0.
#if TRANSPORT_SOLIDE
          hcd=0.
#endif
         endif
! si pas d'eau
         else
          hd=0.
          qud=0.
          qvd=0.
#if TRANSPORT_SOLIDE
          hcd=0.
#endif
! fin du if sur he=0
       endif
       if (je.eq.1) then
         hg=hd
         qug=qud
         qvg=qvd

#if TRANSPORT_SOLIDE
          hcg=hcd
#endif
       endif
! fin du if sur ie=0
       endif
        end do

        ieg = ieva(ia,1)
        ied = ieva(ia,2)

!     calcul de h,qn,qt a gauche de l'interface
      if (ieg.ne.0) then
        if (eau(ieg)) then
!         on en deduit qn et qt
          qng = qug * xna(ia) + qvg * yna(ia)
          qtg = qug * xta(ia) + qvg * yta(ia)
        else
          qng=0.
          qtg=0.
        endif
      else
!         pas d'element a gauche donc valeurs cl
          hg = hal(ia)
          qng = qnal(ia)
          qtg = qtal(ia)
#if TRANSPORT_SOLIDE
          hcg=hcal(ia)
          if(hcg.lt.0.)hcg=hcd
#endif
      endif

!     calcul de h,qn,qt a droite de l'interface
      if (ied.ne.0) then
!         on en deduit qn et qt
         if (eau(ied)) then
          qnd = qud * xna(ia) + qvd * yna(ia)
          qtd = qud * xta(ia) + qvd * yta(ia)
        else
          qnd=0.
          qtd=0.
         endif
          if (ieg.ne.0) then
          if (.not.eau(ieg)) then
           if (.not.eau(ied)) then
! les 8 lignes suivantes sont inutiles
              hd=0.
              hg=hd
              qng=0.
              qnd=0.
              qtg=0.
              qtd=0.
#if TRANSPORT_SOLIDE
              hcd=0.
              hcg=0.
#endif
! si de l'eau seulement a droite
           else
!              qng=0.
!              qtg=0.
              if (nrefa(ia).ne.-2) then
              if (qnd .gt. 0.)qnd=0.
             if (zfe(ieg) .gt. het(ied)+zfe(ied)) then
!              if (zfe(ieg) .gt. hd+zfa(ia)&
!     &         .or.zfe(ieg) .gt. het(ied)+zfe(ied)) then
! correction car dans ce cas pas de raison de pente normale a larete
! permet d'annuler le terme pente + convection
                 hd=het(ied)+zfe(ied)-zfa(ia)
                 hg=hd
                qnd=0.
                qtd=0.
#if TRANSPORT_SOLIDE
                hcg=hcd
#endif
              else
                hg=min(zfe(ieg)-zfa(ia),hd)
#if TRANSPORT_SOLIDE
                hcg=hcd
#endif
              endif
! fin du if sur nrefa
             endif
! fin du if sur eau a droite
           endif
          elseif (.not.eau(ied)) then
! de l'eau seulement a gauche
!              qnd=0.
!              qtd=0.
              if (nrefa(ia).ne.-2) then
              if (qng.lt.0.)qng=0.
              if (zfe(ied) .gt. het(ieg)+zfe(ieg)) then
!              if (zfe(ied) .gt. hg+zfa(ia)
!     :         .or.zfe(ied) .gt. het(ieg)+zfe(ieg)) then
! correction car dans ce cas pas de raison de pente normale a larete
! permet d'annuler le terme pente + convection
                 hg=het(ieg)+zfe(ieg)-zfa(ia)
                hd=hg
                qng=0.
                qtg=0.
#if TRANSPORT_SOLIDE
                hcd=hcg
#endif
              else
                hd=min(zfe(ied)-zfa(ia),hg)
#if TRANSPORT_SOLIDE
                hcd=hcg
#endif
              endif
! fin du if sur nrefa
             endif
           endif
! si ieg=0
         else
             if (.not.eau(ied)) then
                   hd=zfe(ied)-zfa(ia)
                   qnd=0.
                   qtd=0.
#if TRANSPORT_SOLIDE
                  hcd=hcg
#endif
             endif
         endif
! ied=0
      else
          hd = hal(ia)
          qnd = qnal(ia)
          qtd = qtal(ia)
#if TRANSPORT_SOLIDE
          hcd=hcal(ia)
          if(hcd.lt.0.)hcd=hcg
#endif

                if (.not.eau(ieg)) then
                   hg=zfe(ieg)-zfa(ia)
                   qng=0.
                   qtg=0.
#if TRANSPORT_SOLIDE
                    hcg=hcd
#endif
                endif
         endif

!     resolution du probleme de riemann 1d en coordonnees locales (n,t).
!     on obtient les valeurs de flux a l'interface (: sur l'arete)
        hg1(ia,1)=hg
        hg1(ia,2)=hd
! attention : changement de contenu pour qug1 et qvg1
        qug1(ia,1)=qng
        qug1(ia,2)=qnd
        qvg1(ia,1)=qtg
        qvg1(ia,2)=qtd
#if TRANSPORT_SOLIDE
        hcg1(ia,1)=hcg
        hcg1(ia,2)=hcd
#endif

!     fin parcours des aretes
       end do
!$omp end do
!$omp end parallel
      return
      end subroutine flvla1

!******************************************************************************
      subroutine flvla2(dt,t,iapp)
!     calcul du flux van leer f(h,qu,qv) sur les aretes
!******************************************************************************

      use module_tableaux,only:xna,yna,xta,yta,xe,ye,xa,ya,zfa,zfe&
     &,nrefa,ieva,fra,fre,hal,qnal,qtal,het,quet,qvet,alpha,pxzfe&
     &,pyzfe,pxque,pyque,pxqve,pyqve,hae,smbu,smbv,smbu2,smbv2&
     &,smbu3,smbv3,fha,fqua,fqva,pxze,pyze,pxhe,pyhe,se,la,&
     &houv,qouv,xae,yae,fluu,flvv,eau,smbh,hg1,qug1,qvg1,&
     &eps1,eps2,paray,ne,na,nn,g,fro,fvix,fviy
      use module_precision

      use module_loc, only:dh,dqu,dqv

      use module_mpi,only:naloc,aretes_loc,ne_loc,me,mailles_loc,nmv,elt_voisins

#if TRANSPORT_SOLIDE
      use module_ts,only:&
!cou1d     &ccouplts,dcouplts,scouplts,&
     &hcal,pxhce,pyhce,hcg1,smbhc,smbhc2
#endif
!$    use omp_lib

      implicit none

      integer ieg,ied,ia,ie,je,ialoc,i,ieloc
      real(wp) qng,qtg,hd,qud,qvd,qnd,qtd,&
!     &,hg,qug,qvg&
     &dt,smu&
!     &,delh,delu,delv,delx,dely&
     &,fra2,q2
      !real(wp),dimension(:),allocatable::dh,dqu,dqv
!     :,zeg,zed,hmin,hmax,hed,heg
!      integer ind
      real(wp) t
!     :,delx2,dely2
      integer iapp

#if TRANSPORT_SOLIDE
      real(wp) hcd
#endif
!cou1d      integer nmaxcou,ncouplage
!cou1d       parameter(nmaxcou=10000)
!cou1d      integer i,i2d(nmaxcou),i1d(nmaxcou)
!cou1d      logical gcouplage(namax)
!cou1d      real(wp) hcouplage(namax)
!cou1d     :,vxcouplage(namax),vycouplage(namax)



! en cas d'ouvrage final signifie qu'on est au pas de temps complet
!      final=.false.
!      coef=0.01
! en premier lieu, on calcule au centre h,qu,qv a t+1/2 sans frottements
! hcg1 est une concentration
      call calgrd


! calcul du terme de vent et de laplacien
!      if (abs(fvix) .gt. eps2.and.abs(fviy) .gt. eps2.or.abs(cvi) .gt. eps2)
!     : then
      call secmb5(dt)
!      endif

!  on calcule la contribution du second membre frottement de paroi a tn
!********************************************
!       if (fro .gt. eps2) then
           call secmb3(dt)
!       endif

!  on calcule la contribution du second membre apports a tn
!********************************************
! appel a secmb6 en debut de resrmn pour le faire une seule fois
!       if (iapp .gt. 0) then
!         call secmb6(t-dt)
!         do ie=1,ne
!           het(ie)=het(ie)+0.5*dt*smbh(ie)
!           if (het(ie).le.paray) then
!             het(ie)=0.
!             eau(ie)=.false.
!           endif
!         enddo
!       endif
!$omp parallel private(qng,qtg,hd,qud,qvd,qnd,qtd,smu,fra2,q2,
!$omp1 ieg,ied,ia,ie,je)
! en second calcul de ce terme f prime
      !print*,' ne = ', ne

!$omp do schedule(runtime)
      do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
         if (eau(ie)) then
         !if (.not.eau(ie)) cycle
         dh(ie)=0.5*dt*(pxque(ie)+pyqve(ie))
! pxze est la pente sur la cote de la surface libre
! pxzfe est la pente de la cote du fond
         dqu(ie)=0.5*dt*(2.*quet(ie)*pxque(ie)/het(ie)-quet(ie)**2/&
     &           het(ie)**2*(pxze(ie)-pxzfe(ie))+&
     &           quet(ie)/het(ie)*pyqve(ie)+qvet(ie)/het(ie)*pyque(ie)&
     &           -quet(ie)*qvet(ie)/het(ie)**2*&
     &           (pyze(ie)-pyzfe(ie))+g*het(ie)*pxze(ie))
         dqv(ie)=0.5*dt*(2.*qvet(ie)*pyqve(ie)/het(ie)-qvet(ie)**2/&
     &           het(ie)**2*(pyze(ie)-pyzfe(ie))+&
     &           qvet(ie)/het(ie)*pxque(ie)+quet(ie)/het(ie)*pxqve(ie)&
     &           -qvet(ie)*quet(ie)/het(ie)**2*&
     &           (pxze(ie)-pxzfe(ie))+g*het(ie)*pyze(ie))
         endif
      enddo
!$omp end do
!$omp do schedule(runtime)
      do i=1,nmv
        ie=elt_voisins(i)
         if (eau(ie)) then
         !if (.not.eau(ie)) cycle
         dh(ie)=0.5*dt*(pxque(ie)+pyqve(ie))
! pxze est la pente sur la cote de la surface libre
! pxzfe est la pente de la cote du fond
         dqu(ie)=0.5*dt*(2.*quet(ie)*pxque(ie)/het(ie)-quet(ie)**2/&
     &           het(ie)**2*(pxze(ie)-pxzfe(ie))+&
     &           quet(ie)/het(ie)*pyqve(ie)+qvet(ie)/het(ie)*pyque(ie)&
     &           -quet(ie)*qvet(ie)/het(ie)**2*&
     &           (pyze(ie)-pyzfe(ie))+g*het(ie)*pxze(ie))
         dqv(ie)=0.5*dt*(2.*qvet(ie)*pyqve(ie)/het(ie)-qvet(ie)**2/&
     &           het(ie)**2*(pyze(ie)-pyzfe(ie))+&
     &           qvet(ie)/het(ie)*pxque(ie)+quet(ie)/het(ie)*pxqve(ie)&
     &           -qvet(ie)*quet(ie)/het(ie)**2*&
     &           (pxze(ie)-pxzfe(ie))+g*het(ie)*pyze(ie))
         endif
      enddo
!$omp end do
      if (iapp .gt. 0) then
!$omp do schedule(runtime)
        do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
          dh(ie)=dh(ie)-0.5*dt*smbh(ie)
        enddo
!$omp end do
!$omp do schedule(runtime)
        do i=1,nmv
          ie=elt_voisins(i)
          dh(ie)=dh(ie)-0.5*dt*smbh(ie)
        enddo
!$omp end do
      endif
! en cas d'ouvrage final signifie qu'on est au pas de temps complet
!      final=.true.
!$omp do schedule(runtime)
      do ialoc=1,naloc
        ia=aretes_loc(ialoc)
! calcul du terme de frottement
       do je=1,2
        ie=ieva(ia,je)
        if (ie.ne.0) then
          if (eau(ie)) then
            hd=hg1(ia,je)-dh(ie)
! si les debits a tn et tn+1/2 sont de sens oppose on l'annule
         if (hd .gt. paray) then
            qud=qug1(ia,je)-dqu(ie)+0.5*smbu2(ie)
            qvd=qvg1(ia,je)-dqv(ie)+0.5*smbv2(ie)
            if (nrefa(ia).eq.2.or.nrefa(ia).eq.-2) then
              qud=qud/(1.-0.5*smbu3(ie))
              qvd=qvd/(1.-0.5*smbv3(ie))
            endif
#if TRANSPORT_SOLIDE
            hcd=hcg1(ia,je)
#endif
!          if (darcy) then
!           q=sqrt(qud*qud+qvd*qvd)
!           fm1=4.*(log(kse(ie)/(12.*hd)+6.7903/(4.*q/nu)**0.9)**2.)
!           fra2=max(fra(ie),8.*g*fm1*cofr(ie))
!           q2=q/fra2/hd**2
!          else
            fra2=max(fra(ie),fre(ie)*hd**0.3333333)
            q2=sqrt(qud*qud+qvd*qvd)/fra2/hd**2
!          endif
            q2=-0.5*dt*g*q2
            if (abs(q2).lt.eps2) then
              smu=1.+q2
            else
              smu=0.5*(1.-(1.-4.*q2)**0.5)/q2
            endif
            qud=qud*smu
            qvd=qvd*smu
            if (abs(qud/hd).lt.eps2) then
              qud=0.
            endif
            if (abs(qvd/hd).lt.eps2) then
              qvd=0.
            endif
!            if (qud*qug1(ia,je).lt.0.)qud=qug1(ia,je)
!            if (qvd*qvg1(ia,je).lt.0.)qvd=qvg1(ia,je)
            if (qud*qug1(ia,je).lt.0.)qud=0.
            if (qvd*qvg1(ia,je).lt.0.)qvd=0.
#if TRANSPORT_SOLIDE
            if(hcd.lt.eps2)hcd=0.
#endif
!            if (hd.lt.eps1) then
!              qud=0.
!              qvd=0.
!            endif
! hd<paray
          else
            hd=0.
            qud=0.
            qvd=0.
#if TRANSPORT_SOLIDE
            hcd=0.
#endif
          endif
! si pas eau
          else
            hd=0.
            qud=0.
            qvd=0.
#if TRANSPORT_SOLIDE
            hcd=0.
#endif
! fin du if sur he=0
       endif
          if (je.eq.1) then
            hg1(ia,1)=hd
            qug1(ia,1)=qud
            qvg1(ia,1)=qvd
#if TRANSPORT_SOLIDE
            hcg1(ia,1)=hcd
#endif
          else
            hg1(ia,2)=hd
            qug1(ia,2)=qud
            qvg1(ia,2)=qvd
#if TRANSPORT_SOLIDE
            hcg1(ia,2)=hcd
#endif
          endif
! fin du if sur ie=0
       endif
        end do
! fin de la boucle des aretes
       end do
!$omp end do
!$omp end parallel
! recalcul des conditions aux limites
             call lecl2(t-0.5*dt)
! parcours des aretes
!$omp parallel private(qng,qtg,hd,qud,qvd,qnd,qtd,smu,fra2,q2,
!$omp1 ieg,ied,ia,ie,je)
!$omp do schedule(runtime)
      do ialoc = 1,naloc
          ia=aretes_loc(ialoc)
          ieg = ieva(ia,1)
          ied = ieva(ia,2)

!     calcul de h,qn,qt a gauche de l'interface
      if (ieg.ne.0) then
        if (eau(ieg)) then
!         on en deduit qn et qt
          qng = qug1(ia,1) * xna(ia) + qvg1(ia,1) * yna(ia)
          qtg = qug1(ia,1) * xta(ia) + qvg1(ia,1) * yta(ia)
        else
          qng=0.
          qtg=0.
        endif
      else
!         pas d'element a gauche donc valeurs cl
          hg1(ia,1) = hal(ia)
          qng = qnal(ia)
          qtg = qtal(ia)
#if TRANSPORT_SOLIDE
          hcg1(ia,1)=hcal(ia)
          if(hcg1(ia,1).lt.0.)hcg1(ia,1)=hcg1(ia,2)
#endif
          if (.not.eau(ied)) then
             hg1(ia,2)=zfe(ied)-zfa(ia)
             qnd=0.
             qtd=0.
#if TRANSPORT_SOLIDE
            hcg1(ia,2)=hcg1(ia,1)
#endif
          endif
      endif

!     calcul de h,qn,qt a droite de l'interface
      if (ied.ne.0) then
!         on en deduit qn et qt
         if (eau(ied)) then
          qnd = qug1(ia,2) * xna(ia) + qvg1(ia,2) * yna(ia)
          qtd = qug1(ia,2) * xta(ia) + qvg1(ia,2) * yta(ia)
         else
          qnd=0.
          qtd=0.
         endif
      else
          hg1(ia,2) = hal(ia)
          qnd = qnal(ia)
          qtd = qtal(ia)
#if TRANSPORT_SOLIDE
          hcg1(ia,2)=hcal(ia)
          if(hcg1(ia,2).lt.0.)hcg1(ia,2)=hcg1(ia,1)
#endif
          if (.not.eau(ieg)) then
             hg1(ia,1)=zfe(ieg)-zfa(ia)
             qng=0.
             qtg=0.
#if TRANSPORT_SOLIDE
            hcg1(ia,1)=hcg1(ia,2)
#endif
          endif
      endif
      if (ieg.ne.0.and.ied.ne.0) then
          if (.not.eau(ieg)) then
           if (.not.eau(ied)) then
! les 8 lignes suivantes sont inutiles
               hg1(ia,2)=0.
              hg1(ia,1)=hg1(ia,2)
#if TRANSPORT_SOLIDE
              hcg1(ia,1)=0.
              hcg1(ia,2)=0.
#endif
              qng=0.
              qnd=0.
              qtg=0.
              qtd=0.
! cas ou hed non nul et heg nul
           else
!              qng=0.
!              qtg=0.
              if (nrefa(ia).ne.-2) then
              if (qnd .gt. 0.)qnd=0.
              if (zfe(ieg) .gt. het(ied)+zfe(ied)) then
!              if (zfe(ieg) .gt. hg1(ia,2)+zfa(ia)
!     :         .or.zfe(ieg) .gt. het(ied)+zfe(ied)) then
! correction car dans ce cas pas de raison de pente normale a larete
! permet d'annuler le terme pente + convection
                 hg1(ia,2)=het(ied)+zfe(ied)-zfa(ia)
                 hg1(ia,1)=hg1(ia,2)
                qnd=0.
                qtd=0.
#if TRANSPORT_SOLIDE
                hcg1(ia,1)=hcg1(ia,2)
#endif
              else
                hg1(ia,1)=min(zfe(ieg)-zfa(ia),hg1(ia,2))
#if TRANSPORT_SOLIDE
                hcg1(ia,1)=hcg1(ia,2)
#endif
              endif
! fin du if sur nrefa
             endif
           endif
! cas ou heg non nul et hed nul
          elseif (.not.eau(ied)) then
!              qnd=0.
!              qtd=0.
              if (nrefa(ia).ne.-2) then
              if (qng.lt.0.)qng=0.
#if TRANSPORT_SOLIDE
                hcg1(ia,2)=hcg1(ia,1)
#endif
              if (zfe(ied) .gt. het(ieg)+zfe(ieg)) then
!              if (zfe(ied) .gt. hg1(ia,1)+zfa(ia)
!     :         .or.zfe(ied) .gt. het(ieg)+zfe(ieg)) then
! correction car dans ce cas pas de raison de pente normale a larete
! permet d'annuler le terme pente + convection
                 hg1(ia,1)=het(ieg)+zfe(ieg)-zfa(ia)
                hg1(ia,2)=hg1(ia,1)
                qng=0.
                qtg=0.
              else
                hg1(ia,2)=min(zfe(ied)-zfa(ia),hg1(ia,1))
              endif
! fin du if sur nrefa
             endif
          endif
! fin du if sur ieg et ied nuls
        endif

!        hg1(ia,1)=hg
!        hg1(ia,2)=hd
! attention : chnagement de contenu pour qug1 et qvg1
        qug1(ia,1)=qng
        qug1(ia,2)=qnd
        qvg1(ia,1)=qtg
        qvg1(ia,2)=qtd

!     fin parcours des aretes
       end do
!$omp end do
!$omp end parallel
!cou1d      do i=1,ncouplage
!cou1d        ia=i2d(i)
!cou1d        if (nrefa(ia).ne.-1) then
!cou1d          if (gcouplage(ia)) then
!cou1d            hg1(ia,2)=hcouplage(ia)
!cou1d            qnd=vxcouplage(ia)*xna(ia)+vycouplage(ia)*yna(ia)
!cou1d            qug1(ia,2)=qnd*hcouplage(ia)
!cou1d            qtd=vxcouplage(ia)*xta(ia)+vycouplage(ia)*yta(ia)
!cou1d            qvg1(ia,2)=qtd*hcouplage(ia)
!cou1d!conc
#if TRANSPORT_SOLIDE
!cou1d            hcg1(ia,2)=ccouplts(ia)
#endif
!cou1d!concf
!cou1d          else
!cou1d            hg1(ia,1)=hcouplage(ia)
!cou1d            qng=vxcouplage(ia)*xna(ia)+vycouplage(ia)*yna(ia)
!cou1d            qug1(ia,1)=qng*hcouplage(ia)
!cou1d            qtg=vxcouplage(ia)*xta(ia)+vycouplage(ia)*yta(ia)
!cou1d            qvg1(ia,1)=qtg*hcouplage(ia)
!cou1d!conc
#if TRANSPORT_SOLIDE
!cou1d            hcg1(ia,1)=ccouplts(ia)
#endif
!cou1d!concf
!cou1d          endif
! fin du if sur nrefa
! dans le cas de nrefa=-1, on met les valeurs dans houv dans resrmn
!cou1d        endif
! fin boucle sur i
!cou1d      enddo
!      call caltar
!      do ia=1,na
!        ieg = ieva(ia,1)
!        ied = ieva(ia,2)
!      call resrmn(ieg,ied,ia,hg1(ia,1),
!     :  qug1(ia,1),qvg1(ia,1),hg1(ia,2)
!     :,qug1(ia,2),qvg1(ia,2),dt

!     :)
!    enddo
      !deallocate(dh,dqu,dqv)
      return
      end subroutine flvla2

!******************************************************************************
       subroutine grdlp
!******************************************************************************
!     calcul des gradients (pentes) selon ox,oy de h,qu,qv
!     sur les elements, avec limitation des pentes.

!     nota : la liste d'arguments a ete volontairement choisie redondante,
!     pour permettre de modifier aisement cette routine.

!     version actuelle : on calcule les pentes px,py de la fonction h (par
!     exemple) qui minimisent la somme sur les 8 elements voisins de l'elt o
!     de (ho + grad(h) . bobi - hi)2   avec i:1,4  b : barycentre de l'elt.
!     attention: ne convient qu'aux quadrangles
!     on limite ensuite les pentes par une methode tgallouet.
!******************************************************************************

      use module_tableaux, only:nne,xe,ye,xa,ya,se,la,nrefa,ieva,&
     &zfa,zfe,iae,het,quet,qvet,alpha,pxzfe,pyzfe,pxque,pyque,pxqve,&
     &pyqve,px2que,py2que,px2qve,py2qve,ieve,pxze,pyze,pxhe,pyhe&
     &,xna,yna,xta,yta,hal,qnal,qtal,xae,yae,pxqu,pxqv,pyqu,pyqv,&
     &fluu,flvv,eau,nnemax,nevemx,eps1,eps2,paray,ne,na,nn,&
     &lcvi,lcviv,lcvif,debut,nevemx!,tcomm1,tcomm2,tcomm
      use module_precision
      use module_mpi,only:ne_loc,me,mailles_loc
      use module_loc, only:xaa,yaa,hezer,a110,a220,a120,deta0,sse
      use module_laplacien
#ifdef WITH_MPI
      use module_messages
      use mpi
#endif /* WITH_MPI */

#if TRANSPORT_SOLIDE
      use module_ts,only:cet,pxhce,pyhce,hcal,pxhc,pyhc,flhc,diffus,sigm&
      &,dix,diy,modifz,chzn
#endif

!$    use omp_lib

      implicit none

      integer ie,j,ia,iev,ieloc
      real(wp) b1h,b2h,b1qu,b2qu,b1qv,b2qv,qua,qva,gradqu,&
     &     hev,quev,qvev,gradqv,&
     &     qumin,qumax,qvmin,qvmax,&
!     &     cb1qu,cb2qu,cb1qv,cb2qv,&
     &     b1z,b2z,gradz,zmin,zmax,za
      !real(wp),save,dimension(:,:),allocatable::xaa,yaa
      !real(wp),save,dimension(:),allocatable::a110,a220,a120,deta0
      real(wp) xaaa,yaaa,sgn
!      real(wp) px2que(nemax),py2que(nemax),px2qve(nemax)
!     :,py2qve(nemax)
      real(wp) ze
!      real(wp) fro,fvix,fviy
!     : ,fxuu(nemax),
!     :fyuu(nemax),fxvv(nemax),fyvv(nemax),pxuu(nemax),pyuu(nemax)
!     :,pxvv(nemax),pyvv(nemax),ue(nemax),ve(nemax)
!     :,ncvi(namax),g
      real(wp) gradzx,gradzy,gradqux,gradquy,gradqvx,gradqvy&
     &,alfazx(nnemax),alfazy(nnemax)&
     &,alfaqux(nnemax),alfaquy(nnemax)&
     &,alfaqvx(nnemax),alfaqvy(nnemax),alf,alfx,alfy&
     &,ha,zev,alfahx(nnemax),alfahy(nnemax)&
     &,gradhx,gradhy,gradh,hmin,hmax
      !real(wp),dimension(:),allocatable::sse

!      character*8 schema
!      logical debut,calcul,eau(nemax)
      logical hezer2
      !logical,dimension(:),allocatable::hezer

#if TRANSPORT_SOLIDE
      real(wp) :: fxhc(na),fyhc(na),ce(ne),alfahcx(nnemax),alfahcy(nnemax)
      real(wp) :: gradhc,b1hc,b2hc,hca,hcev,hcmin,hcmax,cb1hc,cb2hc,gradhcx,gradhcy
#endif

      external sgn

      !common/nvois/nne
      !common/param/eps1,eps2,paray
!      common/const/fro,fvix,fviy
!      common/iftr/ift
      !common/nomb/ne,na,nn
      !common/cooe/xe,ye
      !common/cooa/xa,ya
      !common/zare/zfa
      !common/zele/zfe
      !common/nrel/iae
      !common/tlig/het,quet,qvet
      !common/grad/alpha,pxzfe,pyzfe,pxque,pyque,pxqve,pyqve
!      common/lapl/px2que,py2que,px2qve,py2qve
      !common/iele/ieve
      !common/gradz/pxze,pyze
      !common/gradh/pxhe,pyhe
      !common/vare/xna,yna,xta,yta
      !common/narf/nrefa
      !common/clim/hal,qnal,qtal
      !common/xyaa/xae,yae
      !common/pxyuv/pxqu,pxqv,pyqu,pyqv
      !common/iela/ieva
      !common/fluuvv/fluu,flvv
      !common/preso/eau
!      common/nuqe/ncvi
!      common/grav/g
!      common/lcvia/cvia
      !common/cvi/lcvi,lcviv,lcvif
      !common/dose/se,la
      !common/debcal/debut


      !save xaa,yaa,xbb,ybb,a11,a12,a22,deta
       !:,hezer,xybb,sse,a110,a120,a220,deta0
      !allocate(xbb(ne,nevemx),ybb(ne,nevemx),xybb(ne,nevemx))
      !allocate(xaa(ne,nevemx),yaa(ne,nevemx))
      !allocate(a11(ne),a22(ne),a12(ne),deta(ne))
      !allocate(a110(ne),a220(ne),a120(ne),deta0(ne))
      !allocate(sse(ne))
      !allocate(hezer(ne))
!     calcul des coefficients invariants (remanents)
!      save calcul,xaa,yaa,xbb,ybb,a11,a12,a22,deta
!      data calcul /.true./


! on ne calcule rien si le schema est godounov et pas de laplacien
!      if (abs(cvi).lt.eps2.and.schema.eq.'godounov') then
!        calcul=.false.
!      endif

!$omp parallel private(b1h,b2h,b1qu,b2qu,b1qv,b2qv,qua,qva,gradqu,hev
!$omp1 ,quev,qvev,gradqv,qumin,qumax,qvmin,qvmax,xaaa,yaaa,gradzx
!$omp2 b1z,b2z,gradz,zmin,zmax,za
!$omp3 ,gradzy,gradqux,gradquy,gradqvx,gradqvy,alfazx,alfazy,alfaqux
!$omp4 ,alfaquy,alfaqvx,alfaqvy,alf,alfx,alfy,ha,zev,alfahx,alfahy
!$omp5 ,gradhx,gradhy,gradh,hmin,hmax,ie,j,ia,iev,hezer2)

      if (debut) then
          fluu=0.
          flvv=0.
#if TRANSPORT_SOLIDE
          flhc=0.
#endif
!$omp do schedule(runtime)
           do ie = 1,ne
             do j = 1,nne(ie)
               ia=iae(ie,j)
               xaaa =xa(ia) - xe(ie)
               if (abs(xaaa).lt.eps2) then
                 xaa(ie,j)=0.
               else
                 xaa(ie,j)=xaaa
               endif
               yaaa =ya(ia) - ye(ie)
               if (abs(yaaa).lt.eps2) then
                 yaa(ie,j)=0.
               else
                 yaa(ie,j)=yaaa
               endif
               if (ie.eq.ieva(ia,1)) then
                 xae(ia,1)=xaa(ie,j)
                 yae(ia,1)=yaa(ie,j)
               else
                 xae(ia,2)=xaa(ie,j)
                 yae(ia,2)=yaa(ie,j)
               endif
               end do
            end do
!$omp end do
!         calcul des coordonnees des vecteurs bobi
!$omp do schedule(runtime)
          do ie = 1,ne
             sse(ie)=sqrt(se(ie))
! le facteur 0.1 est arbitraire
             sse(ie)=0.1*sse(ie)
             do j = 1,nne(ie)
                iev = ieve(ie,j)
                ia=iae(ie,j)
                if (iev.ne.0.and.nrefa(ia).ne.-2) then
                    xbb(ie,j) = xe(iev) - xe(ie)
! si les centres sont peu decales sur un axe, le voisin ne determinera pas le gradient
! ceci doit eviter des discontinuites de pente dans le temps
                    if (abs(xbb(ie,j)).lt.sse(ie))xbb(ie,j)=0.
                    ybb(ie,j) = ye(iev) - ye(ie)
                    if (abs(ybb(ie,j)).lt.sse(ie))ybb(ie,j)=0.
                    xybb(ie,j)=sqrt(xbb(ie,j)**2+ybb(ie,j)**2)
                else
                     xbb(ie,j)=0.
                     ybb(ie,j)=0.
                     xybb(ie,j)=0.
                endif
             end do
          end do
!$omp end do

!         calcul des aij0
!$omp do schedule(runtime)
          do ie = 1,ne
! ici on fait le calcul de base pour la pente du fond
             a110(ie) = 0.
             a220(ie) = 0.
             a120(ie) = 0.
             do j = 1,nne(ie)
                a110(ie) = a110(ie) + xaa(ie,j)**2
                a220(ie) = a220(ie) + yaa(ie,j)**2
                a120(ie) = a120(ie) + xaa(ie,j)*yaa(ie,j)
             end do
             deta0(ie) = a110(ie)*a220(ie) - a120(ie)**2
             if (abs(deta0(ie)).lt.eps2) then
               if (abs(a110(ie)).lt.eps2.and.abs(a220(ie)).lt.eps2) then
                 deta0(ie)=1.
               elseif (abs(a110(ie)).lt.eps2) then
                 a110(ie)=1.
                 deta0(ie)=a220(ie)
               elseif (abs(a220(ie)).lt.eps2) then
                 a220(ie)=1.
                 deta0(ie)=a110(ie)
               else
                 deta0(ie)=a120(ie)**2
                 a120(ie)=0.
               endif
             endif
          end do
!$omp end do
!     calcul des pentes au fond
!$omp do schedule(runtime)
        do ie = 1,ne
           b1h = 0.0
           b2h = 0.0

           do j = 1,nne(ie)
            !iev = ieve(ie,j)  !  ligne inutile
            ia=iae(ie,j)
            b1h =b1h-xaa(ie,j)*(zfe(ie)-zfa(ia))
            b2h = b2h - yaa(ie,j)*(zfe(ie)-zfa(ia))
           end do
!
           pxzfe(ie) = (a220(ie) * b1h - a120(ie) * b2h) / deta0(ie)
           pyzfe(ie) = (a110(ie) * b2h - a120(ie) * b1h) / deta0(ie)
           if (abs(pxzfe(ie)).lt.eps2) pxzfe(ie)=0.0
           if (abs(pyzfe(ie)).lt.eps2) pyzfe(ie)=0.0
         end do
!$omp end do
!         calcul des aij
!$omp do schedule(runtime)
         do ie = 1,ne
! hezer vrai signifie une deconnexion avec un voisin
! ici on fait le calcul de base
             hezer(ie)=.false.
             a11(ie) = 0.
             a22(ie) = 0.
             a12(ie) = 0.
             do j = 1,nne(ie)
                a11(ie) = a11(ie) + xbb(ie,j)**2
                a22(ie) = a22(ie) + ybb(ie,j)**2
                a12(ie) = a12(ie) + xbb(ie,j)*ybb(ie,j)
             end do
             deta(ie) = a11(ie)*a22(ie) - a12(ie)**2
             if (abs(deta(ie)).lt.eps2) then
               if (abs(a11(ie)).lt.eps2.and.abs(a22(ie)).lt.eps2) then
                 deta(ie)=1.
               elseif (abs(a11(ie)).lt.eps2) then
                 a11(ie)=1.
                 deta(ie)=a22(ie)
               elseif (abs(a22(ie)).lt.eps2) then
                 a22(ie)=1.
                 deta(ie)=a11(ie)
               else
                 deta(ie)=a12(ie)**2
                 a12(ie)=0.
               endif
             endif
         end do
!$omp end do

          debut = .false.

!fin du if sur debut
      end if


! calcul si necessaire
!      if (calcul) then
!conc
!          do ie=1,ne
!             if (eau(ie)) then
!               hce(ie)=hce(ie)/he(ie)
!             else
!               hce(ie)=0.
!             endif
!          enddo
!oncf

!     calcul des pentes
!$omp do schedule(runtime)
      do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)


          if (eau(ie)) then
! calcul des gradients si he non nul
! recalcul eventuel des a11,a12,etc si une deconnexion
!!$omp ordered
             ze=het(ie)+zfe(ie)
             hezer2=.false.
             do j = 1,nne(ie)
              iev = ieve(ie,j)
              ia=iae(ie,j)
              if (iev.ne.0.and.nrefa(ia).ne.-2) then
! si il y a une deconnexion
                if (zfa(ia) .gt. ze) then
                  if (.not.eau(iev).or.(zfa(ia) .gt. het(iev)+zfe(iev)))then
!              if (.not.(eau(iev).and.
!     :    (zfa(ia).lt.ze.or.zfa(ia).lt.het(iev)+zfe(iev))))
!       if (zfa(ia) .gt. he(ie)+zfe(ie).and.zfa(ia) .gt. he(iev)+zfe(iev))

                    hezer2=.true.
                    exit
                  endif
                endif
              endif
             end do
!!$omp end ordered
             if (hezer2.or.hezer(ie)) then
! recalcul de a11,a12,etc
             a11(ie) = 0.
             a22(ie) = 0.
             a12(ie) = 0.
             do j = 1,nne(ie)
                 iev = ieve(ie,j)
                ia=iae(ie,j)
                if (iev.ne.0.and.nrefa(ia).ne.-2) then
                  if ((zfa(ia).le.ze).or.&
               &               (eau(iev)&
               &.and.(zfa(ia).le.het(iev)+zfe(iev)))) then
!                  if (eau(iev)) then
!      if (zfa(ia).lt.ze.or.zfa(ia).lt.het(iev)+zfe(iev)) then
                    a11(ie) = a11(ie) + xbb(ie,j)**2
                    a22(ie) = a22(ie) + ybb(ie,j)**2
                    a12(ie) = a12(ie) + xbb(ie,j)*ybb(ie,j)
!      endif
                  endif
                endif
             end do
             deta(ie) = a11(ie)*a22(ie) - a12(ie)**2
             if (abs(deta(ie)).lt.eps2) then
               if (abs(a11(ie)).lt.eps2.and.abs(a22(ie)).lt.eps2) then
                 deta(ie)=1.
               elseif (abs(a11(ie)).lt.eps2) then
                 a11(ie)=1.
                 deta(ie)=a22(ie)
               elseif (abs(a22(ie)).lt.eps2) then
                 a22(ie)=1.
                 deta(ie)=a11(ie)
               else
                 deta(ie)=a12(ie)**2
                 a12(ie)=0.
               endif
!             else
             endif
! fin du if sur recalcul a11,...
             endif
             hezer(ie)=hezer2
             b1h = 0.0
             b2h = 0.0
#if TRANSPORT_SOLIDE
            b1hc = 0.0
            b2hc = 0.0
#endif


         b1z = 0.0
         b2z = 0.0
         b1qu = 0.0
         b2qu = 0.0
         b1qv = 0.0
         b2qv = 0.0
         do j = 1,nne(ie)
            iev = ieve(ie,j)
            ia=iae(ie,j)
!            if (iev.ne.0) then
            if (iev.ne.0.and.nrefa(ia).ne.-2) then
                  if ((zfa(ia).le.ze).or.&
     &               (eau(iev)&
     &.and.(zfa(ia).le.het(iev)+zfe(iev)))) then
!             if (eau(iev)) then
!      if (zfa(ia).lt.ze.or.zfa(ia).lt.het(iev)+zfe(iev)) then

#if TRANSPORT_SOLIDE
                b1hc =b1hc-xbb(ie,j)*(cet(ie)-cet(iev))
                b2hc = b2hc - ybb(ie,j)*(cet(ie)-cet(iev))

#endif

                b1h = b1h - xbb(ie,j)*(het(ie)-het(iev))
                b2h = b2h - ybb(ie,j)*(het(ie)-het(iev))
                if (eau(iev)) then
                  b1z =b1z-xbb(ie,j)*(ze-het(iev)-zfe(iev))
                  b2z =b2z-ybb(ie,j)*(ze-het(iev)-zfe(iev))
! si pas d'eau mais connexion on suppose zev=ze donc contribution nulle si ze<zev
                elseif (het(iev)+zfe(iev).lt.ze) then
                  b1z =b1z-xbb(ie,j)*(ze-het(iev)-zfe(iev))
                  b2z =b2z-ybb(ie,j)*(ze-het(iev)-zfe(iev))
                endif
                b1qu = b1qu - xbb(ie,j) * (quet(ie) - quet(iev))
                b2qu = b2qu - ybb(ie,j) * (quet(ie) - quet(iev))
                b1qv = b1qv - xbb(ie,j) * (qvet(ie) - qvet(iev))
                b2qv = b2qv - ybb(ie,j) * (qvet(ie) - qvet(iev))
!      endif
! fin des if sur iev=0 et sur het(iev)=0
              endif
            endif
         end do

#if TRANSPORT_SOLIDE
         pxhce(ie) = (a22(ie) * b1hc - a12(ie) * b2hc) / deta(ie)
         pyhce(ie) = (a11(ie) * b2hc - a12(ie) * b1hc) / deta(ie)

#endif

         pxhe(ie) = (a22(ie) * b1h - a12(ie) * b2h) / deta(ie)
         pyhe(ie) = (a11(ie) * b2h - a12(ie) * b1h) / deta(ie)
         pxze(ie) = (a22(ie) * b1z - a12(ie) * b2z) / deta(ie)
         pyze(ie) = (a11(ie) * b2z - a12(ie) * b1z) / deta(ie)
         pxque(ie) = (a22(ie) * b1qu - a12(ie) * b2qu) / deta(ie)
         pyque(ie) = (a11(ie) * b2qu - a12(ie) * b1qu) / deta(ie)
         pxqve(ie) = (a22(ie) * b1qv - a12(ie) * b2qv) / deta(ie)
         pyqve(ie) = (a11(ie) * b2qv - a12(ie) * b1qv) / deta(ie)



!     limitation des pentes
      do j =1,nne(ie)
#if TRANSPORT_SOLIDE
           alfahcx(j) = 1.
           alfahcy(j) = 1.

#endif
           alfahx(j) = 1.
           alfahy(j) = 1.
           alfazx(j) = 1.
           alfazy(j) = 1.
           alfaqux(j) = 1.
           alfaquy(j) = 1.
           alfaqvx(j) = 1.
           alfaqvy(j) = 1.
        iev = ieve(ie,j)
        ia=iae(ie,j)
!            if (iev .gt. 0.and.he(iev) .gt. paray) then
        if (iev .gt. 0) then
          if (nrefa(ia).ne.-2) then
!           valeur de z, qu, qv sur l'arete (interface)
#if TRANSPORT_SOLIDE
            gradhcx = pxhce(ie) * xaa(ie,j)
            gradhcy = pyhce(ie) * yaa(ie,j)
            gradhc = gradhcx+gradhcy
            hca = cet(ie) + gradhc

#endif
            gradhx = pxhe(ie) * xaa(ie,j)
            gradhy=pyhe(ie) * yaa(ie,j)
            gradzx = pxze(ie) * xaa(ie,j)
            gradzy=pyze(ie) * yaa(ie,j)
            gradqux = pxque(ie) * xaa(ie,j)
            gradquy = pyque(ie) * yaa(ie,j)
            gradqvx = pxqve(ie) * xaa(ie,j)
            gradqvy = pyqve(ie) * yaa(ie,j)
           gradh=gradhx+gradhy
           gradz=gradzx+gradzy
           gradqu=gradqux+gradquy
           gradqv=gradqvx +gradqvy
            ha=het(ie)+gradh
            za = ze + gradz
            qua = quet(ie) + gradqu
            qva = qvet(ie) + gradqv

!           comparer cette valeur avec la valeur sur l'elt voisin
#if TRANSPORT_SOLIDE
            hcev=cet(iev)

#endif
            hev = het(iev)
            zev = hev+zfe(iev)
            quev = quet(iev)
            qvev = qvet(iev)
! si il y a connexion
                  if ((zfa(ia).le.ze).or.&
     &               (eau(iev)&
     &.and.(zfa(ia).le.het(iev)+zfe(iev)))) then
!            if (zfa(ia).lt.ze.or.zfa(ia).lt.zev) then
              if (eau(iev)) then
                zev=(1.-alpha)*ze+alpha*zev
                if (ze .gt. zev) then
                  zmax=ze
                  zmin=max(zev,zfa(ia))
                else
                  zmin=max(ze,zfa(ia))
                  zmax=max(zev,zfa(ia))
                endif
                hev=(1.-alpha)*het(ie)+alpha*hev
                quev=(1.-alpha)*quet(ie)+alpha*quev
                qvev=(1.-alpha)*qvet(ie)+alpha*qvev
#if TRANSPORT_SOLIDE
                 hcev=(1.-alpha)*cet(ie)+alpha*hcev

#endif
! si het(iev)=0
             else
#if TRANSPORT_SOLIDE
               hcev=0.

#endif
               quev=0.
               qvev=0.
               if (ze .gt. zev) then
                 zmax=ze
                 zmin=zfa(ia)
               else
! zmin est en fait toujours egal a zfa
                 zmin=min(ze,zfa(ia))
                 zev=(1.-alpha)*ze+alpha*zev
                 zmax=max(zev,zfa(ia))
               endif

!             zev=min(zfa(ia),zev)
               hev=0.
!               zmax=max(zev,ze)
!               zmin=min(ze,zfa(ia))
! fin du if sur het(iev)=0
             endif

#if TRANSPORT_SOLIDE
              if(cet(ie).gt.hcev)then
                hcmin = hcev
                hcmax = cet(ie)
              else
                hcmax = hcev
                hcmin = cet(ie)
              endif

#endif

!                if (ze .gt. zev) then
!                 zmax=ze
!                 zmin=zev
!               else
!                 zmin=ze
!                 zmax=zev
!               endif
              if (het(ie) .gt. hev) then
                hmin = hev
                hmax = het(ie)
              else
                hmax = hev
                hmin = het(ie)
              endif
              if (quet(ie) .gt. quev) then
                qumin = quev
                qumax = quet(ie)
              else
                qumax = quev
                qumin = quet(ie)
              endif
              if (qvet(ie) .gt. qvev) then
                qvmin = qvev
                qvmax = qvet(ie)
              else
                qvmax = qvev
                qvmin = qvet(ie)
              endif
            else
! si zfa est superieure aux cotes de l'eau de chaque cote
! il y a deconnexion hydraulique
              zmin=-10000.
              zmax=zfa(ia)
              hmin=0.
              hmax=het(ie)
              quev=0.
              qvev=0.
#if TRANSPORT_SOLIDE
              hcev=0.
              if(cet(ie).gt.hcev)then
                hcmin = hcev
                hcmax = cet(ie)
              else
                hcmax = hcev
                hcmin = cet(ie)
              endif

#endif
              if (quet(ie) .gt. quev) then
                qumin = quev
                qumax = quet(ie)
              else
                qumax = quev
                qumin = quet(ie)
              endif
              if (qvet(ie) .gt. qvev) then
                qvmin = qvev
                qvmax = qvet(ie)
              else
                qvmax = qvev
                qvmin = qvet(ie)
              endif
! fin du if sur deconnexion hydraulique
            endif

!           calcul terme correctif sur les gradients

           if (za .gt. zmax*(1.+sgn(zmax)*eps2)) then
             if (abs(yaa(ie,j)).lt.eps2) then
               if (abs(gradzx) .gt. eps2) then
!             if (abs(gradzx) .gt. eps2.and.abs(gradzy).lt.eps2) then
                 alfazx(j) = (zmax - ze) / gradzx
               endif
             elseif (abs(xaa(ie,j)).lt.eps2) then
               if (abs(gradzy) .gt. eps2) then
!               elseif (abs(gradzy) .gt. eps2.and.abs(gradzx).lt.eps2) then
                 alfazy(j) = (zmax - ze) / gradzy
               endif
!               elseif (abs(gradzy).lt.eps2.and.abs(gradzx).lt.eps2) then
!             else
             elseif (abs(gradzx+gradzy) .gt. eps2) then
               alf=(zmax - ze)/(gradzx+gradzy)
               alfazx(j)=alf
               alfazy(j)=alf
             endif
           else if (za.lt.zmin*(1.-sgn(zmin)*eps2)) then
             if (abs(yaa(ie,j)).lt.eps2) then
               if (abs(gradzx) .gt. eps2) then
!             if (abs(gradzx) .gt. eps2.and.abs(gradzy).lt.eps2) then
                 alfazx(j) = (zmin - ze) / gradzx
               endif
             elseif (abs(xaa(ie,j)).lt.eps2) then
               if (abs(gradzy) .gt. eps2) then
!               elseif (abs(gradzy) .gt. eps2.and.abs(gradzx).lt.eps2) then
               alfazy(j) = (zmin - ze) / gradzy
!               elseif (abs(gradzy).lt.eps2.and.abs(gradzx).lt.eps2) then
!                 alfazx(j) = 1.
!                 alfazy(j) = 1.
               endif
             elseif (abs(gradzx+gradzy) .gt. eps2) then
               alf=(zmin - ze)/(gradzx+gradzy)
               alfazx(j)=alf
               alfazy(j)=alf
             endif
           endif

           if (ha .gt. hmax*(1.+sgn(hmax)*eps2)) then
             if (abs(yaa(ie,j)).lt.eps2) then
               if (abs(gradhx) .gt. eps2) then
                 alfahx(j) = (hmax - het(ie)) / gradhx
               endif
             elseif (abs(xaa(ie,j)).lt.eps2) then
               if (abs(gradhy) .gt. eps2) then
                 alfahy(j) = (hmax - het(ie)) / gradhy
               endif
             elseif (abs(gradhx+gradhy) .gt. eps2) then
               alf=(hmax - het(ie))/&
     &(gradhx+gradhy)
               alfahx(j)=alf
               alfahy(j)=alf
             endif
           else if (ha.lt.hmin*(1.-sgn(hmin)*eps2)) then
             if (abs(yaa(ie,j)).lt.eps2) then
               if (abs(gradhx) .gt. eps2) then
                 alfahx(j) = (hmin - het(ie)) / gradhx
               endif
             elseif (abs(xaa(ie,j)).lt.eps2) then
               if (abs(gradhy) .gt. eps2) then
                 alfahy(j) = (hmin - het(ie)) / gradhy
               endif
             elseif (abs(gradhx+gradhy) .gt. eps2) then
               alf=(hmin - het(ie))/&
     &(gradhx+gradhy)
               alfahx(j)=alf
               alfahy(j)=alf
             endif
           endif

           if (qua .gt. qumax*(1.+sgn(qumax)*eps2)) then
             if (abs(yaa(ie,j)).lt.eps2) then
               if (abs(gradqux) .gt. eps2) then
                 alfaqux(j) = (qumax - quet(ie)) / gradqux
               endif
             elseif (abs(xaa(ie,j)).lt.eps2) then
               if (abs(gradquy) .gt. eps2) then
                 alfaquy(j) = (qumax - quet(ie)) / gradquy
               endif
             elseif (abs(gradqux+gradquy) .gt. eps2) then
               alf=(qumax - quet(ie))/&
     &(gradqux+gradquy)
               alfaqux(j)=alf
               alfaquy(j)=alf
             endif
           else if (qua.lt.qumin*(1.-sgn(qumin)*eps2)) then
             if (abs(yaa(ie,j)).lt.eps2) then
               if (abs(gradqux) .gt. eps2) then
                 alfaqux(j) = (qumin - quet(ie)) / gradqux
               endif
             elseif (abs(xaa(ie,j)).lt.eps2) then
               if (abs(gradquy) .gt. eps2) then
                 alfaquy(j) = (qumin - quet(ie)) / gradquy
               endif
             elseif (abs(gradqux+gradquy) .gt. eps2) then
               alf=(qumin - quet(ie))/&
     &(gradqux+gradquy)
               alfaqux(j)=alf
               alfaquy(j)=alf
             endif
           endif

           if (qva .gt. qvmax*(1.+sgn(qvmax)*eps2)) then
             if (abs(yaa(ie,j)).lt.eps2) then
               if (abs(gradqvx) .gt. eps2) then
                 alfaqvx(j) = (qvmax - qvet(ie)) / gradqvx
               endif
             elseif (abs(xaa(ie,j)).lt.eps2) then
               if (abs(gradqvy) .gt. eps2) then
                 alfaqvy(j) = (qvmax - qvet(ie)) / gradqvy
               endif
             elseif (abs(gradqvx+gradqvy) .gt. eps2) then
               alf=(qvmax - qvet(ie))/&
     &(gradqvx+gradqvy)
               alfaqvx(j)=alf
               alfaqvy(j)=alf
             endif
           else if (qva.lt.qvmin*(1.-sgn(qvmin)*eps2)) then
             if (abs(yaa(ie,j)).lt.eps2) then
               if (abs(gradqvx) .gt. eps2) then
                 alfaqvx(j) = (qvmin - qvet(ie)) / gradqvx
               endif
             elseif (abs(xaa(ie,j)).lt.eps2) then
               if (abs(gradqvy) .gt. eps2) then
                 alfaqvy(j) = (qvmin - qvet(ie)) / gradqvy
               endif
             elseif (abs(gradqvx+gradqvy) .gt. eps2) then
               alf=(qvmin - qvet(ie))/&
     &(gradqvx+gradqvy)
               alfaqvx(j)=alf
               alfaqvy(j)=alf
             endif
           endif

#if TRANSPORT_SOLIDE

          if(eau(iev))then
           if (hca.gt.hcmax*(1.+sgn(hcmax)*eps2)) then
             if(abs(yaa(ie,j)).lt.eps2)then
               if(abs(gradhcx).gt.eps2)then
                 alfahcx(j) = (hcmax - cet(ie)) / gradhcx
               endif
             elseif(abs(xaa(ie,j)).lt.eps2)then
               if(abs(gradhcy).gt.eps2)then
                 alfahcy(j) = (hcmax - cet(ie)) / gradhcy
               endif
             elseif(abs(gradhcx+gradhcy).gt.eps2)then
               alf=(hcmax - cet(ie))/&
     &(gradhcx+gradhcy)
               alfahcx(j)=alf
               alfahcy(j)=alf
             endif
           else if (hca.lt.hcmin*(1.-sgn(hcmin)*eps2)) then
             if(abs(yaa(ie,j)).lt.eps2)then
               if(abs(gradhcx).gt.eps2)then
                 alfahcx(j) = (hcmin - cet(ie)) / gradhcx
               endif
             elseif(abs(xaa(ie,j)).lt.eps2)then
               if(abs(gradhcy).gt.eps2)then
                 alfahcy(j) = (hcmin - cet(ie)) / gradhcy
               endif
             elseif(abs(gradhcx+gradhcy).gt.eps2)then
               alf=(hcmin - cet(ie))/&
     &(gradhcx+gradhcy)
               alfahcx(j)=alf
               alfahcy(j)=alf
             endif
           endif
           alfahcx(j) = abs(alfahcx(j))
           alfahcy(j) = abs(alfahcy(j))
! si pas d'eau
         else
           alfahcx(j)=1.
           alfahcy(j)=1.
         endif

#endif

           alfahx(j) = abs(alfahx(j))
           alfahy(j) = abs(alfahy(j))
           alfazx(j) = abs(alfazx(j))
           alfazy(j) = abs(alfazy(j))
           alfaqux(j) = abs(alfaqux(j))
           alfaquy(j) = abs(alfaquy(j))
           alfaqvx(j) = abs(alfaqvx(j))
           alfaqvy(j) = abs(alfaqvy(j))
! endif sur nrefa=-2
          endif

! fin du if sur iev
         endif
      end do

! on choisit le plus petit des alfa sur chaque axe

#if TRANSPORT_SOLIDE

      alfx=1.
      alfy=1.
      do j =1,nne(ie)
      if(alfx.gt.alfahcx(j))then
       alfx=alfahcx(j)
      endif
      if(alfy.gt.alfahcy(j))then
       alfy=alfahcy(j)
      endif
      enddo
      pxhce(ie) = alfx * pxhce(ie)
      pyhce(ie) = alfy * pyhce(ie)

#endif

      alfx=1.
      alfy=1.
      do j =1,nne(ie)
      if (alfx .gt. alfahx(j)) then
       alfx=alfahx(j)
      endif
      if (alfy .gt. alfahy(j)) then
       alfy=alfahy(j)
      endif
      end do
      pxhe(ie) = alfx * pxhe(ie)
      pyhe(ie) = alfy * pyhe(ie)
      alfx=1.
      alfy=1.
      do j =1,nne(ie)
      if (alfx .gt. alfazx(j)) then
       alfx=alfazx(j)
      endif
      if (alfy .gt. alfazy(j)) then
       alfy=alfazy(j)
      endif
      end do
      pxze(ie) = alfx * pxze(ie)
      pyze(ie) = alfy * pyze(ie)
      alfx=1.
      alfy=1.
      do j =1,nne(ie)
       if (alfx .gt. alfaqux(j)) then
        alfx=alfaqux(j)
       endif
       if (alfy .gt. alfaquy(j)) then
        alfy=alfaquy(j)
       endif
      end do
       pxque(ie) = alfx * pxque(ie)
       pyque(ie) = alfy * pyque(ie)
       alfx=1.
       alfy=1.
      do j =1,nne(ie)
      if (alfx .gt. alfaqvx(j)) then
       alfx=alfaqvx(j)
      endif
      if (alfy .gt. alfaqvy(j)) then
       alfy=alfaqvy(j)
      endif
      end do
      pxqve(ie) = alfx * pxqve(ie)
      pyqve(ie) = alfy * pyqve(ie)

#if TRANSPORT_SOLIDE
      if(abs(pxhce(ie)).lt.eps2) pxhce(ie)=0.0
      if(abs(pyhce(ie)).lt.eps2) pyhce(ie)=0.0

#endif

      if (abs(pxhe(ie)).lt.eps2) pxhe(ie)=0.0
      if (abs(pyhe(ie)).lt.eps2) pyhe(ie)=0.0
      if (abs(pxze(ie)).lt.eps2) pxze(ie)=0.0
      if (abs(pyze(ie)).lt.eps2) pyze(ie)=0.0
      if (abs(pxque(ie)).lt.eps2) pxque(ie)=0.0
      if (abs(pyque(ie)).lt.eps2) pyque(ie)=0.0
      if (abs(pxqve(ie)).lt.eps2) pxqve(ie)=0.0
      if (abs(pyqve(ie)).lt.eps2) pyqve(ie)=0.0
! fin du if sur he

       endif

      end do
!$omp end do
!$omp end parallel

#if TRANSPORT_SOLIDE

! recalcul pxzfe et pyzfe
      if(modifz)then
!     calcul des pentes au fond
      do ieloc = 1,ne_loc(me)
        ie = mailles_loc(ieloc)
         b1h = 0.0
         b2h = 0.0

         do j = 1,nne(ie)
            iev = ieve(ie,j)
            ia=iae(ie,j)
            b1h =b1h-xaa(ie,j)*(zfe(ie)-zfa(ia))
            b2h = b2h - yaa(ie,j)*(zfe(ie)-zfa(ia))
         enddo
!
         pxzfe(ie) = (a220(ie) * b1h - a120(ie) * b2h) / deta0(ie)
         pyzfe(ie) = (a110(ie) * b2h - a120(ie) * b1h) / deta0(ie)
         if(abs(pxzfe(ie)).lt.eps2) pxzfe(ie)=0.0
         if(abs(pyzfe(ie)).lt.eps2) pyzfe(ie)=0.0
       enddo
! fin du if sur modifz
       endif

#endif

#ifdef WITH_MPI
! envoi/reception des messages
      !call cpu_time(tcomm1)
        call message_group_calcul(pyque)
        call message_group_calcul(pxque)
        call message_group_calcul(pyqve)
        call message_group_calcul(pxqve)
        call message_group_calcul(pxze)
        call message_group_calcul(pyze)
        call message_group_calcul(pxzfe)
        call message_group_calcul(pyzfe)

#if TRANSPORT_SOLIDE
      call message_group_calcul(pxhce)
      call message_group_calcul(pyhce)
#endif
      !call cpu_time(tcomm2)
      !tcomm=tcomm+tcomm2-tcomm1
#endif /* WITH_MPI */

! calcul des laplaciens
      if (lcvi) then
        if (lcviv) then
          if (lcvif) then
! diffusion a partir pente frottement
            call laplaf!(xbb,ybb,xybb,a11,a12,a22,deta)
          else
! diffusion a partir pente surface libre
            call laplaz!(xbb,ybb,xybb,a11,a12,a22,deta)
          endif
        else
! diffusion fixe dans le temps
          call laplac!(xbb,ybb,xybb,a11,a12,a22,deta)
        endif
      endif
      !deallocate(xaa,yaa)
      !deallocate(a110,a220,a120,deta0)
      !deallocate(sse)
      !deallocate(hezer)
!conc
!c retablissement des valeurs de hce en hc au lieu de concentration
!          do ie=1,ne
!               hce(ie)=hce(ie)*he(ie)
!          enddo
!concf
      end subroutine grdlp

!******************************************************************************
       subroutine laplac!(xbb,ybb,xybb,a11,a12,a22,deta)
!******************************************************************************
!     calcul du laplacien
!******************************************************************************
      use module_tableaux,only:nne,zfa,zfe,iae,het,quet,qvet,ieve,&
     &xna,yna,xta,yta,nrefa,fluu,flvv,eau,ieva,cvia,&
     &eps1,eps2,paray,ne,na,nn!,tcomm1,tcomm2,tcomm
      use module_precision
      use module_mpi,only:ne_loc,me,mailles_loc,naloc,aretes_loc,nmv,elt_voisins
      use module_loc, only:fxuu,&
     &fyuu,fxvv,fyvv,pxuu,pyuu&
     &,pxvv,pyvv,ue,ve
#ifdef WITH_MPI
      use module_messages
#endif /* WITH_MPI */
      use module_laplacien

!$    use omp_lib

#if TRANSPORT_SOLIDE
      use module_ts,only:hce,pxhce,pyhce,hcal,pxhc,pyhc,flhc,diffus&
      &,sigm,dix,diy,cet
#endif


      implicit none

      integer ie,j,je,ia,iev,ialoc,i,ieloc
      real(wp) cb1qu,cb2qu,cb1qv,cb2qv
      !real(wp),dimension(:),allocatable::a11,a12,a22,deta
      !real(wp),dimension(:,:),allocatable,intent(in)::xbb,ybb,xybb
      real(wp) del
      !real(wp),dimension(:),allocatable::fxuu,
      !:fyuu,fxvv,fyvv,pxuu,pyuu
      !:,pxvv,pyvv,ue,ve
#if TRANSPORT_SOLIDE
      real(wp) :: cb1hc,cb2hc,fxhc(na),fyhc(na)
#endif
      !common/nvois/nne
      !common/param/eps1,eps2,paray
      !common/nomb/ne,na,nn
      !common/zare/zfa
      !common/zele/zfe
      !common/nrel/iae
      !common/tlig/het,quet,qvet
      !common/iele/ieve
      !common/vare/xna,yna,xta,yta
      !common/narf/nrefa
      !common/fluuvv/fluu,flvv
      !common/preso/eau
      !common/iela/ieva
!      common/grav/g
      !common/lcvia/cvia
!      common/frot/fra,fre


      !allocate(fxuu(na),
      !:fyuu(na),fxvv(na),fyvv(na),pxuu(ne),pyuu(ne)
      !:,pxvv(ne),pyvv(ne),ue(ne),ve(ne))
!     calcul des pentes

!$omp parallel
!$omp do schedule(runtime)
      do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
        if (.not.eau(ie)) then
#if TRANSPORT_SOLIDE
          pxhc(ie)=0.0
          pyhc(ie)=0.0
#endif
          pxuu(ie)=0.
          pyuu(ie)=0.
          pxvv(ie)=0.
          pyvv(ie)=0.
        else
           ue(ie)=quet(ie)/het(ie)
           ve(ie)=qvet(ie)/het(ie)
        endif
      end do
!$omp end do
! remplace le message sur ue et ve voisins
!$omp do schedule(runtime)
      do i = 1,nmv
        ie=elt_voisins(i)
        if (eau(ie)) then
           ue(ie)=quet(ie)/het(ie)
           ve(ie)=qvet(ie)/het(ie)
        endif
      end do
!$omp end do
! calcul de pxuu,pyuu,pxvv,pyvv pente moyenne par maille de u et v sur x et y
! si he non nul
!$omp do schedule(runtime)
      do ieloc=1,ne_loc(me)
        ie=mailles_loc(ieloc)
        if (eau(ie)) then
#if TRANSPORT_SOLIDE
          cb1hc=0.0
          cb2hc=0.0
#endif
          cb1qu = 0.0
          cb2qu = 0.0
          cb1qv= 0.0
          cb2qv= 0.0
          do j = 1,nne(ie)
            iev = ieve(ie,j)
            if (iev.ne.0) then
              if (eau(iev)) then
                ia=iae(ie,j)
                if (nrefa(ia).ne.-2) then
                  if(zfa(ia).le.het(ie)+zfe(ie).or.zfa(ia).le.het(iev)+zfe(iev))then
#if TRANSPORT_SOLIDE
                    del = cet(ie)-cet(iev)
                    cb1hc = cb1hc - xbb(ie,j) * del
                    cb2hc = cb2hc- ybb(ie,j) * del

#endif
                    del = ue(ie)-ue(iev)
                    cb1qu = cb1qu - xbb(ie,j) * del
                    cb2qu= cb2qu- ybb(ie,j) * del
                    del = ve(ie)-ve(iev)
                    cb1qv = cb1qv - xbb(ie,j) * del
                    cb2qv= cb2qv- ybb(ie,j) * del
                  endif
                endif
              endif
            endif
          end do
#if TRANSPORT_SOLIDE
          pxhc(ie) = (a22(ie) *cb1hc - a12(ie) *cb2hc) / deta(ie)
          pyhc(ie) = (a11(ie)*cb2hc-a12(ie)*cb1hc)/deta(ie)

#endif
          pxuu(ie) = (a22(ie) *cb1qu - a12(ie) *cb2qu) / deta(ie)
          pyuu(ie) = (a11(ie)*cb2qu-a12(ie)*cb1qu)/deta(ie)
          pxvv(ie) = (a22(ie) *cb1qv - a12(ie) *cb2qv) / deta(ie)
          pyvv(ie) = (a11(ie)*cb2qv-a12(ie)*cb1qv)/deta(ie)
! if sur he =0
        else
#if TRANSPORT_SOLIDE
          pxhc(ie) = 0.
          pyhc(ie) = 0.
#endif
          pxuu(ie) = 0.
          pyuu(ie) = 0.
          pxvv(ie) = 0.
          pyvv(ie) = 0.
! fin du if sur he=0
        endif
      end do
!$omp end do

#ifdef WITH_MPI
! envoie des messages
      !call cpu_time(tcomm1)
      call message_group_calcul(pxuu)
      call message_group_calcul(pyuu)
      call message_group_calcul(pxvv)
      call message_group_calcul(pyvv)

#if TRANSPORT_SOLIDE

      call message_group_calcul(pxhc)
      call message_group_calcul(pyhc)

#endif
      !call cpu_time(tcomm2)
      !tcomm=tcomm+tcomm2-tcomm1
#endif /* WITH_MPI */

! calcul de laplacien
!********************************************
!$omp do schedule(runtime)
      do ialoc=1,naloc
        ia=aretes_loc(ialoc)
                fxuu(ia)=0.
                fyuu(ia)=0.
                fxvv(ia)=0.
                fyvv(ia)=0.
                fluu(ia)=0.
                flvv(ia)=0.
#if TRANSPORT_SOLIDE
                fxhc(ia)=0.
                fyhc(ia)=0.
                flhc(ia)=0.

#endif
       if (nrefa(ia).ne.-2) then
        ie=ieva(ia,1)
        iev=ieva(ia,2)
        if (ie.ne.0) then
          if (eau(ie)) then
            if (iev.ne.0) then
              if (eau(iev)) then
                do je=1,nne(ie)
                  if (iev.eq.ieve(ie,je)) then
                    j=je
                    go to 1902
                  endif
                end do
 1902          if (abs(xbb(ie,j)).lt.eps2) then
                    fxuu(ia)=0.
                  elseif (abs(pxuu(ie)) .gt. abs(pxuu(iev))) then
                    fxuu(ia)=(ue(iev)-ue(ie))/xybb(ie,j)&
     &*abs(pxuu(ie))*xbb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxuu(ie)**2+ybb(ie,j)**2*&
     &pyuu(ie)**2)
! if sur pxuu
                  elseif (abs(pxuu(iev)) .gt. eps2) then
                    fxuu(ia)=(ue(iev)-ue(ie))/xybb(ie,j)&
     &*abs(pxuu(iev))*xbb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxuu(iev)**2+ybb(ie,j)**2*&
     &pyuu(iev)**2)
! if sur pxuu
! cas ou les 2 pxuu sont nuls
                  else
                    fxuu(ia)=0.
! if sur pxuu
                  endif
                  if (abs(ybb(ie,j)).lt.eps2) then
                    fyuu(ia)=0.
                  elseif (abs(pyuu(ie)) .gt. abs(pyuu(iev))) then
                    fyuu(ia)=(ue(iev)-ue(ie))/xybb(ie,j)&
     &*abs(pyuu(ie))*ybb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxuu(ie)**2+ybb(ie,j)**2*&
     &pyuu(ie)**2)
! if sur pyuu
                  elseif (abs(pyuu(iev)) .gt. eps2) then
                    fyuu(ia)=(ue(iev)-ue(ie))/xybb(ie,j)&
     &*abs(pyuu(iev))*ybb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxuu(iev)**2+ybb(ie,j)**2*&
     &pyuu(iev)**2)
! cas ou les 2 pyuu sont nuls
                  else
                    fyuu(ia)=0.
! if sur pyuu
                 endif
                  if (abs(xbb(ie,j)).lt.eps2) then
                    fxvv(ia)=0.
                  elseif (abs(pxvv(ie)) .gt. abs(pxvv(iev))) then
                    fxvv(ia)=(ve(iev)-ve(ie))/xybb(ie,j)&
     &*abs(pxvv(ie))*xbb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxvv(ie)**2+ybb(ie,j)**2*&
     &pyvv(ie)**2)
! if sur pxvv
                  elseif (abs(pxvv(iev)) .gt. eps2) then
                    fxvv(ia)=(ve(iev)-ve(ie))/xybb(ie,j)&
     &*abs(pxvv(iev))*xbb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxvv(iev)**2+ybb(ie,j)**2*&
     &pyvv(iev)**2)
! cas ou les 2 pxvv sont nuls
                  else
                    fxvv(ia)=0.
! if sur pxvv
                  endif
                  if (abs(ybb(ie,j)).lt.eps2) then
                    fyvv(ia)=0.
                  elseif (abs(pyvv(ie)) .gt. abs(pyvv(iev))) then
                    fyvv(ia)=(ve(iev)-ve(ie))/xybb(ie,j)&
     &*abs(pyvv(ie))*ybb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxvv(ie)**2+ybb(ie,j)**2*&
     &pyvv(ie)**2)
! if sur pyvv
                  elseif (abs(pyvv(iev)) .gt. eps2) then
                    fyvv(ia)=(ve(iev)-ve(ie))/xybb(ie,j)&
     &*abs(pyvv(iev))*ybb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxvv(iev)**2+ybb(ie,j)**2*&
     &pyvv(iev)**2)
! if sur pyvv
! cas ou les 2 pyvv sont nuls
                  else
                    fyvv(ia)=0.
! if sur pyvv
                  endif
#if TRANSPORT_SOLIDE


                  if(abs(xbb(ie,j)).lt.eps2)then
                    fxhc(ia)=0.
                  elseif(abs(pxhc(ie)).gt.abs(pxhc(iev)))then
                    fxhc(ia)=(cet(iev)-cet(ie))/xybb(ie,j)&
     &*abs(pxhc(ie))*xbb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxhc(ie)**2+ybb(ie,j)**2*&
     &pyhc(ie)**2)
! if sur pxhc
                  elseif(abs(pxhc(iev)).gt.eps2)then
                    fxhc(ia)=(cet(iev)-cet(ie))/xybb(ie,j)&
     &*abs(pxhc(iev))*xbb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxhc(iev)**2+ybb(ie,j)**2*&
     &pyhc(iev)**2)
! if sur pxhc
! cas ou les 2 pxhc sont nuls
                  else
                    fxhc(ia)=0.
! if sur pxhc
                  endif
                  if(abs(ybb(ie,j)).lt.eps2)then
                    fyhc(ia)=0.
                  elseif(abs(pyhc(ie)).gt.abs(pyhc(iev)))then
                    fyhc(ia)=(cet(iev)-cet(ie))/xybb(ie,j)&
     &*abs(pyhc(ie))*ybb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxhc(ie)**2+ybb(ie,j)**2*&
     &pyhc(ie)**2)
! if sur pyhc
                  elseif(abs(pyhc(iev)).gt.eps2)then
                    fyhc(ia)=(cet(iev)-cet(ie))/xybb(ie,j)&
     &*abs(pyhc(iev))*ybb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxhc(iev)**2+ybb(ie,j)**2*&
     &pyhc(iev)**2)
! cas ou les 2 pyhc sont nuls
                  else
                    fyhc(ia)=0.
! if sur pyhc
                 endif

        if(diffus)then
          flhc(ia)=fxhc(ia)*dix*xna(ia)+fyhc(ia)*diy*yna(ia)
        else
          flhc(ia)=sigm*cvia(ia)*(fxhc(ia)*xna(ia)+fyhc(ia)*yna(ia))
        endif
#endif

        fluu(ia)=cvia(ia)*(fxuu(ia)*xna(ia)+fyuu(ia)*yna(ia))
        flvv(ia)=cvia(ia)*(fxvv(ia)*xna(ia)+fyvv(ia)*yna(ia))
!oncf
! if sur het(iev) : cas ou het(iev)=0  on prend zero
!              else
!conc
!                fxhc(ia)=0.
!                fyhc(ia)=0.
!concf
!                fxuu(ia)=0.
!                fyuu(ia)=0.
!                fxvv(ia)=0.
!                fyvv(ia)=0.
              endif
! if sur iev=0 :pas de voisin donc valeur nulle
!            else
!conc
!              fxhc(ia)=pxhc(ie)
!              fyhc(ia)=pyhc(ie)
!concf
!              fxuu(ia)=pxuu(ie)
!              fyuu(ia)=pyuu(ie)
!              fxvv(ia)=pxvv(ie)
!              fyvv(ia)=pyvv(ie)
            endif
! if sur het(ie)=0 :cas ou het(ie)=0 on prend zero
!          else
!conc
!                fxhc(ia)=0.
!                fyhc(ia)=0.
!concf
!            fxuu(ia)=0.
!            fyuu(ia)=0.
!            fxvv(ia)=0.
!            fyvv(ia)=0.
          endif
! if sur ie=0 :cas ou pas de voisin donc valeur nulle
!        else
!c          if (eau(iev)) then
!conc
!          fxhc(ia)=pxhc(iev)
!          fyhc(ia)=pyhc(iev)
!concf
!            fxuu(ia)=pxuu(iev)
!            fyuu(ia)=pyuu(iev)
!            fxvv(ia)=pxvv(iev)
!            fyvv(ia)=pyvv(iev)
!          else
!onc
!                fxhc(ia)=0.
!                fyhc(ia)=0.
!concf
!            fxuu(ia)=0.
!            fyuu(ia)=0.
!            fxvv(ia)=0.
!            fyvv(ia)=0.
! fin du if sur eau dans iev
!          endif
! fin du if sur ie egal a 0
        endif
! si nrefa=-2
!       else
!conc
!        flhc(ia)=0.
!concf
!        fluu(ia)=0.
!        flvv(ia)=0.
! fin du if sur nrefa
       endif

      end do
!$omp end do
!$omp end parallel
      !deallocate(fxuu,fyuu,fxvv,fyvv,pxuu,pyuu,pxvv,pyvv,ue,ve)
      return
      end subroutine laplac

!******************************************************************************
       subroutine laplaf!(xbb,ybb,xybb,a11,a12,a22,deta)
!******************************************************************************
!     calcul du laplacien avec nu fonction pente frottement
!******************************************************************************
      use module_tableaux,only:nne,zfa,zfe,iae,het,quet,qvet,ieve,&
     &xna,yna,xta,yta,nrefa,fluu,flvv,eau,ieva,cvia,ncvi,fra,fre,&
     &eps1,eps2,paray,ne,na,nn,g!,tcomm1,tcomm2,tcomm
      use module_precision
      use module_mpi,only:ne_loc,me,mailles_loc,naloc,aretes_loc,nmv,elt_voisins
      use module_loc, only:fxuu,&
     &fyuu,fxvv,fyvv,pxuu,pyuu&
     &,pxvv,pyvv,ue,ve,ustar
#ifdef WITH_MPI
      use module_messages
#endif /* WITH_MPI */
      use module_laplacien


#if TRANSPORT_SOLIDE
      use module_ts,only:hce,pxhce,pyhce,hcal,pxhc,pyhc,flhc,diffus&
      &,sigm,dix,diy,cet
#endif

!$    use omp_lib

      implicit none

      integer ie,j,je,ia,iev,ialoc,i,ieloc
      real(wp) cb1qu,cb2qu,cb1qv,cb2qv
      !real(wp),dimension(:),allocatable::a11,a12,a22,deta
      !real(wp),dimension(:,:),allocatable::xbb,ybb,xybb
      real(wp) del
      !real(wp),dimension(:),allocatable::fxuu,
      !:fyuu,fxvv,fyvv,pxuu,pyuu
      !:,pxvv,pyvv,ue,ve,ustar
      real(wp) fra2

#if TRANSPORT_SOLIDE
      real(wp) :: cb1hc,cb2hc,fxhc(na),fyhc(na)
#endif

      !common/nvois/nne
      !common/param/eps1,eps2,paray
      !common/nomb/ne,na,nn
      !common/zare/zfa
      !common/zele/zfe
      !common/nrel/iae
      !common/tlig/het,quet,qvet
      !common/iele/ieve
      !common/vare/xna,yna,xta,yta
      !common/narf/nrefa
      !common/fluuvv/fluu,flvv
      !common/preso/eau
      !common/iela/ieva
      !common/grav/g
      !common/lcvia/cvia
      !common/frot/fra,fre
      !common/nuqe/ncvi


      !allocate(ustar(ne))
      !allocate(fxuu(na),
      !:fyuu(na),fxvv(na),fyvv(na),pxuu(ne),pyuu(ne)
      !:,pxvv(ne),pyvv(ne),ue(ne),ve(ne))
!     calcul des pentes

!$omp parallel
!$omp do schedule(runtime)
      do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
        if (.not.eau(ie)) then
#if TRANSPORT_SOLIDE
          pxhc(ie)=0.0
          pyhc(ie)=0.0
#endif
          pxuu(ie)=0.
          pyuu(ie)=0.
          pxvv(ie)=0.
          pyvv(ie)=0.
        else
           ue(ie)=quet(ie)/het(ie)
           ve(ie)=qvet(ie)/het(ie)
        endif
      end do
!$omp end do
! remplace le message sur ue et ve voisins
!$omp do schedule(runtime)
      do i = 1,nmv
        ie=elt_voisins(i)
        if (eau(ie)) then
           ue(ie)=quet(ie)/het(ie)
           ve(ie)=qvet(ie)/het(ie)
        endif
      end do
!$omp end do
! calcul de pxuu,pyuu,pxvv,pyvv pente moyenne par maille de u et v sur x et y
! si he non nul
!$omp do schedule(runtime)
      do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
        if (eau(ie)) then
#if TRANSPORT_SOLIDE
            cb1hc=0.0
            cb2hc=0.0
#endif
           cb1qu = 0.0
           cb2qu = 0.0
           cb1qv= 0.0
           cb2qv= 0.0

           do j = 1,nne(ie)
            iev = ieve(ie,j)
            if (iev.ne.0) then
              if (eau(iev)) then
                    ia=iae(ie,j)
                    if (nrefa(ia).ne.-2) then
      if(zfa(ia).le.het(ie)+zfe(ie).or.zfa(ia).le.het(iev)+zfe(iev))then
#if TRANSPORT_SOLIDE
                  del = cet(ie)-cet(iev)
                  cb1hc = cb1hc - xbb(ie,j) * del
                  cb2hc = cb2hc- ybb(ie,j) * del

#endif
                  del = ue(ie)-ue(iev)
                   cb1qu = cb1qu - xbb(ie,j) * del
                  cb2qu= cb2qu- ybb(ie,j) * del
                  del = ve(ie)-ve(iev)
                  cb1qv = cb1qv - xbb(ie,j) * del
                  cb2qv= cb2qv- ybb(ie,j) * del
              endif
              endif
            endif
           endif
          end do
#if TRANSPORT_SOLIDE
          pxhc(ie) = (a22(ie) *cb1hc - a12(ie) *cb2hc) / deta(ie)
          pyhc(ie) = (a11(ie)*cb2hc-a12(ie)*cb1hc)/deta(ie)

#endif
          pxuu(ie) = (a22(ie) *cb1qu - a12(ie) *cb2qu) / deta(ie)
          pyuu(ie) = (a11(ie)*cb2qu-a12(ie)*cb1qu)/deta(ie)
          pxvv(ie) = (a22(ie) *cb1qv - a12(ie) *cb2qv) / deta(ie)
          pyvv(ie) = (a11(ie)*cb2qv-a12(ie)*cb1qv)/deta(ie)
          fra2=max(fra(ie),fre(ie)*het(ie)**0.3333333)
          ustar(ie)=sqrt(g*(ue(ie)**2+ve(ie)**2)/fra2)
! if sur he =0
        else
#if TRANSPORT_SOLIDE
          pxhc(ie) = 0.
          pyhc(ie) = 0.
#endif
          pxuu(ie) = 0.
          pyuu(ie) =  0.
          pxvv(ie) =  0.
          pyvv(ie) =  0.
          ustar(ie)=0.
! fin du if sur he=0
        endif
      end do
!$omp end do

#ifdef WITH_MPI
! envoie des messages
      !call cpu_time(tcomm1)
        call message_group_calcul(pxuu)
        call message_group_calcul(pyuu)
        call message_group_calcul(pxvv)
        call message_group_calcul(pyvv)
        call message_group_calcul(ustar)
      !call cpu_time(tcomm2)
      !tcomm=tcomm+tcomm2-tcomm1

#if TRANSPORT_SOLIDE

        call message_group_calcul(pxhc)
        call message_group_calcul(pyhc)

#endif
#endif /* WITH_MPI */


! calcul de laplacien
!********************************************
!$omp do schedule(runtime)
      do ialoc=1,naloc
        ia=aretes_loc(ialoc)
                fxuu(ia)=0.
                fyuu(ia)=0.
                fxvv(ia)=0.
                fyvv(ia)=0.
                fluu(ia)=0.
                flvv(ia)=0.
#if TRANSPORT_SOLIDE
                fxhc(ia)=0.
                fyhc(ia)=0.
                flhc(ia)=0.

#endif
                ncvi(ia)=0.
       if (nrefa(ia).ne.-2) then
        ie=ieva(ia,1)
        iev=ieva(ia,2)
        if (ie.ne.0) then
          if (eau(ie)) then
            if (iev.ne.0) then
              if (eau(iev)) then
                do je=1,nne(ie)
                  if (iev.eq.ieve(ie,je)) then
                    j=je   !   j est l'indice local dans ie de l'element iev
!                    go to 1902
                  endif
                end do
! 1902          if (abs(xbb(ie,j)).lt.eps2) then
                if (abs(xbb(ie,j)).lt.eps2) then
                    fxuu(ia)=0.
                  elseif (abs(pxuu(ie)) .gt. abs(pxuu(iev))) then
                    fxuu(ia)=(ue(iev)-ue(ie))/xybb(ie,j)&
     &*abs(pxuu(ie))*xbb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxuu(ie)**2+ybb(ie,j)**2*&
     &pyuu(ie)**2)
! if sur pxuu
                  elseif (abs(pxuu(iev)) .gt. eps2) then
                    fxuu(ia)=(ue(iev)-ue(ie))/xybb(ie,j)&
     &*abs(pxuu(iev))*xbb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxuu(iev)**2+ybb(ie,j)**2*&
     &pyuu(iev)**2)
! if sur pxuu
! cas ou les 2 pxuu sont nuls
                  else
                    fxuu(ia)=0.
! if sur pxuu
                  endif
                  if (abs(ybb(ie,j)).lt.eps2) then
                    fyuu(ia)=0.
                  elseif (abs(pyuu(ie)) .gt. abs(pyuu(iev))) then
                    fyuu(ia)=(ue(iev)-ue(ie))/xybb(ie,j)&
     &*abs(pyuu(ie))*ybb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxuu(ie)**2+ybb(ie,j)**2*&
     &pyuu(ie)**2)
! if sur pyuu
                  elseif (abs(pyuu(iev)) .gt. eps2) then
                    fyuu(ia)=(ue(iev)-ue(ie))/xybb(ie,j)&
     &*abs(pyuu(iev))*ybb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxuu(iev)**2+ybb(ie,j)**2*&
     &pyuu(iev)**2)
! cas ou les 2 pyuu sont nuls
                  else
                    fyuu(ia)=0.
! if sur pyuu
                 endif
                  if (abs(xbb(ie,j)).lt.eps2) then
                    fxvv(ia)=0.
                  elseif (abs(pxvv(ie)) .gt. abs(pxvv(iev))) then
                    fxvv(ia)=(ve(iev)-ve(ie))/xybb(ie,j)&
     &*abs(pxvv(ie))*xbb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxvv(ie)**2+ybb(ie,j)**2*&
     &pyvv(ie)**2)
! if sur pxvv
                  elseif (abs(pxvv(iev)) .gt. eps2) then
                    fxvv(ia)=(ve(iev)-ve(ie))/xybb(ie,j)&
     &*abs(pxvv(iev))*xbb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxvv(iev)**2+ybb(ie,j)**2*&
     &pyvv(iev)**2)
! cas ou les 2 pxvv sont nuls
                  else
                    fxvv(ia)=0.
! if sur pxvv
                  endif
                  if (abs(ybb(ie,j)).lt.eps2) then
                    fyvv(ia)=0.
                  elseif (abs(pyvv(ie)) .gt. abs(pyvv(iev))) then
                    fyvv(ia)=(ve(iev)-ve(ie))/xybb(ie,j)&
     &*abs(pyvv(ie))*ybb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxvv(ie)**2+ybb(ie,j)**2*&
     &pyvv(ie)**2)
! if sur pyvv
                  elseif (abs(pyvv(iev)) .gt. eps2) then
                    fyvv(ia)=(ve(iev)-ve(ie))/xybb(ie,j)&
     &*abs(pyvv(iev))*ybb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxvv(iev)**2+ybb(ie,j)**2*&
     &pyvv(iev)**2)
! if sur pyvv
! cas ou les 2 pyvv sont nuls
                  else
                    fyvv(ia)=0.
! if sur pyvv
                  endif



#if TRANSPORT_SOLIDE


                  if(abs(xbb(ie,j)).lt.eps2)then
                    fxhc(ia)=0.
                  elseif(abs(pxhc(ie)).gt.abs(pxhc(iev)))then
                    fxhc(ia)=(cet(iev)-cet(ie))/xybb(ie,j)&
     &*abs(pxhc(ie))*xbb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxhc(ie)**2+ybb(ie,j)**2*&
     &pyhc(ie)**2)
! if sur pxhc
                  elseif(abs(pxhc(iev)).gt.eps2)then
                    fxhc(ia)=(cet(iev)-cet(ie))/xybb(ie,j)&
     &*abs(pxhc(iev))*xbb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxhc(iev)**2+ybb(ie,j)**2*&
     &pyhc(iev)**2)
! if sur pxhc
! cas ou les 2 pxhc sont nuls
                  else
                    fxhc(ia)=0.
! if sur pxhc
                  endif
                  if(abs(ybb(ie,j)).lt.eps2)then
                    fyhc(ia)=0.
                  elseif(abs(pyhc(ie)).gt.abs(pyhc(iev)))then
                    fyhc(ia)=(cet(iev)-cet(ie))/xybb(ie,j)&
     &*abs(pyhc(ie))*ybb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxhc(ie)**2+ybb(ie,j)**2*&
     &pyhc(ie)**2)
! if sur pyhc
                  elseif(abs(pyhc(iev)).gt.eps2)then
                    fyhc(ia)=(cet(iev)-cet(ie))/xybb(ie,j)&
     &*abs(pyhc(iev))*ybb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxhc(iev)**2+ybb(ie,j)**2*&
     &pyhc(iev)**2)
! cas ou les 2 pyhc sont nuls
                  else
                    fyhc(ia)=0.
! if sur pyhc
                 endif



#endif

        ncvi(ia)=0.5*cvia(ia)*(ustar(ie)*het(ie)+ustar(iev)*het(iev))

#if TRANSPORT_SOLIDE
        if(diffus)then
          flhc(ia)=fxhc(ia)*dix*xna(ia)+fyhc(ia)*diy*yna(ia)
        else
          flhc(ia)=sigm*ncvi(ia)*(fxhc(ia)*xna(ia)+fyhc(ia)*yna(ia))
        endif

#endif

        fluu(ia)=ncvi(ia)*(fxuu(ia)*xna(ia)+fyuu(ia)*yna(ia))
        flvv(ia)=ncvi(ia)*(fxvv(ia)*xna(ia)+fyvv(ia)*yna(ia))
! if sur het(iev) : cas ou het(iev)=0  on prend zero
!              else
!conc
!                fxhc(ia)=0.
!                fyhc(ia)=0.
!concf
!                fxuu(ia)=0.
!                fyuu(ia)=0.
!c                fxvv(ia)=0.
!                fyvv(ia)=0.
!                ncvi(ia)=0.
              endif
! if sur iev=0 :pas de voisin donc valeur nulle
!            else
!conc
!              fxhc(ia)=pxhc(ie)
!              fyhc(ia)=pyhc(ie)
!concf
!              fxuu(ia)=pxuu(ie)
!              fyuu(ia)=pyuu(ie)
!              fxvv(ia)=pxvv(ie)
!              fyvv(ia)=pyvv(ie)
            endif
! if sur het(ie)=0 :cas ou het(ie)=0 on prend zero
!          else
!conc
!                fxhc(ia)=0.
!                fyhc(ia)=0.
!concf
!            fxuu(ia)=0.
!            fyuu(ia)=0.
!            fxvv(ia)=0.
!            fyvv(ia)=0.
!          endif
! if sur ie=0 :cas ou pas de voisin donc valeur nulle
!        else
!          if (eau(iev)) then
!conc
!          fxhc(ia)=pxhc(iev)
!          fyhc(ia)=pyhc(iev)
!concf
!            fxuu(ia)=pxuu(iev)
!            fyuu(ia)=pyuu(iev)
!            fxvv(ia)=pxvv(iev)
!            fyvv(ia)=pyvv(iev)
!          else
!conc
!                fxhc(ia)=0.
!                fyhc(ia)=0.
!concf
!            fxuu(ia)=0.
!            fyuu(ia)=0.
!            fxvv(ia)=0.
!            fyvv(ia)=0.
! fin du if sur eau dans iev
          endif
! fin du if sur ie egal a 0
        endif
!conc
!        if (diffus) then
!          flhc(ia)=fxhc(ia)*dix*xna(ia)+fyhc(ia)*diy*yna(ia)
!        else
!          flhc(ia)=sigm*ncvi(ia)*(fxhc(ia)*xna(ia)+fyhc(ia)*yna(ia))
!        endif
!concf
!        fluu(ia)=ncvi(ia)*(fxuu(ia)*xna(ia)+fyuu(ia)*yna(ia))
!        flvv(ia)=ncvi(ia)*(fxvv(ia)*xna(ia)+fyvv(ia)*yna(ia))
! si nrefa=-2
!       else
!conc
!        flhc(ia)=0.
!concf
!        fluu(ia)=0.
!        flvv(ia)=0.
!c fin du if sur nrefa
       endif

      end do
!$omp end do
!$omp end parallel

      !deallocate(ustar)
      !deallocate(fxuu,fyuu,fxvv,fyvv,pxuu,pyuu,pxvv,pyvv,ue,ve)
      return
      end subroutine laplaf

!******************************************************************************
       subroutine laplaz!(xbb,ybb,xybb,a11,a12,a22,deta)
!******************************************************************************
!     calcul du laplacien
!******************************************************************************
      use module_tableaux,only:nne,zfa,zfe,iae,het,quet,qvet,ieve,&
     &xna,yna,xta,yta,nrefa,fluu,flvv,eau,ieva,cvia,ncvi,fra,fre,&
     &pxze,pyze,eps1,eps2,paray,ne,na,nn,g!,tcomm1,tcomm2,tcomm
      use module_precision
      use module_loc, only:fxuu,&
     &fyuu,fxvv,fyvv,pxuu,pyuu&
     &,pxvv,pyvv,ue,ve
      use module_mpi,only:ne_loc,me,mailles_loc,naloc,aretes_loc,nmv,elt_voisins
      use module_laplacien
#ifdef WITH_MPI
      use module_messages
#endif /* mpi */

#if TRANSPORT_SOLIDE
      use module_ts,only:hce,pxhce,pyhce,hcal,pxhc,pyhc,flhc,diffus&
      &,sigm,dix,diy,cet
#endif

!$    use omp_lib

      implicit none

      integer ie,j,je,ia,iev,ialoc,i,ieloc
      real(wp) cb1qu,cb2qu,cb1qv,cb2qv
      !real(wp),dimension(:,:),allocatable::xbb,ybb,xybb
      real(wp) del

#if TRANSPORT_SOLIDE
      real(wp) :: cb1hc,cb2hc,fxhc(na),fyhc(na)
#endif


      !real(wp),dimension(:),allocatable::a11,a12,a22,deta
      !real(wp),dimension(:),allocatable::fxuu,
      !:fyuu,fxvv,fyvv,pxuu,pyuu
      !:,pxvv,pyvv,ue,ve


      !common/nvois/nne
      !common/param/eps1,eps2,paray
      !common/nomb/ne,na,nn
      !common/zare/zfa
      !common/zele/zfe
      !common/nrel/iae
      !common/tlig/het,quet,qvet
      !common/iele/ieve
      !common/vare/xna,yna,xta,yta
      !common/narf/nrefa
      !common/fluuvv/fluu,flvv
      !common/preso/eau
      !common/iela/ieva
      !common/gradz/pxze,pyze
      !common/nuqe/ncvi

      !common/grav/g
      !common/lcvia/cvia


      !allocate(fxuu(na),
      !:fyuu(na),fxvv(na),fyvv(na),pxuu(ne),pyuu(ne)
      !:,pxvv(ne),pyvv(ne),ue(ne),ve(ne))
!     calcul des pentes

!$omp parallel
!$omp do schedule(runtime)
      do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
        if (.not.eau(ie)) then
#if TRANSPORT_SOLIDE
          pxhc(ie)=0.0
          pyhc(ie)=0.0
#endif
          pxuu(ie)=0.
          pyuu(ie)=0.
          pxvv(ie)=0.
          pyvv(ie)=0.
        else
           ue(ie)=quet(ie)/het(ie)
           ve(ie)=qvet(ie)/het(ie)
        endif
      end do
!$omp end do
! remplace le message sur ue et ve voisins
!$omp do schedule(runtime)
      do i = 1,nmv
        ie=elt_voisins(i)
        if (eau(ie)) then
           ue(ie)=quet(ie)/het(ie)
           ve(ie)=qvet(ie)/het(ie)
        endif
      end do
!$omp end do
! calcul de pxuu,pyuu,pxvv,pyvv pente moyenne par maille de u et v sur x et y
! si he non nul
!$omp do schedule(runtime)
      do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
        if (eau(ie)) then
#if TRANSPORT_SOLIDE
            cb1hc=0.0
            cb2hc=0.0
#endif
           cb1qu = 0.0
           cb2qu = 0.0
           cb1qv= 0.0
           cb2qv= 0.0
           do j = 1,nne(ie)
            iev = ieve(ie,j)
            if (iev.ne.0) then
              if (eau(iev)) then
                    ia=iae(ie,j)
                    if (nrefa(ia).ne.-2) then
      if(zfa(ia).le.het(ie)+zfe(ie).or.zfa(ia).le.het(iev)+zfe(iev))then
#if TRANSPORT_SOLIDE
                  del = cet(ie)-cet(iev)
                  cb1hc = cb1hc - xbb(ie,j) * del
                  cb2hc = cb2hc- ybb(ie,j) * del

#endif
                  del = ue(ie)-ue(iev)
                   cb1qu = cb1qu - xbb(ie,j) * del
                  cb2qu= cb2qu- ybb(ie,j) * del
                  del = ve(ie)-ve(iev)
                  cb1qv = cb1qv - xbb(ie,j) * del
                  cb2qv= cb2qv- ybb(ie,j) * del
              endif
              endif
            endif
           endif
          end do
#if TRANSPORT_SOLIDE
          pxhc(ie) = (a22(ie) *cb1hc - a12(ie) *cb2hc) / deta(ie)
          pyhc(ie) = (a11(ie)*cb2hc-a12(ie)*cb1hc)/deta(ie)

#endif
          pxuu(ie) = (a22(ie) *cb1qu - a12(ie) *cb2qu) / deta(ie)
          pyuu(ie) = (a11(ie)*cb2qu-a12(ie)*cb1qu)/deta(ie)
          pxvv(ie) = (a22(ie) *cb1qv - a12(ie) *cb2qv) / deta(ie)
          pyvv(ie) = (a11(ie)*cb2qv-a12(ie)*cb1qv)/deta(ie)
! if sur he =0
        else
#if TRANSPORT_SOLIDE
          pxhc(ie) = 0.
          pyhc(ie) = 0.
#endif
          pxuu(ie) = 0.
          pyuu(ie) =  0.
          pxvv(ie) =  0.
          pyvv(ie) =  0.
! fin du if sur he=0
        endif
      end do
!$omp end do

#ifdef WITH_MPI
! envoie des messages
      !call cpu_time(tcomm1)
        call message_group_calcul(pxuu)
        call message_group_calcul(pyuu)
        call message_group_calcul(pxvv)
        call message_group_calcul(pyvv)
      !call cpu_time(tcomm2)
      !tcomm=tcomm+tcomm2-tcomm1

#if TRANSPORT_SOLIDE

        call message_group_calcul(pxhc)
        call message_group_calcul(pyhc)

#endif
#endif /* WITH_MPI */

! calcul de laplacien
!********************************************
!$omp do schedule(runtime)
      do ialoc=1,naloc
        ia=aretes_loc(ialoc)
                fxuu(ia)=0.
                fyuu(ia)=0.
                fxvv(ia)=0.
                fyvv(ia)=0.
                fluu(ia)=0.
                flvv(ia)=0.
                ncvi(ia)=0.
#if TRANSPORT_SOLIDE
                fxhc(ia)=0.
                fyhc(ia)=0.
                flhc(ia)=0.

#endif
       if (nrefa(ia).ne.-2) then
        ie=ieva(ia,1)
        iev=ieva(ia,2)
        if (ie.ne.0) then
          if (eau(ie)) then
            if (iev.ne.0) then
              if (eau(iev)) then
                do je=1,nne(ie)
                  if (iev.eq.ieve(ie,je)) then
                    j=je
                    go to 1902
                  endif
                end do
 1902          if (abs(xbb(ie,j)).lt.eps2) then
                    fxuu(ia)=0.
                  elseif (abs(pxuu(ie)) .gt. abs(pxuu(iev))) then
                    fxuu(ia)=(ue(iev)-ue(ie))/xybb(ie,j)&
     &*abs(pxuu(ie))*xbb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxuu(ie)**2+ybb(ie,j)**2*&
     &pyuu(ie)**2)
! if sur pxuu
                  elseif (abs(pxuu(iev)) .gt. eps2) then
                    fxuu(ia)=(ue(iev)-ue(ie))/xybb(ie,j)&
     &*abs(pxuu(iev))*xbb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxuu(iev)**2+ybb(ie,j)**2*&
     &pyuu(iev)**2)
! if sur pxuu
! cas ou les 2 pxuu sont nuls
                  else
                    fxuu(ia)=0.
! if sur pxuu
                  endif
                  if (abs(ybb(ie,j)).lt.eps2) then
                    fyuu(ia)=0.
                  elseif (abs(pyuu(ie)) .gt. abs(pyuu(iev))) then
                    fyuu(ia)=(ue(iev)-ue(ie))/xybb(ie,j)&
     &*abs(pyuu(ie))*ybb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxuu(ie)**2+ybb(ie,j)**2*&
     &pyuu(ie)**2)
! if sur pyuu
                  elseif (abs(pyuu(iev)) .gt. eps2) then
                    fyuu(ia)=(ue(iev)-ue(ie))/xybb(ie,j)&
     &*abs(pyuu(iev))*ybb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxuu(iev)**2+ybb(ie,j)**2*&
     &pyuu(iev)**2)
! cas ou les 2 pyuu sont nuls
                  else
                    fyuu(ia)=0.
! if sur pyuu
                 endif
                  if (abs(xbb(ie,j)).lt.eps2) then
                    fxvv(ia)=0.
                  elseif (abs(pxvv(ie)) .gt. abs(pxvv(iev))) then
                    fxvv(ia)=(ve(iev)-ve(ie))/xybb(ie,j)&
     &*abs(pxvv(ie))*xbb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxvv(ie)**2+ybb(ie,j)**2*&
     &pyvv(ie)**2)
! if sur pxvv
                  elseif (abs(pxvv(iev)) .gt. eps2) then
                    fxvv(ia)=(ve(iev)-ve(ie))/xybb(ie,j)&
     &*abs(pxvv(iev))*xbb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxvv(iev)**2+ybb(ie,j)**2*&
     &pyvv(iev)**2)
! cas ou les 2 pxvv sont nuls
                  else
                    fxvv(ia)=0.
! if sur pxvv
                  endif
                  if (abs(ybb(ie,j)).lt.eps2) then
                    fyvv(ia)=0.
                  elseif (abs(pyvv(ie)) .gt. abs(pyvv(iev))) then
                    fyvv(ia)=(ve(iev)-ve(ie))/xybb(ie,j)&
     &*abs(pyvv(ie))*ybb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxvv(ie)**2+ybb(ie,j)**2*&
     &pyvv(ie)**2)
! if sur pyvv
                  elseif (abs(pyvv(iev)) .gt. eps2) then
                    fyvv(ia)=(ve(iev)-ve(ie))/xybb(ie,j)&
     &*abs(pyvv(iev))*ybb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxvv(iev)**2+ybb(ie,j)**2*&
     &pyvv(iev)**2)
! if sur pyvv
! cas ou les 2 pyvv sont nuls
                  else
                    fyvv(ia)=0.
! if sur pyvv
                  endif




#if TRANSPORT_SOLIDE




                  if(abs(xbb(ie,j)).lt.eps2)then
                    fxhc(ia)=0.
                  elseif(abs(pxhc(ie)).gt.abs(pxhc(iev)))then
                    fxhc(ia)=(cet(iev)-cet(ie))/xybb(ie,j)&
     &*abs(pxhc(ie))*xbb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxhc(ie)**2+ybb(ie,j)**2*&
     &pyhc(ie)**2)
! if sur pxhc
                  elseif(abs(pxhc(iev)).gt.eps2)then
                    fxhc(ia)=(cet(iev)-cet(ie))/xybb(ie,j)&
     &*abs(pxhc(iev))*xbb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxhc(iev)**2+ybb(ie,j)**2*&
     &pyhc(iev)**2)
! if sur pxhc
! cas ou les 2 pxhc sont nuls
                  else
                    fxhc(ia)=0.
! if sur pxhc
                  endif
                  if(abs(ybb(ie,j)).lt.eps2)then
                    fyhc(ia)=0.
                  elseif(abs(pyhc(ie)).gt.abs(pyhc(iev)))then
                    fyhc(ia)=(cet(iev)-cet(ie))/xybb(ie,j)&
     &*abs(pyhc(ie))*ybb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxhc(ie)**2+ybb(ie,j)**2*&
     &pyhc(ie)**2)
! if sur pyhc
                  elseif(abs(pyhc(iev)).gt.eps2)then
                    fyhc(ia)=(cet(iev)-cet(ie))/xybb(ie,j)&
     &*abs(pyhc(iev))*ybb(ie,j)/sqrt(xbb(ie,j)**2&
     &*pxhc(iev)**2+ybb(ie,j)**2*&
     &pyhc(iev)**2)
! cas ou les 2 pyhc sont nuls
                  else
                    fyhc(ia)=0.
! if sur pyhc
                 endif


#endif





! if sur het(iev) : cas ou het(iev)=0  on prend zero
!              else
!conc
!                fxhc(ia)=0.
!                fyhc(ia)=0.
!concf
!                fxuu(ia)=0.
!                fyuu(ia)=0.
!                fxvv(ia)=0.
!                fyvv(ia)=0.
              endif
! if sur iev=0 :pas de voisin donc valeur nulle
!            else
!conc
!              fxhc(ia)=pxhc(ie)
!              fyhc(ia)=pyhc(ie)
!concf
!              fxuu(ia)=pxuu(ie)
!              fyuu(ia)=pyuu(ie)
!              fxvv(ia)=pxvv(ie)
!              fyvv(ia)=pyvv(ie)
            endif
! if sur het(ie)=0 :cas ou het(ie)=0 on prend zero
!          else
!conc
!                fxhc(ia)=0.
!                fyhc(ia)=0.
!concf
!            fxuu(ia)=0.
!            fyuu(ia)=0.
!            fxvv(ia)=0.
!            fyvv(ia)=0.
          endif
! if sur ie=0 :cas ou pas de voisin donc valeur nulle
!        else
!c          if (eau(iev)) then
!conc
!          fxhc(ia)=pxhc(iev)
!          fyhc(ia)=pyhc(iev)
!concf
!            fxuu(ia)=pxuu(iev)
!            fyuu(ia)=pyuu(iev)
!            fxvv(ia)=pxvv(iev)
!            fyvv(ia)=pyvv(iev)
!          else
!onc
!                fxhc(ia)=0.
!                fyhc(ia)=0.
!concf
!            fxuu(ia)=0.
!            fyuu(ia)=0.
!            fxvv(ia)=0.
!            fyvv(ia)=0.
! fin du if sur eau dans iev
!          endif
! fin du if sur ie egal a 0
        endif
        if (ie .gt. 0) then
          if (eau(ie)) then
            if (iev .gt. 0) then
              if (eau(iev)) then
        ncvi(ia)=cvia(ia)*((0.5*(het(ie)+het(iev)))**1.5*sqrt(g)&
     &*sqrt(abs(0.5*(pxze(ie)+pxze(iev))*xna(ia)&
     &+0.5*(pyze(ie)+pyze(iev))*yna(ia))))
!              else
!           ncvi(ia)=cvia(ia)*(het(ie))**1.5*sqrt(g)*
!     :sqrt(abs(pxze(ie)*xna(ia)+pyze(ie)*yna(ia)))
              endif
! si iev=0
!             else
!           ncvi(ia)=(het(ie))**1.5*sqrt(g)*
!     :sqrt(abs(pxze(ie)*xna(ia)+pyze(ie)*yna(ia)))
             endif
! si pas eau(ie)
!           else
!             if (iev .gt. 0) then
!                if (eau(iev)) then
!                   ncvi(ia)=cvia(ia)*(het(iev))**1.5*sqrt(g)*
!     :sqrt(abs(pxze(iev)*xna(ia)+pyze(iev)*yna(ia)))
!                else
!                  ncvi(ia)=0.
!                endif
! si iev =0
!            else
!                ncvi(ia)=0.
!            endif
! fin du if sur eau(ie)
          endif
! si ie=0
!          else
!            if (iev .gt. 0) then
!             if (eau(iev)) then
!               ncvi(ia)=cvia(ia)*(het(iev))**1.5*sqrt(g)*
!     :sqrt(abs(pxze(iev)*xna(ia)+pyze(iev)*yna(ia)))
!             else
!               ncvi(ia)=0.
!             endif
!c si iev =0
!          else
!              ncvi(ia)=0.
!          endif
! fin du if si ie=0
        endif
        fluu(ia)=(fxuu(ia)*xna(ia)+fyuu(ia)*yna(ia))*ncvi(ia)
        flvv(ia)=(fxvv(ia)*xna(ia)+fyvv(ia)*yna(ia))*ncvi(ia)

#if TRANSPORT_SOLIDE
        if(diffus)then
          flhc(ia)=fxhc(ia)*dix*xna(ia)+fyhc(ia)*diy*yna(ia)
        else
          flhc(ia)=sigm*ncvi(ia)*(fxhc(ia)*xna(ia)+fyhc(ia)*yna(ia))
        endif

#endif

! si nrefa=-2
!       else
!        ncvi(ia)=0.
!        fluu(ia)=0.
!        flvv(ia)=0.
! fin du if sur nrefa
       endif

      end do
!$omp end do
!$omp end parallel

      !deallocate(fxuu,fyuu,fxvv,fyvv,pxuu,pyuu,pxvv,pyvv,ue,ve)
      return
      end subroutine laplaz

!******************************************************************************
      subroutine leap(iapp,if)
!******************************************************************************
!     lecture/edition des apports de pluie
!  entree :
!  sortie :
!******************************************************************************
      use module_tableaux, only : napp,nchro,nbchro,ne,na,nn,&
     &appchr,tappch,nmchro,nmcoap
      use module_mpi, only:me, scribe
      implicit none

      integer ie,if,iapp

! nombre maximal de chroniques et de nombre de couples par chronique
      integer ichro,ncouap

      !common/nomb/ne,na,nn
      !common/nappor/napp,nchro,nbchro
      !common/apport/appchr,tappch
!
      read(iapp,'(10i8)')(napp(ie),ie=1,ne)
      read(iapp,'(i8)')nchro
      if (nchro .gt. nmchro) then
#if ENG
        if(me==scribe)write(*,*)'more than ',nmchro,' rain chronicles'
#else /* FRA */
        if(me==scribe)write(*,*)'plus de ',nmchro,' chroniques de pluie'
#endif /* FRA */
!        pause
        stop
      endif
      do ichro=1,nchro
        read(iapp,'( i8)') nbchro(ichro)
      if (nbchro(ichro) .gt. nmcoap) then
#if ENG
        if(me==scribe)write(*,*)'more than ',nmcoap,' times'
        if(me==scribe)write(*,*)'in the rain chronicle ',ichro
#else /* FRA */
        if(me==scribe)write(*,*)'plus de ',nmcoap,' temps'
        if(me==scribe)write(*,*)'dans la chronique de pluie ',ichro
#endif /* FRA */
!        pause
        stop
      endif
        do ncouap=1,nbchro(ichro)
          read(iapp,'(2f15.4)')appchr(ichro,ncouap),tappch(ichro,ncouap)
! transformation de mm/h en m/s
          appchr(ichro,ncouap)=appchr(ichro,ncouap) *0.001/3600.
        enddo
      enddo
      close(iapp)
!     edition apports
      if (me==scribe)then
      if (if.ne.0) then
        write (if,*)
        write (if,*) '************************************************'
#if ENG
        write (if,*) 'inputs read in mm/hour written here in m/s'
#else /* FRA */
        write (if,*) 'apports  lus en mm/heure ecrits ici en m/s'
#endif /* FRA */
!        write (if,*) '            apports  en mm/heure'
        write (if,*) '************************************************'
        write (if,*)
#if ENG
        write (if,*) 'nombre d elements = ',ne
#else /* FRA */
        write (if,*) 'number of elements = ',ne
#endif /* FRA */
        write (if,*) '=============================='
        do ie = 1,ne
#if ENG
          write (if,*) 'element nb =',ie,' chronicle : ',napp(ie)
#else /* FRA */
          write (if,*) 'element no =',ie,' chronique : ',napp(ie)
#endif /* FRA */
        enddo
#if ENG
        write(if,'(a30,i8)')'total number of chronicles',nchro
#else /* FRA */
        write(if,'(a30,i8)')'nombre total de chroniques',nchro
#endif /* FRA */
        do ichro=1,nchro
#if ENG
          write(if,'( a30,i8)')'chronicle number ',ichro
#else /* FRA */
          write(if,'( a30,i8)')'numero de la chronique ',ichro
#endif /* FRA */
          do ncouap=1,nbchro(ichro)
#if ENG
            write(if,'(a7,i8,a2,f15.10,a15,f15.4)')'  input',ncouap&
     &,' =',appchr(ichro,ncouap),'  input time =',tappch(ichro,ncouap)
#else /* FRA */
            write(if,'(a7,i8,a2,f15.10,a15,f15.4)')' apport',ncouap&
     &,' =',appchr(ichro,ncouap),' temps apport =',tappch(ichro,ncouap)
#endif /* FRA */
!            write(if,'(a7,i8,a2,f15.4,a15,f15.4)')' apport',ncouap,' =',&
!     &appchr(ichro,ncouap),' temps apport =',tappch(ichro,ncouap)
          enddo
          write(if,*)
        enddo

        close(if)
      endif
      end if ! me==scribe
      end subroutine leap

!******************************************************************************
      subroutine leci(xnb,ynb)
!******************************************************************************
!     lecture/edition des conditions initiales
!  entree :
!     ne   : nombre d elements
!     irep : indice de reprise
!  sortie :
!     he,que,qve : hauteur et vecteur debit de chaque element
!******************************************************************************

      use module_precision, only: wp
      use module_tableaux , only : eau,zfe,het,quet,zfm&
     &,qvet,ine,cofr,iae,xe,ye,ina,eps1,eps2,paray,etude,ne,na,nn,&
     &tm,tr,tinit,tmax,nbpt,ienb,trc,ifen,irep,ifdm,ifm,ifin,ifli,&
     &ifro,ifcl,ifci,ifr,ifrt,ifap,ifve,ift,id_tps,id_hyc,id_tpc,id_zfn
      use module_mpi, only: me,scribe

#if TRANSPORT_SOLIDE
    use module_ts,only:hcet,cet,dhe,vitfro,niux,niuy,dens,den,m&
    &,kds,d,tk,consnu,vch,zeta,conckg,solute,diame&
    &,sigmae,sigma0
    use module_tableaux,only : zfn

#if SEDVAR
     use module_ts,only:sedvar
!end if sedvar
#endif



#endif

      implicit none
!
      real(wp) dtrc,dtr
!      real(wp) que(*),qve(*),he(*),zfm,zfe(*),t,ze,tinit,tr,dtr
!        real(wp) ct1,ct2,x0
      integer ie,i,j,idec

      integer::nlimmax
      parameter (nlimmax=300)
      real(wp) v(nlimmax),u(nlimmax),h(nlimmax),&
     & xnb(nlimmax),ynb(nlimmax)

      real(wp) dx,dist1,distnb
      character*6 char*1
      logical vitesse!,eau(nemax)
      real(wp),dimension(:),allocatable::ze

#if TRANSPORT_SOLIDE
      character char1*1
      logical concentr
      integer in
      real(WP) :: co(nlimmax),dh(nlimmax),vf(nlimmax)


#endif

      allocate(ze(ne))

!
! test sur irep
!**************
      if (irep.eq.0) then
      if (me==scribe)then
#if ENG
      write (*,*) 'computing additional cell data'
#else /* FRA */
      write (*,*) 'calcul donnees complementaires elements'
#endif /* FRA */
      end if ! me==scribe
      call cedce(ifm)

!     calcul des donnees topographiques elements et aretes
!*********************************************************
      call topo

! surface libre donnee dans le fichier etude.cin
!***********************************************
#if TRANSPORT_SOLIDE
  read(ifin,'(f15.3,a1,a1)')tm,char,char1

#else
  read(ifin,'(f15.3,a1)')tm,char
#endif


        if (abs(tinit-tm) .gt. .01) then
#if ENG
          if(me==scribe)write(*,*)'the initial time ',tinit,' doesn''t match'
          if(me==scribe)write(*,*)' the initial water line time ',tm
#else /* FRA */
          if(me==scribe)write(*,*)'le temps initial ',tinit,'ne correspond pas'
          if(me==scribe)write(*,*)' a celui de la ligne d''eau initiale ',tm
#endif /* FRA */
          stop
        endif
        if (char.eq.'v'.or.char.eq.'v') then
          if (me==scribe)then
#if ENG
            write(*,*)'initial conditions in velocity'
#else /* FRA */
            write(*,*)'conditions initiales en vitesses'
#endif /* FRA */
          end if ! me==scribe
          vitesse=.true.
        else
          if (me==scribe)then
#if ENG
            write(*,*)'initial conditions in flows'
#else /* FRA */
            write(*,*)'conditions initiales en debits'
#endif /* FRA */
          end if ! me==scribe
          vitesse=.false.
        endif
        tinit=tm
        tr=tm
        trc=tm
        read(ifin,'(8f10.4)')(het(ie),ie=1,ne)
        read(ifin,'(8f10.5)')(quet(ie),ie=1,ne)
        read(ifin,'(8f10.5)')(qvet(ie),ie=1,ne)
        read(ifin,'(8f10.3)')(ze(ie),ie=1,ne)



#if TRANSPORT_SOLIDE
        if(char1.eq.'h'.or.char1.eq.'h')then
#if ENG
           write(*,*)'initial conditions in conc. * height'
#else /* FRA */
           write(*,*)'conditions initiales en conc. * haut. '
#endif /* FRA */
           concentr=.false.
        else
#if ENG
           write(*,*)'initial conditions in concentrations'
#else /* FRA */
           write(*,*)'conditions initiales en concentrations'
#endif /* FRA */
           concentr=.true.
        endif
        read(ifin,'(8f10.7)')(hcet(ie),ie=1,ne)
! initialisation faite dans lecsed si sedvar false
!        do ie=1,ne
!            diame(ie)=d
!            sigmae(ie)=sigma0
!        enddo
#if SEDVAR
        if(sedvar)then
          read(ifin,'(8f10.7)',end=2000)(diame(ie),ie=1,ne)
          read(ifin,'(8f10.7)')(sigmae(ie),ie=1,ne)
#if ENG
           write(*,*)'initial conditions with diameters'
#else /* FRA */
          write(*,*)'conditions initiales avec diametres'
#endif /* FRA */
             do ie=1,ne
               if(diame(ie).lt.eps2)then
#if ENG
                 write(*,*)'initial diameter correction of cell ',ie
#else /* FRA */
                 write(*,*)'correction diametre initial maille ',ie
#endif /* FRA */
                     diame(ie)=d
               endif
               if(sigmae(ie).lt.1.)then
                  sigmae(ie)=sigma0
#if ENG
                 write(*,*)'initial size correction of cell ',ie
#else /* FRA */
                 write(*,*)'correction etendue initiale maille ',ie
#endif /* FRA */
               endif
             enddo
        endif
!end if sedvar
#endif

!end if TRANSPORT_SOLIDE
#endif


        do 20 ie=1,ne
        if (het(ie) .gt. 900.) then
          het(ie)=ze(ie)-zfe(ie)
!          het(ie)=ze(ie)-zfe(ie)-zfm
        endif
        if (het(ie).le.paray) then
          het(ie)=0.
          quet(ie)=0.
          qvet(ie)=0.
          eau(ie)=.false.

#if TRANSPORT_SOLIDE
          hcet(ie)=0.
          cet(ie)=0.
          dhe(ie)=0.
#endif



        else
          eau(ie)=.true.
          if (vitesse) then
            quet(ie)=quet(ie)*het(ie)
            qvet(ie)=qvet(ie)*het(ie)
          endif

#if TRANSPORT_SOLIDE
          if(het(ie).lt.paray)then
!          if(het(ie).lt.eps1)then
            cet(ie)=0.
            hcet(ie)=0.
          elseif(concentr)then
            cet(ie)=hcet(ie)
            hcet(ie)=hcet(ie)*het(ie)
          else
            cet(ie)=hcet(ie)/het(ie)
          endif
          dhe(ie)=0.
!          vitfro(ie)=9999.99
#endif

        endif
 20     continue
        close(ifin)
!      open(id_tps,file=etude//'.tps',status='new',form='unformatted')
      open(id_tps,file=trim(etude)//'.tps',status='unknown',action='write')


#if TRANSPORT_SOLIDE
!      if(avects)then
      open(id_tpc,file=trim(etude)//'.tpc',status='unknown',action='write')
      if(.not.solute)then
!           if(sedvar)then
!             do ie=1,ne
!               diame(ie)=dhe(ie)
!               sigmae(ie)=vitfro(ie)
!             enddo
! fin du if sur sedvar
!           endif
!     ouverture du fichier comprenant la cote des noeuds
!*******************************************
        open(id_zfn,file=trim(etude)//'.zfn',status='unknown')
      endif
!      endif

#endif


! si irep different de 0 c'est a dire il y reprise
        else
#if ENG
          write (*,*) 'reading initial conditions by restart'
#else /* FRA */
          write (*,*) 'lecture conditions initiales par reprise'
#endif /* FRA */
!  surface libre donnee dans le fichier etude.tps a tm
!**************************************************
        open(id_tps,file=trim(etude)//'.tps',status='old',action='readwrite',err=90)
  3     read(id_tps,'(f15.3)',end=4)tm
        read(id_tps,'(8f10.5)')(het(ie),ie=1,ne)
        read(id_tps,'(8f10.5)')(quet(ie),ie=1,ne)
        read(id_tps,'(8f10.5)',end=5)(qvet(ie),ie=1,ne)
        if (abs(tinit-tm).lt..01)go to 6
         go to 3
 5      if (abs(tinit-tm).lt..01)go to 6
 4      if(me==scribe)then
#if ENG
          write(*,*)'initial time doesn''t match '
          write(*,*)' any water line stored'
          write(*,*)'last water line = ',tm
#else /* FRA */
          write(*,*)'le temps initial ne correspond '
          write(*,*)' a aucune ligne d''eau stockee'
          write(*,*)'derniere ligne d''eau = ',tm
#endif /* FRA */
        end if
        stop
 6      tinit=tm


#if TRANSPORT_SOLIDE

        open(id_tpc,file=trim(etude)//'.tpc',status='old',action='readwrite',err=91)
 103    read(id_tpc,'(f15.3)',end=4)tm
        read(id_tpc,'(8f10.7)')(cet(ie),ie=1,ne)
        read(id_tpc,'(8f10.5)')(dhe(ie),ie=1,ne)
        read(id_tpc,'(8f10.5)',end=105)(vitfro(ie),ie=1,ne)
        if (abs(tinit-tm).lt..01)go to 106
         go to 103
 105      if (abs(tinit-tm).lt..01)go to 106
        if(me==scribe)then
#if ENG
          write(*,*)'initial time doesn''t match '
          write(*,*)' any concentration stored'
          write(*,*)'last concentration = ',tm
#else /* FRA */
          write(*,*)'le temps initial ne correspond '
          write(*,*)' a aucune concentration stockee'
          write(*,*)'derniere concentration = ',tm
#endif /* FRA */
        end if
        stop
 106    if(.not.solute)then
        open(id_zfn,file=trim(etude)//'.zfn',status='old',action='readwrite',err=92)
 1103   read(id_zfn,'(f15.3)',end=4)tm
        read(id_zfn,'(10f8.3)',end=1105)(zfn(in),in=1,nn)
        if (abs(tinit-tm).lt..01)go to 1106
        go to 1103
 1105   if (abs(tinit-tm).lt..01)go to 1106
        if(me==scribe)then
#if ENG
          write(*,*)'initial time doesn''t match '
          write(*,*)' any topography stored'
          write(*,*)'last topography = ',tm
#else /* FRA */
          write(*,*)'le temps initial ne correspond '
          write(*,*)' a aucune topographie stockee'
          write(*,*)'derniere topographie = ',tm
#endif /* FRA */
        end if
        stop
! 1106   tinit=tm
 1106   continue

#if SEDVAR
        if(sedvar)then
#if ENG
          write(*,*)'initial conditions in tpc with diameters'
#else /* FRA */
          write(*,*)'conditions initiales dans tpc avec diametres'
#endif /* FRA */
             do ie=1,ne
               diame(ie)=dhe(ie)
               if(diame(ie).lt.eps2)then
#if ENG
                 write(*,*)'correction initial diameter of cell ',ie
#else /* FRA */
                 write(*,*)'correction diametre initial maille ',ie
#endif /* FRA */
                     diame(ie)=d
               endif
               sigmae(ie)=vitfro(ie)
               if(sigmae(ie).lt.1.)then
                  sigmae(ie)=sigma0
#if ENG
                 write(*,*)'correction initial size of cell ',ie
#else /* FRA */
                 write(*,*)'correction etendue initiale maille ',ie
#endif /* FRA */
               endif
             enddo
! fin du if sur sedvar
           endif
!end if sedvar
#endif
! fin du if sur solute
        endif

#endif

        tr=tm
        do 11 ie=1,ne
          if (het(ie).le.paray) then
            het(ie)=0.
            eau(ie)=.false.

#if TRANSPORT_SOLIDE
            hcet(ie)=0.
            cet(ie)=0.
#endif
          else
            eau(ie)=.true.

#if TRANSPORT_SOLIDE
            hcet(ie)=het(ie)*cet(ie)
#endif


          endif
 11     continue
! deplace ici pour que la cote initiale soit celle de zfn et pas celle de dat
        if (me==scribe)then
#if ENG
          write (*,*) 'computing additional cell data'
#else /* FRA */
          write (*,*) 'calcul donnees complementaires elements'
#endif /* FRA */
        end if ! me==scribe
        call cedce(ifm)

!     calcul des donnees topographiques elements et aretes
!*********************************************************
         call topo

! correction le 22/10/95 pour que le fichier out soit ecrit en cas de reprise
!        tr=t+dtr
        if (ift .gt. 0) then
          do 151 j=1,na
           read(ift,*,end=152)trc
           read(ift,*,end=152)(v(i),i=1,nbpt)
           read(ift,*,end=152)(u(i),i=1,nbpt)
           read(ift,*,end=152)(h(i),i=1,nbpt)

#if TRANSPORT_SOLIDE
! lecture dans hyc
           read(id_hyc,*,end=152)trc
           read(id_hyc,*,end=152)(co(i),i=1,nbpt)
           read(id_hyc,*,end=152)(dh(i),i=1,nbpt)
           read(id_hyc,*,end=152)(vf(i),i=1,nbpt)
#endif

           if (trc+dtrc .gt. tm-eps1)go to 152
 151      continue
 152      trc=tm+dtrc
        endif
! fin du if sur reprise
      endif
      if (ift .gt. 0) then
        do 153 i=1,nbpt
          do idec=3,0,-1
! 0.1 car x et y ecrit en f10.1 mais possible erreur d'arrondi
           dx=0.1*10.**(-idec)
           do 154 ie=1,ne
             if (abs(xnb(i)-xe(ie)).lt.dx) then
               if (abs(ynb(i)-ye(ie)).lt.dx) then
                 ienb(i)=ie
                 go to 153
               endif
             endif
 154       continue
         enddo
#if ENG
          if(me==scribe)write(*,*)'error in the coordinates of storage points'
          if(me==scribe)write(*,*)' for hydrographs (fichier .dtr)'
          if(me==scribe)write(*,*)'point number ',i
#else /* FRA */
          if(me==scribe)write(*,*)'erreur dans les coordonnees des points de'
          if(me==scribe)write(*,*)'stockage des hydrogrammes (fichier .dtr)'
          if(me==scribe)write(*,*)'point numero ',i
#endif /* FRA */
          dist1=sqrt((xnb(i)-xe(1))**2+(ynb(i)-ye(1))**2)
          ienb(i)=1
          do 954 ie=2,ne
             distnb=sqrt((xnb(i)-xe(ie))**2+(ynb(i)-ye(ie))**2)
             if (distnb.lt.dist1) then
               ienb(i)=ie
               dist1=distnb
             endif
 954      continue
 153   continue
! si ift = 0 pas de fichier trc donc on prend un temps tres grand
      else
          trc=1.d20
      endif

      deallocate(ze)
!     edition conditions initiales
      if (me==scribe)then
      if (ifci.ne.0) then
      write (ifci,*)
      write (ifci,*) '************************************************'
#if ENG
      write (ifci,*) '            initial conditions    '
      write (ifci,*) '************************************************'
      write (ifci,*) 'time = ',tm
      write (ifci,*) 'number of elements = ',ne
      write (ifci,*) '=============================='
      do 200 ie = 1,ne
         write (ifci,*) 'element nb =',ie,' height : ',het(ie)
         write (ifci,*) 'flow by ox,oy : ',quet(ie),'  ',qvet(ie)

#if TRANSPORT_SOLIDE
         write (ifci,*) 'concentration * height: ',hcet(ie)
         write (ifci,*) 'diameter: ',diame(ie)
         write (ifci,*) 'granulometric extend: ',sigmae(ie)
#endif/* TRANSPORT_SOLIDE */
#else /* FRA */
      write (ifci,*) '            conditions initiales  '
      write (ifci,*) '************************************************'
      write (ifci,*) 'temps = ',tm
      write (ifci,*) 'nombre d elements = ',ne
      write (ifci,*) '=============================='
      do 200 ie = 1,ne
         write (ifci,*) 'element no =',ie,' hauteur : ',het(ie)
         write (ifci,*) 'debits selon ox,oy : ',quet(ie),'  ',qvet(ie)

#if TRANSPORT_SOLIDE
         write (ifci,*) 'concentration * hauteur: ',hcet(ie)
         write (ifci,*) 'diametre: ',diame(ie)
         write (ifci,*) 'etendue granulometrique: ',sigmae(ie)
#endif
#endif /* FRA */

 200  continue
      close(ifci)
      endif
! 300  endif
      end if ! me==scribe

#if TRANSPORT_SOLIDE
! si les concentration sont en kg/m3 il faut les remettre en m3/m3
        if(conckg)then
          do 1011 ie=1,ne
            hcet(ie)=hcet(ie)/dens
            cet(ie)=cet(ie)/dens
 1011     continue
        endif
#endif
      return

#if ENG
 91   if(me==scribe)write(*,*)'restart and no TPS file found'
#else /* FRA */
 90   if(me==scribe)write(*,*)'reprise et pas de fichier TPS existant'
#endif /* FRA */
!      pause
      stop

#if TRANSPORT_SOLIDE
#if ENG
 91   if(me==scribe)write(*,*)'restart and no TPC file found'
#else /* FRA */
 91   if(me==scribe)write(*,*)'reprise et pas de fichier TPC existant'
#endif /* FRA */
!      pause
      stop
#if ENG
 91   if(me==scribe)write(*,*)'restart and no ZFN file found'
#else /* FRA */
 92   if(me==scribe)write(*,*)'reprise et pas de fichier ZFN existant'
#endif /* FRA */
!      pause
      stop
#if ENG
 91   if(me==scribe)write(*,*)'existing SED file and incomplete CIN file'
#else /* FRA */
 2000 if(me==scribe)write(*,*)'fichier SED existant et fichier CIN incomplet'
#endif /* FRA */
!      pause
      stop
#endif

      end subroutine leci

!******************************************************************************
      subroutine caldeb(na30)
! calcule le debit sur chaque arete limite ou debit defini globalement
!******************************************************************************
!      implicit logical (a-z)
      use module_tableaux, only : nrefa,hal,qnal,qtal,xna,yna,xta,&
     &yta,het,quet,qvet,se,la,ieva,fra,fre,nalmax,ne,na,nn,nas,iac&
     &,eps1,eps2,paray
      use module_precision
      use module_mpi,only:me,machine_aretes30

!$    use omp_lib

      implicit none

!      integer ltmax,nalmax
!      parameter (ltmax=1000)
!      parameter (ltmax=20,maxnal=1000)
      integer ia,ias,i,ie,iev
      integer na30
      real(wp) qn,qt,qn2,qt2,surf,xlon,fra2

      !common/nomb/ne,na,nn
      !common/nare/nas,iac
      !common/narf/nrefa
      !common/clim/hal,qnal,qtal
      !common/param/eps1,eps2,paray
      !common/vare/xna,yna,xta,yta
      !common/tlig/het,quet,qvet
      !common/dose/se,la
      !common/iela/ieva
      !common/frot/fra,fre
! boucle sur tous les groupes d'aretes
! on fait une premiere boucle sur les impairs
! correspondant a une reparttion en debit puis une deuxiemme
! sur les pairs pour une repartition en vitesse uniforme

!$omp parallel
!$omp do schedule(runtime)
      do i=31,30+na30,3
        if(machine_aretes30(i)/=me)cycle
! qn2 est le debit a l'interieur du modele
       qn2=0.
       qt2=0.
! qn est le debit entrant en m3/s
       qn=0.
       qt=0.
       surf=0.
       xlon=0.
! boucle sur toutes les aretes rentrantes
       do 1 ias = 1,nas
        if (nrefa(iac(ias)).eq.i ) then
          ia=iac(ias)
          ie=ieva(ia,1)
          if (ie.eq.0) then
               xlon=xlon+la(ia)*het(ieva(ia,2))
          else
               xlon=xlon+la(ia)*het(ie)
          endif
          if (hal(ia) .gt. paray) then
               surf=surf+la(ia)*hal(ia)
          endif
! qn contient la valeur limite globale positive si rentrante
          if (abs(qn).ne.0.) then
! on a deja lu une valeur de qnal on teste si les valeurs suivantes
! sont egales a la premiere
            if (abs(qnal(ia)-qn) .gt. eps2) then
#if ENG
              write(*,*)'reference edge group ',i
              write(*,*)'global incoming flow values are'
              write(*,*)'different for 2 edges '
              write(*,*)'qn1 =',qn,'qn2 =',qnal(ia)
              write(*,*)'keeping the first flow value'
#else /* FRA */
              write(*,*)'groupe d''aretes de reference',i
              write(*,*)'les valeurs du debit entrant global sont'
              write(*,*)'differentes pour 2 aretes '
              write(*,*)'qn1 =',qn,'qn2 =',qnal(ia)
              write(*,*)'on garde la premiere valeur de debit'
#endif /* FRA */
            endif
          else
            qn=qnal(ia)
            qt=qtal(ia)
            if (qn.lt.0.) then
#if ENG
      write(*,*)'non-incoming debit for reference edges',i
#else /* FRA */
      write(*,*)'debit non entrant pour les aretes de reference',i
#endif /* FRA */
!      write(*,*)'impossible de le modeliser ainsi'
!      stop
            endif
! fin du if sur qn=0
          endif
! fin du if sur nrefa = i
        endif
 1     continue
         if (surf .gt. eps2) then
           qn=qn/surf
           qt=qt/surf
           do 7 ias = 1,nas
            if (nrefa(iac(ias)).eq.i) then
             ia=iac(ias)
             if (hal(ia) .gt. paray) then
               qnal(ia)=qn*hal(ia)
!               write(*,*)'surf',ia,qnal(ia)
               qtal(ia)=qt*hal(ia)
             else
               qnal(ia)=0.
               qtal(ia)=0.
             endif
! fin du if sur l'appartenance au groupe
            endif
 7         continue
! si surf=0 hauteurs d'eau amont non donnees repartition selon les debits a
! l'interieur
          else
! boucle sur toutes les aretes rentrantes
       do 9 ias = 1,nas
        if (nrefa(iac(ias)).eq.i) then
          ia=iac(ias)
          ie=ieva(ia,1)
          if (ie.eq.0) then
! element a gauche nul : la normale est rentrante
            ie=ieva(ia,2)
            qnal(ia)=quet(ie)*xna(ia)+qvet(ie)*yna(ia)
            qtal(ia)=quet(ie)*xta(ia)+qvet(ie)*yta(ia)
          else
            qnal(ia)=-(quet(ie)*xna(ia)+qvet(ie)*yna(ia))
            qtal(ia)=-(quet(ie)*xta(ia)+qvet(ie)*yta(ia))
          endif
! si qn entrant
!          if (qnal(ia) .gt. eps2.and.(hal(ia) .gt. paray.or.surf.lt.eps2))
!     :     then
!          if (qnal(ia) .gt. eps2) then
          if (qnal(ia)*qn .gt. 0.) then
            qn2=qn2+qnal(ia)*la(ia)
            qt2=qt2+qtal(ia)*la(ia)
          endif
! fin du if sur nrefa = i
        endif
 9     continue
       if (abs(qn2) .gt. eps2.and.abs(qt2) .gt. eps2) then
         do 4 ias = 1,nas
           if (nrefa(iac(ias)).eq.i) then
             ia=iac(ias)
!          if (qnal(ia) .gt. eps2.and.(hal(ia) .gt. paray.or.surf.lt.eps2))
!     :     then
             if (qnal(ia)*qn .gt. 0.) then
! on repartit selon les valeurs interieures
               qnal(ia)=qn*qnal(ia)/qn2
               qtal(ia)=qt*qtal(ia)/qt2
!               if (ieva(ia,2).eq.0) then
!                 qnal(ia)=-qnal(ia)
!                 qtal(ia)=-qtal(ia)
!               endif
! si qnal nul ou negatif
             else
               qnal(ia)=0.
               qtal(ia)=0.
             endif
! fin du if sur l'appartenance au groupe
           endif
 4       continue
! si qt2 est nul repartition selon qn
       elseif (abs(qn2) .gt. eps2) then
         do 5 ias = 1,nas
           if (nrefa(iac(ias)).eq.i) then
             ia=iac(ias)
!          if (qnal(ia) .gt. eps2.and.(hal(ia) .gt. paray.or.surf.lt.eps2))
!     :     then
             if (qnal(ia)*qn .gt. 0.) then
! on repartit selon les valeurs interieures
               qnal(ia)=qn*qnal(ia)/qn2
!               write(*,*)'qn',ia,qnal(ia)
               qtal(ia)=qt*qnal(ia)/qn2
!               if (ieva(ia,2).eq.0) then
!                 qnal(ia)=-qnal(ia)
!                 qtal(ia)=-qtal(ia)
!               endif
! si qnal nul ou negatif
             else
               qnal(ia)=0.
               qtal(ia)=0.
             endif
! fin du if sur l'appartenance au groupe
           endif
 5       continue
! si qn2 est nul repartition uniforme en vitesse
! si toutes les hauteurs sont nulles repartition uniforme selon la longueur de
! l'arete
!       else
! si hauteur limite non donnee on prend les hauteurs interieures
         elseif (xlon .gt. eps2) then
           qn=qn/xlon
           qt=qt/xlon
           do 8 ias = 1,nas
            if (nrefa(iac(ias)).eq.i) then
             ia=iac(ias)
             ie=ieva(ia,1)
             if (ie.eq.0) then
               qnal(ia)=qn*het(ieva(ia,2))
!               write(*,*)'xlon1',ia,qnal(ia)
               qtal(ia)=qt*het(ieva(ia,2))
             else
               qnal(ia)=qn*het(ie)
!               write(*,*)'xlon2',ia,qnal(ia)
               qtal(ia)=qt*het(ie)
             endif
! fin du if sur l'appartenance au groupe
            endif
 8         continue
!         else
! si surf=0. et xlon =0.
! on ne fait rien car les hauteurs sont nulles a l'interieur et a la limite
!          do 9 ias = 1,nas
!           if (nrefa(iac(ias)).eq.i) then
!             ia=iac(ias)
!             qnal(ia)=qn*la(ia)
!             qtal(ia)=qt*la(ia)
!c fin du if sur l'appartenance au groupe
!           endif
! 9        continue
! fin du if sur valeur qn2,qt2
        endif
! fin du if sur surf=0.
         endif
       do 10 ias = 1,nas
        if (nrefa(iac(ias)).eq.i) then
          ia=iac(ias)
          ie=ieva(ia,2)
          if (ie.eq.0) then
! element a droite nul : la normale est sortante
! on inverse qnal et qtal qui sont donnes sur la normale entrante
            qnal(ia)=-qnal(ia)
            qtal(ia)=-qtal(ia)
          endif
        endif
 10    continue
! fin de la  premiere boucle sur les differents groupes d'aretes
      end do
!$omp end do
! deuxieme boucle sur tous les groupes d'aretes (reparttion en vitesse uniforme)
!$omp do schedule(runtime)
      do i=32,30+na30,3
        if(machine_aretes30(i)/=me)cycle
! qn2 est le debit a l'interieur du modele
       qn2=0.
       qt2=0.
! qn est le debit entrant en m3/s
       qn=0.
       qt=0.
       surf=0.
       xlon=0.
! boucle sur toutes les aretes rentrantes
       do 101 ias = 1,nas
        if (nrefa(iac(ias)).eq.i) then
          ia=iac(ias)
          ie=ieva(ia,1)
          if (ie.eq.0) then
               xlon=xlon+la(ia)*het(ieva(ia,2))
          else
               xlon=xlon+la(ia)*het(ie)
          endif
          if (hal(ia) .gt. paray) then
               surf=surf+la(ia)*hal(ia)
          endif
! qn contient la valeur limite globale positive si rentrante
          if (abs(qn).ne.0.) then
! on a deja lu une valeur de qnal on teste si les valeurs suivantes
! sont egales a la premiere
            if (abs(qnal(ia)-qn) .gt. eps2) then
#if ENG
              write(*,*)'reference edge group ',i
              write(*,*)'global incoming flow values are'
              write(*,*)'different for 2 edges '
              write(*,*)'qn1 =',qn,'qn2 =',qnal(ia)
              write(*,*)'keeping the first flow value'
#else /* FRA */
              write(*,*)'groupe d''aretes de reference',i
              write(*,*)'les valeurs du debit entrant global sont'
              write(*,*)'differentes pour 2 aretes '
              write(*,*)'qn1 =',qn,'qn2 =',qnal(ia)
              write(*,*)'on garde la premiere valeur de debit'
#endif /* FRA */
            endif
          else
            qn=qnal(ia)
            qt=qtal(ia)
            if (qn.lt.0.) then
#if ENG
      write(*,*)'non-incoming debit for reference edges',i
#else /* FRA */
      write(*,*)'debit non entrant pour les aretes de reference',i
#endif /* FRA */
!      write(*,*)'impossible de le modeliser ainsi'
!      stop
            endif
! fin du if sur qn=0
          endif
! fin du if sur nrefa = i
        endif
 101   continue
         if (surf .gt. eps2) then
           qn=qn/surf
           qt=qt/surf
           do 107 ias = 1,nas
            if (nrefa(iac(ias)).eq.i) then
             ia=iac(ias)
             if (hal(ia) .gt. paray) then
               qnal(ia)=qn*hal(ia)
!               write(*,*)'surf',ia,qnal(ia)
               qtal(ia)=qt*hal(ia)
             else
               qnal(ia)=0.
               qtal(ia)=0.
             endif
! fin du if sur l'appartenance au groupe
            endif
 107         continue
! si surf=0 hauteurs d'eau amont non donnees repartition selon les hauteurs a
! l'interieur
         elseif (xlon .gt. eps2) then
           qn=qn/xlon
           qt=qt/xlon
           do 108 ias = 1,nas
            if (nrefa(iac(ias)).eq.i) then
             ia=iac(ias)
             ie=ieva(ia,1)
             if (ie.eq.0) then
               qnal(ia)=qn*het(ieva(ia,2))
!               write(*,*)'xlon1',ia,qnal(ia)
               qtal(ia)=qt*het(ieva(ia,2))
             else
               qnal(ia)=qn*het(ie)
!               write(*,*)'xlon2',ia,qnal(ia)
               qtal(ia)=qt*het(ie)
             endif
! fin du if sur l'appartenance au groupe
            endif
 108       continue
!         else
! si surf=0. et xlon =0.
! on ne fait rien car les hauteurs sont nulles a l'interieur et a la limite
! fin du if sur surf=0.
         endif
       do 110 ias = 1,nas
        if (nrefa(iac(ias)).eq.i) then
          ia=iac(ias)
          ie=ieva(ia,2)
          if (ie.eq.0) then
! element a droite nul : la normale est sortante
! on inverse qnal et qtal qui sont donnes sur la normale entrante
            qnal(ia)=-qnal(ia)
            qtal(ia)=-qtal(ia)
          endif
        endif
 110   continue
! fin de la deuxieme boucle sur les differents groupes d'aretes
      end do
!$omp end do
! deuxieme boucle sur tous les groupes d'aretes (reparttion en vitesse uniforme
!$omp do schedule(runtime)
      do i=33,30+na30,3
        if(machine_aretes30(i)/=me)cycle
! qn2 est le debit a l'interieur du modele
       qn2=0.
       qt2=0.
! qn est le debit entrant en m3/s
       qn=0.
       qt=0.
       surf=0.
       xlon=0.
! boucle sur toutes les aretes rentrantes
       do 201 ias = 1,nas
        if (nrefa(iac(ias)).eq.i) then
          ia=iac(ias)
          ie=ieva(ia,1)
          if (ie.eq.0) then
               iev=ieva(ia,2)
               fra2=max(fra(iev),fre(iev)*het(iev)**0.3333333)
               xlon=xlon+la(ia)*het(iev)**1.1666667*sqrt(fra2)
          else
               fra2=max(fra(ie),fre(ie)*het(ie)**0.3333333)
               xlon=xlon+la(ia)*het(ie)**1.1666667*sqrt(fra2)
          endif
          if (hal(ia) .gt. paray) then
               surf=surf+la(ia)*hal(ia)
          endif
! qn contient la valeur limite globale positive si rentrante
          if (abs(qn).ne.0.) then
! on a deja lu une valeur de qnal on teste si les valeurs suivantes
! sont egales a la premiere
            if (abs(qnal(ia)-qn) .gt. eps2) then
#if ENG
              write(*,*)'reference edge group ',i
              write(*,*)'global incoming flow values are'
              write(*,*)'different for 2 edges '
              write(*,*)'qn1 =',qn,'qn2 =',qnal(ia)
              write(*,*)'keeping the first flow value'
#else /* FRA */
              write(*,*)'groupe d''aretes de reference',i
              write(*,*)'les valeurs du debit entrant global sont'
              write(*,*)'differentes pour 2 aretes '
              write(*,*)'qn1 =',qn,'qn2 =',qnal(ia)
              write(*,*)'on garde la premiere valeur de debit'
#endif /* FRA */
            endif
          else
            qn=qnal(ia)
            qt=qtal(ia)
            if (qn.lt.0.) then
#if ENG
      write(*,*)'non-incoming debit for reference edges',i
#else /* FRA */
      write(*,*)'debit non entrant pour les aretes de reference',i
#endif /* FRA */
!      write(*,*)'impossible de le modeliser ainsi'
!      stop
            endif
! fin du if sur qn=0
          endif
! fin du if sur nrefa = i
        endif
 201   continue
         if (surf .gt. eps2) then
           qn=qn/surf
           qt=qt/surf
           do 207 ias = 1,nas
            if (nrefa(iac(ias)).eq.i) then
             ia=iac(ias)
             if (hal(ia) .gt. paray) then
               qnal(ia)=qn*hal(ia)
!               write(*,*)'surf',ia,qnal(ia)
               qtal(ia)=qt*hal(ia)
             else
               qnal(ia)=0.
               qtal(ia)=0.
             endif
! fin du if sur l'appartenance au groupe
            endif
 207         continue
! si surf=0 hauteurs d'eau amont non donnees repartition selon les hauteurs a
! l'interieur
         elseif (xlon .gt. eps2) then
           qn=qn/xlon
           qt=qt/xlon
           do 208 ias = 1,nas
            if (nrefa(iac(ias)).eq.i) then
             ia=iac(ias)
             ie=ieva(ia,1)
             if (ie.eq.0) then
               iev=ieva(ia,2)
               fra2=max(fra(iev),fre(iev)*het(iev)**0.3333333)
               qnal(ia)=qn*het(iev)**1.1666667*sqrt(fra2)
               qtal(ia)=qt*het(iev)**1.1666667*sqrt(fra2)
             else
               fra2=max(fra(ie),fre(ie)*het(ie)**0.3333333)
               qnal(ia)=qn*het(ie)**1.1666667*sqrt(fra2)
               qtal(ia)=qt*het(ie)**1.1666667*sqrt(fra2)
             endif
! fin du if sur l'appartenance au groupe
            endif
 208       continue
!         else
! si surf=0. et xlon =0.
! on ne fait rien car les hauteurs sont nulles a l'interieur et a la limite
! fin du if sur surf=0.
         endif
       do 210 ias = 1,nas
        if (nrefa(iac(ias)).eq.i) then
          ia=iac(ias)
          ie=ieva(ia,2)
          if (ie.eq.0) then
! element a droite nul : la normale est sortante
! on inverse qnal et qtal qui sont donnes sur la normale entrante
            qnal(ia)=-qnal(ia)
            qtal(ia)=-qtal(ia)
          endif
        endif
 210   continue
! fin de la troisieme boucle sur les differents groupes d'aretes
      end do
!$omp end do
!$omp end parallel
      return
      end subroutine caldeb

!******************************************************************************
      subroutine caltar
! calcule le debit sur chaque arete limite ou loi de tarage definie globalement
!******************************************************************************

      use module_tableaux,only:nrefa,hal,qnal,qtal,se,la,ieva,hg1,&
     &qug1,qvg1,zfa,nalmax,nas,iac,eps1,eps2,paray,na60,debglo,debtar
      use module_precision
      use module_mpi,only:me,machine_aretes60
!$    use omp_lib

      implicit none

!      implicit logical (a-z)
      integer ia,ias,i,ie,iev
!     :     ,xna(namax),yna(namax),xta(namax),yta(namax),
!     :     het(nemax),quet(nemax),qvet(nemax)
      real(wp) qn,qn2,surf,xlon,ul,hl,zfa0,un2,qul,zmoy
      integer ia0
      real(wp) lgamag,dlgamag

      external lgamag,dlgamag

      !common/nomb/ne,na,nn
      !common/nare/nas,iac
      !common/narf/nrefa
      !common/clim/hal,qnal,qtal
      !common/param/eps1,eps2,paray
!      common/vare/xna,yna,xta,yta
!      common/tlig/het,quet,qvet
      !common/dose/se,la
      !common/iela/ieva
  !  common/na60/na60
      !common/debgl/debglo,debtar
      !common/hg12/hg1,qug1,qvg1
      !common/zare/zfa

      if (debtar) then
! boucle sur tous les groupes d'aretes
! on fait une premiere boucle sur les impairs
! pour une repartition en vitesse uniforme puis une deuxiemme
! sur les pairs correspondant a une repartition en debit


!$omp parallel private(qn,qn2,surf,xlon,ul,hl,zfa0,
!$omp1 un2,qul,zmoy,ia0,ia,ias,ie,iev)
!$omp do schedule(runtime)
        do i=61,60+na60,2
          if(machine_aretes60(i)/=me)cycle
! qn2 est le debit sortant a l'interieur du modele
          qn2=0.
!       qt2=0.
! surf contient la surface en eau
          surf=0.
! xlon contient la longueur
          xlon=0.
! zmoy contient le z*surface
          zmoy=0.
! ia0 est arete de cote la plus basse
          ia0=0
          zfa0=9999.
! boucle sur toutes les aretes rentrantes
          do ias = 1,nas
            if (nrefa(iac(ias)).eq.i ) then
              ia=iac(ias)
              ie=ieva(ia,1)
              if (zfa(ia).lt.zfa0) then
                ia0=ia
                zfa0=zfa(ia)
              endif

              if (ie.eq.0) then
                if (hg1(ia,2) .gt. paray) then
                  xlon=xlon+la(ia)
                  zmoy=zmoy+la(ia)*hg1(ia,2)*(zfa(ia)+hg1(ia,2))
                  surf=surf+la(ia)*hg1(ia,2)
! signe - car normale orientee de 1 vers 2
!        qn2=qn2-la(ia)*(qug1(ia,2)*xna(ia)+qvg1(ia,2)*yna(ia))
! qug1 contient le debit norma
                  qn2=qn2-la(ia)*qug1(ia,2)
                endif
              else
                if (hg1(ia,1) .gt. paray) then
                  xlon=xlon+la(ia)
                  zmoy=zmoy+la(ia)*hg1(ia,1)*(zfa(ia)+hg1(ia,1))
                  surf=surf+la(ia)*hg1(ia,1)
! qug1 contient le debit normal
!        qn2=qn2+la(ia)*(qug1(ia,1)*xna(ia)+qvg1(ia,1)*yna(ia))
                  qn2=qn2+la(ia)*qug1(ia,1)
                endif
              endif
! fin du if sur nrefa
            endif
          end do
          if (surf .gt. eps2) then
! zmoy est la cote moyenne aval
            zmoy=zmoy/surf
            un2=qn2/surf
! xlon longueur equivalente sur la hauteur maximale
! supposee invariante entre niveau interieur et limite
            xlon=surf/(zmoy-zfa0)
! toujours gamag car on a un2 >0 si sortante (limite a droite)
! et lgamag car multiplie par longueur
!       ie=ieva(ia0,1)
!       if (ie .gt. 0) then
            qul=1.
            call dicho2(qul,lgamag,dlgamag,zmoy-zfa0,un2,hl,xlon,ia0)
!       write(*,*)xlon,zfa0,hl,qul,ia0,un2,surf,qn2
!         else
!             call dicho2(qul,gamad,dgamad,xlon-zfa0,un2,hl,ia0)
!       endif
            if (hl.gt.paray) then
              ul=qul/(hl*xlon)
            else
              ul=0.
            endif
!       write(*,*)'caltar',hl,ul
!           qn2=qn2/surf
            do ias = 1,nas
              if (nrefa(iac(ias)).eq.i) then
                ia=iac(ias)
                ie=ieva(ia,1)
                hal(ia)=hl+zfa0-zfa(ia)
! qnal >0 si sortant
                if (hal(ia) .gt. paray) then
                  qnal(ia)=ul*hal(ia)
                else
                  qnal(ia)=0.
                  hal(ia)=0.
                endif
                if (ie.eq.0) then
                  hg1(ia,1)=hal(ia)
                  qug1(ia,1)=-qnal(ia)
                else
                  hg1(ia,2)=hal(ia)
                  qug1(ia,2)=qnal(ia)
                endif
! fin du if sur l'appartenance au groupe
              endif
            end do
! si surf=0 pas de debit sortant
          else
! boucle sur toutes les aretes rentrantes
            do ias = 1,nas
              if (nrefa(iac(ias)).eq.i) then
                ia=iac(ias)
                hal(ia)=0.
                qnal(ia)=0.
                ie=ieva(ia,1)
                if (ie.eq.0) then
                  hg1(ia,1)=hal(ia)
                  qug1(ia,1)=-qnal(ia)
                else
                  hg1(ia,2)=hal(ia)
                  qug1(ia,2)=qnal(ia)
                endif
! fin du if sur nrefa = i
              endif
            end do
! fin du if sur surf=0
          endif
! fin de la premiere boucle sur les differents groupes d'aretes
        end do
!$omp end do

! deuxieme boucle sur tous les groupes d'aretes :
! repartition proportionnelle a q

!$omp do schedule(runtime)
        do i=62,60+na60,2
          if(machine_aretes60(i)/=me)cycle
! qn2 est le debit sortant a l'interieur du modele
          qn2=0.
!       qt2=0.
! surf contient la surface en eau
          surf=0.
! xlon contient la longueur des aretes
          xlon=0.
! zmoy contient le z*surface
          zmoy=0.
! ia0 est arete de cote la plus basse
          ia0=0
          zfa0=9999.
! boucle sur toutes les aretes rentrantes
          do ias = 1,nas
            if (nrefa(iac(ias)).eq.i ) then
              ia=iac(ias)
              ie=ieva(ia,1)
              if (zfa(ia).lt.zfa0) then
                ia0=ia
                zfa0=zfa(ia)
              endif

              if (ie.eq.0) then
                if (hg1(ia,2) .gt. paray) then
                  xlon=xlon+la(ia)
                  zmoy=zmoy+la(ia)*hg1(ia,2)*(zfa(ia)+hg1(ia,2))
                  surf=surf+la(ia)*hg1(ia,2)
! signe - car normale orientee de 1 vers 2
!        qn2=qn2-la(ia)*(qug1(ia,2)*xna(ia)+qvg1(ia,2)*yna(ia))
! qug1 contient le debit normal
                  qn2=qn2+max(0.,-la(ia)*qug1(ia,2))
                endif
              else
                if (hg1(ia,1) .gt. paray) then
                  xlon=xlon+la(ia)
                  zmoy=zmoy+la(ia)*hg1(ia,1)*(zfa(ia)+hg1(ia,1))
                  surf=surf+la(ia)*hg1(ia,1)
! qug1 contient le debit normal
!        qn2=qn2+la(ia)*(qug1(ia,1)*xna(ia)+qvg1(ia,1)*yna(ia))
                  qn2=qn2+max(0.,la(ia)*qug1(ia,1))
                endif
              endif
! fin du if sur nrefa
            endif
          end do
          if (surf .gt. eps2) then
! zmoy est la cote moyenne aval
            zmoy=zmoy/surf
            un2=qn2/surf
! xlon longueur equivalente sur la hauteur maximale
! supposee invariante entre niveau interieur et limite
            xlon=surf/(zmoy-zfa0)
! toujours gamag car on a un2 >0 si sortante (limite a droite)
!       ie=ieva(ia0,1)
!       if (ie .gt. 0) then
            qul=1.
            call dicho2(qul,lgamag,dlgamag,zmoy-zfa0,un2,hl,xlon,ia0)
!         else
!             call dicho2(qul,gamad,dgamad,xlon-zfa0,un2,hl,ia0)
!       endif
            if (hl.gt.paray) then
              ul=qul/(hl*xlon)
            else
              ul=0.
            endif
!           qn2=qn2/surf
            do ias = 1,nas
              if (nrefa(iac(ias)).eq.i) then
                ia=iac(ias)
                hal(ia)=hl+zfa0-zfa(ia)
                ie=ieva(ia,1)
! qnal >0 si sortant
                if (hal(ia) .gt. paray) then
                  if (ie.eq.0) then
                    if(hg1(ia,2).gt.paray)then
! signe - car normale orientee de 1 vers 2
!                qn2=qn2-la(ia)*(qug1(ia,2)*xna(ia)+qvg1(ia,2)*yna(ia))
! qug1 contient le debit normal
                      qn=max(0.d0,-qug1(ia,2))
! qug1 contient le debit normal
                    else
                      qn=0.
                    endif
                  else
                    if(hg1(ia,1).gt.paray)then
! qug1 contient le debit normal
!                qn2=qn2+la(ia)*(qug1(ia,1)*xna(ia)+qvg1(ia,1)*yna(ia))
                      qn=max(0.d0,qug1(ia,1))
                    else
                      qn=0.
                    endif
                  endif
! proportionnalite au debit interieur
                  if (qn2 .gt. eps1) then
                    qnal(ia)=qn*ul*surf/qn2
                  else
                    qnal(ia)=ul*hal(ia)
                  endif
! si hal =0
                else
                  qnal(ia)=0.
                  hal(ia)=0.
                endif
!          ie=ieva(ia,1)
                if (ie.eq.0) then
                  hg1(ia,1)=hal(ia)
                  qug1(ia,1)=-qnal(ia)
                else
                  hg1(ia,2)=hal(ia)
                  qug1(ia,2)=qnal(ia)
                endif
! fin du if sur l'appartenance au groupe
              endif
            end do
! si surf=0 pas de debit sortant
          else
! boucle sur toutes les aretes rentrantes
            do ias = 1,nas
              if (nrefa(iac(ias)).eq.i) then
                ia=iac(ias)
                hal(ia)=0.
                qnal(ia)=0.
                ie=ieva(ia,1)
                if(ie.eq.0)then
                    hg1(ia,1)=hal(ia)
                    qug1(ia,1)=-qnal(ia)
                else
                    hg1(ia,2)=hal(ia)
                    qug1(ia,2)=qnal(ia)
                endif
! fin du if sur nrefa = i
              endif
            end do
! fin du if sur surf=0
          endif
! fin de la deuxieme boucle sur les differents groupes d'aretes
        end do
!$omp end do
!$omp end parallel

! endif sur debtar
      endif
      return
      end subroutine caltar

!******************************************************************************
      subroutine lecl(t)
!******************************************************************************
!     lecture/edition des conditions aux limites ; quelques remarques :
!     - a chaque arete est associe un triplet (hlim,qnlim,qtlim), qu'elle
!     soit sur la frontiere ou non (dans ce cas, hlim = qnlim = qtlim = 0)
!     - les valeurs de qnlim et qtlim sont les composantes du vecteur debit
!     (q = h * v) dans le repere local de l'arete, defini par sa normale
!     exterieure et sa tangente (sens direct)
!     - la valeur de hlim n'a pas d'importance (pas prise en consideration)
!     - les cl peuvent dependre du temps (recalcul a chaque iteration) :
!     la liste d'arguments de lecl est redondante pour cette raison.

!     - ATTENTION ! les valeurs de qnal et qtal doivent etre donnees dans
!     le repere local propre a l'arete ; on rappelle que celle-ci est orientee
!     du noeud ina(ia,1) vers le noeud ina(ia,2), le repere local direct (n,t)
!     correspond a cette orientation. si par exemple on veut imposer une cl de
!     flux normal rentrant, il faudra regarder le sens de l'arete pour voir si
!     sa normale n est rentrante ou sortante (ce qui n'a rien d'evident !), et
!     fournir la valeur du flux dans ce repere (n,t)
!      implicit logical (a-z)

      use module_precision, only: wp
      use module_tableaux, only: zfa,nrefa,hal,qnal,qtal,etude,&
     &nltmax,nplmax,ne,na,nn,nas,iac,zfm,&
     &ifdm,ifm,ifin,ifli,ifro,ifcl,ifci,ifr,ifrt,ifap,ifve,qlt,zlt,&
     &ialt,nblt,iltmax,debglo,debtar,na60,id_tar,nouveau_format

      use module_mpi,only:me,scribe
      use module_lecl

!$    use omp_lib


#if TRANSPORT_SOLIDE
      use module_ts,only: hcal,diama,sigmaa,niux,niuy&
     &,dens,den,m,kds,d,tk,consnu,vch,zeta,conckg


#if SEDVAR
     use module_ts,only:sedvar
!end if sedvar
#endif

!end if TRANSPORT_SOLIDE
#endif

      implicit none

      integer nref,ia,j,ias,i,nas2,ifl2,j2
      real(wp) t,det

!#if TRANSPORT_SOLIDE
!      real(WP) :: diamt(nalmax,ltmax),sigmat(nalmax,ltmax)
!#endif


! modification du 5/3/92: on rentre z au lieu de h en condition limite
! debglo true si debit entrant defini globalement sur plusieurs aretes
      debglo=.false.
      debtar=.false.
!     ouverture du fichier tar lois de tarage
      do ia=1,na
        if (nrefa(ia).eq.6.or.nrefa(ia) .gt. 60) then
      ifl2=id_tar
!       write(*,*) trim(etude)//'.tar'
      open(ifl2,file=trim(etude)//'.tar',status='unknown')
      do i=1,nltmax
        read(ifl2,*,end=1151,err=1152)ialt(i),nblt(i)
        do j=1,nblt(i)
          read(ifl2,*,end=1151,err=1152)qlt(i,j),zlt(i,j)
        end do
      end do
      iltmax=i
      if(iltmax.eq.nltmax)then
#if ENG
        write(*,*)'maximum number ',nltmax,' of rating curves reached'
#else
        write(*,*)'nombre maximal ',nltmax,' de lois de tarage atteint'
#endif
      endif
      close(ifl2)
      go to 1154
 1151 iltmax=i-1
      close(ifl2)
      go to 1154
#if ENG
 1152 if(me==scribe)write(*,*)'the file ETUDE.TAR have the wrong format'
#else
 1152 if(me==scribe)write(*,*)'LE FICHIER ETUDE.TAR N''EST PAS AU BON FORMAT'
#endif
      close(ifl2)
      go to 1154
         endif
      end do
 1154 continue
!$omp parallel
!$omp do schedule(runtime) reduction (max:na30,na60)
      do ia=1,na

#if TRANSPORT_SOLIDE
    !par securite pour que hcal soit toujours defini
          hcal(ia)=0
#endif

          if (nrefa(ia) .gt. 30.and.nrefa(ia).lt.60) then
           debglo=.true.
! na30 contient le nombre de groupe d'aretes maximal
           na30=max(na30,nrefa(ia)-30)
!           go to 2154
          elseif (nrefa(ia) .gt. 60) then
           debtar=.true.
! na30 contient le nombre de groupe d'aretes maximal
           na60=max(na60,nrefa(ia)-60)
! fin du if sur nrefa global
         endif
      end do
!$omp end do
!$omp single
!     premiere lecture du fichier cli si le fichier existe
      if (ifli.ne.0) then
! 2154 read(ifli,'(i6)')nt
      read(ifli,'(i6)')nt
      if (nt .gt. ltmax) then
#if ENG
         if(me==scribe)write(*,*)'MORE THAN ', ltmax, ' TIMES IN THE CLI FILE'
#else /* FRA */
         if(me==scribe)write(*,*)'PLUS DE ', ltmax, ' TEMPS DANS LE FICHIER CLI'
#endif /* FRA */
!         pause
         stop
      endif
      if(nouveau_format)then
         read(ifli,'(i9)',end=2)nas2
      else
         read(ifli,'(i6)',end=2)nas2
      endif
      if (nas2.ne.nas) then
#if ENG
        if(me==scribe)write(*,*)'NAS (INLET EDGES) FROM .CLI = ',nas2
        if(me==scribe)write(*,*)'BUT USE NAS FROM .DAT = ',nas
#else /* FRA */
        if(me==scribe)write(*,*)'NAS (ARETES RENTRANTES) DE .CLI = ',nas2
        if(me==scribe)write(*,*)'TANDIS QUE CELUI UTILISE NAS DE .DAT = ',nas
#endif /* FRA */
      endif
      do i=1,nt
        read(ifli,'(f15.3)',end=2)tcl(i)

#if TRANSPORT_SOLIDE

#if SEDVAR
        if(sedvar)then
      if(nouveau_format)then
        do ias=1,nas2
          read(ifli,'(i9,3f10.3,3f10.7)',end=2) j2,hat(ias,i),qnat(ias,i),qtat(ias,i)&
     &,hcat(ias,i),diamt(ias,i),sigmat(ias,i)
! fin boucle sur ias
       enddo
      else
        do ias=1,nas2
          read(ifli,'(i6,3f10.3,3f10.7)',end=2) j2,hat(ias,i),qnat(ias,i),qtat(ias,i)&
     &,hcat(ias,i),diamt(ias,i),sigmat(ias,i)
! fin boucle sur ias
       enddo
      endif
! si pas sedvar
        else
!end if SEDVAR
#endif

!end if TRANSPORT_SOLIDE
#endif

        do ias=1,nas2

#if TRANSPORT_SOLIDE
      if(nouveau_format)then
          read(ifli,'(i9,3f10.3,F10.7)',end=2) j2,hat(ias,i),qnat(ias,i),qtat(ias,i),hcat(ias,i)
      else
          read(ifli,'(i6,3f10.3,F10.7)',end=2) j2,hat(ias,i),qnat(ias,i),qtat(ias,i),hcat(ias,i)
      endif
#else
      if(nouveau_format)then
          read(ifli,'(i9,3f10.3)',end=2) j2,hat(ias,i),qnat(ias,i),qtat(ias,i)
      else
          read(ifli,'(i6,3f10.3)',end=2) j2,hat(ias,i),qnat(ias,i),qtat(ias,i)
      endif
#endif


        end do

#if SEDVAR
!fin du if sur sedvar
   endif
#endif

      end do
 2    close(ifli)
! else du if sur ifli
      else
        nt=0
        nas=0
        nas2=0
! fin du if sur ifli
      endif
      do ias = 1,nas
        nref=nrefa(iac(ias))
        if (nref.lt.3) then
#if ENG
          if(me==scribe)write(*,*)'ERROR DAT FILE: INLET EDGE ',iac(ias)
          if(me==scribe)write(*,*)'HAS A REFERENCE ',nref,' LOWER THAN 3'
#else /* FRA */
          if(me==scribe)write(*,*)'ERREUR FICHIER DAT: L''ARETE RENTRANTE ',iac(ias)
          if(me==scribe)write(*,*)'A UNE REFERENCE ',nref,' INFERIEURE A 3'
#endif /* FRA */
          stop
        endif
    enddo
!     edition conditions aux limites
      if (me==scribe)then
      if (ifcl.eq.0) goto 300
      write (ifcl,*)
      write (ifcl,*) '************************************************'
      write (ifcl,*) '            CONDITIONS AUX LIMITES  '
      write (ifcl,*) '************************************************'
      write (ifcl,*) 'REFERENCE=0 SI ARETE INTERIEURE '
      write (ifcl,*) 'REFERENCE=1 OU 4 OU 6 OU 7 OU > 60 SI DEBIT SORTANT '
      write (ifcl,*) 'REFERENCE=2 SI REFLEXION '
      write (ifcl,*) 'REFERENCE=3 OU 5  ou 8 ou 9 SI DEBIT ENTRANT '
      write (ifcl,*) 'REFERENCE=30 ET PLUS SI DEBIT ENTRANT GLOBAL'
      write (ifcl,*)
      write (ifcl,*) 'NOMBRE D ARETES = ',na
      write (ifcl,*) '=============================='
      do 500 ia=1,na
        write (ifcl,*) 'ARETE NO      : ',ia
        write (ifcl,*) 'REFERENCE     : ',nrefa(ia)
 500  continue
      if (nt.ne.0) then
      do 100 j=1,nt
        write (ifcl,*) 'TEMPS ',j,'= ',tcl(j)
        write (ifcl,*)
 100  continue
      do 200 ias = 1,nas
        write (ifcl,*) 'ARETE NO      : ',iac(ias)
        nref=nrefa(iac(ias))
        write (ifcl,*) 'REFERENCE     : ',nref
        if (nref.eq.3) then
        do 201 j=1,nt
          write (ifcl,*) 'COTE          : ',hat(ias,j)
          write (ifcl,*) 'DEBIT NORMAL  : ',qnat(ias,j)
          write (ifcl,*) 'DEBIT TANGENT : ',qtat(ias,j)
#if TRANSPORT_SOLIDE
          write (ifcl,*) 'CONCENTRATION : ',hcat(ias,j)
#endif


 201    continue
        elseif (nref.eq.4) then
        do 202 j=1,nt
          write (ifcl,*) 'COTE          : ',hat(ias,j)

 202    continue
        elseif (nref.eq.5) then
        do 207 j=1,nt
          write (ifcl,*) 'COTE          : ',hat(ias,j)
          write (ifcl,*) 'DEBIT NORMAL  : ',qnat(ias,j)
          write (ifcl,*) 'DEBIT TANGENT : ',qtat(ias,j)
#if TRANSPORT_SOLIDE
          write (ifcl,*) 'CONCENTRATION : ',hcat(ias,j)
#endif
 207    continue
        elseif (nref.eq.8) then
        do j=1,nt
          write (ifcl,*) 'COTE          : ',hat(ias,j)
!          write (ifcl,*) 'DEBIT NORMAL  : ',qnat(ias,j)
          write (ifcl,*) 'RATIO DEBIT TANGENT/DEBIT NORMAL :&
     & ',qtat(ias,j)
#if TRANSPORT_SOLIDE
          write (ifcl,*) 'CONCENTRATION : ',hcat(ias,j)
#endif
        enddo
        elseif (nref.eq.9) then
        do j=1,nt
          write (ifcl,*) 'CHARGE        : ',hat(ias,j)
          write (ifcl,*) 'DEBIT NORMAL  : ',qnat(ias,j)
          write (ifcl,*) 'DEBIT TANGENT : ',qtat(ias,j)
#if TRANSPORT_SOLIDE
          write (ifcl,*) 'CONCENTRATION : ',hcat(ias,j)
#endif
        enddo
        elseif (nref .gt. 30) then
        do 208 j=1,nt
          write (ifcl,*) 'COTE          : ',hat(ias,j)
          write (ifcl,*) 'DEBIT NORMAL GLOBAL  : ',qnat(ias,j)
          write (ifcl,*) 'DEBIT TANGENT GLOBAL : ',qtat(ias,j)
#if TRANSPORT_SOLIDE
          write (ifcl,*) 'CONCENTRATION : ',hcat(ias,j)
#endif
 208    continue
! supprime ici car mis avant
!        elseif (nref.lt.3) then
!          write(*,*)'erreur de donnee: l''arete rentrante ',iac(ias)
!          write(*,*)'a une reference inferieure a 3'
!          stop
        endif
 200  continue
! fin du if si nt = 0
      endif
      write(ifcl,*)
      write(ifcl,*)' LOIS DE TARAGE'
      write(ifcl,*)
      do 203 i=1,iltmax
         write(ifcl,*)'ARETE = ',ialt(i)
         do 204 j=1,nblt(i)
            write(ifcl,*)'DEBIT = ',qlt(i,j),'  COTE = ',zlt(i,j)
 204     continue
 203  continue
      close(ifcl)
      end if ! me==scribe
! si ifcl = 0 on arrive ici
! 300  k=1
 300  k2=1
! on passe des cotes aux hauteurs pour les lois de tarage
! le 17/11/09 on garde la cote
!      do 205 i=1,iltmax subroutine
!         do 206 j=1,nblt(i)
!            zlt(i,j)=zlt(i,j)-zfa(ialt(i))
!c            zlt(i,j)=zlt(i,j)-zfa(ialt(i))-zfm
! 206     continue
! 205  continue
! verification que chaque arete de ref 6 a une loi de tarage

!$omp end single
!$omp do schedule(runtime)
      do ia=1,na
      if (nrefa(ia).eq.6.or.nrefa(ia) .gt. 60) then
        do  i=1,iltmax
          if (ialt(i).eq.ia)go to 1202
        end do
#if ENG
        write(*,*)'EDGE ',ia,' DOESN''T HAVEA TARING LAW DESPITE ',&
     &'A REFERENCE ',nrefa(ia)
#else /* FRA */
        write(*,*)'L''ARETE ',ia,' N''A PAS DE LOI DE TARAGE MALGRE ',&
     &'UNE REFERENCE ',nrefa(ia)
#endif /* FRA */
      endif
 1202  continue
       end do
!$omp end do
!$omp end parallel
      if (debtar) then
! verification que loi tarage est la meme pour toutes aretes de meme ref>60
    call veriftarglo
    endif
! fin de la premiere lecture

      end subroutine lecl

!*******************************************************************************
         subroutine lecl2(t)
!******************************************************************************
!     lecture/edition des conditions aux limites ; quelques remarques :
!     - a chaque arete est associe un triplet (hlim,qnlim,qtlim), qu'elle
!     soit sur la frontiere ou non (dans ce cas, hlim = qnlim = qtlim = 0)
!     - les valeurs de qnlim et qtlim sont les composantes du vecteur debit
!     (q = h * v) dans le repere local de l'arete, defini par sa normale
!     exterieure et sa tangente (sens direct)
!     - la valeur de hlim n'a pas d'importance (pas prise en consideration)
!     - les cl peuvent dependre du temps (recalcul a chaque iteration) :
!     la liste d'arguments de lecl est redondante pour cette raison.

!     - ATTENTION ! les valeurs de qnal et qtal doivent etre donnees dans
!     le repere local propre a l'arete ; on rappelle que celle-ci est orientee
!     du noeud ina(ia,1) vers le noeud ina(ia,2), le repere local direct (n,t)
!     correspond a cette orientation. si par exemple on veut imposer une cl de
!     flux normal rentrant, il faudra regarder le sens de l'arete pour voir si
!     sa normale n est rentrante ou sortante (ce qui n'a rien d'evident !), et
!     fournir la valeur du flux dans ce repere (n,t)
!      implicit logical (a-z)

      use module_tableaux, only: zfa,nrefa,hal,qnal,qtal,etude,&
     &nalmax,nltmax,nplmax,ne,na,nn,nas,iac,zfm,&
     &ifdm,ifm,ifin,ifli,ifro,ifcl,ifci,ifr,ifrt,ifap,ifve,qlt,zlt,&
     &ialt,nblt,iltmax,debglo,debtar,na60
      use module_precision
      use module_mpi,only:machine_ias,me
      use module_lecl

#if TRANSPORT_SOLIDE
      use module_ts,only:hcal,conckg,dens,diama,sigmaa,sedvar
#endif

!$    use omp_lib

      implicit none

      integer nref,ia,j,ias,i,nas2,ifl2,j2,ialoc
      real(wp) t,det




      !common/etud/etude
      !common/nomb/ne,na,nn
      !common/nare/nas,iac
      !common/zare/zfa
      !common/narf/nrefa
      !common/zbas/zfm
      !common/clim/hal,qnal,qtal
      !common/nofi/ifdm,ifm,ifin,ifli,ifro,ifcl,ifci,ifr,ifrt,ifap,ifve
      !common/qzlt/qlt,zlt
      !common/nbplt/ialt,nblt,iltmax
      !common/debgl/debglo,debtar
  !  common/na60/na60

      !save k2,nt,hat,qnat,qtat,tcl,na30
      !data na30/0/

!      if (debtar)call caltar(na60)
!  k2 est l'indice du pas de temps des conditions aux limites
      if (k2.ge.nt)go to 403
      if (t.lt.tcl(k2)) then
        do 301 ias = 1,nas
        if(machine_ias(ias)/=me)cycle
        hal(iac(ias))=hat(ias,k2)-zfa(iac(ias))
!        hal(iac(ias))=hat(ias,k2)-zfa(iac(ias))-zfm
        qnal(iac(ias))=qnat(ias,k2)
        qtal(iac(ias))=qtat(ias,k2)


#if TRANSPORT_SOLIDE
! si les concentration sont en kg/m3 il faut les remettre en M3/m3
        if(conckg)then
          hcal(iac(ias))=hcat(ias,k2)/dens
        else
          hcal(iac(ias))=hcat(ias,k2)
        endif
#if SEDVAR
        if(sedvar)then
          diama(iac(ias))=diamt(ias,k2)
          sigmaa(iac(ias))=sigmat(ias,k2)
        endif
!end if sedvar
#endif

#endif


 301    continue
        if (debglo)call caldeb(na30)
        go to 402
      endif
      do 400 j=k2+1,nt
        if (t.lt.tcl(j)) then
          det=(t-tcl(j))/(tcl(j-1)-tcl(j))
          do 302 ias=1,nas
          if(machine_ias(ias)/=me)cycle
          hal(iac(ias))=hat(ias,j)+(hat(ias,j-1)-hat(ias,j))*det
          hal(iac(ias))=hal(iac(ias))-zfa(iac(ias))
!          hal(iac(ias))=hal(iac(ias))-zfa(iac(ias))-zfm
          qnal(iac(ias))=qnat(ias,j)+(qnat(ias,j-1)-qnat(ias,j))*det
          qtal(iac(ias))=qtat(ias,j)+(qtat(ias,j-1)-qtat(ias,j))*det

#if TRANSPORT_SOLIDE
! si les concentration sont en kg/m3 il faut les remettre en M3/m3
        if(conckg)then
          hcal(iac(ias))=hcat(ias,j)+(hcat(ias,j-1)-hcat(ias,j))*det
          hcal(iac(ias))=hcal(iac(ias))/dens
        else
          hcal(iac(ias))=hcat(ias,j)+(hcat(ias,j-1)-hcat(ias,j))*det
        endif

#if SEDVAR
        if(sedvar)then
          diama(iac(ias))=diamt(ias,j)&
     &+(diamt(ias,j-1)-diamt(ias,j))*det
          sigmaa(iac(ias))=sigmat(ias,j)&
     &+(sigmat(ias,j-1)-sigmat(ias,j))*det
        endif
!end if sedvar
#endif

#endif

  302     continue
          if (debglo)call caldeb(na30)
          go to 401
        endif
  400 continue
      k2=nt
  403 do 303 ias=1,nas
      if(machine_ias(ias)/=me)cycle
      hal(iac(ias))=hat(ias,nt)-zfa(iac(ias))
!      hal(iac(ias))=hat(ias,nt)-zfa(iac(ias))-zfm
      qnal(iac(ias))=qnat(ias,nt)
      qtal(iac(ias))=qtat(ias,nt)

#if TRANSPORT_SOLIDE
! si les concentration sont en kg/m3 il faut les remettre en M3/m3
        if(conckg)then
          hcal(iac(ias))=hcat(ias,nt)/dens
        else
          hcal(iac(ias))=hcat(ias,nt)
        endif
#if SEDVAR
        if(sedvar)then
          diama(iac(ias))=diamt(ias,nt)
          sigmaa(iac(ias))=sigmat(ias,nt)
        endif
!end if sedvar
#endif

#endif

  303 continue
      if (debglo)call caldeb(na30)
      go to 402
  401 k2=j-1
  402 continue
      end subroutine lecl2

!*******************************************************************************
      subroutine ledd(alpha,nbpt,xnb,ynb,cvi)
!******************************************************************************
!  definition des donnees diverses servant aux calculs
!  eps1    = equivalent de paray pour les debits
!  eps2   = eps1ilon de la machine
!  prec   = precision de calcul pour les itterations de newton
!  nitmax = nombre d iterations maxi de newton
!  irep   = si = 0 pas de reprise . si = 1 reprise
!  ift    = numero du fichier des limnigrammes
!  ifm    = numero du fichier d'edition du maillage
!  ifdm   = numero du fichier du maillage
!  idon   = numero du fichier des parametres de calcul
!  ifli   = numero du fichier des conditions aux limites
!  ifin   = numero du fichier des conditions initiales
!  ifcl   = numero du fichier d'edition des conditions aux limites
!  ifci   = numero du fichier d'edition des conditions initiales
!  ifrt   = numero du fichier d'edition des frottements
!  ifap   = numero du fichier d'edition des apports
!  ifen   = numero du fichier d'edition des enveloppes
!  ifro   = numero du fichier des frottements
!  iapp   = numero du fichier des apports de pluie
!  iofr   = option sur les frottements 0=chezy,1=strickler
!  ifr    = numero du fichier resultat final
!  paray  = valeur min pour h ( gestion des faibles hauteurs)
!  g      = la valeur de la gravite
! tmax    = temps maximum de calcul
! dt0     = pas de temps initial
! icfl    = 0 ou 1 : si calcul avec cfl ou non
! cfl     = si icfl=1 , valeur de la cfl( 0<cfl<1)
! ischem  = 0, 1 ou 2 :godunov, vanleer(x) ou vanleer(x,t)
! iosmb   = 1 ou 0 : calcul avec ou sans ouvrages
! iclvar  = 1 ou 0 : condition limite dependant du temps ou non
! dtr     = pas de temps pour le stockage des resultats
! alpha   = coefficient de correction des pentes pour vanleer
! cvi     = coefficient de viscosite
! fro     = coefficient de frottement a la paroi
! fvix    = contrainte due au vent selon ox
! fviy    = contrainte due au vent selon oy

!onc
! niux    = la diffusion liquide selon ox
! niuy    = la diffusion liquide selon oy
! sigm    = le nombre de schmidt
! dens    = la masse volumique des sediments (kg/m3)
! den     = la masse volumique d'eau
! m       = le coefficient empirique pour le transport de sediments
! ikds    = numero de variante pour le calcul du t.s.
! kds     = la constante donnee par le diagramme de shields
! alprep  = le coefficient de vitesse de depot
! d       = d50
! re      = d* parametre adimensionnel
! vch     = la vitesse de chute
! consnu  = la diffusion lique moyenne constante
! dix     = le coefficient de diffusion selon ox
! diy     = le coefficient de diffusion selon oy
! diagsh = false ou true : si kds est donne par l'utilisateur ou calculer par le diagramme de shields
! vanri   = true : si la concentration d'equilibre est calculee par van rijn
! corvch  =true : si il y a correction sur le vitesse de chute pour le terme de sedimentation
! consed  =true : si il y a calcul d'une contrainte de sedimentation differente
! modes   =true : si il y a calcul different d'erosion et sedimentation
! pour meyer vitesse de transport des sediments fredsoe sinon vitesse liquide
! poro    =porosite
! conckg  oui si concentrations sediments en kg/m3  (solute est non)
! solute  oui si solute et pas sediments
! ppente : prise en compte de la pente pour calcul de taucr
! meyer : calcul erosiona partir meyer peter muller
! zeta  rapport vitesse sediments charries utilisee a vitesse theorique
! idemix vrai si demixage
! dchardiam, dcharsigma : longueur de chargement diametre et etendue si demixage
!oncf
!**************************************************************************



      use module_mpi, only: me,scribe
      use module_precision, only: wp

      use module_tableaux, only:eps1,eps2,paray,ift,ifdm,ifm,ifin,ifli&
     &,ifro,ifcl,ifci,ifr,ifrt,ifap,ifve,ifen,irep,etude,prec,nitmax,&
     &g,fro,fvix,fviy,lcvi,lcviv,lcvif,tm,tr,tinit,tmax,dt,cfl,dt0,t,&
     &icfl,iosmb,iclvar,iapp,iofr,schema,dtr,dtrc,pven,fvix0,fviy0,&
     &latitude,coriolis,id_app,id_eve_ven,id_hyc,id_mas

#if TRANSPORT_SOLIDE
      use module_ts
#endif
      implicit none

!
      integer,parameter::nlimmax=300
      integer i,j
      character ligne*80
      real(wp) alpha,cvi,fvi,xnb(nlimmax),ynb(nlimmax)
!      real(wp) ken,knu,xuu,xuv,xvv
      integer idon,ischem,ire,nbpt

      character donnee*35

#if TRANSPORT_SOLIDE
      integer :: ikds
#endif

!     precision sur les debits ou debits unitaires
! utilise 2 ou 3 fois
      eps1 = 1.e-06
!      eps1 = 1.e-03
!      eps1 = 1.e-10
!     eps1ilon machine
!      eps2 = 1.e-08
      eps2 = 1.e-12
!     precision sur abs(x-y) et nombre d'iterations maxi dans newton (nwt)
      prec = 1.0e-08
      nitmax = 50
! pas de vent fonction de x,y
      pven=.false.
!     ouverture du fichier donnees calculs
      idon=9
      open(idon,file=trim(etude)//'.par',status='old',err=25)

! lecture du fichier contenant les parametres numeriques
!     reprise de calcul a un temps t! #0. 0/1
      read (idon,101)donnee,irep
!  lecture du paray (gestion des faibles hauteurs
      read(idon,103)donnee,paray
!  lecture de la valeur de la gravite
      read(idon,'(a80)')ligne
      read(ligne(1:46),102)donnee,g
      if (ligne(47:53).ne.'       ') then
        read(ligne(47:53),'(f7.3)')latitude
        latitude=sin(latitude*3.1416/180.)
        latitude=1.4584*latitude
        coriolis=.true.
      else
        coriolis=.false.
      endif
!      read (idon,102) donnee,g
!  lecture du temps initial de calcul
      read (idon,102)donnee,tinit
!  lecture du temps maximum de calcul
      read (idon,102)donnee,tmax
!  lecture du pas de temps initial
      read (idon,102)donnee, dt0
!  lecture de icfl 0/1 si calcul fait avec cfl const(1) ou pas(0)
      read (idon,101)donnee,icfl
!  lecture de la cfl de calcul
          read (idon,102)donnee,cfl
!  lecture du type de schema utilise 0:god  1:vleer(x) 2:vleer(x,t)
      read (idon,101)donnee,ischem
      if (ischem.eq.0) then
!          schema='godounov'
#if ENG
          if(me==scribe)write(*,*)'GODUNOV SCHEME REMPLACED BY VAN LEER 1'
#else /* FRA */
          if(me==scribe)write(*,*)'SCHEMA GODUNOV REMPLACE PAR VAN LEER 1'
#endif /* FRA */
          schema='vanleer1'
          ischem=1
      elseif (ischem.eq.1) then
          schema='vanleer1'
!cou1d          ischem=2
!cou1d          write(*,*)'SCHEMA VANLEER 1 REMPLACE PAR VAN LEER 2'
!cou1d          schema='vanleer2'
      elseif (ischem.eq.2) then
          schema='vanleer2'
      endif
!  lecture de iclvar :cond. lim. dep. du tps(1) ou non (0)
      read (idon,101)donnee,iclvar
!  lecture de la valeur de la correction des pentes
      if (ischem.ne.0) then
        read(idon,102)donnee,alpha
      else
        read(idon,*)
      endif
!  lecture du iosmb :calcul avec ou sans ouvrage , avec ou sans apports par pluie
      read (idon,106)donnee,iosmb
! on ajoute 10 a iosmb si nu calcule par pente frottement et 20 si calcule sur pente surface libre
      if (iosmb .gt. 9) then
        iosmb=iosmb-10
        lcviv=.true.
        lcvif=.true.
        if (iosmb .gt. 9) then
          iosmb=iosmb-10
          lcvif=.false.
        endif
      else
        lcviv=.false.
        lcvif=.false.
      endif
      if (iosmb.eq.2) then
        iosmb=0
        iapp=id_app
      elseif (iosmb.eq.3) then
        iosmb=1
        iapp=id_app
      elseif (iosmb.eq.0.or.iosmb.eq.1) then
        iapp=0
      else
#if ENG
        if(me==scribe)write(*,*)'Error in the variable with or without structure'
#else /* FRA */
        if(me==scribe)write(*,*)'Erreur dans la variable avec ou sans ouvrages'
#endif /* FRA */
!        pause
        stop
      endif
      if (iapp .gt. 0) then
        open (iapp,file=trim(etude)//'.app',status='old',err=130)
        goto 131
#if ENG
 130    if(me==scribe)write (*,*) 'can not open file '//trim(etude)//'.app'
#else /* FRA */
 130    if(me==scribe)write (*,*) 'on ne peut ouvrir le fichier '//trim(etude)//'.app'
#endif /* FRA */
!        pause
        stop
      endif
!  lecture du iofr :calcul avec chezy(0) ou strickler(1)
 131  read (idon,101)donnee,iofr
!  lecture du pas de temps pour lequel il y a stockage
      read(idon,102)donnee,dtr
!     ouverture des fichiers dtr et trc o/n (ligne d'eau en des points)
      read (idon,101)donnee,ire
      if (ire.ne.1) then
          ift = 0
      else
          ift = 10
          open(ift,file=trim(etude)//'.dtr',status='unknown')
          read(ift,*,end=151,err=152)dtrc
          read(ift,*,err=152)nbpt
          do 153 i=1,nbpt
          read(ift,*,err=152)j,xnb(i),ynb(i)
 153      continue
          close(ift)
          open (ift,file=trim(etude)//'.trc',status='unknown')


#if TRANSPORT_SOLIDE
          open (id_hyc,file=trim(etude)//'.hyc',status='unknown')
#endif


          go to 154
#if ENG
 151      if(me==scribe)write(*,*)'empty etude.dtr file'
#else /* FRA */
 151      if(me==scribe)write(*,*)'le fichier etude.dtr est vide'
#endif /* FRA */
          ift=0
          go to 154
#if ENG
 152      if(me==scribe)write(*,*)'bad etude.dtr file'
#else /* FRA */
 152      if(me==scribe)write(*,*)'le fichier etude.dtr n''est pas bon'
#endif /* FRA */
          ift=0
      endif
!     ouverture du fichier maillage o/n
 154  read(idon,101)donnee,ire
      if (me==scribe)then
        if (ire.eq.0) then
          ifm = 0
        else
          ifm = ire+20
          open (ifm,file=trim(etude)//'.edm',status='unknown')
        endif
      end if ! me==scribe
!    ouverture du fichier de conditions aux limites
      read(idon,101)donnee,ire
      if (me==scribe)then
        if (ire.ne.1) then
          ifcl = 0
        else
          ifcl = 16
          open(ifcl,file=trim(etude)//'.ecl',status='unknown')
        endif
      end if ! me==scribe
!    ouverture du fichier edition conditions initiales
      read(idon,101)donnee,ire
      if (me==scribe)then
        if (ire.ne.1) then
          ifci = 0
        else
          ifci = 17
          open(ifci,file=trim(etude)//'.eci',status='unknown')
        endif
      end if ! me==scribe
!    ouverture du fichier edition frottements et apports
      read(idon,101)donnee,ire
      if (me==scribe)then
        if (ire.ne.1) then
!      if (ire.ne.1.or.iosmb.ne.1) then
          ifrt = 0
          ifap = 0
          ifve=0
        else
          ifrt = 19
          open(ifrt,file=trim(etude)//'.efr',status='unknown')
          if (iapp.ne.0) then
             ifap=30
             open(ifap,file=trim(etude)//'.eap',status='unknown')
          else
             ifap=0
          endif
! edition des donnees de vent en meme temps que donnees frottement
          ifve=id_eve_ven
        endif
      end if ! me==scribe
!    ouverture du fichier resultat final
      read(idon,101)donnee,ire
      if (me==scribe)then
        if (ire.ne.1) then
          ifr = 0
        else
          ifr = 18
          open(ifr,file=trim(etude)//'.out',status='unknown')
          write (ifr,*)
          write (ifr,*) '********************************************'
          write (ifr,*) ' RESULTATS DES CALCULS PAR VOLUMES FINIS 2D'
          write (ifr,*) '             SORTIES ET ENTREES          '
          write (ifr,*) '********************************************'

#if TRANSPORT_SOLIDE
          open(id_mas,file=trim(etude)//'.mas',status='unknown')
          write (id_mas,*)
          write (id_mas,*) '********************************************'
          write (id_mas,*) ' RESULTATS DES CONCENTRATIONS'
          write (id_mas,*) '             SORTIES ET ENTREES          '
          write (id_mas,*) '********************************************'
#endif

        endif
      end if ! me==scribe
!    ouverture du fichier resultat enveloppe
      read(idon,101)donnee,ire
      if (ire.ne.1) then
          ifen = 0
      else
          ifen = 20
      endif
! lecture du coefficient de viscosite
      read(idon,103,end=3650)donnee,cvi
! lecture du coefficient de frottement a la paroi
      read(idon,103,end=3651)donnee,fro
! calcul en chezy au lieu de strickler
      if (iofr.eq.0)fro=-fro
! lecture de la vitesse du vent selon ox
      read(idon,103,end=3654)donnee,fvix
! lecture de la vitesse du vent selon oy
      read(idon,103,end=3655)donnee,fviy
      go to 3652
 3650 cvi=0.
! 3651 fro=1.e+06
 3651 fro=0.
 3654 fvix=0.
 3655 fviy=0.
      go to 3652
! calcul du coefficient de frottement du au vent selon ox et oy
 3652 fvi=sqrt(fvix**2+fviy**2)
! 3652 fvi=(fvix**2+fviy**2)**0.75
      fvix0=fvix
      fviy0=fviy
      if (fvix.ne.0.) then
      fvix=3.4e-06*fvi*fvix
!      fvix=0.6e-06*fvi*fvix
      endif
      if (fviy.ne.0.) then
!      fviy=0.6e-06*fvi*fviy
      fviy=3.4e-06*fvi*fviy
      endif

#if TRANSPORT_SOLIDE

      d90=0.
! lecture de la diffusion liquide selon ox
      read(idon,'(a80)')ligne
! si tau efficace au lieu de tau totale
! 26/03/14 introduction de efficace
! a introduire la lecture de cette variable
      efficace=.true.
      read(ligne(1:46),105)donnee,niux
      if(ligne(47:53).ne.'       ')then
        read(ligne(47:53),'(f7.3)')zeta
        if(zeta.lt.0.)then
           efficace=.false.
           zeta=-zeta
        endif
      else
        zeta=1.
      endif
      if(.not.efficace)then
#if ENG
        write(*,*)'TOTAL SHEAR = NOT REDUCED SHEAR'
#else
        write(*,*)'CONTRAINTE TOTALE ET PAS EFFICACE'
#endif
      endif
!
      if(ligne(54:60).ne.'       ')then
        read(ligne(54:60),'(f7.3)')exposantaucr
        exposantaucr=0.5*exposantaucr
        exptaucr=.true.
      else
        exptaucr=.false.
        exposantaucr=0.
      endif
! lecture de la diffusion liquide selon oy
      read(idon,'(a80)')ligne
      read(ligne(1:46),105)donnee,niuy
      if(ligne(47:53).ne.'       ')then
         read(ligne(47:53),'(f7.3)')constantea
      else
        constantea=7.
      endif
      if(ligne(54:60).ne.'       ')then
         read(ligne(54:60),'(f7.3)')constanteb
      else
         constanteb=0.85
      endif
      if(constanteb.lt.eps2)then
#if ENG
        write(*,*)'ERREUR 2EME CONSTANTE DEVIATION CHARRIAGE'
        write(*,*)'VALEUR TROUVEE = ',constanteb
#else /* FRA */
        write(*,*)'ERROR 2ND CHARRIAGE DEVIATION CONSTANT'
        write(*,*)'VALUE FOUND = ',constanteb
#endif /* FRA */
      endif
! lecture du nombre de schmidt
      read(idon,103)donnee,sigm
! lecture de la densite des sediments
      read(idon,103)donnee,dens
! lecture de la densite d'eau
      read(idon,103)donnee,den
! lecture du coefficient empirique
      read(idon,105)donnee,m
! lecture de ikds
      read(idon,107)donnee,ikds
      if(ikds.gt.100)then
        ikds=ikds-100
        idemix=.true.
      elseif(ikds.lt.-100)then
        ikds=ikds+100
        idemix=.true.
      else
        idemix=.false.
      endif
! lecture du diametre d'un grain
      read(idon,'(a80)')ligne
      read(ligne(1:46),105)donnee,d
      if(ligne(47:57).ne.'       ')then
         read(ligne(47:57),'(f11.7)')sigma0
      else
        sigma0=0.
      endif
!      read(idon,105)donnee,d
! lecture du coefficient de vitesse de depot
      read(idon,'(a80)')ligne
      read(ligne(1:46),103)donnee,alprep

#if SEDVAR
! si demixage
      if(idemix)then
#if ENG
        write(*,*)'COMPUTING WITH UNMIXING'
#else /* FRA */
        write(*,*)'CALCUL AVEC DEMIXAGE'
#endif /* FRA */
      if(ligne(47:57).ne.'       ')then
         read(ligne(47:57),'(f11.7)')dchardiam
      else
        dchardiam=0.
      endif
      if(ligne(58:68).ne.'       ')then
         read(ligne(58:68),'(f11.7)')dcharsigma
      else
        dcharsigma=0.
      endif
#if ENG
      write(*,*)'CHARGING DIAMETER LENGHT',dchardiam,' m'
      write(*,*)'CHARGING SIZE LENGHT',dcharsigma,' m'
#else /* FRA */
      write(*,*)'LONGUEUR DE CHARGEMENT DIAMETRE',dchardiam,' m'
      write(*,*)'LONGUEUR DE CHARGEMENT ETENDUE',dcharsigma,' m'
#endif /* FRA */
! fin du if sur idemix
      endif
!end if sedvar
#endif


! lecture de la porosite
      read(idon,105)donnee,poro
! lecture de la constante k pas donnee par le diagramme de shields
! ou du d90 selon si vanri vrai ou pas
      read(idon,103)donnee,kds
!      if(ikds.eq.0)then
!        avects=.false.
!        chzn=.false.
!        modifz=.false.
!      else
!        avects=.true.

! calcul de la vitesse de chute
! viscosite du fluide pour calcul de la vitesse de chute (valeur a 20 �c)
      consnu=0.000001
      if(dens-den.gt.eps2)then
        solute=.false.
#if ENG
        write(*,*)'COMPUTING WITH SEDIMENTS'
#else /* FRA */
        write(*,*)'CALCUL AVEC SEDIMENTS'
#endif /* FRA */
! si niux est negatif niux contient la vitesse de chute
        if(niux.lt.-eps2)then
          vch=-niux

#if SEDVAR
          vchfixe=.true.
!end if sedvar
#endif
        else
#if SEDVAR
          vchfixe=.false.
!end if sedvar
#endif

          if(d.lt.0.0001)then
            vch=(dens/den-1.)*g*d*d/(18.*consnu)
          elseif(d.lt.0.001)then
            vch=10.*consnu/d*&
     &(sqrt(1.+0.01*(dens/den-1.)*g*d**3/(consnu**2))-1.)
          else
            vch=1.1*sqrt((dens/den-1.)*g*d)
          endif
! fin du if sur niux donnant la vitesse de chute
        endif
!        if(niuy.lt.-eps2)then
!          zeta=-niuy
!        else
!          zeta=1.
!        endif
        if(dens.gt.1000.)then
         if(den.lt.100.)then
#if ENG
           write(*,*)'INCONSISTANCY BETWEEN VOLUME MASSES  PAR FILE'
#else /* FRA */
           write(*,*)'INCOHERENCE ENTRE MASSES VOLUMIQUES  FICHIER PAR'
#endif /* FRA */
           stop
         else
          conckg=.true.
#if ENG
          write(*,*)'CONCENTRATIONS IN g/l OR kg/m3'
#else /* FRA */
          write(*,*)'CONCENTRATIONS EN g/l OU kg/m3'
#endif /* FRA */
         endif
        else
         if(den.gt.10.)then
#if ENG
           write(*,*)'INCONSISTANCY BETWEEN DENSITIES PAR FILE'
#else /* FRA */
           write(*,*)'INCOHERENCE ENTRE DENSITES FICHIER PAR'
#endif /* FRA */
           stop
         else
          conckg=.false.
#if ENG
          write(*,*)'ADIMENSIONAL CONCENTRATIONS'
#else /* FRA */
          write(*,*)'CONCENTRATIONS ADIMENSIONNELLES'
#endif /* FRA */
         endif
        endif
      else
        solute=.true.
        conckg=.false.
        if(dens.lt.den-eps2)then
#if ENG
          write(*,*)'COMPUTATION WITH SURFACE VELOCITY'
#else /* FRA */
          write(*,*)'CALCUL AVEC VITESSE DE SURFACE'
#endif /* FRA */
          derive=.true.
          if(ikds.ne.0)then
            alfv=d
            betav=m
            if(ikds.eq.2)then
              cvsurf=.true.
              if(iofr.ne.1)then
#if ENG
         write(*,*)'DERIVE OPTION NEEDS STRICKLER DRAG'
#else /* FRA */
         write(*,*)'OPTION DE DERIVE EXIGE FROTTEMENT EN STRICKLER'
#endif /* FRA */
!                    pause
                    stop
! fin du if sur iofr
              endif
! fin du if sur ikds=2
            endif
          else
            alfv=1.16
            betav=0.03
! fin du if sur ikds=0
          endif
! si dens=den veritable solute
        else
          derive=.false.
#if ENG
          write(*,*)'COMPUTATION WITH SOLUTES'
#else /* FRA */
          write(*,*)'CALCUL AVEC SOLUTES'
#endif /* FRA */
        endif
! fin du if sur dens different de den
      endif
      if(.not.solute)then
!  si ikds negatif calcul ts aux centres de maille
      if(ikds.lt.0)then
        ikds=-ikds
        chzn=.false.
      else
        chzn=.true.
      endif
!  par defaut recalcul de la topographie a chaque pas de temps
      if (ikds.gt.40)then
         modifz=.false.
         ikds=ikds-40
#if ENG
         write(*,*)"NO TOPOGRAPHY UPDATE"
#else /* FRA */
         write(*,*)"PAS DE REACTUALISATION DE LA TOPOGRAPHIE"
#endif /* FRA */
      else
         modifz=.true.
      endif
! par defaut la contrainte critique pas reduite a cause pente
      if (ikds.gt.20)then
         ppente=.true.
         ikds=ikds-20
!  on ne permet pas que la contrante critique depende deux fois de la pente
         exptaucr=.false.
!         exposantaucr=0.
! exposantaucr utilise comme angle de pente dans ce cas
      else
         ppente=.false.
      endif

! kds donnee par l'utilisateur dans le fichier des donnees
      devch=.false.
      camenen=.false.
      hansen=.false.
      if (ikds.eq.1) then
        diagsh=.false.
        consed=.false.
        vanri=.false.
        corvch=.false.
        modes=.true.
        meyer=.false.

! kds calculee a paritr du diagramme de shields

      elseif (ikds.eq.2) then
        diagsh=.true.
        consed=.false.
        vanri=.false.
        corvch=.false.
        modes=.true.
        meyer=.false.

! kds utilisateur
! calcul d'une contrainte de sedimentation
! differente de la contrainte d'erosion

      elseif (ikds.eq.3) then
        diagsh=.false.
        consed=.true.
        vanri=.false.
        corvch=.false.
        modes=.true.
        meyer=.false.

! kds calculee a paritr du diagramme de shields
! calcul d'une contrainte de sedimentation
! differente de la contrainte d'erosion

      elseif (ikds.eq.4) then
        diagsh=.true.
        consed=.true.
        vanri=.false.
        corvch=.false.
        modes=.true.
        meyer=.false.

! (kds par shields +) ceq par van rijn

      elseif (ikds.eq.5) then
        diagsh=.true.
        consed=.false.
        vanri=.true.
        d90=kds
        corvch=.false.
        modes=.false.
        meyer=.false.


! kds par shields + ceq pas par van rijn

      elseif (ikds.eq.6) then
        diagsh=.true.
        consed=.false.
        vanri=.false.
        corvch=.false.
        modes=.false.
        meyer=.false.

! (kds par shields + )ceq par van rijn + correction de vch ave! vmin

      elseif (ikds.eq.7) then
        diagsh=.true.
        consed=.false.
        vanri=.true.
        d90=kds
        corvch=.true.
        modes=.false.
        meyer=.false.

! kds par shields + ceq pas par van rijn + correction de vch ave! vmin

      elseif (ikds.eq.8) then
        diagsh=.true.
        consed=.false.
        vanri=.false.
        corvch=.true.
        modes=.false.
        meyer=.false.

! kds par utilisateur + ceq pas par van rijn

      elseif (ikds.eq.9) then
        diagsh=.false.
        consed=.false.
        vanri=.false.
        corvch=.false.
        modes=.false.
        meyer=.false.

! kds par utilisateur + ceq pas par van rijn + correction de vch ave! vmin

      elseif (ikds.eq.10) then
        diagsh=.false.
        consed=.false.
        vanri=.false.
        corvch=.true.
        modes=.false.
        meyer=.false.

! kds par utilisateur + ceq =0 + erosion meyer peter
! vitesse sediments engelund
      elseif (ikds.eq.11) then
        diagsh=.false.
        consed=.false.
        vanri=.false.
        corvch=.false.
        modes=.true.
        meyer=.true.

! kds par utilisateur + ceq =0 + erosion meyer peter
! vitesse sediments proportionnelle a vitesse liquide
      elseif (ikds.eq.12) then
        diagsh=.false.
        consed=.false.
        vanri=.false.
        corvch=.false.
        modes=.false.
        meyer=.true.

! kds par utilisateur + ceq =0 + erosion meyer peter
! vitesse sediments engelund
! et deviation de la direction des sediments
      elseif (ikds.eq.13) then
        diagsh=.false.
        consed=.false.
        vanri=.false.
        corvch=.false.
        modes=.true.
        meyer=.true.
        devch=.true.

! kds par utilisateur + ceq =0 + erosion meyer peter
! vitesse sediments proportionnelle a vitesse liquide
! et deviation de la direction des sediments
      elseif (ikds.eq.14) then
        diagsh=.false.
        consed=.false.
        vanri=.false.
        corvch=.false.
        modes=.false.
        meyer=.true.
        devch=.true.
!
! kds par utilisateur + ceq =0 + erosion camenen
! vitesse sediments engelund
! et pas de deviation de la direction des sediments
      elseif (ikds.eq.15) then
        diagsh=.false.
        consed=.false.
        vanri=.false.
        corvch=.false.
        modes=.true.
        meyer=.true.
        devch=.false.
        camenen=.true.
!
! kds par utilisateur + ceq =0 + erosion camenen
! vitesse sediments engelund
! et deviation de la direction des sediments
      elseif (ikds.eq.16) then
        diagsh=.false.
        consed=.false.
        vanri=.false.
        corvch=.false.
        modes=.true.
        meyer=.true.
        devch=.true.
        camenen=.true.

! KDS PAR UTILISATEUR + CEQ =0 + erosion englund hansen
! vitesse sediments proportionnelle a vitesse liquide
      elseif (ikds.eq.17) then
        diagsh=.false.
        consed=.false.
        vanri=.false.
        corvch=.false.
        modes=.false.
        meyer=.true.
        hansen=.true.

      else
#if ENG
        write(*,*)'ERROR IN IKDS VALUE'
#else /* FRA */
        write(*,*)'ERREUR DANS LA VALEUR DE IKDS'
#endif /* FRA */
        stop
      endif
! fin du if sur ikds=0
!      endif
! on controle que les deux options sont compatibles
      if(diagsh)then
        exptaucr=.false.
        exposantaucr=0.
      endif
! fin du if sur solute
      endif
! constante pour le calcul de la contrainte critique de sedimentattion
      tk=0.0016
! le rapport des  valeurs est celui de la masse volumique selon que
! l'on exprime la concentration en kg/m3 ou en m3/m3
! sauf en entree ou en sortie on exprime les concentration en m3/m3
!      tk=dens*tk


! calcul de kds par le diagramme de shields
! renvoye dans semba2
!      if (diagsh) then
!               re=d*((dens/den-1.)*g/consnu**2)**0.333333
!           if (re.lt.4.) then
!                 kds=0.24/re
!           elseif (re.lt.10.) then
!                kds=0.14*re**(-0.64)
!           elseif (re.lt.20.) then
!                kds=0.04*re**(-0.1)
!           elseif (re.lt.150.) then
!                kds=0.013*re**0.29
!           else
!                 kds=0.055
!           endif
!! fin du if sur diagsh
!      endif
!  correctionde d90 pour �tre en accord ave! d et sigma0
        if(sigma0.gt.0.99)then
          if(d90.lt.d-eps2)then
            d90=d*sigma0**1.15
          elseif(d90.gt.2.*d*sigma0**1.15)then
            d90=d*sigma0**1.15
          endif
        else
          sigma0=1.
          if(d90.lt.d)then
             d90=d
          endif
        endif
! dans tous les cas on met dans dens et den les masses volumiques
      if(.not.conckg)then
        dens=dens*1000.
        den=den*1000.
      endif
! modification de gdd : on enleve d
      gdd=g*(dens-den)
!      gdd=g*(dens-den)*d
      if(meyer)then
! en entree alprep doit contenir la distance de chargement
! et on prend inverse
        if(alprep.lt.eps1)then
#if ENG
         write(*,*)'FOR THIS TRANSPORT OPTION, GIVE'
         write(*,*)' A CHARGE DISTANCE AT LEAST '
         write(*,*)'OF THE ORDER OF THE CELL SIZE'
#else /* FRA */
         write(*,*)'POUR CETTE OPTION DE TRANSPORT, IL FAUT DONNER'
         write(*,*)'UNE DISTANCE DE CHARGEMENT AU MINIMUM '
         write(*,*)'ORDRE DE GRANDEUR DE LA DIMENSION DES MAILLES'
#endif /* FRA */
         stop
        else
          alprep=1./alprep
        endif
! m entre est le coefficient multiplicateur de la capacite solide
        if (hansen)then
          m=m*alprep*0.05*sqrt(den)/(g*g*(dens-den)**2)
        else
          m=8.*alprep/(g*(dens-den)*sqrt(den))*m
          if(camenen)then
!    la valeur de base est 12 au lieu de 8
            m=1.5*m
          endif
! fin du if sur hansen
        endif
      endif
! determination de la presence de diffusion
      if(niux.gt.eps2.or.niuy.gt.eps2)then
           diffus=.true.
      else
           diffus=.false.
! fin du if sur la diffusion
      endif
! calcul des coefficients de diffusion
      if(sigm.ge.0.)then
         dix=niux*sigm
         diy=niuy*sigm
      else
#if ENG
          write(*,*)'NEGATIVE SCHMIDT NUMBER'
#else /* FRA */
          write(*,*)'NOMBRE DE SCHMIDT NEGATIF'
#endif /* FRA */
          stop
! fin du if sur sigm
      endif
! calcul du coefficient de correction due a la porosie
      poro=1./(1.-poro)

#endif

! fermeture de etude.par
      close(idon)
!     ouverture fichier donnees conditions initiales
      ifin=13
! si pas de reprise de calcul
      if (irep.eq.0) then
        open (ifin,file=trim(etude)//'.cin',status='old',err=11)
      endif
!      goto 21
!  21  continue
!     ouverture fichier donnees conditions limites
      ifli=14
      open (ifli,file=trim(etude)//'.cli',status='old',err=12)
      goto 22
#if ENG
  12  if(me==scribe)write (*,*) 'CAN NOT OPEN FILE ',trim(etude)//'.cli'
#else /* FRA */
  12  if(me==scribe)write (*,*) 'ON NE PEUT OUVRIR LE FICHIER ',trim(etude)//'.cli'
#endif /* FRA */
!      stop
      iclvar=0
      ifli=0
  22  continue
!     ouverture fichier donnees frottements
! 1154 if (iosmb.eq.1) then
      ifro=15
! 1154 ifro=15
      open (ifro,file=trim(etude)//'.frt',status='old',err=13)
!      goto 23
!  23  continue
!      endif
!     ouverture fichier donnees maillage
      ifdm = 8
      open (ifdm,file=trim(etude)//'.dat',status='old',err=10)
!      goto 20
!  20  continue
 101  format(a35,i1)
 102  format(a35,f11.3)
 103  format(a35,g11.3)

 106  format(a35,i2)

#if TRANSPORT_SOLIDE

! 104  format(a35,a1)
     105  format(a35,f11.9)

     107  format(a35,i4)
          if(.not.solute)then
! hmin inutilise depuis juillet 2012
!        hmin=max(d,paray)
            sg=sqrt(g)
! rzmax fixe a  25 mm/s soit 90 m en 1 heure
            rzmax=0.025
! si on utilise van rijn
            if(vanri)then
! diam�tre adimensionnel
!          diams=d*((dens/den-1.)*g)**(1./3.)*consnu**(-2./3.)
! le cas dens=den est exclu par .not.solute
! diams modifie
!          diams=d*((dens/den-1.)*g)**0.333333*consnu**(-0.666667)
              diams=((dens/den-1.)*g)**0.333333*consnu**(-0.666667)
            endif
! calcul du coefficient de pente a chaque pas de temps pour le calcul
! de la contrainte critique d erosion taucr
!       write(*,*)'point1'
            if(ppente)then
! calcul des constantes pour le calcul du coefficient de pente dont dependra taucr
! fi est la tangente de l'angle de frottement interne des sediments que
! l'on donne ici en radians (30 degres = 0.52333 radians)
! al=alpha un coefficient empirique fix� � 0.85
!          fi=tan(0.52333)
!          fi2=tan(0.52333)*tan(0.52333)
            if(exposantaucr.lt.eps1)then
              fi=0.576951981
              fi2=0.332873583
#if ENG
          write(*,*)'CRITICAL CONSTRAINT REDUCTION WITH 30 DEGREES ANGLE'
#else /* FRA */
          write(*,*)'REDUCTION CONTRAINTE CRITIQUE AVEC ANGLE DE 30 DEGRES'
#endif /* FRA */
           else
! 2 exposantaucr est l'angle en degres ici transforme en radian
             exposantaucr=2.*exposantaucr
             fi=tan(3.1416*exposantaucr/180)
#if ENG
          write(*,*)'CRITICAL CONSTRAINT REDUCTION WITH ',exposantaucr,' DEGREES ANGLE'
#else /* FRA */
          write(*,*)'REDUCTION CONTRAINTE CRITIQUE AVEC ANGLE DE',&
         &exposantaucr,' DEGRES'
#endif /* FRA */
             fi2=fi*fi
           endif
              al=0.85
            endif
! formule de meyer peter  21/d**1/6 mais ksi contient la puissance -3/2 de ks
! ksi modifie
!        ksi=0.010391*d**0.25
            ksi=0.010391
! fin di if sur solute
          endif

#endif

      return
#if ENG
#else /* FRA */
  10  if(me==scribe)write (*,*) 'ON NE PEUT OUVRIR LE FICHIER ',trim(etude)//'.dat'
#endif /* FRA */
      stop
#if ENG
  13  if(me==scribe)write (*,*) 'CAN NOT OPEN FILE ',trim(etude)//'.frt'
#else /* FRA */
  13  if(me==scribe)write (*,*) 'ON NE PEUT OUVRIR LE FICHIER ',trim(etude)//'.frt'
#endif /* FRA */
      stop
#if ENG
  25  if(me==scribe)write (*,*) 'CAN NOT OPEN FILE ',trim(etude)//'.par'
#else /* FRA */
  25  if(me==scribe)write (*,*) 'ON NE PEUT OUVRIR LE FICHIER ',trim(etude)//'.par'
#endif /* FRA */
      stop
#if ENG
  11  if(me==scribe)write (*,*) 'CAN NOT OPEN FILE ',trim(etude)//'.cin'
      if(me==scribe)write (*,*) 'DESPITE NO RESTART'
#else /* FRA */
  11  if(me==scribe)write (*,*) 'ON NE PEUT OUVRIR LE FICHIER ',trim(etude)//'.cin'
      if(me==scribe)write (*,*) 'BIEN QUE PAS DE REPRISE'
#endif /* FRA */
      stop
      end subroutine ledd


!*****************************************************************************
      subroutine ledm(ifdm,if)
!*****************************************************************************
      use module_precision, only: wp
      use module_tableaux,only:nne,etude,ne,na,nn,nas,iac,xn,yn,&
     &nrefa,iae,ieva,ieve,zfn,ina,ine,nne,ine,ina,dxe,dxen,xna,yna,xta&
     &,yta,xn,yn,xe,ye,xa,ya,zfa,zfe,fra,fre,hal,qnal,qtal,het,quet,&
     &qvet,he,que,qve,pxzfe,pyzfe,pxque,pyque,pxqve,pyqve,pxhe,pyhe,&
     &pxze,pyze,se,la,px2que,py2que,px2qve,py2qve,hae,smbu,smbv,smbu2&
     &,smbv2,smbu3,smbv3,fha,fqua,fqva,houv,qouv,xae,yae,pxqu,pxqv,&
     &pyqu,pyqv,hg1,qug1,qvg1,fluu,flvv,cvia,ncvi,cofr,flgu,flgv,&
     &vouv,vtouv,napp,smbh,nven,kse,eau,alpha,naemax,nnemax,&
     &nevemx,nnamax,nevamx,na60,neva,nevn,ievn,difqouvp,nouveau_format
      use module_laplacien
      use module_tevent
      use module_loc
      use module_lecl,only:na30,k2
      use module_mpi, only:me,scribe,machine
#if TRANSPORT_SOLIDE
      use module_ts
#endif
#if WITH_CGNS
      use cgns
      use cgns_data
#endif
!     lecture/edition donnees maillage
      implicit none
!      implicit logical (a-z)

!      integer nltmax
!      parameter (nltmax=100)
!c   nltmax :nombre maximal de courbes de tarage
      integer if,ie,ia,in,je,ja,jn,ias,ifdm
!     :        ,nae(nemax),
!     :        neve(nemax),
 !    :        nna(namax),
!     :        neva(namax),
!      integer iltmax,ialt(nltmax),nblt(nltmax),i
      integer,dimension(:),allocatable::nae,neve,nna
      integer::nevnmx
      character*9 character_ne
#if WITH_CGNS
      integer :: ier, index_coord, index_flow
      integer,dimension(:,:),allocatable :: ine_cgns
      integer :: isize(3)
#endif

      !common/nvois/nne
      !common/etud/etude
      !common/nomb/ne,na,nn
      !common/nare/nas,iac
      !common/coon/xn,yn
      !common/narf/nrefa
      !common/nrel/iae
      !common/iela/ieva
      !common/iele/ieve
      !common/znoeu/zfn
      !common/nina/ina
      !common/nine/ine

!      common/nbplt/ialt,nblt,iltmax

!     lecture donnees elements
      read (ifdm,'(a9)')character_ne
      if(character_ne(7:9).eq.'   ')then
        nouveau_format=.false.
      else
        nouveau_format=.true.
      endif
      if(nouveau_format)then
        read (character_ne,'(i9)')ne
      else
        read (character_ne(1:6),'(i6)')ne
      endif
!       If(ne.gt.nemax)then
#if ENG
!          write(*,*) 'TOO MUCH CELLS ',ne,' > ',nemax
#else
!          write(*,*) 'TROP DE MAILLES ',ne,' > ',nemax
#endif
!          stop
!       endif
      !allocation des tableaux du module_tableaux qui prennent ne en dimmension
      allocate(iae(ne,naemax),nne(ne),ine(ne,nnemax),ieve(ne,nevemx))
      allocate(dxe(ne,naemax),dxen(ne),xe(ne),ye(ne),zfe(ne))
      allocate(fra(ne),fre(ne),het(ne),quet(ne),qvet(ne))
      allocate(he(ne),que(ne),qve(ne),pxzfe(ne),pyzfe(ne),&
     &         pxque(ne),pyque(ne),pxqve(ne),pyqve(ne))
      allocate(pxhe(ne),pyhe(ne),pxze(ne),pyze(ne),se(ne),cofr(ne))
      allocate(px2que(ne),py2que(ne),px2qve(ne),py2qve(ne))
      allocate(smbu(ne),smbv(ne),smbu2(ne),&
     &         smbv2(ne),smbu3(ne),smbv3(ne))
      allocate(pxqu(ne),pxqv(ne),pyqu(ne),pyqv(ne))
      allocate(napp(ne),smbh(ne),nven(ne),kse(ne))
      allocate(eau(ne))
      !allocation des tableaux du module_laplacien
      allocate(a11(ne),a22(ne),a12(ne),deta(ne))
      allocate(xbb(ne,nevemx),ybb(ne,nevemx),xybb(ne,nevemx))
      !allocation des tableaux du module_tevent
      allocate(xvent(ne),yvent(ne))
      !allocation des tableaux locaux
      allocate(nae(ne),neve(ne))
      !allocation des tableaux du module_loc
      allocate(hem(ne),qum(ne),qvm(ne))
      allocate(hem2(ne),qum2(ne),qvm2(ne))
      allocate(th(ne),tu(ne))
      allocate(xaa(ne,nevemx),yaa(ne,nevemx))
      allocate(a110(ne),a220(ne),a120(ne),deta0(ne))
      allocate(sse(ne))
      allocate(hezer(ne))
      allocate(ustar(ne),pxuu(ne),pyuu(ne)&
     &,pxvv(ne),pyvv(ne),ue(ne),ve(ne))
      allocate(dh(ne),dqu(ne),dqv(ne))
      allocate(hed(ne))
      allocate(machine(0:ne))

      machine(0)=-1
      smbu=0.
      smbv=0.
      smbu2=0.
      smbv2=0.
      smbu3=0.
      smbv3=0.
      na60=0
      na30=0
      k2=0

      if(nouveau_format)then
         do ie = 1,ne
            read (ifdm,'(5i9)') nae(ie),(iae(ie,ja),ja=1,nae(ie))
            read (ifdm,'(5i9)') nne(ie),(ine(ie,jn),jn=1,nne(ie))
            read (ifdm,'(5i9)') neve(ie),(ieve(ie,je),je=1,neve(ie))
         enddo
!     lecture donnees aretes
         read (ifdm,'(i9)') na
      else
         do ie = 1,ne
            read (ifdm,'(5i6)') nae(ie),(iae(ie,ja),ja=1,nae(ie))
            read (ifdm,'(5i6)') nne(ie),(ine(ie,jn),jn=1,nne(ie))
            read (ifdm,'(5i6)') neve(ie),(ieve(ie,je),je=1,neve(ie))
         enddo
!     lecture donnees aretes
         read (ifdm,'(i6)') na
      endif

      !allocation des tableaux du module_tableaux qui prennent na en dimmension
      allocate(ina(na,nnamax),ieva(na,nevamx),nrefa(na))
      allocate(xna(na),yna(na),xta(na),yta(na),xa(na),ya(na),zfa(na))
      allocate(hal(na),qnal(na),qtal(na),la(na),hae(na))
      allocate(fha(na),fqua(na),fqva(na))
      allocate(houv(0:na,nevamx),qouv(0:na,0:nevamx),difqouvp(0:na,nevamx))
      allocate(xae(na,nevamx),yae(na,nevamx))
      allocate(hg1(na,nevamx),qug1(na,nevamx),qvg1(na,nevamx))
      allocate(fluu(na),flvv(na),cvia(na),ncvi(na))
      allocate(flgu(na,nevamx),flgv(na,nevamx))
      allocate(vouv(0:na,nevamx),vtouv(0:na,nevamx))

      !allocation des tableaux locaux
      allocate(nna(na),neva(na))
      !allocation des tableaux du module_loc
      allocate(fxuu(na),fyuu(na),fxvv(na),fyvv(na))
      allocate(qouvp(0:na,0:nevamx))

       hg1=0.

      if(nouveau_format)then
          do ia = 1,na
             read (ifdm,'(5i9)') nna(ia),(ina(ia,jn),jn=1,nna(ia))
             read (ifdm,'(6i9)')nrefa(ia),neva(ia),(ieva(ia,je),je=1,neva(ia))
          enddo
!     lecture donnees noeuds
          read (ifdm,'(i9)') nn
      else
          do ia = 1,na
             read (ifdm,'(5i6)') nna(ia),(ina(ia,jn),jn=1,nna(ia))
             read (ifdm,'(6i6)')nrefa(ia),neva(ia),(ieva(ia,je),je=1,neva(ia))
          enddo
!     lecture donnees noeuds
          read (ifdm,'(i6)') nn
      endif
      !allocation des tableaux du module_tableaux qui prennent nn en dimmension
      allocate(xn(nn),yn(nn),zfn(nn))
      allocate(nevn(nn))
      nevn(:)=0
      nevnmx=0
      do ie=1,ne
        do in=1,nne(ie)
          nevn(ine(ie,in))=nevn(ine(ie,in))+1
          if (nevn(ine(ie,in))>nevnmx)then
            nevnmx=nevn(ine(ie,in))
          endif
        end do
      end do
      nevn(:)=0
      allocate(ievn(nn,nevnmx))
      ievn(:,:)=0
      do ie=1,ne
        do in=1,nne(ie)
          nevn(ine(ie,in))=nevn(ine(ie,in))+1
          ievn(ine(ie,in),nevn(ine(ie,in)))=ie
        end do
      end do

      if(nouveau_format)then
         read(ifdm,'(10f13.4)')(xn(in),in=1,nn)
         read(ifdm,'(10f13.4)')(yn(in),in=1,nn)
         read(ifdm,'(10f8.3)',end=121)(zfn(in),in=1,nn)
         read(ifdm,'(i9)',end=121) nas
!      if (nas.ne.0) then
         read (ifdm,'(13i9)',end=121)(iac(ias),ias=1,nas)
!      endif
      else
         read(ifdm,'(10f8.1)')(xn(in),in=1,nn)
         read(ifdm,'(10f8.1)')(yn(in),in=1,nn)
         read(ifdm,'(10f8.3)',end=121)(zfn(in),in=1,nn)
         read(ifdm,'(i6)',end=121) nas
!      if (nas.ne.0) then
         read (ifdm,'(13i6)',end=121)(iac(ias),ias=1,nas)
!      endif
      endif
 121  close(ifdm)
      if (me==scribe)then
#if WITH_CGNS
      !!! test cgns !!!
      isize(1)=nn
      isize(2)=ne
      isize(3)=0
      call cg_open_f('grid.cgns',CG_MODE_WRITE,index_file,ier)
      if (ier .ne. CG_OK) call cg_error_exit_f
      call cg_base_write_f(index_file,'Base',2,3,index_base,ier)
      call cg_zone_write_f(index_file,index_base,'Zone1',isize,Unstructured,index_zone,ier)
      call cg_coord_write_f(index_file,index_base,index_zone,RealDouble,'CoordinateX',xn,index_coord,ier)
      call cg_coord_write_f(index_file,index_base,index_zone,RealDouble,'CoordinateY',yn,index_coord,ier)
      call cg_coord_write_f(index_file,index_base,index_zone,RealDouble,'CoordinateZ',zfn,index_coord,ier)

      allocate(ine_cgns(4,ne))
      do ie=1,ne
        do in=1,nne(ie)
          ine_cgns(in,ie)=ine(ie,in)
        enddo
        if (nne(ie)<4) then
          ine_cgns(4,ie)=ine_cgns(3,ie)
        end if
      end do
      call cg_section_write_f(index_file,index_base,index_zone,'Elem',QUAD_4,1,ne,0,ine_cgns,index_section,ier)
!       write(sol_names(1),*)'het'
!       write(sol_names(2),*)'quet'
!       write(sol_names(3),*)'qvet'
!       write(sol_names(4),*)'ce'
!       write(sol_names(5),*)'dhe'
!       write(sol_names(6),*)'vitfro'
!       write(sol_names(7),*)'zfn'
!       call cg_sol_write_f(index_file,index_base,index_zone,'SolCells',CellCenter,index_flow,ier)
!       call cg_sol_write_f(index_file,index_base,index_zone,'SolVertex',Vertex,index_flow,ier)
!       call cg_close_f(index_file,ier)
!       allocate(times_cgns(501), sol_names(501), sol_names2(501))
      call c32_allocate(sol_names,10)
      call c32_allocate(sol_names2,10)
      call c32_allocate(gridmotionpointers,10)
      call c32_allocate(gridcoordpointers,10)
      call d_allocate(times_cgns,10)
      deallocate(ine_cgns)
      nb_timesteps_cgns=0
      !!! fin test cgns !!!
#endif
!     edition donnees maillage
      if (if.eq.0) goto 300
      if (if.eq.21.or.if.eq.23.or.if.eq.25.or.if.eq.27) then
      write (if,*)
#if ENG
      write (if,*) '************************************************'
      write (if,*) '            mesh data  '
      write (if,*) '************************************************'
      write (if,*)
      write (if,*) 'number of inlet edges = ',nas
#else /* FRA */
      write (if,*) '************************************************'
      write (if,*) '            donnees  du  maillage  '
      write (if,*) '************************************************'
      write (if,*)
      write (if,*) 'nombre d''aretes rentrantes = ',nas
#endif /* FRA */
      write (if,*) '============================='
      write (if,*)
      do 230 ias=1,nas
#if ENG
      write(if,*) 'number of the ',ias,'th inlet edge: ',iac(ias)
#else /* FRA */
      write(if,*) 'numero de la ',ias,'eme arete rentrante: ',iac(ias)
#endif /* FRA */
 230  continue
      write (if,*)
#if ENG
      write (if,*) 'number of edges = ',na
#else /* FRA */
      write (if,*) 'nombre d aretes = ',na
#endif /* FRA */
      write (if,*) '============================'
      write (if,*)
      do 210 ia = 1,na
#if ENG
         write (if,*)'edge ',ia,' vertices: ',(ina(ia,jn),jn=1,nna(ia))
         write (if,*) 'neighbor cells : ',(ieva(ia,je),je=1,neva(ia))
#else /* FRA */
         write (if,*)'arete ',ia,' noeuds: ',(ina(ia,jn),jn=1,nna(ia))
         write (if,*) 'elements voisins : ',(ieva(ia,je),je=1,neva(ia))
#endif /* FRA */
 210  continue
      endif
      if (if.eq.21.or.if.eq.24.or.if.eq.26.or.if.eq.27) then
      write (if,*)
#if ENG
      write (if,*) 'number of vertices = ',nn
      write (if,*) '============================='
      write (if,*)
      write (if,*) '   vertex     x vertex     y vertex     zf vertex'
#else /* FRA */
      write (if,*) 'nombre de noeuds = ',nn
      write (if,*) '============================='
      write (if,*)
      write (if,*) '   noeud      x noeud      y noeud      zf noeud'
#endif /* FRA */
      write (if,*) '---------------------------------------------'
      do 220 in = 1,nn
         write (if,1) in,xn(in),yn(in),zfn(in)
   1     format(i9,f13.4,f13.4,5x,f8.3)
 220  continue
      endif
      if (if.eq.21.or.if.eq.22.or.if.eq.25.or.if.eq.26) then
#if ENG
      write (if,*) 'number of cells = ',ne
#else /* FRA */
      write (if,*) 'nombre d elements = ',ne
#endif /* FRA */
      write (if,*) '=============================='
         write (if,*)
      do 200 ie = 1,ne
#if ENG
         write (if,*) 'cell nb ',ie
         write (if,*) 'edge indices    : ',(iae(ie,ja),ja=1,nae(ie))
         write (if,*) 'vertice indices : ',(ine(ie,jn),jn=1,nne(ie))
         write (if,*) 'neighbor cells  : ',(ieve(ie,je),je=1,neve(ie))
#else /* FRA */
         write (if,*) 'element no ',ie
         write (if,*) 'indices aretes   : ',(iae(ie,ja),ja=1,nae(ie))
         write (if,*) 'indices noeuds   : ',(ine(ie,jn),jn=1,nne(ie))
         write (if,*) 'elements voisins : ',(ieve(ie,je),je=1,neve(ie))
#endif /* FRA */
 200  continue
      endif
 300  continue
      end if ! me==scribe
#if TRANSPORT_SOLIDE
      !allocation des tableaux du module_ts utilisant ne comme param�tre
      allocate(diamsmb(ne),sigmasmb(ne),sigmae(ne),diame(ne)&
     &,sigmae2(ne),diame2(ne),eseldt(ne),dhe(ne),hcet(ne)&
     &,hce(ne),smbhc(ne),pxhc(ne),pxhce(ne),pyhce(ne)&
     &,pyhc(ne),smbhc2(ne),zfe0(ne),cet(ne),vitfro(ne)&
     &,ceq(ne),taue(ne),rapsmbhc(ne),zfe2(ne),cofr2(ne))

      !allocation des tableaux du module_ts utilisant na comme param�tre
      allocate(diama(na),sigmaa(na),sigmaouv(0:na,nevamx),diamouv(0:na,nevamx)&
     &,hcg1(na,nevamx),flhc(na),hcal(na),fhca(na),coouv(0:na,nevamx),rapva(na),zfa2(na))
!cou1d      allocate(ccouplts(na),dcouplts(na),scouplts(na))

      !allocation des tableaux du module_ts utilisant nn comme param�tre
      allocate(diamn(nn,ncousedmax),epcoun(nn,ncousedmax),sigman(nn,ncousedmax)&
      &,taummn(nn,ncousedmax),diamn2(nn),epcoun2(nn),taummn2(nn),sigman2(nn)&
      &,diamdhn(nn),sigmadhn(nn),diamnt(nn),sigmant(nn),nbcoun(nn),nbcoun2(nn)&
      &,zfndur(nn),dzfn(nn),imailczero(nn),taun(nn),zfn2(nn))

#if SEDVAR
      allocate(eron(nn))
!end if sedvar
#endif


      ! constantes et initialisations : Parties transport solide
      do ie=1,ne
        smbhc(ie)=0.
        smbhc2(ie)=0.
        vitfro(ie)=0.
        ceq(ie)=0.
        hcet(ie)=0.
        rapsmbhc=1.d0
      enddo
      do  in=1,nn
         dzfn(in)=0.
      enddo
      do ia=1,na
         rapva(ia)=1.d0
      enddo

      voloa=0.d0
      eron = .false.
#endif /* TRANSPORT_SOLIDE */
      deallocate(nae,neve)
      deallocate(nna)
      end subroutine ledm

!******************************************************************************
      subroutine lefr(ifro,if,iofr)
!******************************************************************************
!     lecture/edition des frottements
!  entree :
!     cofr   : coefficient d'horizontalite de l'element (voir cedce)
!  sortie :
!     fra,fre : frottements
!******************************************************************************
      use module_precision, only: wp
      use module_tableaux, only: fra,fre,cofr,het,quet,qvet,kse,&
     &eps1,eps2,paray,ne,na,nn,darcy,g
      use module_mpi,only:me,scribe
      implicit none

      integer ie,if,ifro,iofr

! variable pour frottement darcy
      real(wp) q,h,fm1,nu
      real(wp),dimension(:),allocatable::fr

      data nu/0.000001/
      save nu


      !common/param/eps1,eps2,paray
      !common/nomb/ne,na,nn
      !common/frot/fra,fre
      !common/penfon/cofr
      !common/tlig/het,quet,qvet
      !common/darcy/darcy
      !common/kdarcy/kse
      !common/grav/g

      allocate(fr(ne))
!
      darcy=.false.
      if (iofr.eq.0) then
      if (me==scribe)then
          write(*,*)'lecture frottements en chezy'
      end if ! me==scribe
      elseif (iofr.eq.1) then
      if (me==scribe)then
          write(*,*)'lecture frottements en strickler'
      end if ! me==scribe
      else
          iofr=2
          darcy=.true.
      if (me==scribe)then
          write(*,*)'lecture rugosites de darcy-weisbach'
      end if ! me==scribe
      endif
      read(ifro,'(10f8.3)')(fr(ie),ie=1,ne)
      close(ifro)
      if (me==scribe)then
!     edition frottements
      if (if.ne.0) then
        write (if,*)
        write (if,*) '************************************************'
        write (if,*)
        if (iofr.eq.0) then
          write(if,*)'            frottements en chezy'
        elseif (iofr.eq.1) then
          write(if,*)'            frottements en strickler'
        else
          write(if,*)'            frottements en darcy-weisbach'
        endif
        write (if,*) '************************************************'
        write (if,*)
        write (if,*) 'nombre d elements = ',ne
        write (if,*) '=============================='
        do 200 ie = 1,ne
          write (if,*) 'element no =',ie,' frottements : ',fr(ie)
 200    continue
        close(if)
      endif
      end if ! me==scribe
! iofr = 0 :calcul en chezy
! iofr = 1:calcul en strickler avec si y faible un calcul en chezy=10-6
       if (iofr.eq.0) then
        do 1002 ie=1,ne
           fra(ie)=cofr(ie)*fr(ie)*fr(ie)
           fre(ie)=eps2
 1002   continue
       elseif (iofr.eq.1) then
        do 1003 ie=1,ne
           if (fr(ie).lt.eps2) then
             fre(ie)=1.e+08
           else
             fre(ie)=cofr(ie)*fr(ie)*fr(ie)
           endif
           fra(ie)=0.000001
 1003   continue
       elseif (iofr.eq.2) then
        do 1004 ie=1,ne
! dans ce cas on a rentre la rugosite en mm
        kse(ie)=fr(ie)*0.001
        q=sqrt(quet(ie)*quet(ie)+qvet(ie)*qvet(ie))
! parce que nu vaut 10-6 et qu'on veut donner une valeur pour un petit h
        h=max(nu,het(ie))
        if(q.gt.eps2)then
          fm1=4.*(dlog10(kse(ie)/(12.*h)+1.95/(q/nu)**0.9))**2
        else
          fm1=4.*(dlog10(kse(ie)/(12.*h)))**2
        endif
        fre(ie)=cofr(ie)*8.*g*fm1*h**(-0.333333)
!        fre(ie)=cofr(ie)*fr(ie)*fr(ie)
        fra(ie)=0.1
 1004   continue
      endif
      deallocate(fr)
      end subroutine lefr

!******************************************************************************
      subroutine lenv
!=======================================================================
! lecture et ecriture des enveloppes sur etude.env
!    au premier pas de temps lecture si reprise
!=======================================================================
      use module_tableaux
      use module_loc, only:hem,qum,qvm,hem2,qum2,qvm2,th,tu
      use module_mpi,only:me,scribe,ne_loc,mailles_loc
#ifdef WITH_MPI
      use module_messages
#endif /* WITH_MPI */
!$    use omp_lib
      implicit none

      !include 'rubar20_common.for'

! variables locales
      integer :: ie,j,ieloc
      real(wp) :: tt,vx,vy
      real(wp), save :: hmin
      !real(wp),save,dimension(:),allocatable:: hem,qum,qvm
! ajout du 29/10/2007 : hauteur et vitesse aux temps du maximum
      !real(wp),dimension(:),allocatable::hem2,qum2,qvm2
      !real(wp),dimension(:),allocatable::th,tu

      !allocate(hem(ne),qum(ne),qvm(ne))
      !allocate(hem2(ne),qum2(ne),qvm2(ne))
      !allocate(th(ne),tu(ne))
! hauteur minimale pour retenir les vitesses maxi
      hmin = 0.01
      if (tm .eq. tinit) then
         if (irep .eq. 1) then
            open(ifen,file=trim(etude)//'.env',status='unknown')
            read(ifen,*) tt
            if (me==scribe) then
              if (tt .ne. tinit) then
!fra                 write(*,*) 'TINIT= ',tinit,'  T ENVELOPPE= ',tt
!fra                 write(*,*) 'ATTENTION LES MAXIMA POSTERIEURS A TINIT'
!fra                 write(*,*) 'NE SERONT PAS CONSERVES'
              endif
            end if ! me==scribe
            if(nouveau_format)then
               do ie = 1,ne
                  read(ifen,6) j,th(ie),hem(ie),tu(ie),qum(ie),qvm(ie), &
     &                      hem2(ie),qum2(ie),qvm2(ie)
                  if (th(ie) .gt. tinit) then
                      hem(ie) = 0.
                      qum2(ie) = 0.
                      qvm2(ie) = 0.
                      th(ie) = tinit
                  endif
                  if (tu(ie) .gt. tinit) then
                     hem2(ie)=0.
                     qum(ie)=0.
                     tu(ie)=tinit
                     qvm(ie)=0.
                  endif
               enddo
            else  ! else de nouveau_format
               do ie = 1,ne
                  read(ifen,5) j,th(ie),hem(ie),tu(ie),qum(ie),qvm(ie), &
     &                      hem2(ie),qum2(ie),qvm2(ie)
                  if (th(ie) .gt. tinit) then
                      hem(ie) = 0.
                      qum2(ie) = 0.
                      qvm2(ie) = 0.
                      th(ie) = tinit
                  endif
                  if (tu(ie) .gt. tinit) then
                     hem2(ie)=0.
                     qum(ie)=0.
                     tu(ie)=tinit
                     qvm(ie)=0.
                  endif
               enddo
            endif  ! endif de nouveau_format
            close(ifen)
         else
            open(ifen,file=trim(etude)//'.env',status='unknown')
            close(ifen)
            do ie=1,ne
              hem(ie)=0.
              qum2(ie)=0.
              qvm2(ie)=0.
              th(ie)=tinit
              hem2(ie)=0.
              qum(ie)=0.
              tu(ie)=tinit
              qvm(ie)=0.
            enddo
         endif
      endif
! calcul des maxima
!       if (me/=scribe)then
!$omp parallel private(vx,vy)
!$omp do schedule(runtime)
        do ieloc = 1,ne_loc(me)
          ie=mailles_loc(ieloc)
          if(het(ie).gt.paray)then
            if (het(ie) .gt. hem(ie)) then
              hem(ie) = het(ie)
              th(ie) = tm
              qum2(ie) = quet(ie)/het(ie)
              qvm2(ie) = qvet(ie)/het(ie)
            endif
          endif
          if (het(ie) .gt. hmin) then
            vx = quet(ie)/het(ie)
            vy = qvet(ie)/het(ie)
            if (sqrt(vx**2+vy**2) > sqrt(qum(ie)**2+qvm(ie)**2)) then
               qum(ie) = vx
               qvm(ie) = vy
               hem2(ie) = het(ie)
               tu(ie) = tm
            endif
          endif
        enddo
!$omp end do
!$omp end parallel
!       end if !if me/=scribe
#ifdef WITH_MPI
       if ((tm.ge.tr .or. tm.eq.tmax) .and. ifen>0) then
         call message_gatherv2(th)
         call message_gatherv2(hem)
         call message_gatherv2(hem2)
         call message_gatherv2(tu)
         call message_gatherv2(qum)
         call message_gatherv2(qum2)
         call message_gatherv2(qvm)
         call message_gatherv2(qvm2)
       end if
#endif /* WITH_MPI */
! ecriture sur etude.env en meme temps qu'ecriture sur tps
      if (me==scribe)then
      if (tm.ge.tr .or. tm.eq.tmax) then
         open(ifen,file=trim(etude)//'.env',status='unknown')
         write(ifen,'(f15.3)') tm
         if(nouveau_format)then
            do ie = 1,ne
               if (hem(ie) .gt. 9999.999) hem(ie) = 9999.999
               if (qum(ie) .gt. 9999.999) then
                  qum(ie) = 9999.999
               elseif (qum(ie).lt.-9999.999) then
                  qum(ie) = 9999.999
               endif
               if (qvm(ie) .gt. 9999.999) then
                  qvm(ie) = 9999.999
               elseif (qvm(ie).lt.-9999.999) then
                  qvm(ie) = 9999.999
               endif
               if (hem2(ie) .gt. 9999.999)hem2(ie) = 9999.999
               if (qum2(ie) .gt. 9999.999) then
                  qum2(ie) = 9999.999
               elseif (qum2(ie).lt.-9999.999) then
                  qum2(ie) = 9999.999
               endif
               if (qvm2(ie) .gt. 9999.999) then
                  qvm2(ie) = 9999.999
               elseif (qvm2(ie).lt.-9999.999) then
                  qvm2(ie) = 9999.999
               endif

               write(ifen,6) ie,th(ie),hem(ie),tu(ie),qum(ie),qvm(ie),&
     &                    hem2(ie),qum2(ie),qvm2(ie)
            enddo
        ! else du if sur nouveau_format
         else
            do ie = 1,ne
               if (hem(ie) .gt. 9999.999) hem(ie) = 9999.999
               if (qum(ie) .gt. 9999.999) then
                  qum(ie) = 9999.999
               elseif (qum(ie).lt.-9999.999) then
                  qum(ie) = 9999.999
               endif
               if (qvm(ie) .gt. 9999.999) then
                  qvm(ie) = 9999.999
               elseif (qvm(ie).lt.-9999.999) then
                  qvm(ie) = 9999.999
               endif
               if (hem2(ie) .gt. 9999.999)hem2(ie) = 9999.999
               if (qum2(ie) .gt. 9999.999) then
                  qum2(ie) = 9999.999
               elseif (qum2(ie).lt.-9999.999) then
                  qum2(ie) = 9999.999
               endif
               if (qvm2(ie) .gt. 9999.999) then
                  qvm2(ie) = 9999.999
               elseif (qvm2(ie).lt.-9999.999) then
                  qvm2(ie) = 9999.999
               endif

               write(ifen,5) ie,th(ie),hem(ie),tu(ie),qum(ie),qvm(ie),&
     &                    hem2(ie),qum2(ie),qvm2(ie)
             enddo
        ! fin du if sur nouveau_format
         endif
         close (ifen)
      endif
      end if ! me==scribe

 5    format(i6,f15.3,f8.3,f15.3,f15.3,f15.3,f8.3,f15.3,f15.3)
 6    format(i9,f15.3,f8.3,f15.3,f15.3,f15.3,f8.3,f15.3,f15.3)
      !deallocate(hem,qum,qvm)
      !deallocate(hem2,qum2,qvm2)
      !deallocate(th,tu)

      end subroutine lenv

!******************************************************************************
      subroutine leven(if)
!******************************************************************************
!     lecture/edition des vents
!  entree :
!  sortie :
!******************************************************************************

      use module_precision, only: wp
      use module_tableaux, only : nven,nchrov,nbchrv,ne,na,nn,&
     &ventx,venty,tvent,nmchro, nmcoap,pven,etude,id_eve_ven
      use module_mpi,only:me,scribe

      implicit none
      integer ie,if,iven
      integer ichro,ncouap!,nbchrv(nmchro),nven(nemax),nchrov

      !common/nomb/ne,na,nn
      !common/nvent/nven,nchrov,nbchrv
      !common/vents/ventx,venty,tvent
      !common/lvent/pven
      !common/etud/etude
!
      iven=id_eve_ven
      open (iven,file=trim(etude)//'.ven',status='old',err=130)
      read(iven,'(10i8)',end=131)(nven(ie),ie=1,ne)
      read(iven,'(i8)',end=131)nchrov
      if (nchrov .gt. nmchro) then
        if(me==scribe)write(*,*)'plus de ',nmchro,' chroniques de vent'
!        pause
        stop
      endif
      do ichro=1,nchrov
        read(iven,'( i8)',end=131) nbchrv(ichro)
      if (nbchrv(ichro) .gt. nmcoap) then
        if(me==scribe)write(*,*)'plus de ',nmcoap,' temps'
        if(me==scribe)write(*,*)'dans la chronique de vent ',ichro
!        pause
        stop
      endif
        do ncouap=1,nbchrv(ichro)
          read(iven,'(3f15.4)',end=131)ventx(ichro,ncouap)&
     &,venty(ichro,ncouap),tvent(ichro,ncouap)
! transformation de mm/h en m/s
!          appchr(ichro,ncouap)=appchr(ichro,ncouap) *0.001/3600.
        enddo
      enddo
      close(iven)
      pven=.true.
      go to 132
 130  pven=.false.
      if=0
      return
 131  if(me==scribe)write(*,*)'probleme de lecture du fichier ',trim(etude)//'.ven'
      if(me==scribe)write(*,*)'le vent est lu dans le fichier ',trim(etude)//'.par'
      pven=.false.
      if=0
      return
 132  continue
!     edition vents
      if (me==scribe)then
      if (if.ne.0) then
        open (if,file=trim(etude)//'.eve',status='unknown')
        write (if,*)
        write (if,*) '************************************************'
        write (if,*) 'vents  lus et ecrits en m/s'
!        write (if,*) '            apports  en mm/heure'
        write (if,*) '************************************************'
        write (if,*)
        write (if,*) 'nombre d elements = ',ne
        write (if,*) '=============================='
        write(if,*)
        do 200 ie = 1,ne
          write (if,*) 'element no =',ie,' chronique : ',nven(ie)
 200    continue
        write(if,'(a30,i8)')'nombre total de chroniques',nchrov
        do ichro=1,nchrov
          write(if,'( a30,i8)')'numero de la chronique',ichro
          do ncouap=1,nbchrv(ichro)
            write(if,'(a7,i8,a2,f15.10,a1,f15.10,a15,f15.4)')&
     &' vent ',ncouap&
     &,' =',ventx(ichro,ncouap),' ',venty(ichro,ncouap),&
     &' temps vent =',tvent(ichro,ncouap)
!            write(if,'(a7,i8,a2,f15.4,a15,f15.4)')' apport',ncouap,' =',
!     :appchr(ichro,ncouap),' temps apport =',tappch(ichro,ncouap)
          enddo
          write(if,*)
        enddo

        close(if)
      endif
      end if ! me==scribe
      return
      end subroutine leven


!******************************************************************************
      subroutine nwt(qul,y,dy,he,ue,hl)
!******************************************************************************
!     resolution par newton de l'equation en hl : ql = y(he,ue,hl)
!
!     entrees:
!     qul    valeur de qu sur l'extremite ou s'applique la c.l.
!     he,ue  valeurs de h et u sur l'autre extremite
!     y,dy   fonctions donnees (y notee gama dans vila et dy derivee de y)
!     eps1    eps1ilon machine (common param)
!     prec   precision sur abs(x-y) dans newton (common newton)
!     nitmax nombre maxi d'iterations par newton autorise (common newton)
!
!     sorties:
!     hl    valeur de h sur l'extremite ou s'applique la c.l.
!******************************************************************************
!      implicit logical (a-z)

      use module_tableaux, only:eps1,eps2,paray,prec,nitmax,ift,idl
      use module_precision
      implicit none

      integer niter
      !,idl
      real(wp) qul,he,ue,hl,y,dy,xl
      real(wp) delta,hlp,hlm,deltap,deltam
      external y,dy
      !common/param/eps1,eps2,paray
      !common/newton/prec,nitmax
      !common/iftr/ift

! compte de messages avertissement
!      data idl/0/
!      save idl

! xl inutile mais par coherence
      xl=1.

!     valeur initiale
      hl = he
      if (hl.le.paray) then
          hl = 0.0
          return
      endif
      delta = abs(qul - y(he,ue,hl,xl))
      if (delta.lt.prec) goto 100
      niter = 1

! en general resolution par dichotomie emtre 1/2 he et 1,5 he
      hlp= 1.5*he
      hlm=0.5*he
      deltap=qul-y(he,ue,hlp,xl)
      deltam=qul-y(he,ue,hlm,xl)
      if (deltap.lt.0.) then
        if (deltam.lt.0.) then
! on resout en newton
          go to 10
        else
          hlm=hlp
          hlp=0.5*he
        endif
      else
        if (deltam .gt. 0.) then
! on resout en newton
          go to 10
        endif
      endif
! resolution par dichotomie
 110  niter=niter+1
      if (niter .gt. nitmax) then
        hl=0.5*(hlp+hlm)
        go to 100
      else
        hl=0.5*(hlp+hlm)
        delta=qul-y(he,ue,hl,xl)
        if (delta .gt. prec) then
          hlp=hl
          go to 110
        elseif (delta.lt.-prec) then
          hlm=hl
          go to 110
        else
          go to 100
        endif
      endif

!     boucle d'iteration newton
 10   niter = niter + 1
          delta=dy(he,ue,hl,xl)
          if (abs(delta).lt.prec)return
          hlp = hl + (qul - y(he,ue,hl,xl)) / delta
          if (hlp.le.paray) then
              hl = 0.0
              goto 100
          endif
          delta = abs(qul - y(he,ue,hlp,xl))
          if (delta.lt.prec) then
              hl = hlp
              goto 100
          endif
          if (niter .gt. nitmax) then
!26            if (ift .gt. 0) then
!              write(*,*)
            if (abs(delta) .gt. sqrt(prec)) then
              if (idl.lt.1000) then
                idl=idl+1
                write(*,*) 'nwt : niter > nitmax'
                write(*,*)'delta:',delta,' qul:',qul,' q:',y(he,ue,hlp,xl)
              endif
            endif
!26            endif
            hl=hlp
            goto 100
          endif
          hl = hlp
      goto 10
 100  continue
      end subroutine nwt

!******************************************************************************
      subroutine calcvl(qul,zl,dq,i)
!******************************************************************************
! calcule les valeurs de q(h) dans le cas d'une loi de tarage

      use module_tableaux,only:qlt,zlt,ialt,nblt,iltmax,nltmax,nplmax
      use module_precision

      implicit none

      integer j,i
      real(wp) qul,zl,dq

      !common/qzlt/qlt,zlt
      !common/nbplt/ialt,nblt,iltmax
!       do j=1,nblt(i)
!         if (zl.lt.zlt(i,j))
!         else
!             qul=qul*qlt(i,nblt(i))
!             dq=0.
!             return
!         endif
!       end do
      do 1 j=1,nblt(i)
        if (zl.lt.zlt(i,j))go to 2
 1    continue
      qul=qul*qlt(i,nblt(i))
      dq=0.
      return
 2    if (j.eq.1) then
        qul=qul*qlt(i,1)
        dq=0.
      else
        dq=qul*(qlt(i,j)-qlt(i,j-1))/(zlt(i,j)-zlt(i,j-1))
        qul=qul*qlt(i,j-1)+(zl-zlt(i,j-1))*dq
      endif
      return
      end subroutine calcvl

!******************************************************************************
      subroutine nwt2(qul,y,dy,he,ue,hl,xl,ia)
!******************************************************************************
!     resolution par newton de l'equation en hl : qul = y(he,ue,hl)
!           avec qul et hl lies par une loi de tarage et qul inconnu
!
!     entrees:
!     ia d'ou la loi de tarage
!     he,ue  valeurs de h et u sur l'autre extremite
!     y,dy   fonctions donnees (y notee gama dans vila et dy derivee de y)
!     eps1    eps1ilon machine (common param)
!     prec   precision sur abs(x-y) dans newton (common newton)
!     nitmax nombre maxi d'iterations par newton autorise (common newton)
!
!     sorties:
!     qul hl    valeurs de h et q sur l'extremite ou s'applique la c.l.
!******************************************************************************
!      implicit logical (a-z)
!

      use module_tableaux,only:zfa,nltmax,nplmax,eps1,eps2,paray,&
     &prec,nitmax,ift,ialt,nblt,iltmax,idl
      use module_precision
      implicit none

      integer ia,i
      integer niter
!      integer idl
      real(wp) qul,he,ue,hl,y,dy,qul0,dq,zf
      real(wp) delta,hlp,xl
      external y,dy

      !common/param/eps1,eps2,paray
      !common/newton/prec,nitmax
      !common/iftr/ift
      !common/nbplt/ialt,nblt,iltmax
      !common/zare/zfa
! compte de messages avertissement
!      data idl/0/
!      save idl
      zf=0.
      do 1 i=1,iltmax
        if (ia.eq.ialt(i)) then
          zf=zfa(ia)
          go to 2
        endif
  1   continue
!     valeur initiale
  2   hl = he
! on garde dans qul0 le signe de qul
       qul0=qul
      call calcvl(qul,hl+zf,dq,i)
!      if (hl.le.paray) then
!          hl = 0.0
!          return
!      endif
      delta = abs(qul - y(he,ue,hl,xl))
!    write(*,*)'delta',delta,qul,he,ue,hl
      if (delta.lt.prec) return
      niter = 1

!     boucle d'iteration newton
 10   niter = niter + 1
          qul=qul0
          call calcvl(qul,hl+zf,dq,i)
          dq=dy(he,ue,hl,xl)-dq
          if (abs(dq) .gt. prec) then
            hlp = hl + (qul - y(he,ue,hl,xl)) / dq
          else
            write(*,*)'non convergence pour la loi de tarage'
!            write(*,*)'nwt21',hl,qul,niter
            return
          endif
!          hlp = hl + (qul - y(he,ue,hl)) / dy(he,ue,hl)
          if (hlp.le.paray) then
              hl = 0.0
!        write(*,*)'nwt22',hl,qul,niter
              return
          endif
          qul=qul0
          call calcvl(qul,hlp+zf,dq,i)
          delta = abs(qul - y(he,ue,hlp,xl))
          if (delta.lt.prec) then
              hl = hlp
!        write(*,*)'nwt23',hl,qul,niter
              return
          endif
          if (niter .gt. nitmax) then
!26            if (ift .gt. 0) then
!              write(*,*)
            if (delta .gt. sqrt(prec)) then
              if (idl.lt.1000) then
                idl=idl+1
                write(*,*) 'nwt2 : niter > nitmax'
                write(*,*)'delta:',delta,' qul:',qul,' q:',y(he,ue,hlp,xl)
              endif
            endif
!26            endif
            hl=hlp
!        write(*,*)'nwt24',hl,qul,niter

            return
          endif
          hl = hlp
      goto 10
! 100  qul=qul*5./6.*hl**2
!        write(*,*)'nwt25',hl,qul,niter
      return
      end subroutine nwt2

! !******************************************************************************
!       subroutine nwt3(qul,y,he,ue,hl)
! !******************************************************************************
! !     resolution par dichotomie de l'equation en hl : qul = y(he,ue,hl)
! !           avec qul et hl lies par froude =1  et qul inconnu
! !
! !     entrees:
! !     he,ue  valeurs de h et u sur l'autre extremite
! !     y  fonctions donnees (y notee gama dans vila )
! !     eps1    eps1ilon machine (common param)
! !     prec   precision sur abs(x-y) dans newton (common newton)
! !     nitmax nombre maxi d'iterations par newton autorise (common newton)
! !
! !     sorties:
! !     qul hl    valeurs de h et q sur l'extremite ou s'applique la c.l.
! !******************************************************************************
! !      implicit logical (a-z)
!       use module_precision
!       implicit none
!
!       integer nltmax,nplmax
!       parameter (nltmax=100,nplmax=500)
!       integer niter,nitmax
!       integer idl
!       real(wp) eps1,eps2,qul,he,ue,hl,y,qul0
!       real(wp) prec,delta,hlp,paray,g,sg,hlm
!       external y
!
!       common/param/eps1,eps2,paray
!       common/newton/prec,nitmax
!       common/grav/g
! ! compte de messages avertissement
!       data idl/0/
!       save idl
!
!       sg=sqrt(g)
! !     valeur initiale
!       hlp = 2.*he
!       hlm=0.
! ! on garde dans qul0 le signe de qul
!       qul0=qul
!       niter = 1
! !     boucle d'iteration dichotomie
!  10   niter = niter + 1
! ! froude =1
!       hl=0.5*(hlp+hlm)
!       qul=qul0*sg*hl**1.5
!       delta = qul0*(qul - y(he,ue,hl))
!       if (delta .gt. prec) then
!          hlp=hl
!       elseif (delta.lt.-prec) then
!          hlm=hl
!        else
!          go to 100
!        endif
!           if (niter .gt. nitmax) then
!              if (abs(delta) .gt. sqrt(prec)) then
!           if (idl.lt.1000) then
!              write(*,*) 'nwt3 : niter > nitmax'
!              write(*,*)'delta:',delta,' qul:',qul,' q:',y(he,ue,hlp)
!               idl=idl+1
!              endif
!           endif
!
!             goto 100
!           endif
!       go to 10
!  100  return
!       end subroutine nwt3

!******************************************************************************
      subroutine critd(qul,he,ue,hl)
!******************************************************************************
!     resolution par de l'equation en hl : qul = gamad(he,ue,hl)
!           avec qul et hl lies par froude =1  et qul inconnu
!
!     entrees:
!     he,ue  valeurs de h et u sur l'autre extremite
!
!     sorties:
!     qul hl    valeurs de h et q sur l'extremite ou s'applique la c.l.
!******************************************************************************
!      implicit logical (a-z)
      use module_tableaux, only:g,idl
      use module_precision
      implicit none

      real(wp) qul,he,ue,hl
!      integer idl

      !common/grav/g
! compte de messages avertissement
!      data idl/0/
!      save idl

! la vitesse donnee par gamad/hl vaut ue+2ghl**.5-2ghe**.5
      qul=(2.*sqrt(g*he)-ue)/3.
      if (qul .gt. 0.) then
        hl=qul**2/g
        qul=-hl*qul
      else
        if (idl.lt.1000) then
        write(*,*)'critd : solution = vide'
        idl=idl+1
        endif
        hl=0.
        qul=0.
      endif
      return
      end subroutine critd

!******************************************************************************
      subroutine critg(qul,he,ue,hl)
!******************************************************************************
!     resolution de l'equation en hl : qul = gamag(he,ue,hl)
!           avec qul et hl lies par froude =1  et qul inconnu
!
!     entrees:
!     he,ue  valeurs de h et u sur l'autre extremite
!
!     sorties:
!     qul hl    valeurs de h et q sur l'extremite ou s'applique la c.l.
!******************************************************************************
!      implicit logical (a-z)

      use module_tableaux, only:g,idl
      use module_precision
      implicit none

      real(wp) qul,he,ue,hl
!      integer idl

      !common/grav/g
! compte de messages avertissement
!      data idl/0/
!      save idl

! la vitesse donnee par gamag/hl vaut ue+2ghe**.5-2ghl**.5
      qul=(2.*sqrt(g*he)+ue)/3.
      if (qul .gt. 0.) then
        hl=qul**2/g
        qul=hl*qul
      else
        if (idl.lt.1000) then
        write(*,*)'critg : solution = vide'
        idl=idl+1
        endif
        hl=0.
        qul=0.
      endif
      return
      end subroutine critg

!******************************************************************************
! sous - programmes  solveur de riemann 1d des equations de st venant 2d
! ut + f(u)x = 0 ; u = (h, hu, hv) ; h >= 0 ; jpvila hgalley cemagref 87
! version 31/08/87 pour problemes 2d (calcul des flux non integre)
!
!     communique avec l'exterieur par :
!     - les listes d'arguments des routines
!     - le common /param/eps1,eps2,paray    (utilise par la plupart des routines)
!     - le common /newton/prec,nitmax  (utilise par la routine nwt seulement)
!
!     contient :
!     rmn      resolution pb de riemann 1d sur equations de st venant 2d
!     rmng     idem rmn avec condition aux limites en debit a gauche
!     rmnd     idem rmn avec condition aux limites en debit a droite
!     c        calcul de c(h) celerite de l'onde
!     nwt      resolution par newton de l'equation en hl : ql = y(he,ue,hl)
!     gamag    debit pour une 1-courbe passant par h1,u1
!     dgamag   derivee de gamag
!     gamad    debit pour une 2-courbe passant par h2,u2
!     dgamad   derivee de gamad
!     solex    solution exacte pb de riemann 1d test
!******************************************************************************
!******************************************************************************
!******************************************************************************
      subroutine resrmn(ieg,ied,ia,hg,qng,qtg,hd,qnd,qtd,dt&
#if TRANSPORT_SOLIDE
!hcd et hcg sont des concentrations
      &,hcg,hcd&
#endif
     &)
!******************************************************************************
! resolution du probleme de riemann en coordonnees locales
! et calcul des flux en coordonnees absolues

      use module_tableaux,only:xna,yna,xta,yta,zfa,zfe,hae,nrefa,fha&
     &,fqua,fqva,het,quet,qvet,ieva,houv,qouv,se,la,fluu,flvv,vouv&
     &,vtouv,eps1,eps2,paray,g
      use module_precision
#if TRANSPORT_SOLIDE
      use module_ts,only : flhc,coouv,fhca,hcet,diagsh,modes,consed,vanri,corvch&
      &,ppente,meyer,rapva,devch,diamouv,sigmaouv,diame,sigmae,sedvar&
      &,ccouplts,dcouplts,scouplts
#endif
      implicit none

      integer ieg,ied,ia
      real(wp) hg,qng,qtg,hd,qnd,qtd
      real(wp) ha,fqna,fqta,dt,fmax

#if TRANSPORT_SOLIDE
      real(WP) hcg,hcd
#endif
!      real(wp) ha,qna,qta,fqna,fqta
!cou1d      logical couplage1d(namax),gcouplage(namax)
!cou1d      real(wp) hcouplage(namax)&
!cou1d     &,vxcouplage(namax),vycouplage(namax)

!cou1d      common/couplage1d/couplage1d
!cou1d      common/gcouplage/gcouplage
!cou1d      common/hvcouplage/hcouplage,vxcouplage,vycouplage


      if (nrefa(ia).eq.-2) then
          houv(ia,1)=hg
          houv(ia,2)=hd
          if (hg .gt. paray) then
            vouv(ia,1)=qng/hg
            vtouv(ia,1)=qtg/hg
          else
            vouv(ia,1)=0.
            vtouv(ia,1)=0.
          endif
          if (hd .gt. paray) then
            vouv(ia,2)=qnd/hd
            vtouv(ia,2)=qtd/hd
          else
            vouv(ia,2)=0.
            vtouv(ia,2)=0.
          endif
          ha=0.

#if TRANSPORT_SOLIDE
      coouv(ia,1)=hcg
      coouv(ia,2)=hcd
#if SEDVAR
          if(sedvar)then
!cou1d           if(ieg.gt.0)then
              diamouv(ia,1)=diame(ieg)
              sigmaouv(ia,1)=sigmae(ieg)
!cou1d           elseif(couplage1d(ia))then
!cou1d              diamouv(ia,1)=dcouplts(ia)
!cou1d              sigmaouv(ia,1)=scouplts(ia)
!cou1d          endif
!cou1d          if(ied.gt.0)then
            diamouv(ia,2)=diame(ied)
            sigmaouv(ia,2)=sigmae(ied)
!cou1d           elseif(couplage1d(ia))then
!cou1d             diamouv(ia,2)=dcouplts(ia)
!cou1d             sigmaouv(ia,2)=scouplts(ia)
!cou1d           endif
          endif
#endif

#endif


! remise a zero des qouv mise dans le sp qouvr
      elseif (ieg.eq.0) then
!cou1d         if (couplage1d(ia)) then
!cou1d!             call rmn2osher(hg,qng,qtg,hd,qnd,qtd
!cou1d             call rmn2(hg,qng,qtg,hd,qnd,qtd
!cou1d     :        ,fha(ia),fqna,fqta,ha
#if TRANSPORT_SOLIDE
!cou1d     :,hcg,hcd,fhca(ia),ia
#endif
!cou1d     :)
!cou1d! si pas couplage
!cou1d         else

#if TRANSPORT_SOLIDE
! convention si concentration negative, on prend concentration interieure
      if(hcg.lt.-eps2) then
        hcg=hcd
      endif
#endif

      call rmng(hg,qng,qtg,hd,qnd,qtd,ha,fha(ia),fqna,fqta,&
     &nrefa(ia),ia&
#if TRANSPORT_SOLIDE
     &,hcg,hcd,fhca(ia)&
#endif
     &)

#if TRANSPORT_SOLIDE
! si qng >0 on prend les valeurs a gauche sinon a droite
!            pour calculer la deviation du ts par charraige
! on passe de rapva rapport module a rapva rapport vitesses normales
      if(devch)then
! si fha positif rapva=1 car fhca donne
!        if(fha(ia).gt.eps2)then
!         call correccharriage(ia,ied,fhca(ia),fha(ia),ha)
        if(fha(ia).lt.-eps2)then
          call correccharriage(ia,ied,fhca(ia),fha(ia),ha)
!        else
!          fhca(ia)=0.
        endif
      elseif(meyer)then
! si fha positif rapva=1 car fhca donne
        if(fha(ia).lt.-eps2)then
         fhca(ia)=fhca(ia)*rapva(ia)
        endif
      endif
#endif

!cou1d! fin couplage ou pas couplage
!cou1d         endif
      else if (ied.eq.0) then
!cou1d         if (couplage1d(ia)) then
!cou1d!             call rmn2osher(hg,qng,qtg,hd,qnd,qtd
!cou1d             call rmn2(hg,qng,qtg,hd,qnd,qtd
!cou1d     :        ,fha(ia),fqna,fqta,ha
#if TRANSPORT_SOLIDE
!cou1d     :,hcg,hcd,fhca(ia),ia
#endif
!cou1d     :)
!cou1d! si pas couplage
!cou1d         else

#if TRANSPORT_SOLIDE
! convention si concentration negative, on prend concentration interieure
      if(hcd.lt.-eps2) then
        hcd=hcg
      endif
#endif

         call rmnd(hg,qng,qtg,hd,qnd,qtd,ha,fha(ia),fqna,fqta&
     &   ,nrefa(ia),ia&
#if TRANSPORT_SOLIDE
    &,hcg,hcd,fhca(ia)&
#endif
    &)


#if TRANSPORT_SOLIDE
! si qng >0 on prend les valeurs a gauche sinon a droite
!            pour calculer la deviation du ts par charraige
! on passe de rapva rapport module a rapva rapport vitesses normales
      if(devch)then
! si fha positif rapva=1 car fhca donne
!        if(fha(ia).gt.eps2)then
!         call correccharriage(ia,ieg,fhca(ia),fha(ia),ha)
        if(fha(ia).lt.eps2)then
         call correccharriage(ia,ieg,fhca(ia),fha(ia),ha)
!        else
!          fhca(ia)=0.
        endif
      elseif(meyer)then
! si fha positif rapva=1 car fhca donne
        if(fha(ia).lt.eps2)then
         fhca(ia)=fhca(ia)*rapva(ia)
        endif
      endif
#endif


!cou1d! fin couplage ou pas couplage
!cou1d         endif

! si on a les 2 cotes et nrefa different de -2
      else
         if (nrefa(ia).eq.-1) then
          houv(ia,1)=hg
          houv(ia,2)=hd
          if (hg .gt. paray) then
            vouv(ia,1)=qng/hg
            vtouv(ia,1)=0.
          else
            vouv(ia,1)=0.
            vtouv(ia,1)=0.
          endif
          if (hd .gt. paray) then
            vouv(ia,2)=qnd/hd
            vtouv(ia,2)=0.
          else
            vouv(ia,2)=0.
            vtouv(ia,2)=0.
          endif

#if TRANSPORT_SOLIDE
      coouv(ia,1)=hcg
      coouv(ia,2)=hcd
#if SEDVAR
          if(sedvar)then
            diamouv(ia,1)=diame(ieg)
            sigmaouv(ia,1)=sigmae(ieg)
            diamouv(ia,2)=diame(ied)
            sigmaouv(ia,2)=sigmae(ied)
          endif
!end if sedvar
#endif

#endif

! remise a zero des qouv mise dans le sp qouvr
!cou1d! si ona couplage central avec 1d houv cote 1d est different
!cou1d         if (couplage1d(ia)) then
!cou1d           if (gcouplage(ia)) then
!cou1d             houv(ia,2)=hcouplage(ia)
!cou1d             vouv(ia,2)=0.
!cou1d!             vtouv(ia,2)=0.
#if TRANSPORT_SOLIDE
!cou1d          COOUV(IA,2)=ccouplts(ia)
#if SEDVAR
!cou1d          if(sedvar)then
!cou1d            diamouv(ia,2)=dcouplts(ia)
!cou1d            sigmaouv(ia,2)=scouplts(ia)
!cou1d          endif
#endif
#endif
!cou1d           else
!cou1d             houv(ia,1)=hcouplage(ia)
!cou1d             vouv(ia,1)=0.
!cou1d!             vtouv(ia,1)=0.
#if TRANSPORT_SOLIDE
!cou1d          COOUV(IA,1)=ccouplts(ia)
#if SEDVAR
!cou1d          if(sedvar)then
!cou1d            diamouv(ia,1)=dcouplts(ia)
!cou1d            sigmaouv(ia,1)=scouplts(ia)
!cou1d          endif
#endif
#endif
!cou1d           endif
!cou1d! fin du if sur couplage1d
!cou1d         endif
! fin du if sur nrefa=-1
         endif
         if (hg.le.paray) then
           if (hd.le.paray) then
             ha=0.
! si hg=0 et hdnon nul
           else
!             call rmn2osher(hg,qng,qtg,hd,qnd,qtd
             call rmn2(hg,qng,qtg,hd,qnd,qtd&
     &,fha(ia),fqna,fqta,ha&
#if TRANSPORT_SOLIDE
    &,hcg,hcd,fhca(ia),ia&
#endif
     &)

#if TRANSPORT_SOLIDE
! si qng >0 on prend les valeurs a gauche sinon a droite
!             pour calculer la deviation du ts par charraige
! on passe de rapva rapport module a rapva rapport vitesses normales
      if(devch)then
!         call correccharriage(ia,ied,fhca(ia),ha)
        if(abs(fha(ia)).gt.eps2)then
         call correccharriage(ia,ied,fhca(ia),fha(ia),ha)
        else
          fhca(ia)=0.
        endif
      elseif(meyer)then
         fhca(ia)=fhca(ia)*rapva(ia)
      endif
#endif

           endif
! si hg non nul
         else
!             call rmn2osher(hg,qng,qtg,hd,qnd,qtd
             call rmn2(hg,qng,qtg,hd,qnd,qtd&
     &        ,fha(ia),fqna,fqta,ha&
#if TRANSPORT_SOLIDE
    &,hcg,hcd,fhca(ia),ia&
#endif
     &)

#if TRANSPORT_SOLIDE

!! si qng >0 on prend les valeurs a gauche sinon a droite
!             pour calculer la deviation du ts par charraige
! on passe de rapva rapport module a rapva rapport vitesses normales
      if(devch)then
        if(fha(ia).gt.eps2)then
!          call correccharriage(ia,ieg,fhca(ia),ha)
          call correccharriage(ia,ieg,fhca(ia),fha(ia),ha)
          elseif(fha(ia).lt.-eps2)then
!            if(hd.gt.paray)then
!              call correccharriage(ia,ied,fhca(ia),ha)
          call correccharriage(ia,ied,fhca(ia),fha(ia),ha)
! si hd nul
!            else
!                fhca(ia)=0.
!            endif
! si fha  nul
           else
                fhca(ia)=0.
          endif
!        elseif(fhca(ia).lt.-eps2)then
!          if(hd.gt.paray)then
!            call correccharriage(ia,ied,fhca(ia),ha)
!            if(fhca(ia).gt.eps2)then
!              call correccharriage(ia,ieg,fhca(ia),ha)
!              if(fhca(ia).lt.-eps2)then
!              endif
!            endif
!          else
!            call correccharriage(ia,ieg,fhca(ia),ha)
!          endif
!        else
!         fhca(ia)=0.
!        endif
      elseif(meyer)then
         fhca(ia)=fhca(ia)*rapva(ia)
      endif

#endif




! fin du if sur hg=0
         endif
! fin du if sur ieg =0 etc
      endif
!     on reprojette q(qn,qt) dans le repere oxy --> qu, qv avec lesquels
!     on calcule les flux unitaires a l'interface
      if (ha.le.paray) then
          fha(ia) = 0.0
          fqua(ia) = 0.0
          fqva(ia) = 0.0
          fluu(ia)=0.
          flvv(ia)=0.
          hae(ia)=0.
#if TRANSPORT_SOLIDE
          fhca(ia) = 0.0
          flhc(ia) = 0.
#endif
      else
         if (abs(fha(ia)/ha).lt.eps2) then
            fha(ia)=0.
            fqua(ia) = 0.5*g*ha*ha*xna(ia)
            fqva(ia) = 0.5*g*ha*ha*yna(ia)
            fluu(ia)=0.
            flvv(ia)=0.
#if TRANSPORT_SOLIDE
            fhca(ia) = 0.0
            flhc(ia) = 0.
#endif
         else
            fqua(ia)=xna(ia)*fqna+xta(ia)*fqta
            fqva(ia)=yna(ia)*fqna+yta(ia)*fqta
         endif
         hae(ia)=ha
         if (ieg.ne.0) then
          if (het(ieg).le.paray) then
           if (fha(ia) .gt. 0.) then
            fha(ia) = 0.0
            fqua(ia) = 0.5*g*ha*ha*xna(ia)
            fqva(ia) = 0.5*g*ha*ha*yna(ia)
            fluu(ia)=0.
            flvv(ia)=0.
#if TRANSPORT_SOLIDE
            fhca(ia) = 0.0
            flhc(ia) = 0.
#endif
            go to 7250
           elseif (ied.ne.0) then
            if (zfa(ia)+ha.lt.zfe(ieg)) then
              fha(ia) = 0.0
              fqua(ia) = 0.5*g*ha*ha*xna(ia)
              fqva(ia) = 0.5*g*ha*ha*yna(ia)
            fluu(ia)=0.
            flvv(ia)=0.
#if TRANSPORT_SOLIDE
            fhca(ia) = 0.0
            flhc(ia) = 0.
#endif

              go to 7250
            elseif (-fha(ia)*dt*la(ia).le.paray*se(ieg)) then
              fha(ia) = 0.0
              fqua(ia) = 0.5*g*ha*ha*xna(ia)
              fqva(ia) = 0.5*g*ha*ha*yna(ia)
            fluu(ia)=0.
            flvv(ia)=0.
#if TRANSPORT_SOLIDE
            fhca(ia) = 0.0
            flhc(ia) = 0.
#endif

              go to 7250
            else
! 0.5 si egalisation des niveaux immediate
! 0.125 car la maille peut au maximum se remplir par ces 4 voisins
!              fmax=0.25*se(ieg)*(zfe(ied)+het(ied)-zfe(ieg))&
              fmax=0.5*se(ieg)*(zfe(ied)+het(ied)-zfe(ieg))&
     &         /(la(ia)*dt)
              if (fmax.lt.-eps2)write(*,*)'erreur fmax'

              if (-fha(ia) .gt. fmax) then
                fqta=-fqta*fmax/fha(ia)
                fqna=fmax*fmax/ha+0.5*g*ha*ha
#if TRANSPORT_SOLIDE
                fhca(ia) = -fhca(ia)*fmax/fha(ia)
#endif
                fha(ia)=-fmax
                fqua(ia)=xna(ia)*fqna+xta(ia)*fqta
                fqva(ia)=yna(ia)*fqna+yta(ia)*fqta
              endif
              go to 7250
! fin du if sur zfa
            endif
! fin du if sur fha
           endif
! si het positif
          else
! 0.25 car la maille peut au maximum se vider dans ces 4 voisins
!              fmax=0.5*het(ieg)*se(ieg)/(la(ia)*dt)
              fmax=het(ieg)*se(ieg)/(la(ia)*dt)
              if (fha(ia) .gt. fmax) then
                fqta=fqta*fmax/fha(ia)
                fqna=fmax*fmax/ha+0.5*g*ha*ha
#if TRANSPORT_SOLIDE
                fhca(ia) = -fhca(ia)*fmax/fha(ia)
#endif
                fha(ia)=fmax
                fqua(ia)=xna(ia)*fqna+xta(ia)*fqta
                fqva(ia)=yna(ia)*fqna+yta(ia)*fqta
              endif
! fin du if sur het
          endif
! fin du if sur ieg
         endif
         if (ied.ne.0) then
          if (het(ied).le.paray) then
           if (fha(ia).lt.0.) then
            fha(ia) = 0.0
            fqua(ia) = 0.5*g*ha*ha*xna(ia)
            fqva(ia) = 0.5*g*ha*ha*yna(ia)
            fluu(ia)=0.
            flvv(ia)=0.
#if TRANSPORT_SOLIDE
            fhca(ia) = 0.0
            flhc(ia) = 0.
#endif

            go to 7250
           elseif (ieg.ne.0) then
            if (zfa(ia)+ha.lt.zfe(ied)) then
              fha(ia) = 0.0
              fqua(ia) = 0.5*g*ha*ha*xna(ia)
              fqva(ia) = 0.5*g*ha*ha*yna(ia)
            fluu(ia)=0.
            flvv(ia)=0.
#if TRANSPORT_SOLIDE
            fhca(ia) = 0.0
            flhc(ia) = 0.
#endif

              go to 7250
            elseif (fha(ia)*dt*la(ia).le.paray*se(ied)) then
              fha(ia) = 0.0
              fqua(ia) = 0.5*g*ha*ha*xna(ia)
              fqva(ia) = 0.5*g*ha*ha*yna(ia)
            fluu(ia)=0.
            flvv(ia)=0.
#if TRANSPORT_SOLIDE
            fhca(ia) = 0.0
            flhc(ia) = 0.
#endif

            go to 7250
            else
!              fmax=0.25*se(ied)*(zfe(ieg)+het(ieg)-zfe(ied))&
              fmax=0.5*se(ied)*(zfe(ieg)+het(ieg)-zfe(ied))&
     &         /(la(ia)*dt)
              if (fmax.lt.-eps2) write(*,*)'erreur fmax2'
              if (fha(ia) .gt. fmax) then
                fqta=fqta*fmax/fha(ia)
                fqna=fmax*fmax/ha+0.5*g*ha*ha
#if TRANSPORT_SOLIDE
                fhca(ia) = fhca(ia)*fmax/fha(ia)
#endif

                fha(ia)=fmax
                fqua(ia)=xna(ia)*fqna+xta(ia)*fqta
                fqva(ia)=yna(ia)*fqna+yta(ia)*fqta
              endif
              go to 7250
           endif
           endif
! si het positif
          else
!              fmax=0.5*het(ied)*se(ied)/(la(ia)*dt)
              fmax=het(ied)*se(ied)/(la(ia)*dt)
              if (-fha(ia) .gt. fmax) then
                fqta=-fqta*fmax/fha(ia)
                fqna=fmax*fmax/ha+0.5*g*ha*ha
#if TRANSPORT_SOLIDE
                fhca(ia) = -fhca(ia)*fmax/fha(ia)
#endif

                fha(ia)=-fmax
                fqua(ia)=xna(ia)*fqna+xta(ia)*fqta
                fqva(ia)=yna(ia)*fqna+yta(ia)*fqta
              endif
! fin du if sur het
          endif
! fin du if sur ied
         endif
! fin du if sur ha=0
      endif
 7250    return
      end subroutine resrmn

!******************************************************************************
      subroutine rmnd(hg,qug,qvg,hd,qud,qvd&
     & ,he,que,fque,fqve,nrefa,ia&
#if TRANSPORT_SOLIDE
      &,hcg,hcd,fhce&
#endif
     &)
!******************************************************************************
!     idem rmng avec conditions aux limites imposees a droite : hd,qud,qvd
!      implicit logical (a-z)

      use module_tableaux, only:eps1,eps2,paray,ift,g
      use module_precision

      implicit none
      integer nrefa,ia
      real(wp) hg,qug,qvg,hd,qud,qvd,he,que&
     &,fque,fqve,delta,qua
      real(wp) gamag,dgamag,c,vgg,xl

#if TRANSPORT_SOLIDE
      real(WP) hcg,hcd,fhce
#endif

      external gamag,dgamag,c
      !common/param/eps1,eps2,paray
      !common/iftr/ift
      !common/grav/g
      he=0.
      que=0.
      fque=0.
      fqve=0.
#if TRANSPORT_SOLIDE
      fhce = 0.0
#endif
!     cas hg > 0
      if (nrefa.eq.1) then
!         flux de u sortant (avec orientation g -> d !) donc ue = ug
          if (hg.le.paray) then
!            he=0.0
          else
            if (qug.le.0.) then
!              write(*,*)'arete ',ia,' flux non sortant ',qug
              he=hg
!              que=0.0
              fque=0.5*g*he*he
!              fqve=0.0
#if TRANSPORT_SOLIDE
!              fhce = 0.0
#endif
            else
              he=hg
              que=qug
              fque=0.5*g*he*he+que*que/he
              fqve=que*qvg/he
#if TRANSPORT_SOLIDE
              fhce = que*hcg
#endif
            endif
          endif
      else if (nrefa.eq.2) then
!         flux de u nul : condition de reflexion sur une paroi
!         equivaut au probleme de riemann "symetrique" ug = (hg,qug)
!         ud = (hg,-qug) en imposant par ailleurs sur l'arete qv = qvg
          if (hg.le.paray) then
!            he=0.0
          elseif (qug.le.0.) then
            call rmn3(hg,qug,que,fque,fqve,he&
#if TRANSPORT_SOLIDE
      &,fhce&
#endif
     &)
          else
            call rmn2(hg,qug,qvg,hg,-qug,qvg,que,fque,fqve,he&
#if TRANSPORT_SOLIDE
           &,hcg,hcd,fhce,ia&
#endif
           &)
!            he=sqrt(fque/(0.5*g))
! on pose que=0. et on calcule le choc a la reflexion
!            que=0.
!            call dhchoc(he,hg,qug,que)
!            fque=0.5*g*he*he
!            fqve=0.
!#if TRANSPORT_SOLIDE
!            fhce=0.
!#endif

          endif
      else  if (nrefa.eq.4.or.nrefa.eq.5.or.nrefa.eq.8&
     &.or.nrefa.eq.9) then
!         hauteur hd fixee , calcul  de  qud  par  conservation  du
!         premier invariant de rieman et vd=vg
!         on suppose que on passe de wg a we(notee wd aussi)par une 1-detente
          if (hd.le.paray) then
!             he=0.
          elseif (hg.le.paray) then
            if (nrefa.eq.9) then
              he=hd/3.
            else
              he=hd
            endif
! on est forcement en torrentiel entrant
! on prend qud sauf si il est nul car alors indefini
            if (nrefa.eq.5.or.nrefa.eq.8.or.nrefa.eq.9) then
              if (qud.lt.0.) then
              que=qud
              else
              que=-hd*c(hd)
              endif
! nrefa=4
            else
!              que=0.
            endif
!            que=qud
            if (nrefa.eq.8) then
              qvd=qvd*que
            endif
            fqve=qvd*que/hd
            fque=0.5*g*he*he+que*que/he
#if TRANSPORT_SOLIDE
              fhce = que*hcd
#endif
          else
! on calcule la detente si on n'est pas en torrentiel sortant
           if (nrefa.eq.9) then
            vgg=qug/hg + 2.*sqrt(g*hg)
            he = hd/3.+vgg**2/(18.*g)&
     &+vgg*sqrt(12.*hd-2.*vgg**2/g)/(9.*sqrt(g))
            que =(qug/hg + 2.*(sqrt(g*hg)-sqrt(g*he)) )*he
           else
            que =(qug/hg + 2.*(sqrt(g*hg)-sqrt(g*hd)) )*hd
            he = hd
           endif
           if (que .gt. 0.) then
!             if(nrefa.eq.4.or.nrefa.eq.5)then
             if(abs(que).gt.he*c(he))then
! cas supersonique sortant
!               write(*,*)'ARETE ',ia,' FLUX SORTANT SUPERSONIQUE',que
!               write(*,*)'ON ABANDONNE LA COTE IMPOSEE'
               he=hg
               que=qug
               fque=que**2/he+0.5*g*he**2
             endif
! fin du if sur nrefa=4 ou 5
!             endif
!  flux sortant:ve est egal a vg
              fqve=qvg/hg *que
#if TRANSPORT_SOLIDE
              fhce = que*hcg
#endif

! si que negatif flux entrant
            elseif (nrefa.eq.4) then
! on impose 0 bien que flux entrant car la condition est sortante
                que=0.
!                fqve=0.
#if TRANSPORT_SOLIDE
!                fhce = 0.0
#endif
            else
! que negatif
              if (qud.lt.-he*c(he)) then
! on se met en torrentiel si on peut sinon en regime critique
                que=qud
              elseif(que.lt.-he*c(he))then
! on se met en torrentiel si on peut sinon en regime critique
                que=min(qud,-he*c(he))
!               else
! dans le cas contraire on garde la valeur de que calculee precedemment
              endif
! fin du if si nrefa=4
!            endif
! flux entrant:ve est egal a vd qui normalement est impose en meme temps que hd
            if (nrefa.eq.8) then
              qvd=qvd*que
            endif
             fqve=qvd*que/hd
#if TRANSPORT_SOLIDE
              fhce = que*hcd
#endif


! fin du if si que negatif
           endif
           fque=0.5*g*he*he+que*que/he
! fin du if si hd=0
         endif
      elseif (nrefa.eq.6) then
! flux defini par une loi de tarage
        if (hg.le.paray) then
!          he=0.0
        elseif (qug .gt. hg*c(hg)) then
! torrentiel sortant : on abandonne la loi de tarage
!           write(*,*)'arete ',ia,' flux sortant torrentiel',qug,hg
!           write(*,*)'on abandonne la cote imposee'
           he=hg
           que=qug
           fque=0.5*g*he*he +que*que/he
           fqve=que*qvg/hg
#if TRANSPORT_SOLIDE
              fhce = que*hcg
#endif
        else
! on cherche la hauteur sortante et le debit sortant en meme temps
! on pose qua =1. pour savoir le sens de la normale sortante
           qua=1.
       xl=1.
! xl est inutile
           call dicho2(qua,gamag,dgamag,hg,qug/hg,he,xl,ia)
           if (he .gt. paray) then
              que = qua
!               delta=he*(qug/hg-c(hg)+c(he))
! condition de choc ue-ce<ug-cg
!              if (que.lt.delta) then
! on ne calcule  pas le choc car ni qud ni hd fixes
!                 write(*,*)'arete ',ia,'loi de tarage provoque choc'
!              endif
!              if (que.lt.-paray) then
!                write(*,*)'arete ',ia,' flux non sortant',-que
!              endif
              fque=que*que/he+0.5*g*he*he
!              ua=que/he
! en sortie on garde qv de l'interieur
              fqve=qvg*que/he
#if TRANSPORT_SOLIDE
              fhce = que*hcg
#endif
           else
!              he=0.0
           endif
! fin du if sur hg=0
          endif
      elseif (nrefa .gt. 60) then
! flux defini par une loi de tarage
!         if (hg.le.paray) then
!          he=0.0
!        elseif (qug .gt. hg*c(hg)) then
!c torrentiel sortant : on abandonne la loi de tarage
!c           write(*,*)'arete ',ia,' flux sortant torrentiel',qug,hg
!c           write(*,*)'on abandonne la cote imposee'
!           he=hg
!           que=qug
!           fque=0.5*g*he*he +que*que/he
!           fqve=que*qvg/hg
#if TRANSPORT_SOLIDE
!              fhce = que*hcg
#endif
!         else
! on a he et que calcule par caltar
!           qua=1.
!           call dicho2(qua,gamag,dgamag,hg,qug/hg,he,ia)
           he=hd
           if (he .gt. paray) then
!              que = qua
!              delta=he*(qug/hg-c(hg)+c(he))
              que=qud
              fque=que*que/he+0.5*g*he*he
!              ua=que/he
! en sortie on garde qv de l'interieur
              fqve=qvg*que/he
#if TRANSPORT_SOLIDE
              fhce = que*hcg
#endif
           else
!              he=0.0
           endif
! fin du if sur hg=0
!           endif
      elseif (nrefa.eq.7) then
! flux defini par froude =1
        if (hg.le.paray) then
!          he=0.0
        else
! on cherche la hauteur sortante et le debit sortant en meme temps
! on pose qua =1. pour savoir le sens de la normale sortante
!           qua=1.
!           call nwt3(qua,gamag,hg,qug/hg,he)
! dans le cas de flux non sortant, on part d'une vitesse nulle
! plutot que d'une vitesse mal orientee
        if (qug.lt.0.)qug=0.
            call critg(qua,hg,qug/hg,he)
            if (he .gt. paray) then
              que = qua
              fque=que*que/he+0.5*g*he*he
! en sortie on garde qv de l'interieur
              fqve=qvg*que/he
#if TRANSPORT_SOLIDE
              fhce = que*hcg
#endif
           else
!              he=0.0
           endif
! fin du if sur hg=0
          endif
! nrefa entre 30 et 60 (les > 60 deja traites)
      elseif(nrefa.gt.30)then
!         flux de u entrant : on resout par newton l'equation en he :
!         qud = gamag(hg,ug,he)  avec  dgamag = derivee de gamag, et
!         signification de gamag : cf notations jp vila ; ug = qug/hg
!         on suppose que on passe de wg a ww(notee wd aussi)par une 1-detente
!          if(qud.gt.paray)then
!            write(*,*)'arete ',ia,' flux non entrant ',qud
!          endif
! si hd est defini et hg=0.: cas torrentiel on impose le flux entrant
        if (hg.le.paray) then
           if(hd.gt.paray)then
             he=hd
             que=qud
             fque=0.5*g*he*he+que*que/he
             fqve=que*qvd/he
#if TRANSPORT_SOLIDE
             fhce=que*hcd
#endif
! question: si hg et hd est ce que qud est non nul ?
!           else
!             he=0.0
           endif
! si hg positif
        else
! cas torrentiel sortant ou choc
          if(qug.gt.hg*c(hg))then
! on calcule he par le choc a que=qud fixe
            que=qud
            call dhchoc(he,hg,qug,que)
          else
! cas fluvial : on cherche la hauteur entrante
           call nwt(qud,gamag,dgamag,hg,qug/hg,he)
           if (he.gt.paray) then
              que = qud
              delta=he*(qug/hg-c(hg)+c(he))
! condition de choc ue-ce<ug-cg
              if(que.lt.delta)then
! on calcule he par le choc a que=qud fixe
                call dhchoc(he,hg,qug,que)
              endif
!           else
!              he=0.0
!              que=0.0
           endif
!           endif
! fin du if fluvial /torrentiel sortant
          endif
          if(-que.gt.he*c(he))then
! cas torrentiel entrant
              if(hd.gt.paray)then
! on impose les valeurs a droite
                he=hd
                que=qud
!              else
! la valeur de hd n'a pas ete donnee ou est erronee
!      write(*,*)'ARETE ',ia,' FLUX ENTRANT TORRENTIEL',que,he
              endif
          endif
          if(he.gt.paray)then
          que=qud
          fque=que*que/he+0.5*g*he*he
            fqve=que*qvd/he
#if TRANSPORT_SOLIDE
            fhce=que*hcd
#endif
          elseif(hd.gt.paray)then
            he=hd
          que=qud
          fque=que*que/he+0.5*g*he*he
            fqve=que*qvd/he
#if TRANSPORT_SOLIDE
            fhce=que*hcd
#endif
!          elseif(hg.gt.paray)then : on est dans une boucle ou hg positif
          else
            he=hg
            que=qud
            fque=que*que/he+0.5*g*he*he
            fqve=que*qvd/he
#if TRANSPORT_SOLIDE
            fhce=que*hcd
#endif
! fin du if he=0.
          endif
! fin du if hg=0.
        endif
! nrefa=3
      else
!         flux de u entrant : on resout par newton l'equation en he :
!         qud = gamag(hg,ug,he)  avec  dgamag = derivee de gamag, et
!         signification de gamag : cf notations jp vila ; ug = qug/hg
!         on suppose que on passe de wg a ww(notee wd aussi)par une 1-detente
!          if (qud .gt. paray) then
!            write(*,*)'arete ',ia,' flux non entrant ',qud
!          endif
! si hd est defini et hg=0.: cas torrentiel on impose le flux entrant
        if (hg.le.paray) then
           if (hd .gt. paray) then
             he=hd
             que=qud
             fque=0.5*g*he*he+que*que/he
             fqve=que*qvd/he
#if TRANSPORT_SOLIDE
              fhce = que*hcd
#endif
!            else
!             he=0.0
           endif
        else
! cas torrentiel sortant ou choc
          if (qug .gt. hg*c(hg)) then
! on calcule he par le choc a que=qud fixe
            que=qud
            call dhchoc(he,hg,qug,que)
          else
! cas fluvial : on cherche la hauteur entrante
           call nwt(qud,gamag,dgamag,hg,qug/hg,he)
           if (he .gt. paray) then
              que = qud
              delta=he*(qug/hg-c(hg)+c(he))
! condition de choc ue-ce<ug-cg
              if (que.lt.delta) then
! on calcule he par le choc a que=qud fixe
                call dhchoc(he,hg,qug,que)
              endif
!            else
!              he=0.0
!              que=0.0
           endif
!           endif
! fin du if fluvial /torrentiel sortant
          endif
          if (-que .gt. he*c(he)) then
! cas torrentiel entrant
              if (hd .gt. paray) then
! on impose les valeurs a droite
                he=hd
                que=qud
!               else
! la valeur de hd n'a pas ete donnee ou est erronee
!      write(*,*)'arete ',ia,' flux entrant torrentiel',que,he
               endif
          endif
          if (he .gt. paray) then
          fque=que*que/he+0.5*g*he*he
! que est egal a qud donc <0 sauf erreur de donnees
          if (que.lt.0.) then
! on devrait imposer ve=vd mais hd est inconnu  ;on ne connait
! que qud et qvd imposes (identifies a que et qve)
            fqve=que*qvd/he
#if TRANSPORT_SOLIDE
              fhce = que*hcd
#endif
! si he nul
          else
! cas impossible normalement
             que=0.
             fque=0.5*g*he*he
!             fqve=0.
!c debit sortant : on prend ve=vg
!            fqve=que*qvg/hg
#if TRANSPORT_SOLIDE
!              fhce = que*hcg
!              fhce=0.
#endif
          endif
! fin du if he=0.
          endif
! fin du if hg=0.
        endif
! fin du if sur nrefa
      endif
      end subroutine rmnd

!******************************************************************************
      subroutine rmng(hg,qug,qvg,hd,qud,qvd,hw,quw&
     & ,fquw,fqvw,nrefa,ia&
#if TRANSPORT_SOLIDE
      &,hcg,hcd,fhcw&
#endif
     &)
!******************************************************************************
!     resolution du demi probleme de riemann 1d pour les equations de
!     st venant 2d, avec conditions aux limites imposees a gauche : hg,
!     qug,qvg ; resultats a l'interface : hw,quw,qvw (west) ; cf rmn
!
!     entrees:
!     g           intensite de la pesanteur
!     hg,qug,qvg  valeurs de h, qu = hu, qv = hv, a gauche (c.l. imposees)
!     hd,qud,qvd  valeurs de h, qu = hu, qv = hv, a droite
!     eps2         epsilon machine (common param)
!
!     sorties:
!     hw,quw,fquw,fqvw     valeurs de h,qu,et flux de qu et qv a gauche (west)
! *****************************************************************************
!      implicit logical (a-z)
      use module_tableaux, only:eps1,eps2,paray,ift,g
      use module_precision

      implicit none
      integer nrefa,ia
      real(wp) hg,qug,qvg,hd,qud,qvd,hw,quw&
     &,fquw,fqvw,delta,qua
      real(wp) gamad,dgamad,c,vdd,xl

#if TRANSPORT_SOLIDE
      real(WP) hcg,hcd,fhcw
#endif

      external gamad,dgamad,c
      !common/param/eps1,eps2,paray
      !common/iftr/ift
      !common/grav/g
!onc
!     common/tconc/avects
!oncf
      hw=0.
      quw=0.
      fquw=0.
      fqvw=0.
#if TRANSPORT_SOLIDE
      fhcw=0.
#endif


      if (nrefa.eq.1) then
!         flux de u sortant (avec orientation g -> d !) donc uw = ud
          if (hd.le.paray) then
!            hw=0.0
          else
            if (qud.ge.0.) then
!              write(*,*)'arete ',ia,' flux non sortant ',qud
              hw=hd
!              quw=0.0
              fquw=0.5*g*hw*hw
!              fqvw=0.0
#if TRANSPORT_SOLIDE
!              fhcw=0.
#endif
            else
              hw=hd
              quw=qud
              fquw=0.5*g*hw*hw+quw*quw/hw
              fqvw=qvd*quw/hw
#if TRANSPORT_SOLIDE
              fhcw=quw*hcd
#endif
            endif
          endif
      else if (nrefa.eq.2) then
!         flux de u nul : condition de reflexion sur une paroi
!         equivaut au probleme de riemann "symetrique" ug = (hd,-qud),
!         ud = (hd,qud) en imposant en outre sur l'arete : qv = qvd
          if (hd.le.paray) then
!            hw=0.0
          elseif (qud.ge.0.) then
            qud=-qud
            call rmn3(hd,qud,quw,fquw,fqvw,hw&
#if TRANSPORT_SOLIDE
      &,fhcw&
#endif
     &)
            qud=-qud
          else
            call rmn2(hd,-qud,qvd,hd,qud,qvd,quw,fquw,fqvw,hw&
#if TRANSPORT_SOLIDE
     &,hcg,hcd,fhcw,ia&
#endif
     &)
!            hw=sqrt(fquw/(0.5*g))
! on pose quw=0. et on calcule le choc a la reflexion
!            quw=0.
!            call dhchoc(hw,hd,qud,quw)
!            fquw=0.5*g*hw*hw
!            fqvw=0.
!#if TRANSPORT_SOLIDE
!      fhcw=0.
!#endif
          endif
      else  if (nrefa.eq.4.or.nrefa.eq.5.or.nrefa.eq.8&
     &.or.nrefa.eq.9) then
!         hauteur imposee : on calcule qug par la conservation  du
!         deuxieme invariant de rieman et vg=vd
!         on suppose que on passe de wd a ww(notee wg aussi)par une 3-detente
          if (hg.le.paray) then
!            hw=0.
          elseif (hd.le.paray) then
           if (nrefa.eq.9) then
            hw=hg/3.
           else
            hw=hg
           endif
            if (nrefa.eq.4) then
!              quw=0.
! nrefa=5
! on prend qug sauf si il est nul car alors indefini
            else
               if (qug .gt. 0.) then
                 quw=qug
               else
                 quw=c(hg)*hg
               endif
            endif
!            quw=qug
            if (nrefa.eq.8) then
               qvg=qvg*quw
            endif
            fqvw=qvg/hg * quw
            fquw=0.5*g*hw*hw+quw*quw/hw
#if TRANSPORT_SOLIDE
            fhcw=quw*hcg
#endif
          else
! on calcule la detente si on n'est pas en torrentiel sortant
           if (nrefa.eq.9) then
            vdd=qud/hd - 2.*sqrt(g*hd)
            hw = hg/3.+vdd**2/(18.*g)&
     &-vdd*sqrt(12.*hg-2.*vdd**2/g)/(9.*sqrt(g))
            quw=(qud/hd + 2.*(sqrt(g*hg)-sqrt(g*hd)) )*hg
           else
            quw=(qud/hd + 2.*(sqrt(g*hg)-sqrt(g*hd)) )*hg
            hw = hg
           endif
           if (quw.lt.0.) then
!            if(nrefa.eq.4.or.nrefa.eq.5)then
            if(abs(quw).gt.hw*c(hw))then
! cas supersonique sortant
!             write(*,*)'ARETE ',ia,' FLUX SORTANT SUPERSONIQUE',qug
!             write(*,*)'ON ABANDONNE LA COTE IMPOSEE'
             hw=hd
             quw=qud
             fquw=quw**2/hw+0.5*g*hw**2
            endif
! fin du if sur nrefa=4 ou 5
!            ENDIF
!  flux sortant:vw est egal a vd
              fqvw=qvd/hd * quw
#if TRANSPORT_SOLIDE
              fhcw=quw*hcd
#endif
! si quw positif  debit entrant
            elseif (nrefa.eq.4) then
! q=0 car condition sortante meme si la solution semble entrante
              quw=0.
!              fqvw=0.
#if TRANSPORT_SOLIDE
!            fhcw=0.
#endif
            else
! quw >0 entrant

              if(abs(qug).gt.hw*c(hw))then
                quw=qug
              elseif(abs(quw).gt.hw*c(hw))then
! on se met en torrentiel si on peut sinon en regime critique
                quw=max(qug,hw*c(hw))
! dans le cas contraire on garde la valeur de quw calculee precedemment
!               else
              endif
! flux entrant:vw est egal a vg qui normalement est impose en meme temps que hg
            if (nrefa.eq.8) then
               qvg=qvg*quw
            endif
              fqvw=qvg*quw/hg
#if TRANSPORT_SOLIDE
            fhcw=quw*hcg
#endif
! fin du if si quw negatif
          endif
          fquw=0.5*g*hw*hw+quw*quw/hw
! fin du if si hg=0
         endif
      else  if (nrefa.eq.6) then
! flux defini par une loi de tarage
          if (hd.le.paray) then
!            hw=0.0
        elseif (qud.lt.-hd*c(hd)) then
! torrentiel sortant : on abandonne la loi de tarage
!           write(*,*)'arete ',ia,' flux sortant torrentiel',qud,hd
!           write(*,*)'on abandonne la cote imposee'
           hw=hd
           quw=qud
           fquw=0.5*g*hw*hw+quw*quw/hw
           fqvw=qvd*quw/hd
#if TRANSPORT_SOLIDE
            fhcw=quw*hcd
#endif
          else
! on cherche la hauteur sortante et le debit sortant en meme temps
! onpose qua =-1. pour savoir le sens de la normale sortante
           qua=-1.
       xl=1.
! xl est inutile
           call dicho2(qua,gamad,dgamad,hd,qud/hd,hw,xl,ia)
           if (hw .gt. paray) then
             quw = qua
!             delta=hw*(qud/hd+c(hd)-c(hw))
! condition de choc uw+cw>ud+cd
!             if (quw .gt. delta) then
! on ne calcule  pas le choc car ni qug ni hg fixes
!                 write(*,*)'arete ',ia,'loi de tarage provoque choc'
!             endif
!             if (quw .gt. paray) then
!                write(*,*)'arete ',ia,'flux non sortant',quw
!             endif
             fquw=0.5*g*hw*hw+quw*quw/hw
! en sortie on garde qv de l'interieur
             fqvw=qvd*quw/hw
#if TRANSPORT_SOLIDE
            fhcw=quw*hcd
#endif
           else
!              hw=0.0
           endif
! fin du if sur hd=0
          endif
      else  if (nrefa .gt. 60) then
! flux defini par une loi de tarage globale
! on a deja q normal et h
!          write(*,*)'rmng',ia,hd,hg,qug
!           if (hd.le.paray) then
!            hw=0.0
!        elseif (qud.lt.-hd*c(hd)) then
!c torrentiel sortant : on abandonne la loi de tarage
!c           write(*,*)'arete ',ia,' flux sortant torrentiel',qud,hd
!c           write(*,*)'on abandonne la cote imposee'
!           hw=hd
!           quw=qud
!           fquw=0.5*g*hw*hw+quw*quw/hw
!           fqvw=qvd*quw/hd
!           else
!           qua=-1.
!           call dicho2(qua,gamad,dgamad,hd,qud/hd,hw,ia)
            hw=hg
      if (hw .gt. paray) then
!             quw = qua
! on change le signe car qnal est >0 si sortant
! on ne chnage pas car change dans caltar
             quw=qug
             fquw=0.5*g*hw*hw+quw*quw/hw
! en sortie on garde qv de l'interieur
             fqvw=qvd*quw/hw
#if TRANSPORT_SOLIDE
            fhcw=quw*hcd
#endif
!            else
!              hw=0.0
           endif
! fin du if sur hd=0
!           endif

      else if (nrefa.eq.7) then
! flux defini par froude =1
          if (hd.le.paray) then
!          hw=0.0
          else
! on cherche la hauteur sortante et le debit sortant en meme temps
! onpose qua =-1. pour savoir le sens de la normale sortante
!           qua=-1.
!           call nwt3(qua,gamad,hd,qud/hd,hw)
! dans le cas de flux non sortant, on part d'une vitesse nulle
! plutot que d'une vitesse mal orientee
      if (qud .gt. 0.)qud=0.
           call critd(qua,hd,qud/hd,hw)
           if (hw .gt. paray) then
             quw = qua
             fquw=0.5*g*hw*hw+quw*quw/hw
! en sortie on garde qv de l'interieur
             fqvw=qvd*quw/hw
#if TRANSPORT_SOLIDE
            fhcw=quw*hcd
#endif
           else
!              hw=0.0
           endif
! fin du if sur hd=0
          endif

! nrefa entre 30 et 60
      elseif(nrefa.gt.30)then
!         flux de u entrant : on resout par newton l'equation en hw :
!         qug = gamad(hd,ud,hw)  avec  dgamad : derivee de gamad, et
!         signification de gamad : cf notations jp vila ; ud = qud/hd
!         (permis car hd > 0)
!         on suppose que on passe de wd a ww(notee wg aussi)par une 3-detente
!          if (qug.lt.-paray) then
!            write(*,*)'arete ',ia,' flux non entrant ',-qug
!          endif
! si hg est defini et hd=0: cas torrentiel on impose le flux entrant
          if (hd.le.paray) then
            if (hg .gt. paray) then
              hw=hg
              quw=qug
              fquw=0.5*g*hw*hw+quw*quw/hw
              fqvw=qvg*quw/hw
#if TRANSPORT_SOLIDE
            fhcw=quw*hcg
#endif
!             else
!              hw=0.
            endif
! si hd positif
          else
! cas torrentiel sortant ou choc
            if (qud.lt.-hd*c(hd)) then
! on calcule hw par le choc a quw=qug fixe
              quw=qug
              call dhchoc(hw,hd,qud,quw)
            else
! cas fluvial : on cherche la hauteur entrante
              call nwt(qug,gamad,dgamad,hd,qud/hd,hw)
              if (hw .gt. paray) then
                quw=qug
                delta=hw*(qud/hd+c(hd)-c(hw))
! condition de choc uw+cw>ud+cd
                if (quw .gt. delta) then
! on calcule hw par le choc a quw=qug fixe
                  call dhchoc(hw,hd,qud,quw)
                endif
!               else
!                hw=0.0
              endif
! fin du if fluvial /torrentiel sortant
            endif
            if (quw .gt. hw*c(hw)) then
! cas torrentiel entrant
              if (hg .gt. paray) then
! on impose les valeurs a gauche
                hw=hg
                quw=qug
!               else
! la valeur de hg n'a pas ete donnee ou est erronee
!      write(*,*)'arete ',ia,' flux entrant torrentiel',quw,hw
              endif
            endif
            if (hw .gt. paray) then
              quw=qug
              fquw=quw*quw/hw+0.5*g*hw*hw
              fqvw=qvg*quw/hw
#if TRANSPORT_SOLIDE
              fhcw=quw*hcg
#endif
            elseif(hg.gt.paray)then
              hw=hg
              quw=qug
              fquw=quw*quw/hw+0.5*g*hw*hw
              fqvw=qvg*quw/hw
#if TRANSPORT_SOLIDE
              fhcw=quw*hcg
#endif
! hd est positif ici
            else
              hw=hd
              quw=qug
              fquw=quw*quw/hw+0.5*g*hw*hw
              fqvw=qvg*quw/hw
#if TRANSPORT_SOLIDE
              fhcw=quw*hcg
#endif
! fin du if sur hw=0.
            endif
! fin du if hd=0.
          endif
! nrefa=3
      else
!         flux de u entrant : on resout par newton l'equation en hw :
!         qug = gamad(hd,ud,hw)  avec  dgamad : derivee de gamad, et
!         signification de gamad : cf notations jp vila ; ud = qud/hd
!         (permis car hd > 0)
!         on suppose que on passe de wd a ww(notee wg aussi)par une 3-detente
!          if(qug.lt.-paray)then
!            write(*,*)'arete ',ia,' flux non entrant ',-qug
!          endif
! si hg est defini et hd=0: cas torrentiel on impose le flux entrant
          if(hd.le.paray)then
            if(hg.gt.paray)then
              hw=hg
              quw=qug
              fquw=0.5*g*hw*hw+quw*quw/hw
              fqvw=qvg*quw/hw
#if TRANSPORT_SOLIDE
              fhcw=quw*hcg
#endif
!            else
!              hw=0.
            endif
! si hd positif
          else
! cas torrentiel sortant ou choc
            if(qud.lt.-hd*c(hd))then
! on calcule hw par le choc a quw=qug fixe
              quw=qug
              call dhchoc(hw,hd,qud,quw)
            else
! cas fluvial : on cherche la hauteur entrante
              call nwt(qug,gamad,dgamad,hd,qud/hd,hw)
              if (hw.gt.paray) then
                quw=qug
                delta=hw*(qud/hd+c(hd)-c(hw))
! condition de choc uw+cw>ud+cd
                if(quw.gt.delta)then
! on calcule hw par le choc a quw=qug fixe
                  call dhchoc(hw,hd,qud,quw)
                endif
!              else
!                hw=0.0
              endif
! fin du if fluvial /torrentiel sortant
            endif
            if(quw.gt.hw*c(hw))then
! cas torrentiel entrant
              if(hg.gt.paray)then
! on impose les valeurs a gauche
                hw=hg
                quw=qug
!               else
! la valeur de hg n'a pas ete donnee ou est erronee
!      write(*,*)'arete ',ia,' flux entrant torrentiel',quw,hw
              endif
            endif
            if(hw.gt.paray)then
              fquw=quw*quw/hw+0.5*g*hw*hw
              if(quw.gt.0.)then
! on devrait imposer vw=vg mais hg est inconnu  ;on ne connait
! que qug et qvg imposes (identifies a quw et qvw)
                fqvw=qvg*quw/hw
#if TRANSPORT_SOLIDE
                fhcw=quw*hcg
#endif
              else
! quw=qvg toujours positif si entrant impose
!c debit sortant : on prend vw=vd
!                fqvw=quw*qvd/hd
              fquw=0.5*g*hw*hw
!                fqvw=0.
                quw=0.
#if TRANSPORT_SOLIDE
!                fhcw=quw*hcd
!                fhcw=0.
#endif
            endif
! fin du if sur hw=0.
            endif
! fin du if hd=0.
          endif
      endif
      end subroutine rmng

!******************************************************************************
      subroutine schem1(dt,vole,vols&
#if TRANSPORT_SOLIDE
      &,volae,volas&
#endif
     &,iosmb,iapp)
!      subroutine schem1(dt,schema,vole,vols,iosmb,t,debglo,iapp)
!******************************************************************************
!
      use module_tableaux, only:nne,iae,he,que,qve,het,quet,qvet,se,la&
     &,ieva,fha,fqua,fqva,ieve,zfa,zfe,houv,qouv&
     &,nrefa,xna,yna,xta,yta,fluu,flvv,eau,smbh,flgu,flgv&
     &,vouv,vtouv,hg1,qug1,qvg1,eps1,eps2,paray,ne,na,nn,noumax,&
     &ie1,ie2,ia1,ia2,j1,j2,nouv,nbouv,g,hae!,tcomm1,tcomm2,tcomm,ie1,ie2,ia1,ia2,j1,j2
      use module_precision
#ifdef WITH_MPI
      use mpi
      use module_messages
#endif /* WITH_MPI */
      use module_mpi,only:naloc,aretes_loc,ne_loc,me,mailles_loc,nmv,elt_voisins,&
     &nouvb,ouvrages_bord,machine,comm_calcul,statinfo,vecteur_message_c,vecteur_reception_c

#if TRANSPORT_SOLIDE
      use module_ts,only:hcg1,coouv,flhc,hce,hcet,fhca,diame,sigmae,diame2,sigmae2,sedvar
#endif

!$    use omp_lib

      implicit none

      integer :: ia,iev,ie,iev2,ieg,ied,iosmb,ialoc,i,ieloc,iouv,jouv
      real(wp) :: dt,r
!      character schema*8
      real(wp),intent(out) :: vols,vole
      real(wp) :: vols_loc,vole_loc
      integer :: iapp
#ifdef WITH_MPI
      integer,dimension(mpi_status_size)::status
#endif /* WITH_MPI */

#if TRANSPORT_SOLIDE
      real(wp),intent(out) :: volae,volas
      real(wp) :: hcev,coef(0:ne)
!04       logical :: acoef
      integer :: ja
#endif


      !common/nvois/nne
      !common/param/eps1,eps2,paray
      !common/nrel/iae
      !common/nomb/ne,na,nn
      !common/tpli/he,que,qve
      !common/tlig/het,quet,qvet
      !common/dose/se,la
      !common/iela/ieva
      !common/flux/fha,fqua,fqva
      !common/iele/ieve
      !common/zare/zfa
      !common/zele/zfe
      !common/hqouv/houv,qouv
      !common/iouvra/ie1,ie2,ia1,ia2,j1,j2,nouv,nbouv
      !common/narf/nrefa
      !common/grav/g
      !common/vare/xna,yna,xta,yta
      !common/fluuvv/fluu,flvv
      !common/preso/eau
      !common/smbh2/smbh
      !common/flguv/flgu,flgv
!      common/schem/schema
      !common/vouvh/vouv,vtouv
      !common/hg12/hg1,qug1,qvg1



! reporte de flvla1 et flvla2 le 03/01/11
      call caltar
!$omp parallel private(ia,iev,ie,iev2,ieg,ied,iosmb,r,vols,vole,iapp)
!$omp do schedule(runtime)
      do ialoc=1,naloc
        ia=aretes_loc(ialoc)
        ieg = ieva(ia,1)
        ied = ieva(ia,2)
      call resrmn(ieg,ied,ia,hg1(ia,1),&
     &  qug1(ia,1),qvg1(ia,1),hg1(ia,2)&
     &,qug1(ia,2),qvg1(ia,2),dt&
#if TRANSPORT_SOLIDE
    &,hcg1(ia,1),hcg1(ia,2)&
#endif
     &)
    enddo
!$omp end do
#ifdef WITH_MPI
      do jouv=1,nouvb
        iouv=ouvrages_bord(jouv)
        if(machine(ie2(iouv))==me)then
          call mpi_send(houv(ia2(iouv),j2(iouv)),1,mpi_real8,machine(ie1(iouv)),8778+iouv,comm_calcul,statinfo)
          call mpi_send(vouv(ia2(iouv),j2(iouv)),1,mpi_real8,machine(ie1(iouv)),8779+iouv,comm_calcul,statinfo)
          !call mpi_send(vtouv(ia2(iouv),j2(iouv)),1,mpi_integer,machine(ie2(iouv)),7780+iouv,comm_calcul,statinfo)
        elseif(machine(ie1(iouv))==me)then
          call mpi_recv(houv(ia2(iouv),j2(iouv)),1,mpi_real8,machine(ie2(iouv)),8778+iouv,comm_calcul,status,statinfo)
          call mpi_recv(vouv(ia2(iouv),j2(iouv)),1,mpi_real8,machine(ie2(iouv)),8779+iouv,comm_calcul,status,statinfo)
          !call mpi_recv(vtouv(ia2(iouv),j2(iouv)),1,mpi_integer,machine(ie1(iouv)),7780+iouv,comm_calcul,status,statinfo)
        else
          print*,'un ouvrage du bord a forc�ment une maille dans le domaine local'
        end if
      end do

      if (size(vecteur_message_c)>=1.or.size(vecteur_reception_c)>=1)then
        call message_group_calcul_c(het)
        call message_group_calcul_c(quet)
        call message_group_calcul_c(qvet)

#if TRANSPORT_SOLIDE
        call message_group_calcul_c(hcet)
#if SEDVAR
        call message_group_calcul_c(diame)
        call message_group_calcul_c(sigmae)
#endif
#endif
      end if
#endif /* WITH_MPI */

! calcul des debits des ouvrages
!$omp end parallel
      if (iosmb.eq.1) then

!       write(*,*) "Test appel fonction"

          call qouvr
      endif
!!$omp parallel private(ia,iev,ie,iev2,ieg,ied,iosmb,r,vols,vole,iapp)

!         calcul des nouvelles valeurs de (h,qu,qv) en parcourant les
!         aretes : rajout de la contribution du flux de chaque arete,
!         sur les valeurs (h,qu,qv) des 2 elements voisins de l'arete.
!  on ecrit que les nouvelles valeurs sont egales aux anciennes

!!$omp do schedule(runtime)
       do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
            he(ie)=het(ie)
            que(ie)=quet(ie)
            qve(ie)=qvet(ie)
#if TRANSPORT_SOLIDE
            hce(ie)=hcet(ie)
#if SEDVAR
             diame2(ie)=diame(ie)
            sigmae2(ie)=sigmae(ie)
#endif
#endif
      end do
      do i=1,nmv
        ie=elt_voisins(i)
            he(ie)=het(ie)
            que(ie)=quet(ie)
            qve(ie)=qvet(ie)
#if TRANSPORT_SOLIDE
            hce(ie)=hcet(ie)
#if SEDVAR
             diame2(ie)=diame(ie)
            sigmae2(ie)=sigmae(ie)
#endif
#endif
      end do
!!$omp end do
      vole=0.
      vols=0.

#if TRANSPORT_SOLIDE
      volas=0.
      volae=0.
#endif
!!$omp do schedule(runtime)
      do ialoc = 1,naloc
        ia=aretes_loc(ialoc)
        if (nrefa(ia).eq.-2) then
!            contribution sur l'element de gauche (meme normale que l'arete)
             iev=ieva(ia,1)
             if (iev .gt. 0) then
                r = (dt/se(iev)) * la(ia)
!!$omp atomic
                he(iev)  = he(iev)  - r * (qouv(ia,1)/la(ia))

             endif
!            contribution sur l'element de droite (normale opposee)
             iev=ieva(ia,2)
             if (iev .gt. 0) then
                r = (dt/se(iev)) * la(ia)
!!$omp atomic
                he(iev)  = he(iev)+r * (qouv(ia,2)/la(ia))
             endif
        elseif (nrefa(ia).eq.-1) then
!            contribution sur l'element de gauche (meme normale que l'arete)
             iev=ieva(ia,1)
             if (iev .gt. 0) then
                r = (dt/se(iev)) * la(ia)
!!$omp atomic
                he(iev)  = he(iev)  - r * (fha(ia)+qouv(ia,1)/la(ia))
             elseif (fha(ia).lt.0.) then
                 vols=vols-dt*la(ia)*fha(ia)
#if TRANSPORT_SOLIDE
                 volas=volas-dt*la(ia)*fhca(ia)
#endif
             else
                 vole=vole+dt*la(ia)*fha(ia)
#if TRANSPORT_SOLIDE
                 volae=volae+dt*la(ia)*fhca(ia)
#endif
             endif
!            contribution sur l'element de droite (normale opposee)
             iev=ieva(ia,2)
             if (iev .gt. 0) then
                r = (dt/se(iev)) * la(ia)
!!$omp atomic
                he(iev)  = he(iev)+r * (fha(ia)+qouv(ia,2)/la(ia))
             elseif (fha(ia) .gt. 0.) then
                 vols=vols+dt*la(ia)*fha(ia)
#if TRANSPORT_SOLIDE
                 volas=volas+dt*la(ia)*fhca(ia)
#endif
             else
                 vole=vole-dt*la(ia)*fha(ia)
#if TRANSPORT_SOLIDE
                 volae=volae-dt*la(ia)*fhca(ia)
#endif
             endif
        else
!            contribution sur l'element de gauche (meme normale que l'arete)
             iev=ieva(ia,1)
             if (iev .gt. 0) then
                 r = (dt/se(iev)) * la(ia)
!!$omp atomic
                 he(iev)  = he(iev)  - r * fha(ia)
             elseif (fha(ia).lt.0.) then
                 vols=vols-dt*la(ia)*fha(ia)
#if TRANSPORT_SOLIDE
                 volas=volas-dt*la(ia)*fhca(ia)
#endif
             else
                 vole=vole+dt*la(ia)*fha(ia)
#if TRANSPORT_SOLIDE
                 volae=volae+dt*la(ia)*fhca(ia)
#endif
             endif
!            contribution sur l'element de droite (normale opposee)
             iev=ieva(ia,2)
             if (iev .gt. 0) then
                 r = (dt/se(iev)) * la(ia)
!!$omp atomic
                 he(iev)  = he(iev)  + r * fha(ia)
             elseif (fha(ia) .gt. 0.) then
                 vols=vols+dt*la(ia)*fha(ia)
#if TRANSPORT_SOLIDE
                 volas=volas+dt*la(ia)*fhca(ia)
#endif
             else
                 vole=vole-dt*la(ia)*fha(ia)
#if TRANSPORT_SOLIDE
                 volae=volae-dt*la(ia)*fhca(ia)
#endif
             endif
! fin du if sur nrefa
        endif
!!$omp flush
      end do
!!$omp end do
! calcul des apports a t+1/2
      if (iapp .gt. 0) then
! reporte en debut de sp resrmn
!        call secmb6(t-0.5*dt)
!!$omp do schedule(runtime)
        do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
         he(ie)=he(ie)+dt*smbh(ie)
         if (he(ie).le.paray) then
           he(ie)=0.
           eau(ie)=.false.
         else
           eau(ie)=.true.
         endif
        end do
!!$omp end do
      else
!!$omp do schedule(runtime)
        do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
         if (he(ie).le.paray) then
           he(ie)=0.
           eau(ie)=.false.
         else
           eau(ie)=.true.
         endif
        end do
!!$omp end do
      endif
!!$omp end parallel
#ifdef WITH_MPI
      !call cpu_time(tcomm1)
      call message_group_calcul(he)
      call message_group_calcul_logical(eau)
      !call cpu_time(tcomm2)
      !tcomm=tcomm+tcomm2-tcomm1
      call mpi_allreduce(vole,vole,1,mpi_real8,mpi_sum,mpi_comm_world,statinfo)
      call mpi_allreduce(vols,vols,1,mpi_real8,mpi_sum,mpi_comm_world,statinfo)
#if TRANSPORT_SOLIDE
      call mpi_allreduce(volas,volas,1,mpi_real8,mpi_sum,mpi_comm_world,statinfo)
      call mpi_allreduce(volae,volae,1,mpi_real8,mpi_sum,mpi_comm_world,statinfo)
#endif
#endif /* WITH_MPI */

      call secmb1(dt)

#if TRANSPORT_SOLIDE
!04        coef(0)=1.
       do ieloc=1,ne_loc(me)
           ie = mailles_loc(ieloc)
          hcev=hcet(ie)
!04           acoef=.true.
         do ja=1,nne(ie)
          ia=iae(ie,ja)
          if(nrefa(ia).eq.-2)then
!04              acoef=.false.
           elseif(nrefa(ia).eq.-1)then
!            contribution sur l'element de gauche (meme normale que l'arete)
!04              acoef=.false.
             else
!            contribution sur l'element de gauche (meme normale que l'arete)
               iev=ieva(ia,1)
               iev2=ieva(ia,2)

                if (iev.eq.ie) then
                   if(het(iev).lt.paray)then
                     r = (dt/se(iev)) * la(ia)
                 hcev = hcev - r * fhca(ia)
!                 hcev = hcev - r * fhca(ia)+r*flhc(ia)
                   endif
                 else
                   if(het(iev2).lt.paray)then
                      r = (dt/se(iev2)) * la(ia)
                  hcev = hcev + r * fhca(ia)
!                  hcev = hcev + r * fhca(ia)-r*flhc(ia)
                    endif
                  endif
! fin du if sur nrefa
              endif
            enddo
!04      if(acoef)then
      !write(*,*)'Coeffs : ',hcev,eps2
!04              if(hcev.lt.-eps2)then

!04                 coef(ie)=hcet(ie)/(hcet(ie)-hcev)
!04               write(*,*)'ie=',ie,coef(ie)
! on ne fait rien sauf ecrire
!04                 coef(ie)=1.
!                 coef(ie)=max(coef(ie),0.d0)
!04               else
!04                 coef(ie)=1.
!04               endif
!04             else
!04                 coef(ie)=1.
!04             endif
! fin du do sur ie
            enddo

#endif



!!$omp parallel private(ia,iev,ie,iev2,ieg,ied,iosmb,r,vols,vole,iapp)
!!$omp do schedule(runtime)
      do ialoc = 1,naloc
        ia=aretes_loc(ialoc)
        if (nrefa(ia).eq.-2) then
!            contribution sur l'element de gauche (meme normale que l'arete)
             iev=ieva(ia,1)
             iev2=ieva(ia,2)
             if (iev .gt. 0) then
                r = (dt/se(iev)) * la(ia)
                if (houv(ia,1) .gt. paray) then
!!$omp atomic
                 que(iev) = que(iev) - r * xna(ia)*(  &
     & 0.5*g*houv(ia,1)**2+qouv(ia,1)**2/(houv(ia,1)*la(ia)**2))&
! ajout d'une dissipation de q tangentiel
     &-r*xta(ia)*qouv(ia,1)*vtouv(ia,1)/la(ia)
!!$omp atomic
                 qve(iev) = qve(iev) - r * yna(ia)*(  &
     & 0.5*g*houv(ia,1)**2+qouv(ia,1)**2/(houv(ia,1)*la(ia)**2)) &
! ajout d'une dissipation de q tangentiel
     &-r*yta(ia)*qouv(ia,1)*vtouv(ia,1)/la(ia)

#if TRANSPORT_SOLIDE
#if SEDVAR
             if(sedvar)then
               call melangeiaouv(ia,iev,1,hce(iev),&
     &- r *coouv(ia,1)*qouv(ia,1)/la(ia))
             else
!end if sedvar
#endif
             hce(iev) = hce(iev) - r *coouv(ia,1)*(qouv(ia,1)/la(ia))

#if SEDVAR
             endif
!end if sedvar
#endif

#endif


                endif
!!$omp atomic
                 que(iev) = que(iev)+flgu(ia,1)
!!$omp atomic
                 qve(iev) = qve(iev)+flgv(ia,1)
             endif
!            contribution sur l'element de droite (normale opposee)
!             IEV2=IEVA(IA,2)
             IF (IEV2.GT.0) THEN
                R = (DT/SE(IEV2)) * LA(IA)
                if (houv(ia,2) .gt. paray) then
!!$omp atomic
                 que(iev2) = que(iev2) + r * xna(ia)*  &
     &(0.5*g*houv(ia,2)**2+qouv(ia,2)**2/(houv(ia,2)*la(ia)**2))  &
! ajout d'une dissipation de q tangentiel
     &+r*xta(ia)*qouv(ia,2)*vtouv(ia,2)/la(ia)
!$omp atomic
                 qve(iev2) = qve(iev2) + r * yna(ia)*  &
     &(0.5*g*houv(ia,2)**2+qouv(ia,2)**2/(houv(ia,2)*la(ia)**2)) &
! ajout d'une dissipation de q tangentiel
     &+r*yta(ia)*qouv(ia,2)*vtouv(ia,2)/la(ia)

#if TRANSPORT_SOLIDE
#if SEDVAR
             if(sedvar)then
               call melangeiaouv(ia,iev2,2,hce(iev2),&
     &r *coouv(ia,2)*(qouv(ia,2)/la(ia)))
             else
!end if sedvar
#endif
             hce(iev2) = hce(iev2) + r *coouv(ia,2)*(qouv(ia,2)/la(ia))
#if SEDVAR
             endif
!end if sedvar
#endif
#endif


                endif
!!$omp atomic
                 que(iev2) = que(iev2)+flgu(ia,2)
!!$omp atomic
                 qve(iev2) = qve(iev2)+flgv(ia,2)
             endif
        elseif (nrefa(ia).eq.-1) then
!            contribution sur l'element de gauche (meme normale que l'arete)
             iev=ieva(ia,1)
             iev2=ieva(ia,2)

             if (iev .gt. 0) then
               if (iev2 .gt. 0) then
                 if (.not.eau(iev)) then
                   fluu(ia)=0.
                   flvv(ia)=0.
#if TRANSPORT_SOLIDE
                   flhc(ia)=0.
#endif
! la maille etait vide et le reste : on suppose un transfert nul
! seul le terme de gravite de la maille non vide ne doit pas etre nul
                   if (het(iev).lt.paray) then
                     fqua(ia)=0.
                     flgu(ia,1)=0.
                     fqva(ia)=0.
                     flgv(ia,1)=0.
                     r = (dt/se(iev2)) * la(ia)
                     r=0.5*g*(0.5*(het(iev2)+he(iev2)))**2*r
                     flgu(ia,2) = r * xna(ia)
                     flgv(ia,2) = r * yna(ia)
#if TRANSPORT_SOLIDE
                      fhca(ia)=0.
#endif
                   endif
                 endif
                 if (.not.eau(iev2)) then
                   fluu(ia)=0.
                   flvv(ia)=0.
#if TRANSPORT_SOLIDE
                   flhc(ia)=0.
#endif

! la maille etait vide et le reste : on suppose un transfert nul
! seul le terme de gravite de la maille non vide ne doit pas etre nul
                   if (het(iev2).lt.paray) then
                     fqua(ia)=0.
                     r = (dt/se(iev)) * la(ia)
                     r=0.5*g*(0.5*(het(iev)+he(iev)))**2*r
                     flgu(ia,1) =  - r * xna(ia)
                     flgv(ia,1) = - r * yna(ia)
                     flgu(ia,2)=0.
                     fqva(ia)=0.
                     flgv(ia,2)=0.
#if TRANSPORT_SOLIDE
                      fhca(ia)=0.
#endif
                   endif
                 endif
               endif
                r = (dt/se(iev)) * la(ia)
                if (houv(ia,1) .gt. paray) then
!!$omp atomic
                 que(iev) = que(iev) - r * (fqua(ia)  &
     &+xna(ia)*qouv(ia,1)**2/(houv(ia,1)*la(ia)**2))
!!$omp atomic
                 qve(iev) = qve(iev) - r * (fqva(ia)  &
     &+yna(ia)*qouv(ia,1)**2/(houv(ia,1)*la(ia)**2))

#if TRANSPORT_SOLIDE
#if SEDVAR
            if(sedvar)then
               call melangeiaouv(ia,iev,1,hce(iev),&
     &-r *coouv(ia,1)*(qouv(ia,1)/la(ia)))
             else
!end if sedvar
#endif
                 hce(iev2) = hce(iev2) - r *coouv(ia,1)*(qouv(ia,1)/la(ia))
#if SEDVAR
             endif
!end if sedvar
#endif
#endif



                else
!!$omp atomic
                 que(iev) = que(iev) - r * fqua(ia)
!!$omp atomic
                 qve(iev) = qve(iev) - r * fqva(ia)
                endif
!!$omp atomic
                 que(iev) = que(iev)+r*fluu(ia)+flgu(ia,1)
!!$omp atomic
                 qve(iev) = qve(iev)+r*flvv(ia)+flgv(ia,1)

#if TRANSPORT_SOLIDE
#if SEDVAR
             if(sedvar)then
               call melangeia(ia,iev,iev2,hce(iev),&
     &- r * fhca(ia)+r*flhc(ia))
             else
!end if sedvar
#endif
                 hce(iev) = hce(iev) - r * fhca(ia)+r*flhc(ia)
#if SEDVAR
             endif
!end if sedvar
#endif

#endif


             endif
!            contribution sur l'element de droite (normale opposee)
!             iev2=ieva(ia,2)
             if (iev2 .gt. 0) then
                r = (dt/se(iev2)) * la(ia)
                if (houv(ia,2) .gt. paray) then
!!$omp atomic
                 que(iev2) = que(iev2) + r * (fqua(ia)  &
     &+xna(ia)*qouv(ia,2)**2/(houv(ia,2)*la(ia)**2))
!!$omp atomic
                 qve(iev2) = qve(iev2) + r * (fqva(ia)  &
     &+yna(ia)*qouv(ia,2)**2/(houv(ia,2)*la(ia)**2))


#if TRANSPORT_SOLIDE
#if SEDVAR
            if(sedvar)then
               call melangeiaouv(ia,iev2,2,hce(iev2),&
     &r *coouv(ia,2)*(qouv(ia,2)/la(ia)))
             else
!end if sedvar
#endif
                 hce(iev2) = hce(iev2) + r *coouv(ia,2)*(qouv(ia,2)/la(ia))
#if SEDVAR
             endif
!end if sedvar
#endif
#endif


                else
!!$omp atomic
                 que(iev2) = que(iev2) + r * fqua(ia)
!!$omp atomic
                 qve(iev2) = qve(iev2) + r * fqva(ia)
                endif
!!$omp atomic
                 que(iev2) = que(iev2)-r*fluu(ia)+flgu(ia,2)
!!$omp atomic
                 qve(iev2) = qve(iev2)-r*flvv(ia)+flgv(ia,2)

#if TRANSPORT_SOLIDE
#if SEDVAR
             if(sedvar)then
               call melangeia(ia,iev2,iev,hce(iev2),&
     &-r*flhc(ia)+r*fhca(ia))
             else
!end if sedvar
#endif
             hce(iev2) = hce(iev2)-r*flhc(ia)+r*fhca(ia)
#if SEDVAR
             endif
!end if sedvar
#endif
#endif


             endif
        else
!            contribution sur l'element de gauche (meme normale que l'arete)
             iev=ieva(ia,1)
             iev2=ieva(ia,2)

             if (iev .gt. 0) then
               if (iev2 .gt. 0) then
                 if (.not.eau(iev)) then
                   fluu(ia)=0.
                   flvv(ia)=0.
#if TRANSPORT_SOLIDE
                   flhc(ia)=0.
#endif
! la maille etait vide et le reste : on suppose un transfert nul
! seul le terme de gravite de la maille non vide ne doit pas etre nul
                   if (het(iev).lt.paray) then
                     fqua(ia)=0.
                     flgu(ia,1)=0.
                     fqva(ia)=0.
                     flgv(ia,1)=0.
                     r = (dt/se(iev2)) * la(ia)
                     r=0.5*g*(0.5*(het(iev2)+he(iev2)))**2*r
                     flgu(ia,2) = r * xna(ia)
                     flgv(ia,2) = r * yna(ia)
#if TRANSPORT_SOLIDE
                      fhca(ia)=0.
#endif
                   endif
                 endif
                 if (.not.eau(iev2)) then
                   fluu(ia)=0.
                   flvv(ia)=0.
#if TRANSPORT_SOLIDE
                   flhc(ia)=0.
#endif
! la maille etait vide et le reste : on suppose un transfert nul
! seul le terme de gravite de la maille non vide ne doit pas etre nul
                   if (het(iev2).lt.paray) then
                     fqua(ia)=0.
                     r = (dt/se(iev)) * la(ia)
                     r=0.5*g*(0.5*(het(iev)+he(iev)))**2*r
                     flgu(ia,1) =  - r * xna(ia)
                     flgv(ia,1) = - r * yna(ia)
                     flgu(ia,2)=0.
                     fqva(ia)=0.
                     flgv(ia,2)=0.
#if TRANSPORT_SOLIDE
                      fhca(ia)=0.
#endif
                   endif
                 endif
               endif
                 r = (dt/se(iev)) * la(ia)
!!$omp atomic
                 que(iev) = que(iev)-r*fqua(ia)+r*fluu(ia)+flgu(ia,1)
!!$omp atomic
                 qve(iev) = qve(iev)-r*fqva(ia)+r*flvv(ia)+flgv(ia,1)

#if TRANSPORT_SOLIDE
#if SEDVAR
             if(sedvar)then
               call melangeia(ia,iev,iev2,hce(iev),&
!04     & -r * fhca(ia)*min(coef(iev),coef(iev2))+r*flhc(ia))
     & -r * fhca(ia)+r*flhc(ia))
             else
!end if sedvar
#endif
!04             hce(iev) = hce(iev) - r * fhca(ia)*min(coef(iev),coef(iev2))+r*flhc(ia)
             hce(iev) = hce(iev) - r * fhca(ia)+r*flhc(ia)
#if SEDVAR
             endif
!end if sedvar
#endif
#endif

             endif
!            contribution sur l'element de droite (normale opposee)
             iev2=ieva(ia,2)
             if (iev2 .gt. 0) then
                 r = (dt/se(iev2)) * la(ia)
!!$omp atomic
                 que(iev2) = que(iev2)+r*fqua(ia)-r*fluu(ia)+flgu(ia,2)
!!$omp atomic
                 qve(iev2) = qve(iev2)+r*fqva(ia)-r*flvv(ia)+flgv(ia,2)

#if TRANSPORT_SOLIDE
#if SEDVAR
             if(sedvar)then
               call melangeia(ia,iev2,iev,hce(iev2),&
!04     &r * fhca(ia)*min(coef(iev),coef(iev2))-r*flhc(ia))
     &r * fhca(ia)-r*flhc(ia))
             else
!end if sedvar
#endif
!04             hce(iev2) = hce(iev2) + r * fhca(ia)*min(coef(iev),coef(iev2))-r*flhc(ia)
             hce(iev2) = hce(iev2) + r * fhca(ia)-r*flhc(ia)
#if SEDVAR
             endif
!end if sedvar
#endif
#endif


             endif
! fin du if sur nrefa
        endif
       end do
!!$omp end do
! rajout du 31 aout 2007
! une maille qui passe en eau a une vitesse nulle
!!$omp do schedule(runtime)
        do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
          if (eau(ie)) then
            if (het(ie).le.paray) then
              que(ie)=0.
              qve(ie)=0.
#if TRANSPORT_SOLIDE
! rajout du 7 fevrier 2013
          HCE(IE)=0.
#endif
            endif
          endif
        end do
!!$omp end do
!!$omp end parallel

      end subroutine schem1

!******************************************************************************
       subroutine secmb1(dt)
! second membre pente, laplacien et vent
!******************************************************************************

      use module_tableaux, only:houv,qouv,xna,yna,xta,yta,zfa,zfe,nrefa,&
     &het,quet,qvet,he,que,qve,ieva,hae,smbu,smbv,alpha,pxzfe,pyzfe,&
     &pxque,pyque,pxqve,pyqve,pxqu,pxqv,pyqu,pyqv,hg1,qug1,qvg1,&
     &fluu,flvv,eau,flgu,flgv,se,la,eps1,eps2,paray,g,fro,fvix,fviy,&
     &ne,na,nn,schema,pven,latitude,coriolis
      use module_precision
      use module_mpi,only:ne_loc,me,mailles_loc,naloc,aretes_loc,elt_voisins,nmv
      use module_loc, only:hed

      use module_tevent

!$    use omp_lib
#if TRANSPORT_SOLIDE
      use module_ts,only: flhc,smbhc2,avects
#endif

      implicit none

      real(wp) r,dt,h
      integer ia,ie,ialoc,i,ieloc
      !real(wp),dimension(:),allocatable::hed
      !real(wp),dimension(:),allocatable::xvent,yvent
!
      !common/param/eps1,eps2,paray
      !common/grav/g
      !common/const/fro,fvix,fviy
      !common/hqouv/houv,qouv
      !common/nomb/ne,na,nn
      !common/vare/xna,yna,xta,yta
      !common/zare/zfa
      !common/zele/zfe
      !common/narf/nrefa
!      common/nrel/iae
      !common/tlig/het,quet,qvet
      !common/tpli/he,que,qve
      !common/dose/se,la
!      common/lapl/px2que,py2que,px2qve,py2qve
      !common/iela/ieva
      !common/hali/hae
      !common/smbr/smbu,smbv
      !common/grad/alpha,pxzfe,pyzfe,pxque,pyque,pxqve,pyqve
!      common/smbr3/smbu3,smbv3
      !common/pxyuv/pxqu,pxqv,pyqu,pyqv
      !common/hg12/hg1,qug1,qvg1
      !common/fluuvv/fluu,flvv
      !common/preso/eau
      !common/flguv/flgu,flgv
      !common/schem/schema
      !common/lvent/pven
      !common/coriol/latitude,coriolis


      !allocate(hed(ne))
      !allocate(xvent(ne),yvent(ne))

!$omp parallel private(r,h,ia,ie)
!$omp do schedule(runtime)
      do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
         smbu(ie) = 0.
         smbv(ie) = 0.
#if TRANSPORT_SOLIDE
          smbhc2(ie)=0.
#endif
      end do
!$omp end do
!$omp do schedule(runtime)
      do i =1,nmv
        ie=elt_voisins(i)
         smbu(ie) = 0.
         smbv(ie) = 0.
#if TRANSPORT_SOLIDE
          smbhc2(ie)=0.
#endif
      end do
!$omp end do
!$omp do schedule(runtime)
      do ialoc=1,naloc
        ia=aretes_loc(ialoc)
        flgu(ia,1)=0.
        flgu(ia,2)=0.
        flgv(ia,1)=0.
        flgv(ia,2)=0.
      enddo
!$omp end do
! contribution du terme de pente
!*******************************
!      if (schema.eq.'vanleer2') then
!$omp do schedule(runtime)
        do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
          hed(ie)=0.5*(het(ie)+he(ie))
! on utilise les derivees du flux par rapport a x et a y
!     :      -.5*dt* ( pxque(ie) + pyqve(ie) )
!        if (hed(ie).le.0.5*paray)hed(ie)=0.
        end do
!$omp end do
!$omp do schedule(runtime)
        do i =1,nmv
          ie=elt_voisins(i)
          hed(ie)=0.5*(het(ie)+he(ie))
! on utilise les derivees du flux par rapport a x et a y
!     :      -.5*dt* ( pxque(ie) + pyqve(ie) )
!        if (hed(ie).le.0.5*paray)hed(ie)=0.
        end do
!$omp end do
!      else
!        do 52 ie=1,ne
!        hed(ie)=het(ie)
! 52     continue
!      endif
!$omp do schedule(runtime)
      do ialoc = 1,naloc
        ia=aretes_loc(ialoc)
           if (nrefa(ia).eq.-2) then
             h=houv(ia,1)
           else
!            h = hg1(ia,1)
             h=hae(ia)
           endif
           if (h .gt. paray) then
! laplacien
               fluu(ia)=h*fluu(ia)
               flvv(ia)=h*flvv(ia)
#if TRANSPORT_SOLIDE
               flhc(ia)=h*flhc(ia)
#endif

             else
               fluu(ia)=0.
               flvv(ia)=0.
#if TRANSPORT_SOLIDE
               flhc(ia)=0.
#endif
              endif

        ie = ieva(ia,1)
!       ie2 = ieva(ia,2)
! contribution sur l element de gauche
        if (ie.ne.0) then
!            r = dt*la(ia)/(se(ie)+se(ie2))
            r = dt/se(ie)*la(ia)
!            h = hed(ie)+(zfe(ie)-zfa(ia))
!           if (nrefa(ia).eq.-2) then
!             h=houv(ia,1)
!           else
!c            h = hg1(ia,1)
!             h=hae(ia)
!           endif
           if (eau(ie)) then
             if (h .gt. paray) then
!c laplacien
!               smbu(ie)=smbu(ie)+r*h*fluu(ia)
!               smbv(ie)=smbv(ie)+r*h*flvv(ia)
!               if (het(ie) .gt. paray) then
               if (hed(ie) .gt. paray) then
                 h=0.5*(hed(ie)+h)
!               r=-g*r*h*min(h,zfe(ie)-zfa(ia))
                 r=r*h*g*(zfa(ia)-zfe(ie))
               else
!               r=-0.5*r*h*h*g
                 r=-0.5*r*h*min(h,zfe(ie)-zfa(ia))*g
               endif
!c dans le cas present h=zfe-zfa : faux
!c               h=hae(ia)
!               r=-0.5*g*r*h*h
!c               r=-0.5*g*r*h*min(h,zfe(ie)-zfa(ia))
             else
!                r=0.5*r*hed(ie)**2*g
               r=0.5*r*hed(ie)*min(hed(ie),zfa(ia)-zfe(ie))*g
!               if (r.lt.0.)r=0.
!c               h=0.5*hed(ie)
             endif
            flgu(ia,1) =  - r * xna(ia)
            flgv(ia,1) =  - r * yna(ia)
! si he nouveau=0 pas de calcul a faire
           endif
!            r=r*h*g*(zfa(ia)-zfe(ie))
! fin du if sur ie =0
         endif
         ie = ieva(ia,2)
! contribution sur l element de droite
         if (ie.ne.0) then
            r = dt/se(ie)*la(ia)
!            h = hed(ie)+(zfe(ie)-zfa(ia))
           if (nrefa(ia).eq.-2) then
             h=houv(ia,2)
           else
             h=hae(ia)
!            h = hg1(ia,2)
           endif
           if (eau(ie)) then
             if (h .gt. paray) then
!c laplacien
!               smbu(ie)=smbu(ie)-r*h*fluu(ia)
!               smbv(ie)=smbv(ie)-r*h*flvv(ia)
!              if (het(ie) .gt. paray) then
              if (hed(ie) .gt. paray) then
               h=0.5*(hed(ie)+h)
!               r=-g*r*h*min(h,zfe(ie)-zfa(ia))
               r=r*h*g*(zfa(ia)-zfe(ie))
              else
!                h=hae(ia)
!                r=-0.5*g*r*h*h
               r=-0.5*g*r*h*min(h,zfe(ie)-zfa(ia))
              endif
             else
!               r=0.5*r*hed(ie)**2*g
               r=0.5*r*hed(ie)*min(hed(ie),zfa(ia)-zfe(ie))*g
!               if (r.lt.0.)r=0.
!               h=0.5*hed(ie)
             endif
            flgu(ia,2) =   r * xna(ia)
            flgv(ia,2) =   r * yna(ia)
!           smbu(ie) = smbu(ie) + r * xna(ia)
!           smbv(ie) = smbv(ie) + r * yna(ia)
! si he nouveau=0 pas de calcul a faire
           endif
! fin du if sur ie =0
         endif
! fin de la boucle sur ia
       end do
!$omp end do
!$omp end parallel
!
! contribution du frottement du au vent
!************************************
      if (pven) then
        call tevent!(xvent,yvent)
      end if

!$omp parallel
      if (pven) then
!$omp do schedule(runtime)
        do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
          if (eau(ie)) then
            smbu(ie) = smbu(ie) + xvent(ie) * dt
            smbv(ie) = smbv(ie) + yvent(ie) * dt
          endif
        enddo
!$omp end do

      elseif (fvix .gt. eps2.or.fviy .gt. eps2) then
!$omp do schedule(runtime)
        do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
          if (eau(ie)) then
            smbu(ie) = smbu(ie) + fvix * dt
            smbv(ie) = smbv(ie) + fviy * dt
          endif
        end do
!$omp end do
      endif

! contribution force de coriolis
!************************************
      if (coriolis) then
!$omp do schedule(runtime)
        do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
          if (eau(ie)) then
            smbu(ie) = smbu(ie) - latitude * qve(ie)* dt
            smbv(ie) = smbv(ie) + latitude * que(ie) * dt
          endif
        enddo
!$omp end do
      endif
!$omp end parallel
      !deallocate(hed)

      return
      end subroutine secmb1

!******************************************************************************
       subroutine secmb2(dt)
! frottement fond et paroi
!******************************************************************************

      use module_tableaux,only:se,la,ieva,dxe,dxen,fra,fre,he,que,qve&
     &,xna,yna,xta,yta,nrefa,eau,cofr,kse,eps1,eps2,paray,fro,fvix,&
     &fviy,g,ne,na,nn,darcy
      use module_precision
      use module_mpi,only:ne_loc,me,mailles_loc,naloc,aretes_loc
!$    use omp_lib

      implicit none

      real(wp) dt,fra2,q2,smu
      integer ie,ieloc
      integer ia,ialoc
      real(wp) qt,q1
      real(wp) fm1,q,nu

      data nu/0.000001/
      save nu
!
      !common/param/eps1,eps2,paray
      !common/const/fro,fvix,fviy
      !common/dose/se,la
      !common/iela/ieva
      !common/dxdy/dxe,dxen
      !common/grav/g
      !common/nomb/ne,na,nn
      !common/frot/fra,fre
      !common/tpli/he,que,qve
      !common/vare/xna,yna,xta,yta
      !common/narf/nrefa
      !common/preso/eau
! frottement darcy
      !common/penfon/cofr
      !common/darcy/darcy
      !common/kdarcy/kse

!$omp parallel private(fra2,q2,smu,ia,ie,qt,q1,fm1,q,nu)
! contribution du terme de frottement au fond
!********************************************
!$omp do schedule(runtime)
      do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
         if (eau(ie)) then
           q=sqrt(que(ie)*que(ie)+qve(ie)*qve(ie))
!        q2=q/he(ie)
!      if (q2 .gt. 10.) then
!       que(ie)=que(ie)*10./q2
!       qve(ie)=qve(ie)*10./q2
!       q=q*10./q2
!      endif
           if (darcy) then
             if(q.gt.eps2)then
!        fm1=dlog10(kse(ie)/(12.*he(ie))+1.95/((q/nu)**0.9))
!         fm1 =4.*fm1**2
               fm1=4.*(dlog10(kse(ie)/(12.*he(ie))+1.95/(q/nu)**0.9))**2
             else
               fm1=4.*(dLOG10(KSE(IE)/(12.*He(ie))))**2
             endif
!           fm1=4.*(log(kse(ie)/(12.*he(ie))+6.7903/(4.*q/nu)**0.9)**2.)
             fre(ie)=cofr(ie)*8.*g*fm1*he(ie)**(-0.333333)
           endif
           fra2=max(fra(ie),fre(ie)*he(ie)**0.3333333)
           q2=q/fra2/he(ie)**2
           q2=- g * q2 * dt
! smu est le coefficient entre qve definitif et qve provisoire
           if (abs(q2) .gt. eps2) then
               smu=0.5*(1.-sqrt(1.-4.*q2))/q2
           else
               smu=1.+q2
           endif
               qve(ie)=smu*qve(ie)
               que(ie)=smu*que(ie)
! cas des vitesses nulles
           if (abs(que(ie)/he(ie)).lt.eps2) then
               que(ie)=0.
           endif
           if (abs(qve(ie)/he(ie)).lt.eps2) then
               qve(ie)=0.
           endif
! si he nul
         else
            he(ie)=0.
            que(ie)=0.
            qve(ie)=0.
         endif
      end do
!$omp end do

! contribution du frottement de paroi
!************************************
!
      if (abs(fro) .gt. eps2) then
! calcul en strickler
       if (fro .gt. 0.) then
!$omp do schedule(runtime)
        do ialoc = 1,naloc
         ia=aretes_loc(ialoc)
!$omp flush(que,qve)
         if (nrefa(ia).eq.2.or.nrefa(ia).eq.-2) then
          ie = ieva(ia,1)
! contribution sur l element de gauche
          if (ie.ne.0) then
            if (eau(ie)) then
              qt=que(ie)*xta(ia)+qve(ie)*yta(ia)
! si darcy fro est la rugosite en mm
              if (darcy) then
                if(abs(qt).gt.eps2)then
                  fm1=4.*(dlog10(fro/(12000.*he(ie))+1.95/(abs(qt)/nu)**0.9))**2
                  fm1=8.*g*fm1
                  q1=abs(qt)/fm1/he(ie)*la(ia)/se(ie)
! si qt=0
                 else
                   q1=0.
                 endif
              else
                q1=abs(qt)/fro**2/he(ie)**1.33333*la(ia)/se(ie)
              endif
! division par 2 pour tenir compte que le frottement n'est applique
! en moyenne que par la moitie de la colonne deau
              q1=0.5*q1
              q2=- g * q1 * dt*abs(xta(ia))
!$omp atomic
              que(ie)=que(ie)/(1.-q2)
              q2=- g * q1 * dt*abs(yta(ia))
!$omp atomic
              qve(ie)=qve(ie)/(1.-q2)
           endif
         endif
         ie = ieva(ia,2)
! contribution sur l element de droite
         if (ie.ne.0) then
            if (eau(ie)) then
              qt=que(ie)*xta(ia)+qve(ie)*yta(ia)
              if (darcy) then
! si darcy fro est la rugosite en mm
                if(abs(qt).gt.eps2)then
                  fm1=4.*(dlog10(fro/(12000.*he(ie))+1.95/(abs(qt)/nu)**0.9))**2
                  fm1=8.*g*fm1
                  q1=abs(qt)/fm1/he(ie)*la(ia)/se(ie)
! si qt=0
                else
                  q1=0.
                endif
              else
              q1=abs(qt)/fro**2/he(ie)**1.33333*la(ia)/se(ie)
              endif
! division par 2 pour tenir compte que le frottement n'est applique
! en moyenne que par la moitie de la colonne deau
              q1=0.5*q1
              q2=- g * q1 * dt*abs(xta(ia))
!$omp atomic
              que(ie)=que(ie)/(1.-q2)
              q2=- g * q1 * dt*abs(yta(ia))
!$omp atomic
              qve(ie)=qve(ie)/(1.-q2)
           endif
         endif
! fin du if sur nrefa=2
       endif
      end do
!$omp end do
      else
! calcul en chezy
!$omp do schedule(runtime)
       do ialoc = 1,naloc
         ia=aretes_loc(ialoc)
!$omp flush(que,qve)
         if (nrefa(ia).eq.2.or.nrefa(ia).eq.-2) then
          ie = ieva(ia,1)
! contribution sur l element de gauche
          if (ie.ne.0) then
            if (eau(ie)) then
              qt=que(ie)*xta(ia)+qve(ie)*yta(ia)
              q1=abs(qt)/fro**2/he(ie)*la(ia)/se(ie)
! division par 2 pour tenir compte que le frottement n'est applique
! en moyenne que par la moitie de la colonne deau
              q1=0.5*q1
              q2=- g * q1 * dt*abs(xta(ia))
!$omp atomic
              que(ie)=que(ie)/(1.-q2)
              q2=- g * q1 * dt*abs(yta(ia))
!$omp atomic
              qve(ie)=qve(ie)/(1.-q2)
           endif
         endif
         ie = ieva(ia,2)
! contribution sur l element de droite
         if (ie.ne.0) then
            if (eau(ie)) then
              qt=que(ie)*xta(ia)+qve(ie)*yta(ia)
              q1=abs(qt)/fro**2/he(ie)*la(ia)/se(ie)
! division par 2 pour tenir compte que le frottement n'est applique
! en moyenne que par la moitie de la colonne deau
              q1=0.5*q1
              q2=- g * q1 * dt*abs(xta(ia))
!$omp atomic
              que(ie)=que(ie)/(1.-q2)
              q2=- g * q1 * dt*abs(yta(ia))
!$omp atomic
              qve(ie)=qve(ie)/(1.-q2)
           endif
         endif
! fin du if sur nrefa=2
       endif
      end do
!$omp end do
! fin du if sur chezy ou strickler
       endif
! fin du if sur fro=0
      endif
!$omp end parallel

      end subroutine secmb2

!******************************************************************************
       subroutine secmb5(dt)
! laplacien et vent a tn+1/2
!******************************************************************************
      use module_tableaux,only:smbu2,smbv2,het,quet,qvet,se,la,ieva&
     &,hg1,qug1,qvg1,fluu,flvv,eps1,eps2,paray,g,ne,na,nn,fro,fvix&
     &,fviy,lcvi,lcviv,lcvif,pven,latitude,coriolis!,tcomm1,tcomm2,tcomm
      use module_precision
      use module_tevent
      use module_mpi,only:ne_loc,me,mailles_loc,naloc,aretes_loc
#ifdef WITH_MPI
      use module_messages
#endif /* WITH_MPI */

#if TRANSPORT_SOLIDE
      use module_ts,only:flhc,smbhc2
#endif

!$    use omp_lib
      implicit none
!
      real(wp) dt,r
      integer ie,ia,ialoc,ieloc
      !real(wp),dimension(:),allocatable::xvent,yvent



         smbu2 = 0.
         smbv2 = 0.
#if TRANSPORT_SOLIDE
        smbhc2 = 0.
#endif
      if (pven) then ! pvent hors de la zone parall�le
        call tevent!(xvent,yvent)
      end if
! contribution du laplacien
!*******************************
!$omp parallel private(ie,r)
      if (lcvi) then
!$omp do schedule(runtime)
      do ialoc = 1,naloc
        ia=aretes_loc(ialoc)
        ie = ieva(ia,1)
! contribution sur l element de gauche
        if (ie.ne.0) then
            r = dt/se(ie)*la(ia)
           if (het(ie) .gt. paray) then
! laplacien
            smbu2(ie)=smbu2(ie)+r*hg1(ia,1)*fluu(ia)
            smbv2(ie)=smbv2(ie)+r*hg1(ia,1)*flvv(ia)
#if TRANSPORT_SOLIDE
            smbhc2(ie) = smbhc2(ie)+r*hg1(ia,1)*flhc(ia)
#endif
! si he =0 pas de calcul a faire
           endif
! fin du if sur ie =0
         endif
         ie = ieva(ia,2)
! contribution sur l element de droite
         if (ie.ne.0) then
            r = dt/se(ie)*la(ia)
           if (het(ie) .gt. paray) then
! laplacien
            smbu2(ie)=smbu2(ie)-r*hg1(ia,2)*fluu(ia)
            smbv2(ie)=smbv2(ie)-r*hg1(ia,2)*flvv(ia)
#if TRANSPORT_SOLIDE
            smbhc2(ie) = smbhc2(ie)-r*hg1(ia,1)*flhc(ia)
#endif

! si he nouveau=0 pas de calcul a faire
           endif
! fin du if sur ie =0
         endif
! fin de la boucle sur ia
       end do
!$omp end do

! fin if sur lcvi
        endif
!
! contribution du frottement du au vent
!************************************
      if (pven) then
        !call tevent!(xvent,yvent)
!$omp do schedule(runtime)
        do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
          if (het(ie) .gt. paray) then
            smbu2(ie) = smbu2(ie) + xvent(ie) * dt
            smbv2(ie) = smbv2(ie) + yvent(ie) * dt
          endif
        enddo
!$omp end do
      elseif (fvix .gt. eps2.or.fviy .gt. eps2) then
!$omp do schedule(runtime)
        do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
          if (het(ie) .gt. paray) then
            smbu2(ie) = smbu2(ie) + fvix * dt
            smbv2(ie) = smbv2(ie) + fviy * dt
          endif
        end do
!$omp end do
      endif
! contribution force de coriolis
!************************************
      if (coriolis) then
!$omp do schedule(runtime)
        do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
           if (het(ie) .gt. paray) then
            smbu2(ie) = smbu2(ie) - latitude * qvet(ie)* dt
            smbv2(ie) = smbv2(ie) + latitude * quet(ie) * dt
          endif
        enddo
!$omp end do
      endif
!$omp end parallel

#ifdef WITH_MPI
      !call cpu_time(tcomm1)
      call message_group_calcul(smbu2)
      call message_group_calcul(smbv2)

#if TRANSPORT_SOLIDE
      call message_group_calcul(smbhc2)
#endif
      !call cpu_time(tcomm2)
      !tcomm=tcomm+tcomm2-tcomm1
#endif /* WITH_MPI */

      end subroutine secmb5

!******************************************************************************
       subroutine secmb3(dt)
! frottement de paroi a tn+1/2
!******************************************************************************

      use module_tableaux, only:xna,yna,xta,yta,nrefa,het,quet,qvet&
     &,se,la,px2que,py2que,px2qve,py2qve,ieva,smbu3,smbv3,dxe,dxen,&
     &eps1,eps2,paray,g,fro,fvix,fviy,ne,na,nn,darcy!,tcomm1,tcomm2,tcomm
      use module_precision
      use module_mpi,only:naloc,aretes_loc,ne_loc,me,mailles_loc,nmv,elt_voisins
#ifdef WITH_MPI
      use module_messages
#endif /* WITH_MPI */

      implicit none
!
!      parameter(maxne=100000)
!      real(wp) he(*),que(*),qve(*),zfa(*),xna(*),yna(*),se(*),la(*)
!     : ,smbu(*),smbv(*),xta(*),yta(*),fro,
!     :px2que(*),py2que(*),px2qve(*),py2qve(*),zfe(*),hae(*)
      real(wp) dt,q1,qt
      integer ia,ie,ialoc,i,ieloc
! variables frottement darcy
      real(wp) fm1,nu

      data nu/0.000001/
      save nu
!

!$omp parallel private(fm1,q1,qt,ie)
!$omp do schedule(runtime)
        do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
          smbu3(ie) = 0.
          smbv3(ie) = 0.
        end do
!$omp end do
!$omp do schedule(runtime)
        do i =1,nmv
          ie=elt_voisins(i)
          smbu3(ie) = 0.
          smbv3(ie) = 0.
        end do
!$omp end do

! contribution du frottement de paroi
!************************************
!
      if (abs(fro) .gt. eps2) then
! frottement en strickler
        if (fro .gt. 0.) then
!$omp do schedule(runtime)
        do ialoc = 1,naloc
         ia=aretes_loc(ialoc)
         if (nrefa(ia).eq.2.or.nrefa(ia).eq.-2) then
          ie = ieva(ia,1)
! contribution sur l element de gauche
          if (ie.ne.0) then
            if (het(ie) .gt. paray) then
              qt=quet(ie)*xta(ia)+qvet(ie)*yta(ia)
!             q1=abs(qt)/fro**2/het(ie)**2
!              q1=abs(qt)/fro**2/dxe(ie,1)**2.33333
              if (darcy) then
! si darcy fro est la rugosite en mm
                if(abs(qt).gt.eps2)then
                  fm1=4.*(dlog10(fro/(12000.*het(ie))+1.95/(abs(qt)/nu)**0.9))**2
                  fm1=8.*g*fm1
                  q1=abs(qt)/fm1/het(ie)*la(ia)/se(ie)
! si qt=0
                else
                  q1=0.
                endif
              else
                q1=abs(qt)/fro**2/het(ie)**1.33333*la(ia)/se(ie)
              endif
! division par 2 pour tenir compte que le frottement n'est applique
! en moyenne que par la moitie de la colonne deau
              q1=0.5*q1
              smbu3(ie) = smbu3(ie) - g * q1 * dt *abs(xta(ia))
              smbv3(ie) = smbv3(ie) - g * q1 * dt *abs(yta(ia))
           endif
         endif
         ie = ieva(ia,2)
! contribution sur l element de droite
         if (ie.ne.0) then
           if (het(ie) .gt. paray) then
             qt=quet(ie)*xta(ia)+qvet(ie)*yta(ia)
!             q1=abs(qt)/fro**2/dxe(ie,1)**2.33333
!            q1=abs(qt)/fro**2/het(ie)**2
              if (darcy) then
! si darcy fro est la rugosite en mm
                if(abs(qt).gt.eps2)then
                  fm1=4.*(dlog10(fro/(12000.*het(ie))+1.95/(abs(qt)/nu)**0.9))**2
                  fm1=8.*g*fm1
                  q1=abs(qt)/fm1/het(ie)*la(ia)/se(ie)
! si qt=0
                else
                  q1=0.
                endif
              else
                q1=abs(qt)/fro**2/het(ie)**1.33333*la(ia)/se(ie)
              endif
! division par 2 pour tenir compte que le frottement n'est applique
! en moyenne que par la moitie de la colonne deau
              q1=0.5*q1
             smbu3(ie) = smbu3(ie) - g * q1 * dt*abs(xta(ia))
             smbv3(ie) = smbv3(ie) - g * q1 * dt *abs(yta(ia))
           endif
         endif
! fin du if sur nrefa=2
       endif
      end do
!$omp end do
! frottement en chezy
      else
!$omp do schedule(runtime)
        do ialoc = 1,naloc
         ia=aretes_loc(ialoc)
         if (nrefa(ia).eq.2.or.nrefa(ia).eq.-2) then
          ie = ieva(ia,1)
! contribution sur l element de gauche
          if (ie.ne.0) then
            if (het(ie) .gt. paray) then
              qt=quet(ie)*xta(ia)+qvet(ie)*yta(ia)
!             q1=abs(qt)/fro**2/het(ie)**2
!              q1=abs(qt)/fro**2/dxe(ie,1)**2.33333
              q1=abs(qt)/fro**2/het(ie)*la(ia)/se(ie)
! division par 2 pour tenir compte que le frottement n'est applique
! en moyenne que par la moitie de la colonne deau
              q1=0.5*q1
              smbu3(ie) = smbu3(ie) - g * q1 * dt *abs(xta(ia))
              smbv3(ie) = smbv3(ie) - g * q1 * dt *abs(yta(ia))
           endif
         endif
         ie = ieva(ia,2)
! contribution sur l element de droite
         if (ie.ne.0) then
           if (het(ie) .gt. paray) then
             qt=quet(ie)*xta(ia)+qvet(ie)*yta(ia)
!             q1=abs(qt)/fro**2/dxe(ie,1)**2.33333
!            q1=abs(qt)/fro**2/het(ie)**2
              q1=abs(qt)/fro**2/het(ie)*la(ia)/se(ie)
! division par 2 pour tenir compte que le frottement n'est applique
! en moyenne que par la moitie de la colonne deau
              q1=0.5*q1
             smbu3(ie) = smbu3(ie) - g * q1 * dt*abs(xta(ia))
             smbv3(ie) = smbv3(ie) - g * q1 * dt *abs(yta(ia))
           endif
         endif
! fin du if sur nrefa=2
       endif
      end do
!$omp end do
! fin du if sur strickler ou chezy
       endif
! fin du if sur fro=0
      endif
!$omp end parallel

#ifdef WITH_MPI
      !call cpu_time(tcomm1)
      call message_group_calcul(smbu3)
      call message_group_calcul(smbv3)
      !call cpu_time(tcomm2)
      !tcomm=tcomm+tcomm2-tcomm1
#endif /* WITH_MPI */

      end subroutine secmb3

! !******************************************************************************
!        subroutine secmb4(dt,hg1)
! ! calcul du second membre de pente a tn+1/2
! ! inutilise
! !******************************************************************************
!       integer nemax,naemax,nnemax,nevemx,namax,nnamax,nevamx,nnmax,
!      :        nalmax
!       parameter (nemax  = 300000,naemax = 4,nnemax = 4,nevemx = 4,
!      :           namax  = 900000,nnamax = 2,nevamx = 2,nnmax  = 360000,
!      :           nalmax = 1000)
!       real(wp) fro,fvix,fviy
!       real(wp) r,dt,g,paray,eps1,eps2,h
!       integer ia,na,ieva(namax,nevamx),ie,ne,nn
!       real(wp) xna(namax),yna(namax),xta(namax),yta(namax)
!       real(wp) zfa(namax),zfe(nemax)
!       real(wp) het(nemax),quet(nemax),qvet(nemax)
!       real(wp) hg1(namax,nevamx)
!       real(wp) se(nemax),la(namax)
!       real(wp) smbu(nemax),smbv(nemax)
!       real(wp) pxqu(nemax),pxqv(nemax),pyqu(nemax),pyqv(nemax)
!
!       common/param/eps1,eps2,paray
!       common/grav/g
!       common/const/fro,fvix,fviy
!       common/nomb/ne,na,nn
!       common/vare/xna,yna,xta,yta
!       common/zare/zfa
!       common/zele/zfe
!       common/tlig/het,quet,qvet
!       common/dose/se,la
!       common/iela/ieva
!       common/smbr/smbu,smbv
!       common/pxyuv/pxqu,pxqv,pyqu,pyqv
!
!       do 2 ie =1,ne
!          smbu(ie) = 0.
!          smbv(ie) = 0.
!  2    continue
!
! ! contribution du terme de pente
! !*******************************
!       do 1 ia = 1,na
!        ie = ieva(ia,1)
! ! contribution sur l element de gauche
!        if (ie.ne.0) then
!             r = dt/se(ie)*la(ia)
! !            h = het(ie)+(zfe(ie)-zfa(ia))
!             h=hg1(ia,1)
!             if (h .gt. paray) then
!               if (het(ie) .gt. paray) then
!                h=0.5*(het(ie)+h)
! !               r=-g*r*h*min(h,zfe(ie)-zfa(ia))
!                r=r*h*g*(zfa(ia)-zfe(ie))
!               else
! !c               h=hg1(ia,1)
!                r=-0.5*g*r*h*h
! !c               r=-0.5*g*r*h*min(h,zfe(ie)-zfa(ia))
!               endif
!             else
!                r=0.5*r*het(ie)*het(ie)*g
!             endif
!             smbu(ie) = smbu(ie) - r * xna(ia)
!             smbv(ie) = smbv(ie) - r * yna(ia)
! ! fin du if sur ie =0
!        endif
!        ie = ieva(ia,2)
! ! contribution sur l element de droite
!        if (ie.ne.0) then
!             r = dt/se(ie)*la(ia)
! !            h = het(ie)+(zfe(ie)-zfa(ia))
!             h=hg1(ia,2)
!             if (h .gt. paray) then
!               if (het(ie) .gt. paray) then
!                h=0.5*(het(ie)+h)
! !               r=-g*r*h*min(h,zfe(ie)-zfa(ia))
!                r=r*h*g*(zfa(ia)-zfe(ie))
!               else
! !c               h=hg1(ia,2)
!                r=-0.5*g*r*h*h
! !c               r=-0.5*g*r*h*min(h,zfe(ie)-zfa(ia))
!               endif
!             else
!                r=0.5*r*het(ie)*het(ie)*g
!             endif
!             smbu(ie) = smbu(ie) + r * xna(ia)
!             smbv(ie) = smbv(ie) + r * yna(ia)
! ! fin du if sur ie =0
!        endif
! ! fin de la boucle sur ia
!  1     continue
!
!       end subroutine secmb4

!******************************************************************************
       subroutine intapp(t,appt)
! calcul des apports a t
!******************************************************************************

      use module_tableaux, only:napp,nchro,nbchro,nmchro,nmcoap&
     &,appchr,tappch
      use module_precision

      implicit none
!
! nombre maximal de chroniques et de nombre de couples par chronique
      integer ichro,ncouap
      real(wp) t
      real(wp) appt(nmchro)
      logical cherche
!
      !common/nappor/napp,nchro,nbchro
      !common/apport/appchr,tappch

! boucle sur les chroniques
!$omp parallel private(cherche, ncouap)
!$omp do schedule(runtime)
      do ichro=1,nchro
        cherche=.true.
        ncouap=1
        if (t.lt.tappch(ichro,ncouap)) then
          appt(ichro)=appchr(ichro,ncouap)
          cherche=.false.
        endif
        if (cherche) then
          do ncouap=2,nbchro(ichro)
            if (cherche) then
              if (t.lt.tappch(ichro,ncouap)) then
                appt(ichro)=appchr(ichro,ncouap)+&
     &(appchr(ichro,ncouap)-appchr(ichro,ncouap-1))&
     &/(tappch(ichro,ncouap)-tappch(ichro,ncouap-1))&
     & *(t-tappch(ichro,ncouap))
                cherche=.false.
              endif
            endif
! fin de boucle sur ncouap
          enddo
! on est arrive au couple maxi
          if (cherche) then
            appt(ichro)=appchr(ichro,nbchro(ichro))
          endif
        endif
! fin de boucle sur les chroniques
      enddo
!$omp end do
!$omp end parallel
      end subroutine intapp

!******************************************************************************
       subroutine secmb6(t)
! calcul des apports a t   (repartition, le calcul etant fait dans intapp)
!******************************************************************************

      use module_tableaux,only:smbh,napp,nchro,nbchro,nmchro,ne,na,nn
      use module_precision
      use module_mpi,only:ne_loc,me,mailles_loc,nmv,elt_voisins
!$    use omp_lib
      implicit none
!
! nombre maximal de chroniques
      real(wp) t
      integer ie,i,ieloc
      real(wp) appt(nmchro)
!
      !common/nomb/ne,na,nn
      !common/smbh2/smbh
      !common/nappor/napp,nchro,nbchro
!        do ieloc=1,ne_loc(me)
!         ie=mailles_loc(ieloc)
!          smbh(ie) = 0. ! ???
!        end do
! calcul des apports au temps t : interpolation
      call intapp(t,appt)
! repartition sur les mailles
!$omp parallel
!$omp do schedule(runtime)
      do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
        smbh(ie) = appt(napp(ie))
      end do
!$omp end do
!$omp do schedule(runtime)
      do i =1,nmv
        ie=elt_voisins(i)
        smbh(ie) = appt(napp(ie))
      end do
!$omp end do
!$omp end parallel

      end subroutine secmb6

!******************************************************************************
      subroutine topo
!******************************************************************************
! sous-programme de calcul de la topographie par rapport
! a la cote  moyenne zfm
! entree  :
!          xn  : abcsisses des noeuds
!          yn  : ordonnees des noeuds
!          zfn : cotes des noeuds
!onc
!          zfndur : cote du fond dur
!oncf
!  sortie :
!          zfe : cotes des elements
!          zfm : cote moyenne
!          zfa : cotes des aretes
!          zfn : cotes des noeuds par rapport a la moyenne
!
!******************************************************************************
      use module_tableaux, only : zfa,zfe,nrefa,iae,ieva,nne,zfn,ina,&
     &eps1,eps2,paray,zfm,ne,na,nn,ift,etude
      use module_precision

#if TRANSPORT_SOLIDE
      use module_ts, only : zfndur,solute,epcoun,diamn,sigman,taummn&
      &,nbcoun,sedvar
#endif

      implicit none
!
!      real(wp) zfn(*),zfe(*),zfa(*),zfm,zfmin,zfmax,eps1,eps2,paray
      integer ie,ia,j
!     :    nn,in,ie,ne,na,ia,ift,nrefa(*),iae(nemax,naemax),zf1,zf2

#if TRANSPORT_SOLIDE
      integer :: idon,in,jn
      real(WP) :: zfbas
      logical :: encore
#endif


!  limitation des cotes des noeuds en fonction de zfmin et zfmax
!**************************************************************
!  calcul de la moyenne des cotes des noeuds
!*******************************************
!        zfm = zfn(1)
!        do 2 in=2,nn
!        zfm = zfm+zfn(in)
! 2      continue
!        zfm = zfm / nn
! correction du 27/7/2000 pour simplifier
         zfm = 0.


#if TRANSPORT_SOLIDE

      if(.not.solute)then
!     ouverture du fichier fond dur
      idon=9
      open(idon,file=trim(etude)//'.dur',status='old',err=999)
      read(idon,'(10f8.3)',end=997,err= 997)(zfndur(in),in=1,nn)
      write(*,*)'presence du fichier ',trim(etude)//'.dur'
! correction du 10 mai 2007 pour controler erreur en entree
      do in=1,nn
        if(zfndur(in).gt.zfn(in))then
          write(*,*)'noeud ', in, ' erreur fond dur'
          write(*,*) 'zdur=',zfndur(in),' z=',zfn(in)
          zfndur(in)=zfn(in)
        endif
      enddo
#if SEDVAR
      if(sedvar)then
        do in=1,nn
          zfbas=zfn(in)-epcoun(in,1)
          do jn=2,nbcoun(in)
          zfbas=zfbas-epcoun(in,jn)
          enddo
          if(zfbas.lt.zfndur(in))then
! on enleve les couches plus basses que fond dur
          encore=.true.
          do jn=nbcoun(in),1,-1
            if(encore)then
            if(zfbas+epcoun(in,jn).gt.zfndur(in))then
              nbcoun(in)=jn
              epcoun(in,jn)=zfbas+epcoun(in,jn)-zfndur(in)
              encore=.false.
            else
              zfbas=zfbas+epcoun(in,jn)
            endif
! fin du if sur encore
            endif
!  fin du do sur jn
           enddo
           if(encore)then
! zfndur est egal a zfn
             nbcoun(in)=1
             epcoun(in,1)=0.1*eps2
             taummn(in,1)=999.999
            endif
!          elseif(zfbas.gt.zfndur(in))then
! dans ce cas on garde les couches et on abandonne zfndur
           endif
! fin boucle sur in
        enddo
! fin du if sur sedvar
      endif
!end if sedvar
#endif
      go to 998
 999  do in=1,nn
        zfndur(in)=-9999.
      enddo
      write(*,*)'pas de fichier ',trim(etude)//'.dur'
      go to 998
 997  write(*,*)'erreur de lecture dans le fichier ',trim(etude)//'.dur'
!      pause
      stop

! fin du if sur solute
      endif


#endif

!  calcul des cotes des noeuds par rapport a la moyenne
!******************************************************
! inutile si zfm=0
!      do 3 in=1,nn
!        zfn(in)=zfn(in)
!        zfn(in)=zfn(in)-zfm
! 3    continue
!  calcul des cotes des aretes
!******************************************************
 998  do 5 ia = 1,na
        zfa(ia) =.5*(zfn(ina(ia,1))+zfn(ina(ia,2)))
 5    continue
!  calcul des cotes des elements
!*******************************
      do 4 ie = 1,ne
        zfe(ie)=zfa(iae(ie,1))
        do 41 j=2,nne(ie)
          zfe(ie)=zfe(ie)+zfa(iae(ie,j))
 41     continue
        zfe(ie)=zfe(ie)/float(nne(ie))
 4    continue

      end subroutine topo

!******************************************************************************
      real(wp) function c(h)
!******************************************************************************
!     calcul de la celerite (c) de l'onde ; g = gravite, h = hauteur
!      implicit logical (a-z)
      use module_tableaux,only:g
      use module_precision
      implicit none
      real(wp) h
      !common/grav/g

      c = sqrt(g*h)
      end function c

!******************************************************************************
      real(wp) function dgamad(h2,u2,h,xl)
!******************************************************************************
!     derivee de la fonction gamad
!      implicit logical (a-z)
      use module_tableaux,only:g
      use module_precision
      implicit none
      real(wp) h2,u2,h,xl
      !common/grav/g

      dgamad = u2 - 2.0*sqrt(g*h2) + 3.0*sqrt(g*h)
      end function dgamad

!******************************************************************************
      real(wp) function dgamag(h1,u1,h,xl)
!******************************************************************************
!     derivee de la fonction gamag
!      implicit logical (a-z)
      use module_tableaux,only:g
      use module_precision
      implicit none
      real(wp) h1,u1,h,xl
      !common/grav/g

      dgamag = u1 + 2.0*sqrt(g*h1) - 3.0*sqrt(g*h)
      end function dgamag
!******************************************************************************
      real(wp) function dlgamag(h1,u1,h,xl)
!******************************************************************************
!     derivee de la fonction gamag
!      implicit logical (a-z)
      use module_tableaux,only:g
      use module_precision
      implicit none
      real(wp) h1,u1,h,xl
      !common/grav/g

      dlgamag = xl*(u1 + 2.0*sqrt(g*h1) - 3.0*sqrt(g*h))
      end function dlgamag

!******************************************************************************
      real(wp) function gamad(h2,u2,h,xl)
!******************************************************************************
!     debit pour une 2-courbe passant par h2,u2 pour la surface s
!      implicit logical (a-z)
      use module_tableaux,only:g
      use module_precision
      implicit none
      real(wp) h2,u2,h,xl
      !common/grav/g

      gamad = (u2 + 2.0 * (sqrt(g*h) - sqrt(g*h2))) * h
      end function gamad

!******************************************************************************
      real(wp) function gamag(h1,u1,h,xl)
!******************************************************************************
!     debit pour une 1-courbe passant par h1,u1 pour la surface s
!      implicit logical (a-z)
      use module_tableaux,only:g
      use module_precision
      implicit none
      real(wp) h1,u1,h,xl
      !common/grav/g

      gamag = (u1 + 2.0 * (sqrt(g*h1) - sqrt(g*h))) * h
      end function gamag

!******************************************************************************
      real(wp) function lgamag(h1,u1,h,xl)
!******************************************************************************
!     debit pour une 1-courbe passant par h1,u1 pour la surface s
!      implicit logical (a-z)
      use module_tableaux,only:g
      use module_precision
      implicit none
      real(wp) h1,u1,h,xl
      !common/grav/g

      lgamag =xl*(u1 + 2.0 * (sqrt(g*h1) - sqrt(g*h))) * h
      end  function lgamag

!***************************************************************************
      real(wp) function sgn(x)
!***************************************************************************
!          fonction signe
      use module_precision
      implicit none

      real(wp) x

!      if (x.lt.0.)  sgn=-1.
      if (x.eq.0.) then
        sgn=0.
      elseif (x.lt.0.) then
        sgn=-1.
      else
        sgn=1.
    endif
!      if (x .gt. 0.)  sgn=1.
      return
      end function sgn

! *******************************************************************************
!       subroutine rmn2osher(hg,qug,qvg,hd,qud,qvd,fhc,fuc,fvc,hc
! !liqconc
! !liq     :,hcg,hcd,fhcc,ia
! !liqconcf
!      :)
! !*******************************************************************************
! !     resolution probleme de riemann 1d : ut + f(u)x = 0, u = ug(x<0), ud(x>0)
! !     pour les equations de st venant avec u = (h, hu, hv).
! !
! !     entrees:
! !     g       intensite de la pesanteur
! !     hg,hd   hauteur a gauche, a droite
! !     qug,qud debit selon x (= hu) a gauche, a droite
! !     qvg,qvd debit selon y (= hv) a gauche, a droite
! !     eps1     eps1ilon machine (common param)
! !
! !     sorties:
! !     hc,quc,qvc    hauteur, debit qu = hu, debit qv = hv, au centre (x = 0)
! !
! !     notations : les suffixes g,d,e signifient ici gauche, droite, etoile.
! !     biblio : jp vila, siam j.numer.anal. vol.23, no.6, dec.86,
! !              pp 1173-1192 "simplified godunov schemes ..."
! !     algo : on parcourt les differents cas possibles en verifiant pour
! !     chacun si ses premisses sont respectees : si oui, c'est le bon.
! !******************************************************************************
!
!       logical l0,l1,l2,l3,l4,l5,l6,l7
!       real(wp) g,hg,qug,qvg,hd,qud,qvd,eps1,eps2
!       real(wp) ug,ud,vg,vd,c,cg,cd,uc,vc
! !      real(wp) hc,quc,qvc
!       real(wp) he,ue1,ue2,ce,uu,cc,paray,fhc,fvc,fuc,hc
!       external c
!
!       common/param/eps1,eps2,paray
!       common/grav/g
!
! !     calcul des vitesses ug, ud, vg, vd a partir des debits et des hauteurs
! !     calcul des celerites a gauche et a droite
!
!       if (hg .gt. paray) then
!           ug = qug / hg
!           vg = qvg / hg
!           cg = c(hg)
!       else
!           hg=0.
!           ug = 0.
!           vg = 0.
!           cg = 0.
!       endif
!       if (hd .gt. paray) then
!           ud = qud / hd
!           vd = qvd / hd
!           cd = c(hd)
!       else
!           hd=0.
!           ud = 0.
!           vd = 0.
!           cd = 0.
!       endif
!
! !     debut du calcul proprement dit
!       ce = (ug - ud + 2.0*(cg+cd)) / 4.0
!       if (ce.lt.eps2) then
!           ce = 0.0
!           he = 0.0
!           ue1 = ug
!           ue2 = ud
!       else
!           he = ce * ce / g
!           ue1 = (ug + ud)/2.0 + cg - cd
!           ue2 = ue1
!       endif
!       l0 = ce .gt. cg
!       l1 = ce .gt. cd
!
! !     cas uc = ug
!       uu = ug - cg
!       l2 = uu.lt.0.0
!       l3 = (ue1 - ce + uu).lt.0.0
!       if (((.not.l0).and.(.not.l2)).or.(l0.and.(.not.l3))) then
!           hc = hg
!           uc = ug
!           vc = vg
!           goto 10
!       endif
!
! !     cas uc = ud
!       uu = ud + cd
!       l4 = uu .gt. 0.0
!       l5 = (ue2 + ce + uu) .gt. 0.0
!       if (((.not.l4).and.(.not.l1)).or.(l1.and.(.not.l5))) then
!           hc = hd
!           uc = ud
!           vc = vd
!           goto 10
!       endif
!
! !     cas uc = omega g inter lambda g
!       uu = ue1 - ce
!       l6 = uu.lt.0.0
!       if ((.not.l6).and.l2) then
!           cc = (ug + 2.0 * cg)/3.0
!           hc = cc * cc / g
!           uc = cc
!           vc = vg
! !          write(*,*)'ug=',ug,'hg=',hg,'hd=',hd,'ud=',ud,'uc=',uc
!           goto 10
!       endif
!
! !     cas uc = omega d inter lambda d
!       uu = ue2 + ce
!       l7 = uu .gt. 0.0
!       if ((.not.l7).and.l4) then
!           cc = (2.0 * cd - ud)/3.0
!           hc = cc * cc / g
!           uc = - cc
!           vc = vd
! !          write(*,*)'ud=',ud,'hg=',hg,'hd=',hd,'ug=',ug,'uc=',uc
!           goto 10
!       endif
!
! !     cas uc = ue (cas restant)
!       hc = he
!       uc = ue1
!       if (uc.lt.0.0) then
!           vc = vd
!       else
!           vc = vg
!       endif
! !      goto 10
!
! !     suite : calcul de qu,qv
!   10  if (hc .gt. paray) then
!               fhc = hc * uc
!               fuc=fhc*fhc/hc+0.5*g*hc*hc
!               fvc = hc * vc * uc
!       else
!           hc=0.
!         fhc=0.
!         fuc=0.
!         fvc=0.
!       endif
!       end subroutine rmn2osher

!*******************************************************************************
      subroutine rmn2(hg,qug,qvg,hd,qud,qvd,fhc,fuc,fvc,hc&
#if TRANSPORT_SOLIDE
      &,hcg,hcd,fhcc,ia&
#endif
     &)
!*******************************************************************************
!     resolution probleme de riemann 1d : ut + f(u)x = 0, u = ug(x<0), ud(x>0)
!     pour les equations de st venant avec u = (h, hu, hv).
!
!     entrees:
!     g       intensite de la pesanteur
!     hg,hd   hauteur a gauche, a droite
!     qug,qud debit selon x (= hu) a gauche, a droite
!     qvg,qvd debit selon y (= hv) a gauche, a droite
!     eps1     eps1ilon machine (common param)
!
!     sorties:
!     fhc,fuc,fvc flux des hauteur, debit qu = hu, debit qv = hv
!                    au centre (x = 0)
!
!     notations : les suffixes g,d,e signifient ici gauche, droite, etoile.
!     resolution par solveur de roe avec correction de leveque n02
!******************************************************************************

      use module_tableaux, only:eps1,eps2,paray,g
      use module_precision

#if TRANSPORT_SOLIDE
      use module_tableaux,only:xna,yna,xta,yta
      use module_ts,only : derive,cvsurf,alfv,betav
#endif

      implicit none

      real(wp) hg,qug,qvg,hd,qud,qvd
      real(wp) ug,ud,vg,vd,c,cg,cd,uc,vc,cc
      real(wp) fhc,fuc,fvc,hc
      real(wp) w1,w2,w3,alpha,hg2,hd2,dl,lb,l1,l2,l3,&
     &                 du1,du2,du3,alp1,uag,hag,cag

#if TRANSPORT_SOLIDE
      real(Wp) hcd,hcg,fhcc,cod,cog,alfven,v,venv,vx,vy
      integer ia
#endif
      external c


!     calcul des vitesses ug, ud, vg, vd a partir des debits et des hauteurs
!     calcul des celerites a gauche et a droite

      if (hg .gt. paray) then
          ug = qug / hg
          vg = qvg / hg
          cg = c(hg)
#if TRANSPORT_SOLIDE
          cog=hcg
#endif
      else
          hg=0.
          ug = 0.
          vg = 0.
          cg = 0.
#if TRANSPORT_SOLIDE
          cog=0.
#endif
      endif
      if (hd .gt. paray) then
          ud = qud / hd
          vd = qvd / hd
          cd = c(hd)
#if TRANSPORT_SOLIDE
          cod=hcd
#endif
      else
          hd=0.
          ud = 0.
          vd = 0.
          cd = 0.
#if TRANSPORT_SOLIDE
          cod=0.
#endif
      endif
! hc n'estpasdonne:onposesimplement
!      hc=0.5*(hg+hd)
!      hc=max(hg,hd)
! calcul des differentes variables intermediaires
      hg2=hg**.5
      hd2=hd**.5
      w1=0.5*(hg2+hd2)
!       if (w1 .gt. eps2) then
      hc=w1*w1
      if(hc.gt.paray)then
        w2=0.5*(ug*hg2+ud*hd2)
        w3=0.5*(vg*hg2+vd*hd2)
        uc=w2/w1
        vc=w3/w1
        alpha=0.25*g*(hd+hg)*(hd2+hg2)
!        if (abs(hd-hg) .gt. eps2) then
!          alpha=0.25*g*(hd**2-hg**2)/(hd2-hg2)
!        else
!          alpha=0.5*g*hd*hg2
!        endif
        cc=(alpha/w1)**.5
!        hc=cc*cc/g
      else
! si w1=0 on met tout a zero et on repart
        hc=0.
        fhc=0.
        fuc=0.
        fvc=0.
#if TRANSPORT_SOLIDE
        fhcc=0.
#endif
        return
      endif
!  suite des variables intermediaires :valaeurs propres et deltas
      l1=0.5*(abs(uc-cc)+uc-cc)
      l2=0.5*(abs(uc)+uc)
      l3=0.5*(abs(uc+cc)+uc+cc)
      du1=hd-hg
      du2=qud-qug
      du3=qvd-qvg
      alp1=0.5*(-du2+du1*(uc+cc))/cc
!      alp2=du3-du1*vc
!      alp3=0.5*(du2-du1*(uc-cc))/cc
      hag=hg+alp1
      if (hag .gt. paray) then
        cag=c(hag)
        uag=(qug+alp1*(uc-cc))/hag
!        vag=(qvg+alp1*vc)/hag
!        vad=vag+alp2/hag
      else
        cag=0.
        uag=0.
!        vag=0.
!        vad=0.
      endif

!     debut du calcul proprement dit
!      on effectue la correction de leveque si necessaire
      if (ug-cg.lt.-eps2.and.uag-cag .gt. eps2) then
!          write(*,*)'l1',hg,hag,qug,uag*hag
!          delta=uag-cag-ug+cg
!          if (abs(uc-cc).lt.delta) then
!            l1=(ud-cd)*(uc-cc-ug+cg)/(ud-cd-ug+cg)
             l1=(uag-cag)*(uc-cc-ug+cg)/(uag-cag-ug+cg)
!             l1=0.5*(delta+uc-cc)
!          endif
      endif
!      if (ug.lt.0..and.ud .gt. 0.) then
!          l2=(ud)*(uc-ug)/(ud-ug)
!          delta=ud-ug
!          write(*,*)'l2',hg,hd,qug,qud
!          if (abs(uc).lt.delta) then
!             l2=0.5*(delta+uc)
!          endif
!      endif
      if (uag+cag.lt.-eps2.and.ud+cd .gt. eps2) then
!          l3=(ud+cd)*(uc+cc-ug-cg)/(ud+cd-ug-cg)
          l3=(ud+cd)*(uc+cc-uag-cag)/(ud+cd-uag-cag)
!          write(*,*)'l3',hag,hd,uag*hag,qud
!          delta=ud+cd-uag-cag
!          if (abs(uc+cc).lt.delta) then
!             l3=0.5*(delta+uc+cc)
!          endif
      endif
! calcul des flux: flux a droite -variation de roe
      dl=0.5*(l3-l1)
      lb=0.5*(l3+l1)
      fhc=qud-(lb-uc*dl/cc)*du1-dl*du2/cc
      fuc=qud*ud+0.5*g*hd**2&
     &   -dl*du1*(cc**2-uc**2)/cc-(uc*dl/cc+lb)*du2
      fvc=qud*vd-vc*(lb-l2-uc*dl/cc)*du1-vc*dl*du2/cc-l2*du3

#if TRANSPORT_SOLIDE
!       if(hc.gt.paray)then
      if(.not.derive)then
        if(fhc.gt.0.)then
          fhcc=fhc*cog
        else
          fhcc=fhc*cod
        endif
! si derive vitesse de surface
      else
        v=fhc/hc
! vx,vy vitesse du vent
        call venta(vx,vy,ia)
        venv=vx*xna(ia)+vy*yna(ia)
        call calalf(alfven,ia,vx,vy)
        v=alfven*v+betav*venv
        if(v.gt.0.)then
          fhcc=hc*cog*v
        else
          fhcc=hc*cod*v
        endif
      endif
! cas ou hc=0
!       else
!         fhcc=0.
!       endif

#endif

      return
      end subroutine rmn2

#if TRANSPORT_SOLIDE
!******************************************************************************
       subroutine calalf(alfven,ia,vx,vy)
! calcul la valeur du rapport vitesse de surface sur vitesse moyenne
!******************************************************************************
!      implicit none
!
      use module_precision
      use module_tableaux,only:ne,na,nn,fra,fre,eps1,eps2,paray,ieva&
      &,he,que,qve

      use module_ts,only:derive,cvsurf,alfv,betav

      implicit none
       real(wp) alfven,vx,vy,rap,u,v,vxc,vyc
       integer ia,ie


       if (.not.cvsurf) then
         alfven=alfv
       else
           ie=ieva(ia,1)
           if (ie.eq.0)ie=ieva(ia,2)
           if (he(ie) .gt. paray) then
           u=que(ie)/he(ie)
           v=qve(ie)/he(ie)
         if ((u**2+v**2) .gt. eps2) then
           vxc=vx-alfv*u
           vyc=vy-alfv*v
 ! rap est le rapport des vitesses de frottement fond et surface
           rap=1.3/1000./(u**2+v**2)&
      &*(vxc**2+vyc**2)*he(ie)**0.333*fra(ie)
           if ((vxc*u+vyc*v).lt.0.) then
 ! cas ou vent oppose a courant
             rap=-rap
           endif
 ! limitation de rap pour eviter denominateur nul
 !          if (rap .gt. 2.)rap=2.
 !          alfven=alfv*(1.-rap)/(1.-rap+alfv)
           if (rap .gt. 4.) then
             rap=4.
           elseif (rap.lt.-4.) then
              rap=-4.
           endif
 ! formule inventee  a partir de shen 1988
           alfven=alfv*(1+0.25*rap)
         else
           alfven=alfv
         endif
         else
           alfven=alfv
         endif
       endif
       return
       end subroutine calalf
#endif

!*******************************************************************************
      subroutine rmn3(hg,qug,fhc,fuc,fvc,hc&
#if TRANSPORT_SOLIDE
      &,fhcc&
#endif
      &)
!*******************************************************************************
!     resolution probleme de riemann 1d : ut + f(u)x = 0, u = ug(x<0), ud(x>0)
!     pour les equations de st venant avec u = (h, hu, hv).
!
!     entrees:
!     g       intensite de la pesanteur
!     hg      hauteur a gauche
!     qug     debit selon x (= hu) a gauche
!     qvg     debit selon y (= hv) a gauche
!     eps1     eps1ilon machine (common param)
!
!     sorties:
!     hc,quc=fhc,fuc,fvc    hauteur,debit qu=fh et flux de qu et qv au centre
!
!     notations : les suffixes g,c signifient ici gauche, centre.
!     algo : simplification de rmn2 pour detente et reflexion sur une paroi
!******************************************************************************

      use module_tableaux, only:eps1,eps2,paray,g
      use module_precision

      implicit none
!      implicit logical (a-z)

      real(wp) hg,ug,qug
      real(wp) c,cg
      real(wp) hc
      real(wp) cc,fhc,fvc,fuc
      external c
      !common/param/eps1,eps2,paray
      !common/grav/g

#if TRANSPORT_SOLIDE
      real(WP) fhcc
      fhcc = 0.
#endif

!     calcul des celerites a gauche et a droite

      if (hg .gt. paray) then
          cg = c(hg)
          ug=qug/hg
      else
          cg = 0.
          ug=0.
      endif
!     debut du calcul proprement dit
      cc = 0.5 * ug + cg
      if (cc.lt.eps2) then
          hc = 0.0
      else
          hc = cc * cc / g
      endif

!     suite : calcul de qu,qv
      if (hc .gt. paray) then
              fhc = 0.
              fuc = 0.5*g*hc*hc
              fvc = 0.
      else
          hc=0.
      endif
      end subroutine rmn3

!******************************************************************************
      real(wp) function volume(t)
!******************************************************************************

      use module_tableaux,only:het,quet,qvet,se,la,ne,na,nn
      use module_precision

      implicit none
!
      integer ie
      real(wp) t
      !common/nomb/ne,na,nn
      !common/tlig/het,quet,qvet
      !common/dose/se,la

! calcul du volume d'eau dans le modele
!**************************************
      volume=0.
      do 1 ie=1,ne
        volume=volume+het(ie)*se(ie)
 1    continue
      return
      end function volume

!**********************************************************************
              subroutine dhchoc(h,h2,qu2,qu)
!---------------------------------------------------------------------c
!                                                                     c
!       resolution par newton de l'equation en h :                    c
!          g*h*h2*(h-h2)*(h**2-h2**2)-2.*(h2*qu-h*qu2)**2=0           c
!       entrees : qu                        sortie : h                c
!                 h2,qu2                                              c
!=====================================================================c
      use module_tableaux, only:eps1,eps2,paray,g,prec,nitmax,idl
      use module_precision
      implicit none

      integer niter
      !,idl
      real(wp) qu2,h,qu,h2
      real(wp) delta,hp,ddelt,hm

      !common/param/eps1,eps2,paray
      !common/newton/prec,nitmax
      !common/grav/g

!      data idl/0/
!      save idl

!     valeur initiale
      h = h2
      if (h.le.paray) then
          h = 0.0
          return
      endif
      if (qu.lt.prec) then
          h = 0.0
          return
      endif
      delta = g*(h-h2)*(h**2-h2**2)*h*h2-2.*(h2*qu-h*qu2)**2
      if (abs(delta).lt.prec) return
      niter = 1

!     boucle d'iteration newton
 10   niter = niter + 1
      ddelt=g*(h-h2)*h2*(4.*h**2+h*h2-h2**2)+4.*qu2*(h2*qu-h*qu2)
      if (abs(ddelt).lt.eps1) then
!        write(*,*)'dhchoc : on passe en dichotomie'
!        write(*,*)'h2=',h2,'h=',h,'delta=',delta
!        h=2.*h
        niter=0
        go to 20
      endif
      hp = h - delta/ ddelt
      if (hp.le.paray) then
              h = 0.0
              return
      endif
      delta = g*(hp-h2)*(hp**2-h2**2)*hp*h2-2.*(h2*qu-hp*qu2)**2
      if (abs(delta).lt.prec.or.abs(h-hp).lt.paray) then
        h = hp
        return
      endif
      if (niter .gt. nitmax) then
        if (idl.lt.1000) then
         idl=idl+1
!              write(*,*) 'dhchoc : niter > nitmax'
!              write(*,*)'delta=',delta,hp,h2,qu,qu2
        endif
        h=hp
        niter=0
! modif du 24/09/2020 on va en dichotomlie plutot que de sortir directement
! normalement quasi �quivalent
        goto 20
!         return
      endif
      h = hp
      goto 10
! calcul par dichotomie
! on commence par regarder la position respective de q et s2*v2
! on arrive avec delta calcule pour h
 20   if (delta.gt.0.) then
        hm=h2
        hp=h
      else
        niter=niter+1
        if(niter.gt.nitmax) then
          if (idl.lt.1000) then
            idl=idl+1
            write(*,*)'DHCHOC: PAS DE SOLUTION TROUVEE : h=h2'
            write(*,*)'h2=',h2,'qu2=',qu2,'qu=',qu
          endif
          h=h2
          return
        endif
        if(niter.gt.nitmax/2)then
          h=h2*2**(niter-nitmax/2)
        else
          h=h2/2**niter
        endif
        delta = g*(h-h2)*(h**2-h2**2)*h*h2-2.*(h2*qu-h*qu2)**2
        if(abs(delta).lt.prec) return
        go to 20
! fin du if sur delta
      endif
! apres avoir trouve l'intervalle, on commence la dichotomie
      niter=1
 30   h = .5*(hm+hp)
      if (abs(hp-hm).lt.eps2) return
      delta = g*(h-h2)*(h**2-h2**2)*h*h2-2.*(h2*qu-h*qu2)**2
      if(abs(delta).lt.prec) return
      if (delta.lt.0.) then
        hm = h
      else
        hp = h
      endif
      niter = niter+1
      if (niter .gt. nitmax) then
        if (idl.lt.1000) then
          idl=idl+1
          write(*,*)'sp dhchoc non convergent en',nitmax,'iterations'
          write(*,*)'hm=',hm,'hp=',hp,'h2=',h2,'delta=',delta
        endif
        return
      endif
!  100  continue
      return
      end subroutine dhchoc

!**********************************************************************
      subroutine duchoc(h,h2,qu2,qua,qub)
!---------------------------------------------------------------------c
!                                                                     c
!       calcul de qu a partir de l'egalite
!          g*h*h2*(h-h2)*(h**2-h2**2)-2.*(h2*qu-h*qu2)**2=0           c
!       entrees : h                        sortie : qua,qub           c
!                 h2,qu2                                              c
!       qua et qub sont les 2 valeurs possibles
!=====================================================================c

      use module_tableaux, only:g
      use module_precision
      implicit none

      real(wp) qu2,h,qua,qub,h2
      real(wp) delta

!      common/param/eps1,eps2,paray
      !common/grav/g


      delta=sqrt(0.5*g*h*h2*(h-h2)*(h**2-h2**2))
      qua=(h*qu2+delta)/h2
      qub=(h*qu2-delta)/h2
      end subroutine duchoc

!**********************************************************************
      subroutine louvr
!---------------------------------------------------------------------c
!      lecture des caracteristiques des ouvrages dans le fichier .ouv
!=====================================================================c

      use module_precision, only: wp
      use module_tableaux, only : nrefa,ieva,se,la,houv,qouv,etude,&
     &ie1,ie2,ia1,ia2,j1,j2,nouv,nbouv,nobmax,noumax,noemax,ntrmax,&
     &long,zdev,haut,coef,nbcou1,nbcou2,qcoup,zcoup,typouv,zfm,&
     &tm,tr,tinit,tmax,zc,zp,alp,alc,z0,d50,rho,phi,db0,zb0,&
     &ym,sm,pm,rhm,alm,nu,ql,qs,dbr,z,zav,zbr,eta,ka,c1,c2,dt2,nt,&
     &zb,db,it,largmail,trect,ioub,nob,elap,suivan,tini,ouv,&
     &ext1,ext2,ieamont,ieaval,controlet,qcoup2,zcoup2,nbcou12,nbcou22,&
     &typouv2,long2,zdev2,haut2,coef2,tcoup2,nvaltz1,nvaltz2,difqouvp
      use module_mpi,only:me,scribe

      implicit none

      real(wp) xia1,xia2,xie1,xie2,yia1,yia2,yie1,yie2&
     &                ,xiei,yiei,zori
      integer iouv,idon,nref,nbmail,i,j,k
      character rien*9
      logical trouve
      integer nbcoup,nbcoum
      integer nbcoum2,kouv
!ech       integer nocan(noumax,noemax),nocan2(noumax)


      !common/etud/etude
      !common/iouvra/ie1,ie2,ia1,ia2,j1,j2,nouv,nbouv
      !common/couvra/long,zdev,haut,coef
      !common/nouvra/nbcou1,nbcou2
      !common/zouvra/qcoup,zcoup
      !common/touvra/typouv
      !common/zbas/zfm
      !common/narf/nrefa
      !common/iela/ieva
      !common/dose/se,la
      !common/tenv/tm,tr,tinit,tmax
      !common/hqouv/houv,qouv

      !common/digue/zc,zp,alp,alc,z0,d50,rho,phi,db0,zb0
      !common/moyen/ym,sm,pm,rhm,alm,nu
      !common/resul/ql,qs,dbr,z,zav,zbr
      !common/conrup/eta,ka,c1,c2,dt2
      !common/nconst/nt
      !common/breche/zb,db,it
      !common/maxbre/largmail,trect
      !common/iobrec/ioub,nob
      !common/elappr/elap,suivan
      !common/tinib/tini
! variables relatives aux ouvrages C
      !common/ext12/ext1,ext2
      !common/ieamontaval/ieamont,ieaval
      !common/controlouvc/controlet
      !common/zouvrac/qcoup2,zcoup2
      !common/nouvrac/nbcou12,nbcou22
      !common/touvrac/typouv2
      !common/couvrac/long2,zdev2,haut2,coef2
      !common/tcontrole/tcoup2
      !common/nvaltz12/nvaltz1,nvaltz2
!echc donnees de canoe, ajoute par s.jankowfsky
!ech     common/canoe/nocan,nocan2


      nbcoum=0
      nbcoum2=0
      kouv=0
! pour les ouvrages b
      nob=0

      trouve=.false.
      idon=9


      open(idon,file=trim(etude)//'.ouv',status='old',err=1)

      do 3 iouv=1,noumax
! nref :reference des aretes
! nref=-1:le debit de l'ouvrage se rajoute au debit saint-venant
! nref=-2: le seul debit est le debit de l'ouvrage
! nbmail : nombre de mailles interne a l'ouvrage
! si nbmail=0 on a normalement ia1=ia2
        read(idon,*,err=5,end=8)xia1,yia1,xie1,yie1,nref,nbmail
        if (me==scribe)then
          write(*,*)'lecture de l''ouvrage',iouv
        end if ! me==scribe
        call trxyia(xia1,yia1,ia1(iouv),trouve)
        if (trouve) then
          trouve=.false.
          if (nref.eq.-1.and.nrefa(ia1(iouv)) .gt. 0) then
            if (me==scribe)then
              write(*,*)'ouvrage ',iouv
              write(*,*)'reference -1 impossible sur une frontiere'
            end if ! me==scribe
            stop
          elseif (nrefa(ia1(iouv)).eq.-1) then
            if (me==scribe)then
              write(*,*)'ouvrage ',iouv
              write(*,*)'2 ouvrages sur une meme arete 1 impossible'
            end if ! me==scribe
            stop
          endif
          nrefa(ia1(iouv))=nref
        else
          if(me==scribe)write(*,*)'les coordonnees de l''arete 1',&
                                 &' sont fausses'
          stop
        endif
        call trxyix(xie1,yie1,ie1(iouv),trouve)
        if (trouve) then
          trouve=.false.
        else
          if(me==scribe)write(*,*)'les coordonnees de la maille 1',&
                                 &' sont fausses'
          stop
        endif
        if (nbmail.ne.0) then
          do 2 i=1,nbmail
            read(idon,*,err=5)xiei,yiei
            call trxyie(xiei,yiei,nref,trouve)
            if (trouve) then
              trouve=.false.
!              k=k+1
            else
              if(me==scribe)write(*,*)'les coordonnees d''une maille interne d''un',&
                                     &' ouvrage sont fausses'
              stop
            endif
    2     continue
        endif
! nouv nombre d'ouvrages elementaires
        read (idon,*,err=5)xia2,yia2,xie2,yie2,nouv(iouv)
        if (nouv(iouv) .gt. noemax) then
          if(me==scribe)write(*,*)'trop d''ouvrages elementaires : nombre limite a',noemax
        endif
        call trxyia(xia2,yia2,ia2(iouv),trouve)
        if (trouve) then
          trouve=.false.
          if (nref.eq.-1.and.nrefa(ia2(iouv)) .gt. 0) then
            if(me==scribe)write(*,*)'ouvrage ',iouv
            if(me==scribe)write(*,*)'reference -1 impossible sur une frontiere'
            stop
          elseif (nrefa(ia2(iouv)).eq.-1.and.ia1(iouv).ne.ia2(iouv)) then
            if(me==scribe)write(*,*)'ouvrage ',iouv
            if(me==scribe)write(*,*)'2 ouvrages sur une meme arete 2 impossible'
            stop
          endif
          nrefa(ia2(iouv))=nref
        else
          if(me==scribe)write(*,*)'les coordonnees de l''arete 2 ne correspondent pas a une arete'
          if(me==scribe)write(*,*)'on prend une arete nulle soit une sortie du modele'
          ia2(iouv)=0
        endif
        call trxyix(xie2,yie2,ie2(iouv),trouve)
        if (trouve) then
            trouve=.false.
            if (ia2(iouv).eq.0) then
              if(me==scribe)write(*,*)'attention incoherence : l''arete 2 est nulle et pas la maille 2'
              stop
            endif
        elseif (ia2(iouv).eq.0) then
            if(me==scribe)write(*,*)'les coordonnees de la maille 2 ne correspondent pas une maille'
            if(me==scribe)write(*,*)'on prend une maille nulle soit une sortie du modele'
            ie2(iouv)=0
        else
            if(me==scribe)write(*,*)'les coordonnees de la maille 2 ne correspondent pas une maille'
            stop
        endif
        if (nbmail.eq.0) then
!          if (ia2(iouv).ne.ia1(iouv).and.ia2(iouv).ne.0) then
          if(ia2(iouv).ne.0)then
          if(ia2(iouv).ne.ia1(iouv))then
          if(nrefa(ia1(iouv)).eq.-2)then
            if(me==scribe)write(*,*)'attention incoherence : un ouvrage'
            if(me==scribe)write(*,*)'de reference -2'
            if(me==scribe)write(*,*)'sans maille interne a 2 aretes differentes'
            stop
!           else
!             nrefa(ia2(iouv))=-1
! fin du if sur nrefa
           endif
          elseif (ie2(iouv).eq.ie1(iouv)) then
            if(me==scribe)write(*,*)'les mailles de part et d''autre de l''arete sont identiques'
            stop
! fin du if sur ia2=ia1
          endif
! fin du if sur ia2=0
          endif
! fin du if sur nbmail
        endif
        if (ie1(iouv).eq.ieva(ia1(iouv),1)) then
          j1(iouv)=1
        elseif (ie1(iouv).eq.ieva(ia1(iouv),2)) then
          j1(iouv)=2
        else
!          write(*,*)ie1(iouv),ieva(ia1(iouv),1),ieva(ia1(iouv),2)
          if(me==scribe)write(*,*)'incoherence entre arete 1 et maille 1'
        endif
        if (ie2(iouv).ne.0) then
         if (ie2(iouv).eq.ieva(ia2(iouv),1)) then
          j2(iouv)=1
         elseif (ie2(iouv).eq.ieva(ia2(iouv),2)) then
          j2(iouv)=2
         else
!          write(*,*)ie2(iouv),ieva(ia2(iouv),1),ieva(ia2(iouv),2)
         if(me==scribe)write(*,*)'incoherence entre arete 2 et maille 2'
         endif
        endif
! initialisation des qouv
    qouv(ia1(iouv),j1(iouv))=0.
    difqouvp(ia1(iouv),j1(iouv))=0.
!     qouv(ia2(iouv),j2(iouv))=0.
    !qouv=0.
! lecture des caracteristiques des ourages
        do 4 j=1,nouv(iouv)
! ! format fluvia
!           read(idon,'(a1,a9,4f10.2)',err=5)&
!      &typouv(iouv,j),rien,long(iouv,j),zdev(iouv,j),zori&
!      &,coef(iouv,j)
!           if (typouv(iouv,j).eq.'d'.or.typouv(iouv,j).eq.'d') then
!             typouv(iouv,j)='d'
!             haut(iouv,j)=zori-zdev(iouv,j)
! !            zdev(iouv,j)=zdev(iouv,j)-zfm
! ! h correspond au meme ouvrage que d mais avec les cotes remplacees par les charges
!           elseif (typouv(iouv,j).eq.'h'.or.typouv(iouv,j).eq.'h') then
!             typouv(iouv,j)='h'
!             haut(iouv,j)=zori-zdev(iouv,j)
! !            zdev(iouv,j)=zdev(iouv,j)-zfm
! ! i correspond au meme ouvrage que d mais avec la formule de n. riviere pour le canal experimental
!           elseif (typouv(iouv,j).eq.'i'.or.typouv(iouv,j).eq.'i') then
!             typouv(iouv,j)='i'
!             haut(iouv,j)=zori-zdev(iouv,j)
! ! o correspond au meme ouvrage que d mais avec une longuuer et en circulaire
!           elseif (typouv(iouv,j).eq.'o'.or.typouv(iouv,j).eq.'o') then
!             typouv(iouv,j)='o'
! ! haut contient le diametre
!             haut(iouv,j)=zori
! !            zdev(iouv,j)=zdev(iouv,j)-zfm
! ! ouvrage q(zamont)
!            elseif (typouv(iouv,j).eq.'z'.or.typouv(iouv,j).eq.'z') then
!             typouv(iouv,j)='z'
!             read(rien,'(i9)')nbcoup
!             if (nbcoup.lt.1.or.nbcoup .gt. noumax*noemax)&
!      &then
!               if(me==scribe)write(*,*)'erreur dans le nombre de couples'
!               if(me==scribe)write(*,*)'le nombre de couples est: ',nbcoup
!               if(me==scribe)write(*,*)'alors que le nombre maximal permis est :',&
!      &noumax*noemax
!               stop
!             endif
!             nbcou1(iouv,j)=nbcoum+1
!             nbcoum=nbcoum+nbcoup
!             if (nbcoum .gt. noumax*noemax*noemax) then
!               if(me==scribe)write(*,*)'trop de couples (z,q) :'
!               if(me==scribe)write(*,*)'plus de ',noumax*noemax*noemax,' couples'
!               stop
!             endif
!             nbcou2(iouv,j)=nbcoum
!             do 9 k=nbcou1(iouv,j),nbcou2(iouv,j)
!               read(idon,*,err=5)zcoup(k),qcoup(k)
!               zcoup(k)=zcoup(k)
! !              zcoup(k)=zcoup(k)-zfm
!               if (k.ne.nbcou1(iouv,j)) then
!                 if (zcoup(k).lt.zcoup(k-1)) then
!                   if(me==scribe)write(*,*)'cotes non croissantes dans la definition'
!                   if(me==scribe)write(*,*)'du couple (z,q) numero ',k,' d''un ouvrage de type z'
! ne peuvent etre vrais que si typouv="c"
          ext1(iouv,j)=.false.
          ext2(iouv,j)=.false.
          call lecparouv(iouv,j,idon,nbcoum,nbcoum2,kouv)
    4   continue
    3 continue
      write(*,*)'attention'
      write(*,*)'le nombre maximal d''ouvrages ',noumax,' a ete lu'
    8 nbouv=iouv-1
      go to 6
    1 write(*,*)'le fichier ',trim(etude),'.ouv n''existe pas'
      go to 6
    5 write (*,*)'erreur dans la lecture du fichier ',trim(etude),'.ouv'
      stop
    6 close(idon)
      return
      end subroutine louvr

!**********************************************************************
      subroutine lecparouv(iouv,j,idon,nbcoum,nbcoum2,kouv)
!---------------------------------------------------------------------c
!      lecture des caracteristiques d'un ouvrage elementaire
!        dans le fichier .ouv
! subroutine recopiee dans lecparouv2
!=====================================================================c
      use module_tableaux,only:etude,ie1,ie2,ia1,ia2,j1,j2,nouv,nbouv,&
     &long,zdev,haut,coef,nbcou1,nbcou2,qcoup,zcoup,zfm,typouv,nrefa,&
     &ieva,se,la,tm,tr,tinit,tmax,houv,qouv,zc,zp,alp,alc,z0,d50,rho,phi,db0,zb0,&
     &ym,sm,pm,rhm,alm,nu,ql,qs,dbr,z,zav,zbr,eta,ka,c1,c2,dt2,nt,&
     &zb,db,it,largmail,trect,ioub,nob,elap,suivan,tini,ext1,ext2,&
     &ieamont,ieaval,controlet,qcoup2,zcoup2,nbcou12,nbcou22,typouv2,&
     &long2,zdev2,haut2,coef2,tcoup2,nvaltz1,nvaltz2,nobmax,noemax,noumax,&
     &ouv
      use module_mpi,only:me,scribe

      use module_precision, only:wp
#if TRANSPORT_SOLIDE
      use module_ts, only:ccoup,sedvar,dens,conckg,diamcoup,sigmacoup,ccoup2,diamcoup2,&
     &sigmacoup2
#endif

      implicit none

      double precision zori
      integer iouv,idon,nref,j,k
      character rien*9
      integer nbcoup,nbcoum
      logical trouve

      integer ibouv,i
! variables relatives aux ouvrages c
      integer nbcoum2,kouv,nvaltz,nval

!ech       integer nocan(noumax,noemax),nocan2(noumax)



      !common/etud/etude
      !common/iouvra/ie1,ie2,ia1,ia2,j1,j2,nouv,nbouv
      !common/couvra/long,zdev,haut,coef
      !common/nouvra/nbcou1,nbcou2
      !common/zouvra/qcoup,zcoup
      !common/touvra/typouv
      !common/zbas/zfm
      !common/narf/nrefa
      !common/iela/ieva
      !common/dose/se,la
      !common/tenv/tm,tr,tinit,tmax
      !common/hqouv/houv,qouv

      !common/digue/zc,zp,alp,alc,z0,d50,rho,phi,db0,zb0
      !common/moyen/ym,sm,pm,rhm,alm,nu
      !common/resul/ql,qs,dbr,z,zav,zbr
      !common/conrup/eta,ka,c1,c2,dt2
      !common/nconst/nt
      !common/breche/zb,db,it
      !common/maxbre/largmail,trect
      !common/iobrec/ioub,nob
      !common/elappr/elap,suivan
      !common/tinib/tini
! variables relatives aux ouvrages c
      !common/ext12/ext1,ext2
      !common/ieamontaval/ieamont,ieaval
      !common/controlouvc/controlet
      !common/zouvrac/qcoup2,zcoup2
      !common/nouvrac/nbcou12,nbcou22
      !common/touvrac/typouv2
      !common/couvrac/long2,zdev2,haut2,coef2
      !common/tcontrole/tcoup2
      !common/nvaltz12/nvaltz1,nvaltz2
!echc donnees de canoe, ajoute par s.jankowfsky
!ech       common/canoe/nocan,nocan2




! format fluvia
          read(idon,'(a1,a9,4f10.2)',err=5)&
     &typouv(iouv,j),rien,long(iouv,j),zdev(iouv,j),zori&
     &,coef(iouv,j)
          if(typouv(iouv,j).eq.'d'.or.typouv(iouv,j).eq.'D')then
            typouv(iouv,j)='d'
            haut(iouv,j)=zori-zdev(iouv,j)
!            zdev(iouv,j)=zdev(iouv,j)-zfm
! h correspond au meme ouvrage que d mais avec les cotes remplacees par les charges
          elseif(typouv(iouv,j).eq.'h'.or.typouv(iouv,j).eq.'H')then
            typouv(iouv,j)='h'
            haut(iouv,j)=zori-zdev(iouv,j)
!            zdev(iouv,j)=zdev(iouv,j)-zfm
! i correspond au meme ouvrage que d mais avec la formule de n. riviere pour le canal experimental
          elseif(typouv(iouv,j).eq.'i'.or.typouv(iouv,j).eq.'I')then
            typouv(iouv,j)='i'
            haut(iouv,j)=zori-zdev(iouv,j)
! o correspond au meme ouvrage que d mais avec une longuuer et en circulaire
          elseif(typouv(iouv,j).eq.'o'.or.typouv(iouv,j).eq.'O')then
            typouv(iouv,j)='o'
! haut contient le diametre
            haut(iouv,j)=zori
!            zdev(iouv,j)=zdev(iouv,j)-zfm
! ouvrage q(zamont)
           elseif(typouv(iouv,j).eq.'z'.or.typouv(iouv,j).eq.'Z')then
            typouv(iouv,j)='z'
            read(rien,'(i9)')nbcoup
            if(nbcoup.lt.1.or.nbcoup.gt.noumax*noemax)then
              write(*,*)'erreur dans le nombre de couples'
              write(*,*)'le nombre de couples est: ',nbcoup
              write(*,*)'alors que le nombre maximal permis est :',noumax*noemax
              stop
            endif
            nbcou1(iouv,j)=nbcoum+1
            nbcoum=nbcoum+nbcoup
            if(nbcoum.gt.noumax*noemax*noemax)then
              write(*,*)'trop de couples (z,q) :'
              write(*,*)'plus de ',noumax*noemax*noemax,' couples'
              stop
            endif
            nbcou2(iouv,j)=nbcoum
            do 9 k=nbcou1(iouv,j),nbcou2(iouv,j)
              read(idon,*,err=5)zcoup(k),qcoup(k)
              zcoup(k)=zcoup(k)
!              zcoup(k)=zcoup(k)-zfm
              if(k.ne.nbcou1(iouv,j))then
                if(zcoup(k).lt.zcoup(k-1))then
                  write(*,*)'cotes non croissantes dans la definition'
        write(*,*)'du couple (z,q) numero ',k,' d''un ouvrage de type z'
                endif
              endif
 9          continue
! ouvrage q(t)
! attention zcoup contient les temps
           elseif (typouv(iouv,j).eq.'q'.or.typouv(iouv,j).eq.'Q') then
            typouv(iouv,j)='q'
            read(rien,'(i9)')nbcoup
            if (nbcoup.lt.1.or.nbcoup .gt. noumax*noemax)then
              if(me==scribe) write(*,*)'erreur dans le nombre de couples'
              if(me==scribe) write(*,*)'le nombre de couples est: ',nbcoup
              if(me==scribe)write(*,*)'alors que le nombre maximal permis est :',noumax*noemax
              stop
            endif
            nbcou1(iouv,j)=nbcoum+1
            nbcoum=nbcoum+nbcoup
            if (nbcoum .gt. noumax*noemax*noemax) then
              if(me==scribe)write(*,*)'trop de couples (t,q) :'
              if(me==scribe)write(*,*)'plus de ',noumax*noemax*noemax,' couples'
              stop
            endif
            nbcou2(iouv,j)=nbcoum
            do k=nbcou1(iouv,j),nbcou2(iouv,j)
#if TRANSPORT_SOLIDE
#if SEDVAR
              if(sedvar)then
                read(idon,*,err=5)zcoup(k),qcoup(k),ccoup(k),diamcoup(k),sigmacoup(k)
              else
#endif
#endif

#if TRANSPORT_SOLIDE
                read(idon,*,err=5)zcoup(k),qcoup(k),ccoup(k)
#if SEDVAR
!fin du if sur sedvar
              endif
#endif
              if(conckg)then
                ccoup(k)=ccoup(k)/dens
              endif

#else /* TRANSPORT_SOLIDE */
              read(idon,*,err=5)zcoup(k),qcoup(k)
#endif

! zcoup contient ici les temps
!              zcoup(k)=zcoup(k)
              if (k.ne.nbcou1(iouv,j)) then
                if (zcoup(k).lt.zcoup(k-1)) then
                  if(me==scribe)write(*,*)'temps non croissants dans la definition'
                  if(me==scribe)write(*,*)'du couple (t,q) numero ',k,' d''un ouvrage de type q'
                endif
              endif
! fin du do sur k
           enddo
! ouvrage type rupro breche rectangulaire ou renard
       elseif (typouv(iouv,j).eq.'B'.or.typouv(iouv,j).eq.'b') then
         if (nob.lt.nobmax) then
           ibouv=nob+1
           nob=ibouv
!           iob(ibouv)=iouv
           ioub(iouv,j)=ibouv
           ouv(ibouv)=iouv
         else
           if(me==scribe)write(*,*)'le nombre ouvrages b depasse le maximum'
           if(me==scribe)write(*,*)'de ',nobmax,' possibles'
!           pause
           stop
         endif
       typouv(iouv,j)='b'
       zc(ibouv)=long(iouv,j)
!       zc(ibouv)=zc(ibouv)-zfm
       zp(ibouv)=zdev(iouv,j)
!       zp(ibouv)=zp(ibouv)-zfm
       alc(ibouv)=zori
       alp(ibouv)=coef(iouv,j)
! calcul de la largeur maximale de la breche
       if(ia2(iouv).ne.0)then
         largmail(ibouv)=min(la(ia1(iouv)),la(ia2(iouv)))
       else
         largmail(ibouv)=la(ia1(iouv))
       endif
! si elap vrai alors contrainte reduite sur les berges et approfondissement avec elargissement
! si suivan vrai les breches s'enchainent
! celle de numero suivant a la precedente si tini superieur a tmax
       read(rien(1:1),'(i1)')nbcoup

       if (nbcoup.eq.1.or.nbcoup.eq.2) then
         elap(ibouv)=.true.
       else
         elap(ibouv)=.false.
       endif
       if (nbcoup.eq.2.or.nbcoup.eq.3) then
         suivan(ibouv)=.true.
! pour ne pas chercher celui apres le dernier
         if (ibouv.le.nobmax) then
           suivan(ibouv+1)=.false.
         endif
       else
         suivan(ibouv)=.false.
       endif
!       if (abs(tini(ibouv)).lt.0.05) then
       if (rien(2:9).eq.'        ') then
         tini(ibouv)=tinit
       else
         read(rien(2:9),'(f8.0)')tini(ibouv)
       endif

! ouverture du fichier pour resultats de ouvrage b
      if (me==scribe)then
       call ouver(ibouv)
      end if ! me==scribe
! ouverture reportee a chaque stockage
!      sous-programme de lecture et de controle des donnees
       call lectes(iouv,ibouv)
! ecriture dans le fichier de resultats pour b
      if (me==scribe)then
       call impdon(iouv,ibouv)
      end if ! me==scribe
! inutile sauf peut etre pour ecriture
      z(1,ibouv)=z0(ibouv)
      zav(1,ibouv)=z0(ibouv)
      dbr(1,ibouv)=db(ibouv)
      zbr(1,ibouv)=zb(ibouv)
      qs(1,ibouv)=0.
      ql(1,ibouv)=0.
! un seul type d'ouvrage d'�change avec r�seau
!ech       elseif (typouv(iouv,j).eq.'a'.or.typouv(iouv,j).eq.'a') then
!ech         typouv(iouv,j)='a'
!ech         if (rien.eq.'         ') then
!ech            nocan(iouv,j)=0
!ech         else
!ech           read(rien,'(i9)',err=1005)nocan(iouv,j)
!ech         endif
!ech         call louvechange(iouv,j)
       elseif(typouv(iouv,j).eq.'c'.or.typouv(iouv,j).eq.'C')then
          if(rien(1:1).eq.'t'.or.rien(1:1).eq.'T')then
            controlet(iouv,j)=.true.
          elseif(rien(1:1).eq.'z'.or.rien(1:1).eq.'Z')then
            controlet(iouv,j)=.false.
          else
            controlet(iouv,j)=.true.
            write(*,*)'erreur ouvrage ',iouv,' ouvrage elementaire c'
            write(*,*)'controle par temps mal note'
          endif
          read (rien(2:9),'(i8)')nvaltz
          trouve=.false.
          call trxyix(long(iouv,j),zdev(iouv,j),ieamont(iouv,j),trouve)
          if(trouve)then
            trouve=.false.
            if(ieamont(iouv,j).eq.ie1(iouv))then
                ieamont(iouv,j)=0
            elseif(controlet(iouv,j))then
                ext1(iouv,j)=.true.
            endif
          else
           ieamont(iouv,j)=0
          endif
          if(controlet(iouv,j))then
          call trxyix(zori,coef(iouv,j),ieaval(iouv,j),trouve)
          if(trouve)then
            trouve=.false.
            if(ieaval(iouv,j).eq.ie2(iouv))then
                ieaval(iouv,j)=0
            else
                ext2(iouv,j)=.true.
            endif
          else
           ieaval(iouv,j)=0
          endif
! fin du if sur controlet
          endif
          nvaltz1(iouv,j)=kouv+1
          nvaltz2(iouv,j)=nvaltz1(iouv,j)+nvaltz-1
          if(nvaltz.gt.0)then
            read(idon,'(20f15.3)')(tcoup2(nval),nval=nvaltz1(iouv,j),nvaltz2(iouv,j))
          endif
          do nval=1,nvaltz+1
            kouv=kouv+1
          call lecparouv2(iouv,j,idon,nbcoum,nbcoum2,kouv)
! les carcteristiques ouvrage elementaire
! transferres dans sous ouvrage
          typouv2(kouv)=typouv(iouv,j)
          if(typouv2(kouv).eq.'d'.or.typouv2(kouv).eq.'h'&
     &.or.typouv2(kouv).eq.'o'.or.typouv2(kouv).eq.'i')then
              zdev2(kouv)=zdev(iouv,j)
              haut2(kouv)=haut(iouv,j)
              long2(kouv)=long(iouv,j)
              coef2(kouv)=coef(iouv,j)
          elseif(typouv2(kouv).eq.'q'.or.typouv2(kouv).eq.'z'&
     &)then
          nbcou12(kouv)=nbcoum2+1
          nbcoum=nbcoum-(nbcou2(iouv,j)-nbcou1(iouv,j)+1)
          do i=1,nbcou2(iouv,j)-nbcou1(iouv,j)+1
            zcoup2(nbcoum2+i)=zcoup(nbcoum+i)
            qcoup2(nbcoum2+i)=qcoup(nbcoum+i)
          enddo
#if TRANSPORT_SOLIDE
          if(typouv2(kouv).eq.'q')then
            do i=1,nbcou2(iouv,j)-nbcou1(iouv,j)+1
              ccoup2(nbcoum2+i)=ccoup(nbcoum+i)
              diamcoup2(nbcoum2+i)=diamcoup(nbcoum+i)
              sigmacoup2(nbcoum2+i)=sigmacoup(nbcoum+i)
            enddo
          endif
#endif
          nbcoum2=nbcoum2+nbcou2(iouv,j)-nbcou1(iouv,j)+1
          nbcou22(kouv)=nbcoum2
! fin du if sur le type ouvrage
! on ne fait rien si b
! on ne traite pas a ou c qui ne doivent pas etre la
           endif
! fin boucle sur nval
          enddo
          typouv(iouv,j)='c'
       elseif(typouv(iouv,j).ne.' ')then
            if(me==scribe)write(*,*)'erreur dans le type d''ouvrage'
          endif
          return
#if ENG
    5 if(me==scribe)write (*,*)'ERROR WHILE READING FILE ',trim(etude),'.ouv'
#else
    5 if(me==scribe)write (*,*)'ERREUR DANS LA LECTURE DU FICHIER ',trim(etude),'.ouv'
#endif
      stop
      return
      end subroutine lecparouv

!**********************************************************************
      subroutine lecparouv2(iouv,j,idon,nbcoum,nbcoum2,kouv)
!---------------------------------------------------------------------!
!      lecture des caracteristiques d'un ouvrage elementaire
!        dans le fichier .ouv
! copie exacte de lecparouv sauf traitement de l'ouvrage c supprime
!=====================================================================!
      use module_tableaux,only:etude,ie1,ie2,ia1,ia2,j1,j2,nouv,nbouv,&
     &long,zdev,haut,coef,nbcou1,nbcou2,qcoup,zcoup,zfm,typouv,nrefa,&
     &ieva,se,la,tm,tr,tinit,tmax,houv,qouv,zc,zp,alp,alc,z0,d50,rho,phi,db0,zb0,&
     &ym,sm,pm,rhm,alm,nu,ql,qs,dbr,z,zav,zbr,eta,ka,c1,c2,dt2,nt,&
     &zb,db,it,largmail,trect,ioub,nob,elap,suivan,tini,ext1,ext2,&
     &ieamont,ieaval,controlet,qcoup2,zcoup2,nbcou12,nbcou22,typouv2,&
     &long2,zdev2,haut2,coef2,tcoup2,nvaltz1,nvaltz2,nobmax,noemax,noumax

      use module_precision
#if TRANSPORT_SOLIDE
      use module_ts, only:ccoup,sedvar,dens,conckg,diamcoup,sigmacoup
#endif

      implicit none

      double precision zori
      integer iouv,idon,nref,j,k
      character rien*9
      integer nbcoup,nbcoum

      integer ibouv
! variables relatives aux ouvrages c
      integer nbcoum2,kouv,nvaltz,nval

!ech       integer nocan(noumax,noemax),nocan2(noumax)



      !common/etud/etude
      !common/iouvra/ie1,ie2,ia1,ia2,j1,j2,nouv,nbouv
      !common/couvra/long,zdev,haut,coef
      !common/nouvra/nbcou1,nbcou2
      !common/zouvra/qcoup,zcoup
      !common/touvra/typouv
      !common/zbas/zfm
      !common/narf/nrefa
      !common/iela/ieva
      !common/dose/se,la
      !common/tenv/tm,tr,tinit,tmax
      !common/hqouv/houv,qouv

      !common/digue/zc,zp,alp,alc,z0,d50,rho,phi,db0,zb0
      !common/moyen/ym,sm,pm,rhm,alm,nu
      !common/resul/ql,qs,dbr,z,zav,zbr
      !common/conrup/eta,ka,c1,c2,dt2
      !common/nconst/nt
      !common/breche/zb,db,it
      !common/maxbre/largmail,trect
      !common/iobrec/ioub,nob
      !common/elappr/elap,suivan
      !common/tinib/tini
! variables relatives aux ouvrages c
      !common/ext12/ext1,ext2
      !common/ieamontaval/ieamont,ieaval
      !common/controlouvc/controlet
      !common/zouvrac/qcoup2,zcoup2
      !common/nouvrac/nbcou12,nbcou22
      !common/touvrac/typouv2
      !common/couvrac/long2,zdev2,haut2,coef2
      !common/tcontrole/tcoup2
      !common/nvaltz12/nvaltz1,nvaltz2
!echc donnees de canoe, ajoute par s.jankowfsky
!ech       common/canoe/nocan,nocan2




! format fluvia
          read(idon,'(a1,a9,4f10.2)',err=5)&
     &typouv(iouv,j),rien,long(iouv,j),zdev(iouv,j),zori&
     &,coef(iouv,j)
          if(typouv(iouv,j).eq.'d'.or.typouv(iouv,j).eq.'D')then
            typouv(iouv,j)='d'
            haut(iouv,j)=zori-zdev(iouv,j)
!            zdev(iouv,j)=zdev(iouv,j)-zfm
! h correspond au meme ouvrage que d mais avec les cotes remplacees par les charges
          elseif(typouv(iouv,j).eq.'h'.or.typouv(iouv,j).eq.'H')then
            typouv(iouv,j)='h'
            haut(iouv,j)=zori-zdev(iouv,j)
!            zdev(iouv,j)=zdev(iouv,j)-zfm
! i correspond au meme ouvrage que d mais avec la formule de n. riviere pour le canal experimental
          elseif(typouv(iouv,j).eq.'i'.or.typouv(iouv,j).eq.'I')then
            typouv(iouv,j)='i'
            haut(iouv,j)=zori-zdev(iouv,j)
! o correspond au meme ouvrage que d mais avec une longueur et en circulaire
          elseif(typouv(iouv,j).eq.'o'.or.typouv(iouv,j).eq.'O')then
            typouv(iouv,j)='o'
! haut contient le diametre
            haut(iouv,j)=zori
!            zdev(iouv,j)=zdev(iouv,j)-zfm
! ouvrage q(zamont)
           elseif(typouv(iouv,j).eq.'z'.or.typouv(iouv,j).eq.'Z')then
            typouv(iouv,j)='z'
            read(rien,'(i9)')nbcoup
            if(nbcoup.lt.1.or.nbcoup.gt.noumax*noemax)then
              write(*,*)'erreur dans le nombre de couples'
              write(*,*)'le nombre de couples est: ',nbcoup
              write(*,*)'alors que le nombre maximal permis est :',noumax*noemax
              stop
            endif
            nbcou1(iouv,j)=nbcoum+1
            nbcoum=nbcoum+nbcoup
            if(nbcoum.gt.noumax*noemax*noemax)then
              write(*,*)'trop de couples (z,q) :'
              write(*,*)'plus de ',noumax*noemax*noemax,' couples'
              stop
            endif
            nbcou2(iouv,j)=nbcoum
            do 9 k=nbcou1(iouv,j),nbcou2(iouv,j)
              read(idon,*,err=5)zcoup(k),qcoup(k)
              zcoup(k)=zcoup(k)
!              zcoup(k)=zcoup(k)-zfm
              if(k.ne.nbcou1(iouv,j))then
                if(zcoup(k).lt.zcoup(k-1))then
                  write(*,*)'cotes non croissantes dans la definition'
        write(*,*)'du couple (z,q) numero ',k,' d''un ouvrage de type z'
                endif
              endif
 9          continue
! ouvrage q(t)
! attention zcoup contient les temps
           elseif(typouv(iouv,j).eq.'q'.or.typouv(iouv,j).eq.'Q')then
            typouv(iouv,j)='q'
            read(rien,'(i9)')nbcoup
            if(nbcoup.lt.1.or.nbcoup.gt.noumax*noemax)then
              write(*,*)'erreur dans le nombre de couples'
              write(*,*)'le nombre de couples est: ',nbcoup
              write(*,*)'alors que le nombre maximal permis est :',noumax*noemax
              stop
            endif
            nbcou1(iouv,j)=nbcoum+1
            nbcoum=nbcoum+nbcoup
            if(nbcoum.gt.noumax*noemax*noemax)then
              write(*,*)'trop de couples (t,q) :'
              write(*,*)'plus de ',noumax*noemax*noemax,' couples'
              stop
            endif
            nbcou2(iouv,j)=nbcoum
            do k=nbcou1(iouv,j),nbcou2(iouv,j)
#if TRANSPORT_SOLIDE
#if SEDVAR
              if(sedvar)then
                read(idon,*,err=5)zcoup(k),qcoup(k),ccoup(k),diamcoup(k),sigmacoup(k)
              else
#endif
                read(idon,*,err=5)zcoup(k),qcoup(k),ccoup(k)
! fin du if sur sedvar
#if SEDVAR
              endif
#endif
              if(conckg)then
                ccoup(k)=ccoup(k)/dens
              endif

#else
              read(idon,*,err=5)zcoup(k),qcoup(k)
#endif
! zcoup contient ici les temps
!              zcoup(k)=zcoup(k)
              if(k.ne.nbcou1(iouv,j))then
                if(zcoup(k).lt.zcoup(k-1))then
                  write(*,*)'temps non croissants dans la definition'
        write(*,*)'du couple (t,q) numero ',k,' d''un ouvrage de type q'
                endif
              endif
! fin du do sur k
           enddo
! ouvrage type rupro breche rectangualire ou renard
       elseif(typouv(iouv,j).eq.'b'.or.typouv(iouv,j).eq.'B')then
         if(nob.lt.nobmax)then
           ibouv=nob+1
           nob=ibouv
!           iob(ibouv)=iouv
           ioub(iouv,j)=ibouv
         else
           write(*,*)'le nombre ouvrages b depasse le maximum'
           write(*,*)'de ',nobmax,' possibles'
!           pause
           stop
         endif
       typouv(iouv,j)='b'
       zc(ibouv)=long(iouv,j)
!       zc(ibouv)=zc(ibouv)-zfm
       zp(ibouv)=zdev(iouv,j)
!       zp(ibouv)=zp(ibouv)-zfm
       alc(ibouv)=zori
       alp(ibouv)=coef(iouv,j)
! calcul de la largeur maximale de la breche
       if(ia2(iouv).ne.0)then
         largmail(ibouv)=min(la(ia1(iouv)),la(ia2(iouv)))
       else
         largmail(ibouv)=la(ia1(iouv))
       endif
! si elap vrai alors contrainte reduite sur les berges et approfondissement avec elargissement
! si suivan vrai les breches s'enchainent
! celle de numero suivant a la precedente si tini superieur a tmax
       read(rien(1:1),'(i1)')nbcoup

       if(nbcoup.eq.1.or.nbcoup.eq.2)then
         elap(ibouv)=.true.
       else
         elap(ibouv)=.false.
       endif
       if(nbcoup.eq.2.or.nbcoup.eq.3)then
         suivan(ibouv)=.true.
! pour ne pas chercher celui apres le dernier
         if(ibouv.le.nobmax)then
           suivan(ibouv+1)=.false.
         endif
       else
         suivan(ibouv)=.false.
       endif
!       if(abs(tini(ibouv)).lt.0.05)then
       if(rien(2:9).eq.'        ')then
         tini(ibouv)=tinit
       else
         read(rien(2:9),'(f8.0)')tini(ibouv)
       endif

! ouverture du fichier pour resultats de ouvrage b
       call ouver(ibouv)
! ouverture reportee a chaque stockage
!      sous-programme de lecture et de controle des donnees
       call lectes(iouv,ibouv)
! ecriture dans le fichier de resultats pour b
       call impdon(iouv,ibouv)
! inutile sauf peut etre pour ecriture
      z(1,ibouv)=z0(ibouv)
      zav(1,ibouv)=z0(ibouv)
      dbr(1,ibouv)=db(ibouv)
      zbr(1,ibouv)=zb(ibouv)
      qs(1,ibouv)=0.
      ql(1,ibouv)=0.
! un seul type d'ouvrage d'�change avec r�seau
!ech       elseif(typouv(iouv,j).eq.'a'.or.typouv(iouv,j).eq.'a')then
!ech         typouv(iouv,j)='a'
!ech         if(rien.eq.'         ')then
!ech            nocan(iouv,j)=0
!ech         else
!ech           read(rien,'(i9)',err=1005)nocan(iouv,j)
!ech         endif
!ech         call louvechange(iouv,j)
       elseif(typouv(iouv,j).eq.'c'.or.typouv(iouv,j).eq.'C')then
           write(*,*)'erreur ouvrage ',iouv
      write(*,*)'ouvrage elementaire c dans un ouvrage elementaire c'
       elseif(typouv(iouv,j).ne.' ')then
            write(*,*)'erreur dans le type d''ouvrage'
       endif
          return
#if ENG
    5 write (*,*)'ERROR WHILE READING FILE ',trim(etude),'.ouv'
#else
    5 write (*,*)'ERREUR DANS LA LECTURE DU FICHIER ',trim(etude),'.ouv'
#endif
      stop
      return
!ech 1005 write(*,*)'ouvrage ',iouv,' erreur format ouvrage elementaire a'
!ech      stop
!ech      return
      end subroutine lecparouv2

!**********************************************************************
      subroutine trxyix (x,y,ie,trouve)
!---------------------------------------------------------------------c
!     verifie que les coordonnees des mailles externes a un ouvrgae sont bonnes
!     entree: x, y au centre de l'element
!     sortie: numero de l'element
!     la variable logique trouve indique si l'element est trouve
!=====================================================================c
      use module_tableaux, only: xe,ye,ne,na,nn
      use module_precision

      implicit none

      integer ie,idec
      real(wp) x,y,dist
      logical trouve

      !common/cooe/xe,ye
      !common/nomb/ne,na,nn
      do idec=3,0,-1
         dist=0.06*10.**(-idec)
         do 1 ie=1,ne
         if (abs(x-xe(ie)).lt.dist) then
           if (abs(y-ye(ie)).lt.dist) then
             trouve=.true.
             go to 2
           endif
         endif
   1     continue
      enddo
   2  return
      end subroutine trxyix

!**********************************************************************
      subroutine trxyia (x,y,ia,trouve)
!---------------------------------------------------------------------c
!     verifie que les coordonnees des aretes d'un ouvrage sont bonnes
!     entree: x, y du milieu de l'arete
!     sortie: numero de l'arete
!     la variable logique trouve indique si l'arete est trouvee
!=====================================================================c

      use module_tableaux, only: xa,ya,ne,na,nn
      use module_precision

      implicit none

      integer ia,idec
      real(wp) x,y,dist
      logical trouve

      !common/nomb/ne,na,nn
      !common/cooa/xa,ya

      do idec=3,0,-1
         dist=0.06*10.**(-idec)
         do 1 ia=1,na
         if (abs(x-xa(ia)).lt.dist) then
           if (abs(y-ya(ia)).lt.dist) then
             trouve=.true.
             go to 2
           endif
         endif
   1     continue
      enddo
   2  return
      end subroutine trxyia

!**********************************************************************
      subroutine trxyie (x,y,nref,trouve)
!---------------------------------------------------------------------c
!     verifie que les coordonnees des mailles internes a un ouvrgae sont bonnes
!     entree: x, y au centre de l'element
!     sortie: references des aretes de l'element pour nref=-2
!     la variable logique trouve indique si l'element est trouve
!=====================================================================c

      use module_tableaux, only: xe,ye,nrefa,iae,nne,het,quet,qvet&
     &,ne,na,nn
      use module_precision

      implicit none

      integer ie,nref,ja,ia,idec
      real(wp) x,y,dist
      logical trouve

      !common/cooe/xe,ye
      !common/nomb/ne,na,nn
      !common/narf/nrefa
      !common/nrel/iae
      !common/nvois/nne
      !common/tlig/het,quet,qvet

! 0.1 car les coordonnees sont en f8.1 et il peut y avoir une erreur
! d'arrondi
      do idec=3,0,-1
         dist=0.06*10.**(-idec)
         do 1 ie=1,ne
          if (abs(x-xe(ie)).lt.dist) then
           if (abs(y-ye(ie)).lt.dist) then
              trouve=.true.
              if (nref.eq.-2) then
                het(ie)=0.
                quet(ie)=0.
                qvet(ie)=0.
                do 3 ja=1,nne(ie)
                  ia=iae(ie,ja)
!              if (nrefa(ia).le.0) then
                  nrefa(ia)=nref
!              endif
    3           continue
              endif
              go to 2
           endif
         endif
   1    continue
      enddo
   2  return
      end subroutine trxyie

!**********************************************************************
      subroutine qouvr
!---------------------------------------------------------------------c
!     calcule les debits transitant par les ouvrages
!=====================================================================c

      use module_tableaux, only:houv,qouv,zfa,vouv,vtouv,nobmax,noumax&
     &,noemax,ntrmax,ne,na,nn,g,ie1,ie2,ia1,ia2,j1,j2,nouv,nbouv,&
     &long,zdev,haut,coef,typouv,qcoup,zcoup,eps1,eps2,paray,dt,cfl,&
     &dt0,t,volouv,ioub,nob,ext1,ext2,ieamont,ieaval,controlet,qcoup2,zcoup2,&
     &nbcou12,nbcou22,typouv2,long2,zdev2,haut2,coef2,tcoup2,nvaltz1,nvaltz2,&
     &zfe,het,quet,qvet,difqouvp,nbcou2,nbcou1
      use module_precision
!$    use omp_lib
      use module_mpi,only:nouvb,me,ouvrages_bord,ouvrages_loc,nbouvloc,&
     &statinfo,machine,comm_calcul
      use module_loc, only:qouvp
#ifdef WITH_MPI
      use mpi
#endif /* WITH_MPI */

#if TRANSPORT_SOLIDE
      use module_ts,only:voaouv,coouv,ccoup,diamouv,sigmaouv,diamcoup,sigmacoup&
      &,diamcoup2,sigmacoup2,ccoup2,sedvar
#endif

      implicit none

      real(wp) z1,z2,zg,zd,q,difqouv
      integer ibouv
      real(wp) touv,qprime
      real(wp) coefriviere
      real(wp) zouv
#ifdef WITH_MPI
      integer,dimension(mpi_status_size)::status
#endif /* WITH_MPI */
!cou1d       logical couplage(noumax)


      integer iouv,j,jouv,iec,kouv,nval
      logical encore

#if TRANSPORT_SOLIDE
      real(wp) qsouv,diamsouv,sigmsouv
#endif


!       rac2g=sqrt(2.*g)
      touv=t-0.5*dt
!$omp parallel private(iouv,j,k,nbcou2,nbcou1,ibouv
!$omp1 ,rac2g,z1,z2,zg,zd,q,touv,qprime,coefriviere)

!$omp do schedule(runtime)
      do jouv=1,nbouvloc
        iouv=ouvrages_loc(jouv)
!$omp flush(qouvp,qouv)
        qouvp(ia1(iouv),j1(iouv))= qouv(ia1(iouv),j1(iouv))
        qouvp(ia2(iouv),j2(iouv))= qouv(ia2(iouv),j2(iouv))
        volouv(iouv)=0.
#if TRANSPORT_SOLIDE
        voaouv(iouv)=0.
        qsouv=0.
#if SEDVAR
        if(sedvar)then
          diamsouv=DIAMOUV(IA1(IOUV),J1(IOUV))
          sigmsouv=SIGMAOUV(IA1(IOUV),J1(IOUV))
        endif
!end if sedvar
#endif
#endif
!        q1=qouv(ia1(iouv),j1(iouv))
!        q2=qouv(ia2(iouv),j2(iouv))
        qouv(ia1(iouv),j1(iouv))=0.
!cou1d         if(couplage(iouv))then
!cou1d           qouv(ia1(iouv),j2(iouv))=0.
!cou1d         else
        qouv(ia2(iouv),j2(iouv))=0.
!cou1d         endif
        do j=1,nouv(iouv)
          if (typouv(iouv,j).eq.'d'.or.typouv(iouv,j).eq.'h'&
     &.or.typouv(iouv,j).eq.'o'.or.typouv(iouv,j).eq.'i') then
             call qdouvr(typouv(iouv,j),iouv,j,zdev(iouv,j)&
     &,haut(iouv,j),long(iouv,j),coef(iouv,j)&
     &,ext1(iouv,j),ext2(iouv,j),0,0&
#if TRANSPORT_SOLIDE
      &,qsouv&
#if SEDVAR
       &,diamsouv,sigmsouv&
!end if sedvar
#endif
#endif
     &)
! ouvrage de type  q(zamont)
          elseif(typouv(iouv,j).eq.'z')then
             call qzouvr(iouv,j,nbcou1(iouv,j)&
     &,nbcou2(iouv,j),qcoup,zcoup&
     &,ext1(iouv,j),ext2(iouv,j),0,0&
#if TRANSPORT_SOLIDE
      &,qsouv&
#if SEDVAR
       &,diamsouv,sigmsouv&
!end if sedvar
#endif
#endif
     &)
! ouvrage de type q(t) : q injecte � l'amont
          elseif(typouv(iouv,j).eq.'q')then
      call qqouvr(iouv,j,touv,nbcou1(iouv,j),nbcou2(iouv,j)&
     &,qcoup,zcoup&
#if TRANSPORT_SOLIDE
     &,CCOUP,DiamCOUP,SigmaCOUP&
     &,QSOUV&
#if SEDVAR
     &,diamsouv,sigmsouv&
#endif
#endif
     &)
      elseif(typouv(iouv,j).eq.'b')then
            ibouv=ioub(iouv,j)
            call qbouvr(iouv,ibouv,touv&
     &,ext1(iouv,j),ext2(iouv,j),0,0&
#if TRANSPORT_SOLIDE
     &,QSOUV&
#if SEDVAR
     &,diamsouv,sigmsouv&
#endif
#endif
     &)
      elseif(typouv(iouv,j).eq.'c')then
        if(controlet(iouv,j))then
           encore=.true.
           do nval=nvaltz1(iouv,j),nvaltz2(iouv,j)
            if(encore)then
             if(touv.lt.tcoup2(nval))then
               kouv=nval
               encore=.false.
             endif
            endif
           enddo
             if(encore)then
              kouv=nvaltz2(iouv,j)+1
             endif
! else du if sur controlet    : controle par z
        else
           if(ieamont(iouv,j).gt.0)then
            iec=ieamont(iouv,j)
        else
            iec=ie1(iouv)
        endif
           encore=.true.
           zouv=zfe(iec)+het(iec)
           do nval=nvaltz1(iouv,j),nvaltz2(iouv,j)
            if(encore)then
             if(zouv.lt.tcoup2(nval))then
               kouv=nval
               encore=.false.
             endif
            endif
            enddo
             if(encore)then
              kouv=nvaltz2(iouv,j)+1
             endif
! fin du if sur controlet
        endif
          if(typouv2(kouv).eq.'d'.or.typouv2(kouv).eq.'h'&
     &.or.typouv2(kouv).eq.'o'.or.typouv2(kouv).eq.'i')then
             call qdouvr(typouv2(kouv),iouv,j,zdev2(kouv)&
     &,haut2(kouv),long2(kouv),coef2(kouv)&
     &,ext1(iouv,j),ext2(iouv,j),ieamont(iouv,j),ieaval(iouv,j)&
#if TRANSPORT_SOLIDE
     &,qsouv&
#if SEDVAR
     &,diamsouv,sigmsouv&
!end if sedvar
#endif
#endif
     &)
! ouvrage de type  q(zamont)
          elseif(typouv2(kouv).eq.'z')then
             call qzouvr(iouv,j,nbcou12(kouv)&
     &,nbcou22(kouv),qcoup2,zcoup2&
     &,ext1(iouv,j),ext2(iouv,j),ieamont(iouv,j),ieaval(iouv,j)&
#if TRANSPORT_SOLIDE
     &,qsouv&
#if SEDVAR
     &,diamsouv,sigmsouv&
#endif
#endif
     &)
! ouvrage de type q(t) : q injecte � l'amont
          elseif(typouv2(kouv).eq.'q')then
      call qqouvr(iouv,j,touv,nbcou12(kouv),nbcou22(kouv)&
     &,qcoup2,zcoup2&
#if TRANSPORT_SOLIDE
     &,ccoup2,diamcoup2,sigmacoup2&
     &,qsouv&
#if SEDVAR
     &,diamsouv,sigmsouv&
#endif
#endif
     &)
      elseif(typouv2(kouv).eq.'b')then
            ibouv=ioub(iouv,j)
            call qbouvr(iouv,ibouv,touv&
     &,ext1(iouv,j),ext2(iouv,j),ieamont(iouv,j),ieaval(iouv,j)&
#if TRANSPORT_SOLIDE
     &,QSOUV&
#if SEDVAR
     &,diamsouv,sigmsouv&
#endif
#endif
     &)
! fin du if sur typouv2
      endif
! un seul type d'ouvrage d'�change avec r�seau
!ech      elseif(typouv(iouv,j).eq.'a')then
!ech         call qouvechange(iouv,j,touv)
! fin du if sur le type d'ouvrage
!          else
!            q=0.
          endif

        end do
! si q precedent de signe different, on prend 0
! si qouv non nul on prend la demi somme precedent + actuel
! sinon on garde la valeur actuelle
!         if(final)then
! correction du 13 juin 2014
      difqouv=qouv(ia1(iouv),j1(iouv))-qouvp(ia1(iouv),j1(iouv))
! si changement signe variation
      if(difqouv*difqouvp(ia1(iouv),j1(iouv))&
     &.lt.-eps1)then
             difqouvp(ia1(iouv),j1(iouv))=0.
             qouv(ia1(iouv),j1(iouv))=qouvp(ia1(iouv),j1(iouv))
            if(ia2(iouv).gt.0)then
          qouv(ia2(iouv),j2(iouv))=qouvp(ia2(iouv),j2(iouv))
!cou1d            elseif(couplage(iouv))then
!cou1d          qouv(ia1(iouv),j2(iouv))=qouvp(ia1(iouv),j2(iouv))
! fin du if sur ia2
        endif
      else
! on garde valeur nouvelle si difqouv de meme signe
             difqouvp(ia1(iouv),j1(iouv))=difqouv
       endif
!#if TRANSPORT_SOLIDE
!            coouv(ia1(iouv),j1(iouv))=0.
!#endif
!            if(ia2(iouv).gt.0
!!cou1d     :.or.couplage(iouv)
!     :)then
!             qouv(ia2(iouv),j2(iouv))=0.
!#if TRANSPORT_SOLIDE
!            coouv(ia2(iouv),j2(iouv))=0.
!#endif
!! fin du if sur ia2>0
!            endif
!!             write(*,*)'pas de temps trop grand pour l''ouvrage',iouv
! si les deux q sont de meme signe
!        else
        if(abs(qouv(ia1(iouv),j1(iouv))).lt.eps2)then
             qouv(ia1(iouv),j1(iouv))=0.
#if TRANSPORT_SOLIDE
            coouv(ia1(iouv),j1(iouv))=0.
#endif
            if(ia2(iouv).gt.0)then
             qouv(ia2(iouv),j2(iouv))=0.
#if TRANSPORT_SOLIDE
            coouv(ia2(iouv),j2(iouv))=0.
#endif
!cou1d            elseif(couplage(iouv))then
!cou1d              qouv(ia1(iouv),j2(iouv))=0.
#if TRANSPORT_SOLIDE
!cou1d              coouv(ia1(iouv),j2(iouv))=0.
#endif
! fin du if sur ia2>0
            endif
! cas ou qouv (ia1) non nul
        else

! ajout de tests si het>0 le 12/2/2021
! si arete orientee pour que maille calculee a gauche
        if(j1(iouv).eq.1)then
        if(qouv(ia1(iouv),j1(iouv)).gt.eps2)then
! debit de 1 vers 2
          if(het(ie1(iouv)).lt.paray)then
            qouv(ia1(iouv),j1(iouv))=0.
#if TRANSPORT_SOLIDE
            coouv(ia1(iouv),j1(iouv))=0.
#endif
            if(ia2(iouv).gt.0)then
              qouv(ia2(iouv),j2(iouv))=0.
#if TRANSPORT_SOLIDE
              coouv(ia2(iouv),j2(iouv))=0.
#endif
!cou1d            elseif(couplage(iouv))then
!cou1d              qouv(ia1(iouv),j2(iouv))=0.
#if TRANSPORT_SOLIDE
!cou1d              coouv(ia1(iouv),j2(iouv))=0.
#endif
! fin du if sur ia2>0
            endif
! fin du if sur het
          endif
        elseif(qouv(ia1(iouv),j1(iouv)).lt.-eps2)then
! debit de 2 vers 1
          if(ia2(iouv).gt.0)then
            if(het(ie2(iouv)).lt.paray)then
              qouv(ia1(iouv),j1(iouv))=0.
#if TRANSPORT_SOLIDE
              coouv(ia1(iouv),j1(iouv))=0.
#endif
              qouv(ia2(iouv),j2(iouv))=0.
#if TRANSPORT_SOLIDE
              coouv(ia2(iouv),j2(iouv))=0.
#endif
! fin du if sur het
            endif
! fin du if sur ia2>0
          endif
! fin du if sur qouv
        endif
! si arete orientee pour que maille calculee a droite
        else
! else equivaut a       if(j1(iouv).ne.1)then
        if(qouv(ia1(iouv),j1(iouv)).lt.-eps2)then
! debit de 1 vers 2
            if(het(ie1(iouv)).lt.paray)then
              qouv(ia1(iouv),j1(iouv))=0.
#if TRANSPORT_SOLIDE
              coouv(ia1(iouv),j1(iouv))=0.
#endif
            if(ia2(iouv).gt.0)then
              qouv(ia2(iouv),j2(iouv))=0.
#if TRANSPORT_SOLIDE
            coouv(ia2(iouv),j2(iouv))=0.
#endif
!cou1d            elseif(couplage(iouv))then
!cou1d             qouv(ia1(iouv),j2(iouv))=0.
#if TRANSPORT_SOLIDE
!cou1d            coouv(ia1(iouv),j2(iouv))=0.
#endif
! fin du if sur ia2>0
            endif
! fin du if sur het
          endif
        elseif(qouv(ia1(iouv),j1(iouv)).gt.eps2)then
! debit de 2 vers 1
          if(ia2(iouv).gt.0)then
            if(het(ie2(iouv)).lt.paray)then
              qouv(ia1(iouv),j1(iouv))=0.
#if TRANSPORT_SOLIDE
              coouv(ia1(iouv),j1(iouv))=0.
#endif
              qouv(ia2(iouv),j2(iouv))=0.
#if TRANSPORT_SOLIDE
              coouv(ia2(iouv),j2(iouv))=0.
#endif
! fin du if sur het
            endif
! fin du if sur ia2>0
          endif
! fin du if sur qouv
        endif
! fin du if sur j1
        endif
! fin des tests ajoutes le 12/2/2021

! si arete orientee pour que maille calculee a gauche
#if TRANSPORT_SOLIDE
        if(j1(iouv).eq.1)then
          if(qouv(ia1(iouv),j1(iouv)).gt.eps2)then
! debit de 1 vers 2
            if(ia2(iouv).gt.0)then
! a modifier suite a corection du 13 juin 2014 car qouv modifie avant
            coouv(ia2(iouv),j2(iouv))=qsouv/qouv(ia1(iouv),j1(iouv))
#if SEDVAR
            if(sedvar)then
      diamouv(ia2(iouv),j2(iouv))=diamsouv
      sigmaouv(ia2(iouv),j2(iouv))=sigmsouv
            endif
#endif
!cou1d            elseif(couplage(iouv))then
! a modifier suite a corection du 13 juin 2014 car qouv modifie avant
!cou1d            coouv(ia1(iouv),j2(iouv))=qsouv/&
!cou1d     &qouv(ia1(iouv),j1(iouv))
!cou1d            coouv(ia1(iouv),j1(iouv))=&
!cou1d     &coouv(ia1(iouv),j2(iouv))
#if SEDVAR
!cou1d            if(sedvar)then
!cou1d      diamouv(ia1(iouv),j2(iouv))=diamsouv
!cou1d      sigmaouv(ia1(iouv),j2(iouv))=sigmsouv
!cou1d      diamouv(ia1(iouv),j1(iouv))=diamsouv
!cou1d      sigmaouv(ia1(iouv),j1(iouv))=sigmsouv
!cou1d            endif
#endif
! fin du if sur ia2>0
            endif
! si qouv negatif
         else
! debit de 2 vers 1
            coouv(ia1(iouv),j1(iouv))=-qsouv/&
     &qouv(ia1(iouv),j1(iouv))
#if SEDVAR
            if(sedvar)then
      diamouv(ia1(iouv),j1(iouv))=diamsouv
      sigmaouv(ia1(iouv),j1(iouv))=sigmsouv
            endif
#endif
! fin du if sur qouv positif
         endif
! else du if sur j1
        else
         if(qouv(ia1(iouv),j1(iouv)).lt.-eps2)then
! debit de 1 vers 2
            if(ia2(iouv).gt.0)then
            coouv(ia2(iouv),j2(iouv))=-qsouv/qouv(ia1(iouv),j1(iouv))
#if SEDVAR
            if(sedvar)then
      diamouv(ia2(iouv),j2(iouv))=diamsouv
      sigmaouv(ia2(iouv),j2(iouv))=sigmsouv
            endif
#endif
!cou1d            elseif(couplage(iouv))then
!cou1d            coouv(ia1(iouv),j2(iouv))=-qsouv/&
!cou1d     &qouv(ia1(iouv),j1(iouv))
#if SEDVAR
!cou1d            if(sedvar)then
!cou1d      diamouv(ia1(iouv),j2(iouv))=diamsouv
!cou1d      sigmaouv(ia1(iouv),j2(iouv))=sigmsouv
!cou1d            endif
#endif
! fin du if sur ia2>0
            endif
! si qouv negatif
         else
! debit de 2 vers 1
            coouv(ia1(iouv),j1(iouv))=qsouv/&
     &qouv(ia1(iouv),j1(iouv))
#if SEDVAR
            if(sedvar)then
      diamouv(ia1(iouv),j1(iouv))=diamsouv
      sigmaouv(ia1(iouv),j1(iouv))=sigmsouv
            endif
#endif
! fin du if sur qouv positif
         endif
! fin du if sur j1
         endif
#endif /* TRANSPORT_SOLIDE */

!       qouv(ia1(iouv),j1(iouv))&
!     &=0.5*(qouv(ia1(iouv),j1(iouv))+qouvp(ia1(iouv),j1(iouv)))
!            if(ia2(iouv).gt.0
!!cou1d     :.or.couplage(iouv)
!     :)then
!          qouv(ia2(iouv),j2(iouv))
!     :=0.5*(qouv(ia2(iouv),j2(iouv))+qouvp(ia2(iouv),j2(iouv)))
!c fin du if sur ia2
!        endif
! fin du if sur qouv non nul
       endif
! fin du if sur qouv*qouvp
!      endif
! fin boucle sur numero ouvrage iouv
      end do

#ifdef WITH_MPI
      do jouv=1,nouvb
        iouv=ouvrages_bord(jouv)
        if(machine(ie1(iouv))==me)then
          call mpi_send(qouv(ia2(iouv),j2(iouv)),1,mpi_real8,machine(ie2(iouv)),7777+iouv,comm_calcul,statinfo)
        elseif(machine(ie2(iouv))==me)then
          call mpi_recv(qouv(ia2(iouv),j1(iouv)),1,mpi_real8,machine(ie1(iouv)),7777+iouv,comm_calcul,status,statinfo)
        else
          print*,'un ouvrage du bord a forc�ment une maille dans le domaine local'
        end if
      end do
#endif /* WITH_MPI */

      end subroutine qouvr

!**********************************************************************
      subroutine qdouvr(typouvd,iouv,j,zdevd,hautd,longd,coefd&
     &,ext1,ext2,ieamont,ieaval&
#if TRANSPORT_SOLIDE
      &,qsouv&
#if SEDVAR
     &,diamsouv,sigmsouv&
!end if sedvar
#endif
#endif
     &)
!---------------------------------------------------------------------c
!     calcule les debits transitant par les ouvrages d,h,o,i
!=====================================================================c
      use module_tableaux,only:g,houv,qouv,ie1,ie2,ia1,ia2,j1,j2,nouv,nbouv,&
     &nbcou1,nbcou2,qcoup,zcoup,zfa,eps1,eps2,paray,dt,cfl,dt0,t,volouv,vouv,vtouv,&
     &zfe,het,quet,qvet

      use module_precision

#if TRANSPORT_SOLIDE
      use module_ts,only:voaouv,coouv,ccoup,diamouv,sigmaouv,diamcoup,sigmacoup,sedvar
#endif

      implicit none

      real(wp) :: rac2g,z1,z2,zg,zd,q
      real(wp) :: longd,zdevd,hautd,coefd,qprime
      real(wp) :: coefriviere
      logical :: ext1,ext2
      integer :: ieamont,ieaval
!cou1d       logical couplage(noumax)

      character*1 :: typouvd

      integer :: iouv,j


#if TRANSPORT_SOLIDE
      real(wp) :: qsouv,qs,diamsouv,sigmsouv
#endif

      !common/grav/g
      !common/hqouv/houv,qouv
      !common/iouvra/ie1,ie2,ia1,ia2,j1,j2,nouv,nbouv
      !common/nouvra/nbcou1,nbcou2
      !common/zouvra/qcoup,zcoup
      !common/zare/zfa
      !common/param/eps1,eps2,paray
      !common/cflr/dt,cfl,dt0,t
      !common/volou/volouv
      !common/vouvh/vouv,vtouv
      !common/zele/zfe
      !common/tlig/het,quet,qvet
!cou1d      common/ouvcouple/couplage



      rac2g=sqrt(2.*g)
!          if(typouv(iouv,j).eq.'d'.or.typouv(iouv,j).eq.'h'
!     :.or.typouv(iouv,j).eq.'o'.or.typouv(iouv,j).eq.'i')then
           if(ext1)then
            z1=het(ieamont)+zfe(ieamont)
            if(typouvd.eq.'h'.or.typouvd.eq.'o')then
                if(het(ieamont).gt.paray)then
      z1=z1+(quet(ieamont)**2+qvet(ieamont)**2)/(het(ieamont)**2*2.*g)
                endif
            endif
! si pas ext 1 (cas normal)
          else
            z1=houv(ia1(iouv),j1(iouv))+zfa(ia1(iouv))
            if(typouvd.eq.'h'.or.typouvd.eq.'o')then
              z1=z1+vouv(ia1(iouv),j1(iouv))**2/(2.*g)
            endif
! fin du if sur ext1
          endif
          if(ext2)then
            z2=het(ieaval)+zfe(ieaval)
            if(typouvd.eq.'h'.or.typouvd.eq.'o')then
                if(het(ieaval).gt.paray)then
      z2=z2+(quet(ieaval)**2+qvet(ieaval)**2)/(het(ieaval)**2*2.*g)
                endif
            endif
! si pas ext2 (cas normal)
          else
            if(ia2(iouv).gt.0)then
              z2=houv(ia2(iouv),j2(iouv))+zfa(ia2(iouv))
              if(typouvd.eq.'h'.or.typouvd.eq.'o')then
                z2=z2+vouv(ia2(iouv),j2(iouv))**2/(2.*g)
              endif
!cou1d            elseif(couplage(iouv))then
!cou1d             z2=houv(ia1(iouv),j2(iouv))+zfa(ia1(iouv))
!cou1d! pour h ou o on prend vitesse nulle
!cou1d!              endif
            else
              z2=-99999.999
            endif
! fin du if sur ext2
            endif
            if(z1.gt.z2+paray)then
              if(houv(ia1(iouv),j1(iouv)).gt.paray)then
               zg=z1
               zd=z2
              elseif(ext1)then
! controle par niveau sans appel a hauteur d'eau
               zg=z1
               zd=z2
              else
               return
              endif
!              zg=z1
!              zd=z2
            elseif(z2.gt.z1+paray)then
!              zg=z2
!              zd=z1
!cou1d            if(couplage(iouv)
!cou1d     :)then
!cou1d             if(houv(ia1(iouv),j2(iouv)).gt.paray)then
!cou1d               zg=z2
!cou1d               zd=z1
!cou1d              else
!cou1d               return
!cou1d              endif
!cou1d            else
              if(houv(ia2(iouv),j2(iouv)).gt.paray)then
               zg=z2
               zd=z1
              elseif(ext2)then
! controle par niveau sans appel a hauteur d'eau
               zg=z2
               zd=z1
              else
               return
              endif
!cou1d              endif
           else
!              q=0
              return
           endif
           if(typouvd.eq.'o')then
! si submersionde l'entree
                if(zg-zdevd.gt.hautd)then
              if(zg.gt.zdevd+paray)then
! deversoir denoye
               q=(zg-zdevd)**1.5
! perte de charge singuliere de coefficient 0.5
! materiau suppose beton strickler de 90
! coef standard suppose egal a 0.3
! 10.472=pi/0.3
! modification le 22/10/19
! 2.618=pi/0.3/4
! conduite noyee
!              qprime=10.472*hautd*
               qprime=2.618*hautd*sqrt((zg-zd)/(1.5+0.000610*g*longd*hautd**(-1.333)))
               q=min(q,qprime)*hautd*coefd*rac2g
              else
               q=0.
              endif
! si non submersion de l'entree
                          else
! deversoir denoye = seul cas pris en compte
              if(zg.gt.zdevd+paray)then
               q=coefd*hautd**0.6*rac2g*(zg-zdevd)**1.9
              else
               q=0.
              endif
! fin du if sur submersion entree
                          endif
! introduit pour calcul aval carrefour (experience selon formule n. riviere)
            elseif(typouvd.eq.'i')then
              if(zg.gt.zdevd+paray)then
               coefriviere=1.+0.793*(zdevd/coefd)**0.731
               coefriviere=(2.*coefriviere**3)**(-0.5)
               q=coefriviere*longd*rac2g*(zg-zdevd)**1.5
              else
               q=0.
              endif
! si typouv est d ou h
            else
           if(zd.gt.0.6667*zg+0.3333*zdevd)then
! ecoulement noye
             if(zd.lt.zdevd+0.58095*hautd)then
! deversoir noye
              if(zd.gt.zdevd+paray)then
               q=2.59808*coefd*longd*rac2g*(zd-zdevd)*sqrt(zg-zd)
              else
               q=0.
              endif
             else
! orifice noye
               q=1.50935*coefd*longd*rac2g*hautd*sqrt(zg-zd)
             endif
           else
! ecoulement denoye
             if(zg.lt.zdevd+1.125*hautd)then
! deversoir denoye
              if(zg.gt.zdevd+paray)then
               q=coefd*longd*rac2g*(zg-zdevd)**1.5
              else
               q=0.
              endif
             elseif(zd.lt.zdevd+0.5*hautd)then
! orifice denoye
               q=1.50935*coefd*longd*rac2g*sqrt(zg-zdevd-0.5*hautd)*hautd
             else
! orifice noye
               q=1.50935*coefd*longd*rac2g*hautd*sqrt(zg-zd)
             endif
! fin du if sur ecoulement noye/denoye
           endif
! fin du if sur o ou (d ou h ou i)
           endif
           if(z1.gt.z2)then
            volouv(iouv)=volouv(iouv)+q
! qs est positif si courant de 1 vers 2 et inversement
#if TRANSPORT_SOLIDE
            qs=q*coouv(ia1(iouv),j1(iouv))
            voaouv(iouv)=voaouv(iouv)+qs
#endif
! mis en commentaire car ia2 non nul dans ce cas
            if(ia2(iouv).gt.0)then
!#if TRANSPORT_SOLIDE
!            coouv(ia2(iouv),j2(iouv))=coouv(ia1(iouv),j1(iouv))
!#endif
            if(j2(iouv).eq.1)then
             qouv(ia2(iouv),j2(iouv))=qouv(ia2(iouv),j2(iouv))-q
            else
             qouv(ia2(iouv),j2(iouv))=qouv(ia2(iouv),j2(iouv))+q
            endif
!cou1d      elseif(couplage(iouv))then
!#if TRANSPORT_SOLIDE
!cou1d!            coouv(ia1(iouv),j2(iouv))=coouv(ia1(iouv),j1(iouv))
!#endif
!cou1d            if(j2(iouv).eq.1)then
!cou1d             qouv(ia1(iouv),j2(iouv))=qouv(ia1(iouv),j2(iouv))-q
!cou1d            else
!cou1d             qouv(ia1(iouv),j2(iouv))=qouv(ia1(iouv),j2(iouv))+q
!cou1d            endif
! fin du if sur ia2
            endif
#if TRANSPORT_SOLIDE
#if SEDVAR
            if(sedvar)then
! qs et qsouv sont positifs
               call melangeds(qsouv,qs,diamsouv,sigmsouv,diamouv(ia1(iouv),j1(iouv)),sigmaouv(ia1(iouv),j1(iouv)))
            else
#endif
               qsouv=qsouv+qs
#if SEDVAR
            endif
#endif
#endif /* TRANSPORT_SOLIDE */
            if(j1(iouv).eq.1)then
             qouv(ia1(iouv),j1(iouv))=qouv(ia1(iouv),j1(iouv))+q
            else
             qouv(ia1(iouv),j1(iouv))=qouv(ia1(iouv),j1(iouv))-q
            endif
! si z2 >z1
           else
            volouv(iouv)=volouv(iouv)-q
! mis en commentaire car ia2 non nul dans ce cas sauf si couplage
!cou1d            if(ia2(iouv).gt.0)then
#if TRANSPORT_SOLIDE
            qs=q*coouv(ia2(iouv),j2(iouv))
            voaouv(iouv)=voaouv(iouv)-qs
#if SEDVAR
            if(sedvar)then
! qs et qsouv sont positifs
               call melangeds(qsouv,qs,diamsouv,sigmsouv&
     &,diamouv(ia2(iouv),j2(iouv)),sigmaouv(ia2(iouv),j2(iouv)))
            else
#endif
               qsouv=qsouv+qs
#if SEDVAR
            endif
#endif
#endif /* TRANSPORT_SOLIDE */
            if(j2(iouv).eq.1)then
             qouv(ia2(iouv),j2(iouv))=qouv(ia2(iouv),j2(iouv))+q
            else
             qouv(ia2(iouv),j2(iouv))=qouv(ia2(iouv),j2(iouv))-q
            endif
! si ia2=0 et pas de couplage (ne doit jamais arriver normalement)
!cou1d            else
#if TRANSPORT_SOLIDE
!cou1d            qs=q*coouv(ia1(iouv),j2(iouv))
!cou1d            voaouv(iouv)=voaouv(iouv)-qs
#if SEDVAR
!cou1d            if(sedvar)then
!cou1d! qs et qsouv sont positifs
!cou1d               call melangeds(qsouv,qs,diamsouv,sigmsouv&
!cou1d     &,diamouv(ia1(iouv),j2(iouv)),sigmaouv(ia1(iouv),j2(iouv)))
!cou1d            else
#endif
!cou1d               qsouv=qsouv+qs
#if SEDVAR
!cou1d            endif
#endif
#endif /* TRANSPORT_SOLIDE */
!cou1d            if(j2(iouv).eq.1)then
!cou1d             qouv(ia1(iouv),j2(iouv))=qouv(ia1(iouv),j2(iouv))+q
!cou1d            else
!cou1d             qouv(ia1(iouv),j2(iouv))=qouv(ia1(iouv),j2(iouv))-q
!cou1d            endif
! fin du if sur ia2
!cou1d            endif
        if(j1(iouv).eq.1)then
          qouv(ia1(iouv),j1(iouv))=qouv(ia1(iouv),j1(iouv))-q
        else
          qouv(ia1(iouv),j1(iouv))=qouv(ia1(iouv),j1(iouv))+q
        endif
! fin du if sur z1 et z2
      endif
      return
      end subroutine qdouvr

!**********************************************************************
      subroutine qzouvr(iouv,j,nb1,nb2,qcou,zcou,ext1,ext2,ieamont,ieaval&
#if TRANSPORT_SOLIDE
     &,qsouv&
#if SEDVAR
     &,diamsouv,sigmsouv&
#endif
#endif
      &)
!---------------------------------------------------------------------c
!     calcule les debits transitant par les ouvrages z
!=====================================================================c

      use module_tableaux,only:g,houv,qouv,ie1,ie2,ia1,ia2,j1,j2,nouv,nbouv,&
     &zfa,eps1,eps2,paray,dt,cfl,dt0,t,volouv,vouv,vtouv,zfe,het,quet,qvet,noumax,noemax

      use module_precision
#if TRANSPORT_SOLIDE
      use module_ts,only : coouv,voaouv,diamouv,sigmaouv,&
#if SEDVAR
      &sedvar,&
#endif
     &sigma0
#endif

      implicit none

      real(wp) :: z1,z2,q,zg,zd
      integer :: nb2,nb1
      real(wp) :: qcou(noumax*noemax*noemax),zcou(noumax*noemax*noemax)
      logical :: ext1,ext2
      integer :: ieamont,ieaval
!cou1d       logical couplage(noumax)

#if TRANSPORT_SOLIDE
      real(wp) :: qsouv,qs
#if SEDVAR
      real(wp) :: diamsouv,sigmsouv
#endif
#endif

      integer :: iouv,j,k

      !common/grav/g
      !common/hqouv/houv,qouv
      !common/iouvra/ie1,ie2,ia1,ia2,j1,j2,nouv,nbouv
      !common/zare/zfa
      !common/param/eps1,eps2,paray
      !common/cflr/dt,cfl,dt0,t
      !common/volou/volouv
      !common/vouvh/vouv,vtouv
      !common/zele/zfe
      !common/tlig/het,quet,qvet
!cou1d      common/ouvcouple/couplage

!      rac2g=sqrt(2.*g)
           if(ext1)then
            z1=het(ieamont)+zfe(ieamont)
! si pas ext 1 (cas normal)
          else
            z1=houv(ia1(iouv),j1(iouv))+zfa(ia1(iouv))
! fin du if sur ext1
          endif
          if(ia2(iouv).gt.0)then
            if(ext2)then
              z2=het(ieaval)+zfe(ieaval)
            else
              z2=houv(ia2(iouv),j2(iouv))+zfa(ia2(iouv))
           endif
!cou1d            elseif(couplage(iouv))then
!cou1d             z2=houv(ia1(iouv),j2(iouv))+zfa(ia1(iouv))
!cou1d! pour h ou o on prend vitesse nulle
!cou1d!              endif
            else
              z2=-99999.999
            endif
            if(z1.gt.z2+paray)then
              if(houv(ia1(iouv),j1(iouv)).gt.paray)then
               zg=z1
               zd=z2
              elseif(ext1)then
! controle par niveau sans appel a hauteur d'eau
               zg=z1
               zd=z2
              else
               return
              endif
!              zg=z1
!              zd=z2
            elseif(z2.gt.z1+paray)then
!              zg=z2
!              zd=z1
!cou1d            if(couplage(iouv))then
!cou1d             if(houv(ia1(iouv),j2(iouv)).gt.paray)then
!cou1d               zg=z2
!cou1d               zd=z1
!cou1d              else
!cou1d               return
!cou1d              endif
!cou1d            else
              if(houv(ia2(iouv),j2(iouv)).gt.paray)then
               zg=z2
               zd=z1
              elseif(ext2)then
! controle par niveau sans appel a hauteur d'eau
               zg=z2
               zd=z1
              else
               return
              endif
!cou1d              endif
           else
!              q=0
              return
           endif
            do k=nb1,nb2
              if(zcou(k).gt.zg)then
                if(k.eq.nb1)then
                  q=qcou(k)
                else
                  q=qcou(k-1)+(zg-zcou(k-1))/(zcou(k)-zcou(k-1))*(qcou(k)-qcou(k-1))
                endif
                go to 4
              endif
            enddo
            q=qcou(nb2)
 4         if(z1.gt.z2)then
            volouv(iouv)=volouv(iouv)+q
! qs est positif si courant de 1 vers 2 et inversement
#if TRANSPORT_SOLIDE
            qs=q*coouv(ia1(iouv),j1(iouv))
            voaouv(iouv)=voaouv(iouv)+qs
#endif
! mis en commentaire car ia2 non nul dans ce cas
            if(ia2(iouv).gt.0)then
#if TRANSPORT_SOLIDE
            coouv(ia2(iouv),j2(iouv))=coouv(ia1(iouv),j1(iouv))
#endif
            if(j2(iouv).eq.1)then
             qouv(ia2(iouv),j2(iouv))=qouv(ia2(iouv),j2(iouv))-q
            else
             qouv(ia2(iouv),j2(iouv))=qouv(ia2(iouv),j2(iouv))+q
            endif
!cou1d      elseif(couplage(iouv))then
!#if TRANSPORT_SOLIDE
!cou1d!            coouv(ia1(iouv),j2(iouv))=coouv(ia1(iouv),j1(iouv))
!#endif
!cou1d            if(j2(iouv).eq.1)then
!cou1d             qouv(ia1(iouv),j2(iouv))=qouv(ia1(iouv),j2(iouv))-q
!cou1d            else
!cou1d             qouv(ia1(iouv),j2(iouv))=qouv(ia1(iouv),j2(iouv))+q
!cou1d            endif
! fin du if sur ia2
            endif
#if TRANSPORT_SOLIDE
#if SEDVAR
            if(sedvar)then
! qs et qsouv sont positifs
               call melangeds(qsouv,qs,diamsouv,sigmsouv&
     &,diamouv(ia1(iouv),j1(iouv)),sigmaouv(ia1(iouv),j1(iouv)))
            else
#endif
               qsouv=qsouv+qs
#if SEDVAR
            endif
#endif
#endif /* TRANSPORT_SOLIDE */
            if(j1(iouv).eq.1)then
             qouv(ia1(iouv),j1(iouv))=qouv(ia1(iouv),j1(iouv))+q
            else
             qouv(ia1(iouv),j1(iouv))=qouv(ia1(iouv),j1(iouv))-q
            endif
! si z2 >z1
           else
            volouv(iouv)=volouv(iouv)-q
! mis en commentaire car ia2 non nul dans ce cas sauf si couplage
!cou1d            if(ia2(iouv).gt.0)then
#if TRANSPORT_SOLIDE
            qs=q*coouv(ia2(iouv),j2(iouv))
            voaouv(iouv)=voaouv(iouv)-qs
#if SEDVAR
            if(sedvar)then
! qs et qsouv sont positifs
               call melangeds(qsouv,qs,diamsouv,sigmsouv&
     &,diamouv(ia2(iouv),j2(iouv)),sigmaouv(ia2(iouv),j2(iouv)))
            else
#endif
               qsouv=qsouv+qs
#if SEDVAR
            endif
#endif
#endif /* TRANSPORT_SOLIDE */
            if(j2(iouv).eq.1)then
             qouv(ia2(iouv),j2(iouv))=qouv(ia2(iouv),j2(iouv))+q
            else
             qouv(ia2(iouv),j2(iouv))=qouv(ia2(iouv),j2(iouv))-q
            endif
! si ia2=0 et pas de couplage (ne doit jamais arriver normalement)
!cou1d            else
#if TRANSPORT_SOLIDE
!cou1d            qs=q*coouv(ia1(iouv),j2(iouv))
!cou1d            voaouv(iouv)=voaouv(iouv)-qs
#if SEDVAR
!cou1d            if(sedvar)then
!cou1d! qs et qsouv sont positifs
!cou1d               call melangeds(qsouv,qs,diamsouv,sigmsouv&
!cou1d     &,diamouv(ia1(iouv),j2(iouv)),sigmaouv(ia1(iouv),j2(iouv)))
!cou1d            else
#endif
!cou1d               qsouv=qsouv+qs
#if SEDVAR
!cou1d            endif
#endif
#endif /* TRANSPORT_SOLIDE */
!cou1d            if(j2(iouv).eq.1)then
!cou1d             qouv(ia1(iouv),j2(iouv))=qouv(ia1(iouv),j2(iouv))+q
!cou1d            else
!cou1d             qouv(ia1(iouv),j2(iouv))=qouv(ia1(iouv),j2(iouv))-q
!cou1d            endif
! fin du if sur ia2
!cou1d            endif
            if(j1(iouv).eq.1)then
             qouv(ia1(iouv),j1(iouv))=qouv(ia1(iouv),j1(iouv))-q
            else
             qouv(ia1(iouv),j1(iouv))=qouv(ia1(iouv),j1(iouv))+q
            endif
! fin du if sur z1 et z2
           endif
           return
           end subroutine qzouvr

!**********************************************************************
      subroutine qqouvr(iouv,j,touv,nb1,nb2,qcou,zcou&
#if TRANSPORT_SOLIDE
     &,ccou,diamcou,sigmacou,qsouv&
#if SEDVAR
     &,diamsouv,sigmsouv&
#endif
#endif
     &)
!---------------------------------------------------------------------c
!     calcule les debits transitant par les ouvrages q
!=====================================================================c

      use module_tableaux,only:g,eps1,eps2,paray,houv,qouv,ie1,ie2,ia1,ia2,j1,j2,&
     &nouv,nbouv,dt,cfl,dt0,t,volouv,vouv,vtouv,noumax,noemax
#if TRANSPORT_SOLIDE
      use module_ts,only : coouv,voaouv,diamouv,sigmaouv,&
#if SEDVAR
     &sedvar,&
#endif
     &sigmaouv
#endif

      use module_precision

      implicit none

      real(wp) :: q
      integer :: nb2,nb1
      real(wp) :: qcou(noumax*noemax*noemax),zcou(noumax*noemax*noemax),touv
!cou1d       logical couplage(noumax)

#if TRANSPORT_SOLIDE
      logical avects
      real(wp) :: cc,diamouvc,sigmaouvc,qsouv,qs,ccou(noumax*noemax*noemax),&
     &diamcou(noumax*noemax*noemax),sigmacou(noumax*noemax*noemax)
#if SEDVAR
      real(wp) :: diamsouv,sigmsouv
#endif
#endif

      integer :: iouv,j,k


      !common/grav/g
      !common/hqouv/houv,qouv
      !common/iouvra/ie1,ie2,ia1,ia2,j1,j2,nouv,nbouv
      !common/param/eps1,eps2,paray
      !common/cflr/dt,cfl,dt0,t
      !common/volou/volouv
      !common/vouvh/vouv,vtouv
!cou1d      common/ouvcouple/couplage


            do k=nb1,nb2
              if(zcou(k).gt.touv)then
                if(k.eq.nb1)then
                  q=qcou(k)
#if TRANSPORT_SOLIDE
                  cc=ccou(k)
#if SEDVAR
                  if(sedvar)then
                  diamouvc=diamcou(k)
                  sigmaouvc=sigmacou(k)
                  endif
#endif
#endif
                else
                  q=qcou(k-1)+(touv-zcou(k-1))/(zcou(k)-zcou(k-1))*(qcou(k)-qcou(k-1))
#if TRANSPORT_SOLIDE
                  cc=ccou(k-1)+(t-zcou(k-1))/(zcou(k)-zcou(k-1))*&
     &(ccou(k)-ccou(k-1))
#if SEDVAR
                  if(sedvar)then
                  diamouvc=diamcou(k-1)+&
     &(t-zcou(k-1))/(zcou(k)-zcou(k-1))*&
     &(diamcou(k)-diamcou(k-1))
                  sigmaouvc=sigmacou(k-1)+&
     &(t-zcou(k-1))/(zcou(k)-zcou(k-1))*&
     &(sigmacou(k)-sigmacou(k-1))
                  endif
#endif
#endif
                endif
                go to 40
              endif
! fin du do sur k
            enddo
            q=qcou(nb2)
#if TRANSPORT_SOLIDE
            cc=ccou(nb2)
#if SEDVAR
                  if(sedvar)then
                  diamouvc=diamcou(nb2)
                  sigmaouvc=sigmacou(nb2)
                  endif
#endif
#endif
! chnagement de signe par rapport aux autres ouvrages
! rien n'est fait
! un debit positif signifie apport de debit dans la maille amont
! (changement de signe par rapport aux autres ouvrages)
 40          volouv(iouv)=volouv(iouv)-q
! qs est positif  et debit entrant dans 1
#if TRANSPORT_SOLIDE
              if(cc.lt.-eps2)then
! on prend la concentration de la arete amont car concentration non donnee
                cc=coouv(ia1(iouv),j1(iouv))
              endif
            qs=q*cc
            voaouv(iouv)=voaouv(iouv)-qs
#if SEDVAR
            if(sedvar)then
! qs et qsouv sont positifs
               call melangeds(qsouv,qs,diamsouv,sigmsouv,diamouvc,sigmaouvc)
            else
#endif
               qsouv=qsouv+qs
#if SEDVAR
            endif
#endif
#endif
            if(j1(iouv).eq.1)then
             qouv(ia1(iouv),j1(iouv))=qouv(ia1(iouv),j1(iouv))-q
            else
             qouv(ia1(iouv),j1(iouv))=qouv(ia1(iouv),j1(iouv))+q
            endif
           return

      end subroutine qqouvr

!**********************************************************************
      subroutine qbouvr(iouv,ibouv,touv,ext1,ext2,ieamont,ieaval&
#if TRANSPORT_SOLIDE
     &,qsouv&
#if SEDVAR
     &,diamsouv,sigmsouv&
#endif
#endif
      &)
!---------------------------------------------------------------------c
!     calcule les debits transitant par les ouvrages b
!=====================================================================c

      use module_tableaux, only:houv,qouv,zfa,nobmax,noumax,noemax,&
     &ntrmax,g,ie1,ie2,ia1,ia2,j1,j2,nouv,nbouv,eps1,eps2,paray,&
     &volouv,zc,zp,alp,alc,z0,d50,rho,phi,db0,zb0,eta,ka,c1,c2,dt2,&
     &nt,ym,sm,pm,rhm,alm,nu,zb,db,it,largmail,trect,ql,qs,dbr,z,&
     &zav,zbr,tini,kappa,dt,cfl,dt0,t,zfe,het
#if TRANSPORT_SOLIDE
      use module_ts,only : coouv,voaouv,diamouv,sigmaouv,&
#if SEDVAR
     &sedvar,&
#endif
     &sigma0
#endif
      use module_precision

      implicit none

      real(wp) z1,z2,zg,zd,q
      real(wp) touv
      logical ext1,ext2
      integer ieamont,ieaval

!      character*1 typouv(noumax,noemax)

      integer iouv,i(nobmax),ibouv
!      logical total
!cou1d       logical couplage(noumax)
      real(wp) tprec(nobmax)
      real(wp) q2
#if TRANSPORT_SOLIDE
      !logical avects
      real(wp) :: qsouv,qsa
#if SEDVAR
      real(wp) :: diamsouv,sigmsouv
#endif
#endif
      !common/grav/g
      !common/hqouv/houv,qouv
      !common/iouvra/ie1,ie2,ia1,ia2,j1,j2,nouv,nbouv
      !common/zare/zfa
      !common/param/eps1,eps2,paray
      !common/volou/volouv
!      common/fina/final
      !common/digue/zc,zp,alp,alc,z0,d50,rho,phi,db0,zb0
      !common/conrup/eta,ka,c1,c2,dt2
      !common/nconst/nt
      !common/moyen/ym,sm,pm,rhm,alm,nu
      !common/breche/zb,db,it
      !common/maxbre/largmail,trect
      !common/resul/ql,qs,dbr,z,zav,zbr
      !common/tinib/tini
      !common/lag/kappa
      !common/cflr/dt,cfl,dt0,t
!cou1d      common/ouvcouple/couplage

      data i/nobmax*1/

      save tprec,i

      if (touv.lt.tini(ibouv)) then
        return
      else
!---- definition de la cote gauche et droite---
           if(ext1)then
            z1=het(ieamont)+zfe(ieamont)
           else
         z1=houv(ia1(iouv),j1(iouv))+zfa(ia1(iouv))
           endif
         if(ia2(iouv).gt.0)then
           if(ext2)then
             z2=het(ieaval)+zfe(ieaval)
           else
             z2=houv(ia2(iouv),j2(iouv))+zfa(ia2(iouv))
           endif
!cou1d         elseif(couplage(iouv))then
!cou1d           z2=houv(ia1(iouv),j2(iouv))+zfa(ia1(iouv))
       else
           z2=-99999.999
         endif
         if(z1.gt.z2+paray)then
           if(houv(ia1(iouv),j1(iouv)).gt.paray)then
             zg=z1
             zd=z2
              elseif(ext1)then
! controle par niveau sans appel a hauteur d'eau
               zg=z1
               zd=z2
           else
             return
           endif
         elseif(z2.gt.z1+paray)then
!cou1d       if(couplage(iouv))then
!cou1d             zg=z2
!cou1d             zd=z1
!cou1d           else
           if(houv(ia2(iouv),j2(iouv)).gt.paray)then
             zg=z2
             zd=z1
              elseif(ext2)then
! controle par niveau sans appel a hauteur d'eau
               zg=z2
               zd=z1
           else
             return
           endif
!cou1d           endif
         else
           return
         endif

!    boucle de calcul

!  (initialisation des variables)
      if (i(ibouv).eq.1) then
        zav(i(ibouv),ibouv)=zd
        z(i(ibouv),ibouv)=zg
        dbr(i(ibouv),ibouv)=db(ibouv)
        zbr(i(ibouv),ibouv)=zb(ibouv)
        qs(i(ibouv),ibouv)=0.
        ql(i(ibouv),ibouv)=0.
        tprec(ibouv)=tini(ibouv)
        i(ibouv)=2
        zav(i(ibouv),ibouv)=zd
        z(i(ibouv),ibouv)=zg
        dbr(i(ibouv),ibouv)=db(ibouv)
        zbr(i(ibouv),ibouv)=zb(ibouv)
        qs(i(ibouv),ibouv)=0.
        ql(i(ibouv),ibouv)=0.
      endif


      call calcul(zg,q,q2,zd,ibouv)
!  (boucle de changement de geometrie d''erosion)
      if (touv .gt. tprec(ibouv)) then
        zav(i(ibouv),ibouv)=zd
        z(i(ibouv),ibouv)=zg
        zbr(i(ibouv),ibouv)=zb(ibouv)
        dbr(i(ibouv),ibouv)=db(ibouv)
        ql(i(ibouv),ibouv)=q
        qs(i(ibouv),ibouv)=q2
! renard
        if (it(ibouv).eq.0) then
! effondrement voute
          if (db(ibouv).ge.0.66666667*(zc(ibouv)-zb0(ibouv))) then
            kappa(ibouv)=.true.
            it(ibouv)=1
            if (ym(ibouv) .gt. 0.) then
            db(ibouv)=sm(ibouv)/ym(ibouv)
            endif
            dbr(i(ibouv),ibouv)=db(ibouv)
            write(*,*)
#if ENG
            write(*,120)&
     &'passing from pipe to rect. at time',touv
  120       format(a40,f10.0)
            write(*,*)'work ',iouv,' breach ',ibouv
#else
            write(*,120)&
     &'passage en elargissement rectangulaire au temps',touv
  120       format(a40,f10.0)
            write(*,*)'ouvrage ',iouv,' breche ',ibouv
#endif
            trect(ibouv)=touv
            write(*,*)
          endif
! surverse en approfondissement
        elseif (it(ibouv).lt.0) then
!           if (r(i).lt.zp)r(i)=zp
! basculement en elargissement ramene de sp calcul
            if (zb(ibouv).lt.zp(ibouv)) then
              it(ibouv)=-it(ibouv)
              zb(ibouv)=zp(ibouv)
              zbr(i(ibouv),ibouv)=zb(ibouv)
            write(*,*)
#if ENG
            write(*,120)&
     &'passing to widening at time',touv
            write(*,*)'work ',iouv,' breach ',ibouv
#else
            write(*,120)&
     &'passage en elargissement rectangulaire au temps',touv
            write(*,*)'ouvrage ',iouv,' breche ',ibouv
#endif
            trect(ibouv)=touv
            write(*,*)
            endif
!     si(it .gt. 0)rien
        endif
        if (touv .gt. float(i(ibouv)-1)*dt2(ibouv)+tini(ibouv)) then
!     :.and.t .gt. tprec(ibouv)
!     :) then
!  (boucle d''incrementation)
! on enregistre jusqua ntrmax et pas nt pour le cas de fausse manoeuvre
          if (i(ibouv).lt.ntrmax) then
            i(ibouv)=i(ibouv)+1
            dbr(i(ibouv),ibouv)=dbr(i(ibouv)-1,ibouv)
            zbr(i(ibouv),ibouv)=zbr(i(ibouv)-1,ibouv)
          endif
        endif
        tprec(ibouv)=touv
! else du if sur t et tprec pour changement geometrie
! dans ce cas on revient en arriere
      else
        zb(ibouv)=zbr(i(ibouv),ibouv)
        db(ibouv)=dbr(i(ibouv),ibouv)
! fin du if sur t et tprec pour changement geometrie
      endif

!      tprec(ibouv)=touv
! enregistrement du debit q calcule
         if (z1 .gt. z2) then
            volouv(iouv)=volouv(iouv)+q
! qs est positif si courant de 1 vers 2 et inversement
#if TRANSPORT_SOLIDE
            qsa=q*coouv(ia1(iouv),j1(iouv))
            qsa=qsa+qs(i(ibouv),ibouv)
            voaouv(iouv)=voaouv(iouv)+qsa
#endif
! mis en commentaire car ia2 non nul dans ce cas
            if(ia2(iouv).gt.0)then
#if TRANSPORT_SOLIDE
!            coouv(ia2(iouv),j2(iouv))=coouv(ia1(iouv),j1(iouv))
#endif
            if(j2(iouv).eq.1)then
             qouv(ia2(iouv),j2(iouv))=qouv(ia2(iouv),j2(iouv))-q
            else
             qouv(ia2(iouv),j2(iouv))=qouv(ia2(iouv),j2(iouv))+q
            endif
!cou1d      elseif(couplage(iouv))then
#if TRANSPORT_SOLIDE
!cou1d!            coouv(ia1(iouv),j2(iouv))=coouv(ia1(iouv),j1(iouv))
#endif
!cou1d            if(j2(iouv).eq.1)then
!cou1d             qouv(ia1(iouv),j2(iouv))=qouv(ia1(iouv),j2(iouv))-q
!cou1d            else
!cou1d             qouv(ia1(iouv),j2(iouv))=qouv(ia1(iouv),j2(iouv))+q
!cou1d            endif
! fin du if sur ia2
            endif
#if TRANSPORT_SOLIDE
#if SEDVAR
            if(sedvar)then
! qs et qsouv sont positifs
               call melangeds(qsouv,qsa,diamsouv,sigmsouv,d50(ibouv),sigma0)
            else
#endif
               qsouv=qsouv+qsa
#if SEDVAR
            endif
#endif
#endif
            if(j1(iouv).eq.1)then
             qouv(ia1(iouv),j1(iouv))=qouv(ia1(iouv),j1(iouv))+q
            else
             qouv(ia1(iouv),j1(iouv))=qouv(ia1(iouv),j1(iouv))-q
            endif
! si z2 >z1
           else
            volouv(iouv)=volouv(iouv)-q
! mis en commentaire car ia2 non nul dans ce cas sauf si couplage
!cou1d            if(ia2(iouv).gt.0)then
#if TRANSPORT_SOLIDE
            qsa=q*coouv(ia2(iouv),j2(iouv))
            qsa=qsa+qs(i(ibouv),ibouv)
            voaouv(iouv)=voaouv(iouv)-qsa
#if SEDVAR
            if(sedvar)then
! qs et qsouv sont positifs
               call melangeds(qsouv,qsa,diamsouv,sigmsouv,d50(ibouv),sigma0)
            else
#endif
               qsouv=qsouv+qsa
#if SEDVAR
            endif
#endif
#endif
            if(j2(iouv).eq.1)then
             qouv(ia2(iouv),j2(iouv))=qouv(ia2(iouv),j2(iouv))+q
            else
             qouv(ia2(iouv),j2(iouv))=qouv(ia2(iouv),j2(iouv))-q
            endif
! si ia2=0 et pas de couplage (ne doit jamais arriver normalement)
!cou1d            else
#if TRANSPORT_SOLIDE
!cou1d            qsa=q*coouv(ia1(iouv),j2(iouv))
!cou1d            qsa=qsa+qs(i(ibouv),ibouv)
!cou1d            voaouv(iouv)=voaouv(iouv)-qsa
#if SEDVAR
!cou1d            if(sedvar)then
!cou1d! qs et qsouv sont positifs
!cou1d               call melangeds(qsouv,qsa,diamsouv,sigmsouv,d50(ibouv),sigma0)
!cou1d            else
#endif
!cou1d               qsouv=qsouv+qsa
#if SEDVAR
!cou1d            endif
#endif
#endif
!cou1d            if(j2(iouv).eq.1)then
!cou1d             qouv(ia1(iouv),j2(iouv))=qouv(ia1(iouv),j2(iouv))+q
!cou1d            else
!cou1d             qouv(ia1(iouv),j2(iouv))=qouv(ia1(iouv),j2(iouv))-q
!cou1d            endif
! fin du if sur ia2
!cou1d            endif
            if(j1(iouv).eq.1)then
             qouv(ia1(iouv),j1(iouv))=qouv(ia1(iouv),j1(iouv))-q
            else
             qouv(ia1(iouv),j1(iouv))=qouv(ia1(iouv),j1(iouv))+q
            endif
! fin du if sur z1 et z2
           endif
! fin du if sur touv superieur a tini
      endif
      return
      end subroutine qbouvr

!----------------------------------------------------------------------------------------------------------
      subroutine calcul(z,q,qs,zd,ibouv)

      use module_tableaux, only:zc,zp,alp,alc,z0,d50,rho,phi,db0,zb0,&
     &eta,ka,c1,c2,dt2,nt,ym,sm,pm,rhm,alm,nu,zb,db,it,g,dt,cfl,dt0,t&
     &,largmail,trect,elap,suivan,dbprec,tini,tm,tr,tinit,tmax,nobmax
      use module_precision

      implicit none

      integer ibouv
!      real(wp) q2
      real(wp) z,qs

      real(wp) s2,v0,beta,s3,rtau
! beta vaut pi/4
      parameter (beta=0.7854)
      real(wp) zd,q

      !common/digue/zc,zp,alp,alc,z0,d50,rho,phi,db0,zb0
      !common/conrup/eta,ka,c1,c2,dt2
      !common/nconst/nt
      !common/moyen/ym,sm,pm,rhm,alm,nu
      !common/breche/zb,db,it
      !common/cflr/dt,cfl,dt0,t
      !common/grav/g
      !common/maxbre/largmail,trect
      !common/elappr/elap,suivan
      !common/dbprec/dbprec
      !common/tinib/tini
      !common/tenv/tm,tr,tinit,tmax

! surface erosion berges utilisee uniquement en premiere phase de surverse (it negatif)
      s3=0.
! rapport des tau-tauc **1.5 entre fond et berges = rapport d'epaisseur erosion
      rtau=0.
!  calcul de l''elargissement de la breche
      call debit(z,q,qs,zd,rtau,ibouv)
      call surf(s2,s3,ibouv)
! calcul elargissement et approfondissement
      if (it(ibouv).lt.0) then
! si elargissement et approfondissement
        if (elap(ibouv)) then
          v0=qs/(s2+rtau*s3)
          db(ibouv)=db(ibouv)+2.*rtau*v0*dt
        else
          v0=qs/s2
!          db(ibouv)=db(ibouv)
        endif
        zb(ibouv)=zb(ibouv)-v0*dt
! calcul elargissement renard ou breche rectangulaire
      else
!      if (it.ge.0)
        v0=qs/s2
        db(ibouv)=db(ibouv)+2.*v0*dt
      endif
! on limite la largeur de la breche soit rectangulaire soit diametre renard
! a la largeur de la maille
       if (db(ibouv) .gt. largmail(ibouv)) then
         db(ibouv)=largmail(ibouv)
         if (suivan(ibouv)) then
           suivan(ibouv)=.false.
           if (ibouv.ne.nobmax) then
            if (suivan(ibouv+1)) then
             if (tini(ibouv+1) .gt. tmax) then
               dbprec(ibouv+1)=dbprec(ibouv)+largmail(ibouv)
               tini(ibouv+1)=t
             endif
            endif
           endif
         endif
! approximation grossiere
         qs=0.
       endif
!  calcul de nu a la fin du pas de temps
      if (rhm(ibouv) .gt. 0.) then
        nu(ibouv)=2.*g*alm(ibouv)/(ka(ibouv)**2&
     &*rhm(ibouv)**1.3333333)
      else
        nu(ibouv)=0.
      endif
      return
      end subroutine calcul

!----------------------------------------------------------
      subroutine debit(z,q,q2,zd,rtau,ibouv)
! retourne q, q2=qs,rtau et zd si on obtient un zavi<zd
      use module_tableaux, only:zc,zp,alp,alc,z0,d50,rho,phi,db0,zb0&
     &,eta,ka,c1,c2,dt2,nt,ym,sm,pm,rhm,alm,nu,zb,db,it,elap,suivan,&
     &g,nobmax,dbprec,mb,mblineaire
      use module_precision

      implicit none

      integer ibouv
      integer i
      real(wp) dbtotal
      real(wp) beta
      parameter (beta=3.1416*0.25)

      real(wp) z,h,q,q2,y,y0,y1,y2,ye,r,s,se,sr,teta,al
      real(wp) df,f,zavi,f1,c,zd,rtau,qs1,qs2

      !common/digue/zc,zp,alp,alc,z0,d50,rho,phi,db0,zb0
      !common/conrup/eta,ka,c1,c2,dt2
      !common/nconst/nt
      !common/moyen/ym,sm,pm,rhm,alm,nu
      !common/breche/zb,db,it
      !common/elappr/elap,suivan
      !common/grav/g
      !common/dbprec/dbprec

!   ce programme calcule les debits liquide et solide
!   ainsi que les valeurs moyennes caracterisant
!   l ecoulement
!    variables
!   it=0 circulaire              db=diametre
!   it=1 ou -1 rectangulaire  db=largeur
!   zb=cote de la breche
!   z=cote du plan d eau
!   q=debit liquide
!   q2=debit solide
!   ym,sm,pm,rhm,alm,nu;hauteur,section,perimetre mouille
!   rayon hydraulique,longueur d ecoulement,coefficient de
!   perte de charge en valeurs moyennes

!    on ne fait pas la correction pour ne pas interferer avec le chnagement dans it
!      if (zb(ibouv).le.zp(ibouv))zb(ibouv)=zp(ibouv)
      h=z-zb(ibouv)
      if (h.le.0.) then
        q=0.
        q2=0.
        z=zb(ibouv)
        ym(ibouv)=0.
        return
      else
        if (it(ibouv).ne.0) then
!  calcul en rectangulaire
          y=0.66666666667*h
!   calcul de y par une methode de newton
          do 10 i=1,100
            ym(ibouv)=0.5*(y+h)
            c=1+(eta(ibouv)/(h*h)+nu(ibouv)/&
     &(ym(ibouv)**2))*y**2
            f=y+0.5*y*c-h
            df=1.5*c-0.5*nu(ibouv)*y**3/&
     &(ym(ibouv)**3)
            y1=y-f/df
            if (y1.lt.0.)y1=0.
            if (abs(y-y1).lt.0.001)go to 11
            y=y1
   10     continue
   11     y=y1
!   calcul des caracteristiques moyennes
          ym(ibouv)=0.5*(y+h)
          f1=1+y*y*(eta(ibouv)/(h*h)+nu(ibouv)/&
     &(ym(ibouv)*ym(ibouv)))
            dbtotal=db(ibouv)+dbprec(ibouv)
            sm(ibouv)=dbtotal*ym(ibouv)
            pm(ibouv)=dbtotal+2.*ym(ibouv)
            rhm(ibouv)=sm(ibouv)/pm(ibouv)
          if (zb(ibouv)+ym(ibouv).lt.zc(ibouv)) then
            alm(ibouv)=alc(ibouv)+((alp(ibouv)-alc(ibouv))&
     &/(zp(ibouv)-zc(ibouv)))*(zb(ibouv)+ym(ibouv)-zc(ibouv))
          else
            alm(ibouv)=alc(ibouv)
          endif
          s=dbtotal*y
        else
!  calcul en circulaire
          r=0.5*db(ibouv)
!  calcul de la section d entree
          ye=2.*r
          se=3.1416*r*r
          if (h.ge.2.*r)go to 21
          ye=h
          teta=acos(1.-h/r)
          se=r*r*(teta-sin(teta)*cos(teta))
   21     y1=ye
          y0=0.
!   calcul de dichotomie pour trouver y aval
          do 23 i=1,20
            y=0.5*(y1+y0)
            teta=acos(1.-y/r)
            s=r*r*(teta-sin(teta)*cos(teta))
            al=2.*r*sin(teta)
            sm(ibouv)=0.5*(se+s)
            f1=1+s*s*(eta(ibouv)/(se*se)+nu(ibouv)/&
     &(sm(ibouv)*sm(ibouv)))
            f=y-h+0.5*f1*s/al
            if (f.eq.0.)go to 24
            if (f.lt.0.)go to 22
            y1=y
            go to 23
   22       y0=y
   23     continue
   24     sm(ibouv)=0.5*(s+se)
!   calcul des caracteristiques moyennes
          sr=sm(ibouv)/(r*r)
          if (sr.ge.3.1416)go to 26
          y1=1.
          do 25 i=1,100
            teta=acos(1.-y1)
            f=teta-sin(teta)*cos(teta)-sr
            y2=y1-f/(2.*sin(teta))
            if (y2.ge.2.)go to 26
            if (y2.le.0.)y2=0.01
            if (abs(y1-y2).lt.0.001)go to 27
            y1=y2
   25     continue
   26     ym(ibouv)=2.*r
          go to 28
   27     ym(ibouv)=y1*r
   28     teta=acos(1.-ym(ibouv)/r)
          pm(ibouv)=2.*r*teta
          sm(ibouv)=r*r*(teta-sin(teta)*cos(teta))
          rhm(ibouv)=sm(ibouv)/pm(ibouv)
          if (zb(ibouv)+ym(ibouv).lt.zc(ibouv)) then
            alm(ibouv)=alc(ibouv)+((alp(ibouv)-alc(ibouv))&
     &/(zp(ibouv)-zc(ibouv)))*(zb(ibouv)+ym(ibouv)-zc(ibouv))
          else
            alm(ibouv)=alc(ibouv)
          endif
! fin du if sur rectangulaire/circulaire
        endif
        zavi=y+zb(ibouv)

!   on reprend le calcul si z a droite est plus grand que z aval (critique)

        if (zavi.lt.zd) then
          if (it(ibouv).ne.0) then
!  calcul en rectangulaire
            y=zd-zb(ibouv)
            if (y.le.0.) then
              zd=zb(ibouv)
              q=0.
              q2=0.
              return
            endif
!   calcul des caracteristiques moyennes
            ym(ibouv)=0.5*(y+h)
            dbtotal=db(ibouv)+dbprec(ibouv)
            sm(ibouv)=dbtotal*ym(ibouv)
            pm(ibouv)=dbtotal+2.*ym(ibouv)
            rhm(ibouv)=sm(ibouv)/pm(ibouv)
          if (zb(ibouv)+ym(ibouv).lt.zc(ibouv)) then
            alm(ibouv)=alc(ibouv)+((alp(ibouv)-alc(ibouv))&
     &/(zp(ibouv)-zc(ibouv)))*(zb(ibouv)+ym(ibouv)-zc(ibouv))
          else
            alm(ibouv)=alc(ibouv)
          endif
            s=dbtotal*y
          else
!  calcul en circulaire
            r=0.5*db(ibouv)
!  calcul de la section d entree
            ye=2.*r
            if (h.lt.2.*r)ye=h
            y=zd-zb(ibouv)
            if (y.lt.0.) then
              zd=zb(ibouv)
              q=0.
              q2=0.
              return
            endif
            if (y.ge.2.*r)y=2.*r
!   calcul des caracteristiques moyennes
            ym(ibouv)=0.5*(y+ye)
            if (ym(ibouv).lt.2.*r) then
              teta=acos(1.-ym(ibouv)/r)
              pm(ibouv)=2.*r*teta
              sm(ibouv)=r*r*(teta-sin(teta)*cos(teta))
            else
              ym(ibouv)=2.*r
              pm(ibouv)=6.2832*r
              sm(ibouv)=3.1416*r*r
            endif
            rhm(ibouv)=sm(ibouv)/pm(ibouv)
          if (zb(ibouv)+ym(ibouv).lt.zc(ibouv)) then
            alm(ibouv)=alc(ibouv)+((alp(ibouv)-alc(ibouv))&
     &/(zp(ibouv)-zc(ibouv)))*(zb(ibouv)+ym(ibouv)-zc(ibouv))
          else
            alm(ibouv)=alc(ibouv)
          endif
! fin du if sur rectangulaire/circulaire
          endif
! else du if sur zavi>zd
! on enregistre dans zd la cote aval breche (critique)
        else
          zd=zavi
! fin du if sur zavi>zd
        endif

!  calcul des debits
        if (y .gt. h)y=h
        q=s*sqrt(2.*g*(h-y)/f1)
        qs1=q/(sm(ibouv)*rhm(ibouv)**(1./6.))
! correction sur Q si dbprec non nul
        if(s.gt.0.)then
          if (it(ibouv).ne.0) then
            q=q*y*db(ibouv)/s
          endif
        endif
!        taui(ibouv)=qs1*qs1/c2*0.047*(rho(ibouv)-1000.)*g*d50(ibouv)
        q2=qs1*qs1-c2(ibouv)
        if (q2.le.0.) then
          q2=0.
        elseif (it(ibouv).eq.0) then
! circulaire
          if(mblineaire(ibouv))then
            q2=mb(ibouv)*alm(ibouv)*pm(ibouv)*q2/c2(ibouv)
          else
            q2=c1(ibouv)*pm(ibouv)*q2**1.5
          endif
        elseif (.not.elap(ibouv)) then
! mode approfondissement seul et contrainte non reduite
         if(mblineaire(ibouv))then
          q2=mb(ibouv)*alm(ibouv)*pm(ibouv)*q2/c2(ibouv)
         else
          q2=c1(ibouv)*pm(ibouv)*q2**1.5
         endif
        else
! qs1 debit sur le fond
! qs2 debit sur les cotes
! on r�duit la contrainte laterale a 0.6 contrainte moyenne pour largeur infinie
! article de knight et formulation simplifiee
          dbtotal=db(ibouv)+dbprec(ibouv)
          if (dbtotal.lt.2.*ym(ibouv)) then
            qs2=q2
          elseif (0.083*dbtotal.lt.ym(ibouv)) then
            qs2=(1.06-0.03*dbtotal/ym(ibouv))*qs1*qs1-c2(ibouv)
!310321            qs2=(1.06-0.03*db(ibouv)/ym(ibouv))*qs1*qs1-c2(ibouv)
          else
            qs2=(0.6+ym(ibouv)/(0.83*dbtotal))*qs1*qs1-c2(ibouv)
!310321            qs2=(0.6+ym(ibouv)/(0.83*db(ibouv)))*qs1*qs1-c2(ibouv)
          endif
          if (qs2.le.0.) qs2=0.
          if (it(ibouv).lt.0) then
            qs1=qs1*qs1-c2(ibouv)
            if (qs1.le.0.) then
              qs1=0.
              rtau=0.
            else
              rtau=(qs2/qs1)**1.5
            endif
! si on a atteint le fond, le debit unitaire est pris uniquement sur les cotes
          else
            qs1=0.
          endif
!010618 introduction erosion lineaire
         if(mblineaire(ibouv))then
           q2=(db(ibouv)*qs1+2.*ym(ibouv)*qs2)/c2(ibouv)
           q2=mb(ibouv)*alm(ibouv)*q2
         else
          q2=c1(ibouv)*(db(ibouv)*qs1**1.5+2.*ym(ibouv)*qs2**1.5)
         endif
! fin du if sur type de calcul du debit
        endif
        return
! fin du if sur h positif
      endif
      end subroutine debit

!---------------------------------------------------------------
      subroutine surf(s2,s3,ibouv)

      use module_tableaux, only:zc,zp,alp,alc,z0,d50,rho,phi,db0,zb0,&
     &zb,db,it,elap,suivan
      use module_precision

      implicit none

      integer ibouv
      real(wp) s2,s3

      !common/digue/zc,zp,alp,alc,z0,d50,rho,phi,db0,zb0
      !common/breche/zb,db,it
      !common/elappr/elap,suivan

!  ce sous programme calcule la superficie erodable (s2)
!  pour zb et db donnes
      if (it(ibouv).lt.0) then
!  calcul en rectangulaire (submersion, erosion vers le bas et eventuellement sur le cote)
        s2=(alc(ibouv)-(zc(ibouv)-zb(ibouv))*(alp(ibouv)-alc(ibouv))/(zp(ibouv)-zc(ibouv)))*db(ibouv)
        if (elap(ibouv)) then
! erosion berges
          s3=(alc(ibouv)+0.5*(zb(ibouv)-zc(ibouv))&
     &*(alp(ibouv)-alc(ibouv))/(zp(ibouv)-zc(ibouv)))&
     &*(zc(ibouv)-zb(ibouv))
! on met dans s3 la surface des deux cotes
          s3=2.*s3
        else
          s3=0.
        endif
      elseif (it(ibouv) .gt. 0) then
!  calcul en rectangulaire (renard ou submersion, elargissement breche)
        s2=(alc(ibouv)+0.5*(zb(ibouv)-zc(ibouv))&
     &*(alp(ibouv)-alc(ibouv))/(zp(ibouv)-zc(ibouv)))&
     &*(zc(ibouv)-zb(ibouv))
! on met dans s2 la surface des deux cotes (=surface erosion comme en renard)
          s2=2.*s2
        s3=0.
      else
!  calcul en circulaire (-renard-)avec db diametre
        s2=3.1416*db(ibouv)*(alp(ibouv)+(zb(ibouv)-zp(ibouv))&
     &*(alc(ibouv)-alp(ibouv))/(zc(ibouv)-zp(ibouv))&
     &+0.75*db(ibouv)*(alc(ibouv)-alp(ibouv))/(zc(ibouv)-zp(ibouv)))
! valeur 0.75 et pas 0.5 ??: valeur differente dans le bilan
        s3=0.
! fin du if sur forme breche
      endif
      s2=s2*(1.-phi(ibouv))
      s3=s3*(1.-phi(ibouv))
      return
      end subroutine surf

      subroutine impri(ibouv)
!=======================================================================
! impression
!=======================================================================

      use module_tableaux
      implicit none
      !include 'rubar20_common.for'

! prototype
      integer :: ibouv
! variables locales
      integer :: m1,m2,m3,i
      integer :: imp(nobmax)
      real(wp) :: qst,tt
      real(wp) :: lbef,dbf


      imp(ibouv) = id_res+ibouv
      qst = 0.
      if (trect(ibouv) .gt. tini(ibouv)) then
         write(imp(ibouv),*)&
#if ENG
     &'GOING TO RECTANGULAR BREACH AT TIME ',trect(ibouv)
#else
     &'PASSAGE EN ELARGISSEMENT RECTANGULAIRE AU TEMPS ',trect(ibouv)
#endif
      endif
      write(imp(ibouv),*)
      write(imp(ibouv),*)
      write(imp(ibouv),'(a28,a43)')&
#if ENG
     &'   HR   MN    S      WATER Z    BREACH Z  WIDTH BREACH FLOW  SOLID FLOW'
#else
     &'   HR   MN    S     COTE EAU COTE BRECHE  LARG BRECHE  DEBIT  DEBIT SOL'
#endif
      lbef = 0.
      do i = 1, nt(ibouv)
         tt = float(i-1)*dt2(ibouv)+tini(ibouv)
         m1 = int(tt/3600.)
         m2 = int((tt-3600.*float(m1))/60.)
         m3 = int(tt-3600.*float(m1)-60.*float(m2))

!          write(imp(ibouv),101) m1,m2,m3,z(i,ibouv),zbr(i,ibouv),&
!      &                         dbr(i,ibouv),ql(i,ibouv),qs(i,ibouv)
! on cherche la largeur breche apres effondrement renard pour calcul volume
         if (tt .lt. trect(ibouv)) then
           lbef = dbr(i,ibouv)
! a priori inutile et deja initialise
!          else
!            lbef = 0.
         endif
! calcul du volume erode
         qst = qst+qs(i,ibouv)
      enddo
      qst = qst*dt2(ibouv)
#if ENG
      write(imp(ibouv),'(a,f10.0)')' ERODED VOLUME =', qst
#else
      write(imp(ibouv),'(a,f10.0)')' VOLUME ERODE =', qst
#endif

! calcul  (volume initial digue - volume final digue = volume parti) -----
      if (it(ibouv) .ne. 0) then
         if (kappa(ibouv)) then
! --- premier cas : si effondrement du renard
! dans ce cas, le volume calcule est faux car on ne tient pas compte du volume
! lors de l'effondrement
! nouvelle methode doit etre verifiee
! calcul volume pour le renard jusqu'a effondrement
            dbf = (zc(ibouv)-zp(ibouv))/3.
            qst = 3.1416*dbf*dbf*(alc(ibouv)+(alp(ibouv)-alc(ibouv))&
     &             *(zb(ibouv)+dbf-zc(ibouv))/(zp(ibouv)-zc(ibouv)))&
     &             *(1.-phi(ibouv))
            qst = qst-3.1416*db0(ibouv)*db0(ibouv)*(alc(ibouv)+&
     &             (alp(ibouv)-alc(ibouv))*(zb0(ibouv)+db0(ibouv)-&
     &              zc(ibouv))/(zp(ibouv)-zc(ibouv)))*(1.-phi(ibouv))
! puis ajout du volume en rectangulaire   allant jusqu au fond
            qst = qst+(db(ibouv)-lbef)*(alc(ibouv)+.5*(alp(ibouv)-&
     &               alc(ibouv)))*(zc(ibouv)-zp(ibouv))*(1.-phi(ibouv))
         else
! --- second cas : si surverse
            qst = db(ibouv)*(alc(ibouv)+.5*(alp(ibouv)-alc(ibouv))&
     &                     *(zb(ibouv)-zc(ibouv))/(zp(ibouv)-zc(ibouv)))&
     &                     *(zc(ibouv)-zb(ibouv))*(1.-phi(ibouv))
            qst = qst-db0(ibouv)*(alc(ibouv)+.5*(alp(ibouv)-alc(ibouv))&
     &               *(zb0(ibouv)-zc(ibouv))/(zp(ibouv)-zc(ibouv)))&
     &               *(zc(ibouv)-zb0(ibouv))*(1.-phi(ibouv))
         endif
      else
! --- troisieme cas : renard sans effondrement
         qst = 0.7854*db(ibouv)*db(ibouv)*(alc(ibouv)+&
     &               (alp(ibouv)-alc(ibouv))*(zb(ibouv)+0.5*db(ibouv)&
     &               -zc(ibouv))/(zp(ibouv)-zc(ibouv)))*(1.-phi(ibouv))
         qst = qst-3.1416*db0(ibouv)*db0(ibouv)*(alc(ibouv)&
     &            +(alp(ibouv)-alc(ibouv))*(zb0(ibouv)+db0(ibouv)&
     &            -zc(ibouv))/(zp(ibouv)-zc(ibouv)))*(1.-phi(ibouv))
      endif
#if ENG
      write (imp(ibouv),'(a)') ' TO BE COMPARED WITH '
      write (imp(ibouv),'(a,f10.0)')' MISSING VOLUME =',QST
#else
      write (imp(ibouv),'(a)') ' QU IL FAUT COMPARER AVEC'
      write (imp(ibouv),'(a,f10.0)')' VOLUME PARTI =',qst
#endif
      close(imp(ibouv))

 101  format(2x,i3,2x,i3,2x,i3,5x,5f10.4)

      end subroutine impri

!********************************************************************
      subroutine impri2 (t,i,ibouv)
! sauvegarde en meme temps que tps

      use module_tableaux, only:nobmax,ntrmax,ql,qs,dbr,z,zav,zbr,zfm,tini,id_res
      use module_precision

      implicit none

      integer ibouv
      integer i


      integer imp(nobmax)

      integer m1,m2,m3

      real(wp) t

      !common/resul/ql,qs,dbr,z,zav,zbr
      !common/zbas/zfm
      !common/tinib/tini

      imp(ibouv)=id_res+ibouv
      if (i.eq.1) then
        write(imp(ibouv),'(a)')&
#if ENG
     &'   HR   MN    S      WATER Z    BREACH Z  WIDTH BREACH FLOW  SOLID FLOW'
#else
     &'   HR   MN    S     COTE EAU COTE BRECHE  LARG BRECHE  DEBIT  DEBIT SOL'
#endif
      endif
      m1=int(t/3600.)
      m2=int((t-3600.*float(m1))/60.)
      m3=int(t-3600.*float(m1)-60.*float(m2))
!      m1=t/3600
!      m2=(t-3600.*m1)/60
!      m3=t-3600.*m1-60.*m2
      if (t.ge.tini(ibouv)) then
        write(imp(ibouv),101)m1,m2,m3,z(i,ibouv),zbr(i,ibouv)&
!        write(imp(ibouv),101)m1,m2,m3,z(i,ibouv)+zfm,zbr(i,ibouv)&
     &,dbr(i,ibouv),ql(i,ibouv),qs(i,ibouv)
      else
        write(imp(ibouv),101)m1,m2,m3,0.0,0.0&
     &,0.0,0.0,0.0
      endif
  101   format(2x,i3,2x,i3,2x,i3,5x,5f10.4)
      return
      end subroutine impri2

!---------------------------------------------------------
      subroutine lectes(iouv,ibouv)
! fin de lecture des donnees et calcul des constantes

      use module_tableaux, only: zfa,nobmax,etude,zc,zp,alp,alc,z0&
     &,d50,rho,phi,db0,zb0,eta,ka,c1,c2,dt2,nt,ym,sm,pm,rhm,alm,nu&
     &,ql,qs,dbr,z,zav,zbr,zb,db,it,tini,ie1,ie2,ia1,ia2,j1,j2,nouv&
     &,nbouv,zfm,g,dbprec,ntrmax,noumax,noemax,mb,mblineaire
      use module_precision

      implicit none

      integer ibouv,iamont,iouv
      integer lec
!     integer imp(nobmax),lec
!      integer noumax,noemax,lmax,ncmax,ntrmax
!      parameter(noumax=8500,noemax=5,lmax=1500,ncmax=20,ntrmax=9000)

      !common/etud/etude
      !common/digue/zc,zp,alp,alc,z0,d50,rho,phi,db0,zb0

      !common/conrup/eta,ka,c1,c2,dt2
      !common/nconst/nt
      !common/moyen/ym,sm,pm,rhm,alm,nu
      !common/resul/ql,qs,dbr,z,zav,zbr
      !common/breche/zb,db,it
      !common/tinib/tini

      !common/iouvra/ie1,ie2,ia1,ia2,j1,j2,nouv,nbouv
      !common/zare/zfa
      !common/zbas/zfm
      !common/grav/g
      !common/dbprec/dbprec

!      imp(ibouv)=id_res+ibouv
! lec=idon de louvr
      lec=9
!  lecture du fichier des caracteristiques de la digue
      read(lec,'(4f10.2)',err=5) d50(ibouv),ka(ibouv)&
     & ,rho(ibouv),phi(ibouv)
!010618 mb est le taux d'erosion si formulation lineaire
      read(lec,'(3f10.2)',err=5) zb0(ibouv),db0(ibouv),mb(ibouv)
      if(mb(ibouv).gt.0.0000000000001)then
         mblineaire(ibouv)=.true.
      else
         mblineaire(ibouv)=.false.
      endif
!      zb0(ibouv)=zb0(ibouv)-zfm
      zb(ibouv)=zb0(ibouv)
! d50 et db0 en mm passes en m
      d50(ibouv)=d50(ibouv)*0.001
      db0(ibouv)=db0(ibouv)*0.001
!  caracteristiques du calcul
      read(lec,103) dt2(ibouv),eta(ibouv),it(ibouv),nt(ibouv)
 103  format(2f6.2,i2,i4)
!  dt2 est le pas de stockage des variables d''erosion de la digue
!  nt est le nb de pas de temps pour les donnees d''erosion enregistrees
      if (nt(ibouv) .gt. ntrmax) then
#if ENG
         write (*,*)'THE NUMBER OF STORAGE TIME STEP IS TOO BIG'
#else
         write (*,*) 'LE NOMBRE DE PAS DE TEMPS DE STOCKAGE DEMANDE EST&
     & TROP GRAND'
#endif
         stop
       endif

! on suppose que l'amont etait bien l'amont de l'ouvrage
      iamont=ia1(iouv)
      z0(ibouv)=zfa(iamont)
!      z0=ctdf(iamont)+yn(iamont)

!    les constantes
      c2(ibouv)=0.001*rho(ibouv)-1.
      c1(ibouv)=8.*sqrt(g)/(c2(ibouv)*ka(ibouv)**3)
      c2(ibouv)=0.047*c2(ibouv)*d50(ibouv)*ka(ibouv)**2
      alm(ibouv)=alc(ibouv)+(alp(ibouv)-alc(ibouv))&
     & *(zb0(ibouv)-zc(ibouv))/(zp(ibouv)-zc(ibouv))
!----------------------------------------------------------------
!            modification : probleme de common; initialisation db
!----------------------------------------------------------------
      if (it(ibouv).lt.0) then
        if (z0(ibouv) .gt. zb0(ibouv)) then
          rhm(ibouv)=db0(ibouv)*(z0(ibouv)-zb0(ibouv))&
     &/(db0(ibouv)+2.*(z0(ibouv)-zb0(ibouv)))
          nu(ibouv)=2*g*alm(ibouv)/(ka(ibouv)**2*rhm(ibouv)**1.3333333)
        else
          nu(ibouv)=0.
          rhm(ibouv)=0.
        endif
        db(ibouv)=db0(ibouv)
      else
! en circulaire on suppose eau au dessus du renard
        rhm(ibouv)=0.5*db0(ibouv)
! db0 contenait le rayon mais db contient le diametre
        db(ibouv)=2.*db0(ibouv)

        nu(ibouv)=2*g*alm(ibouv)/(ka(ibouv)**2*rhm(ibouv)**1.3333333)
      endif
      dbprec(ibouv)=0.
      return
#if ENG
    5 write (*,*)'ERROR WHILE READING FILE ',ETUDE,'.ouv'
#else
    5 write (*,*)'ERREUR DANS LA LECTURE DU FICHIER ',trim(etude),'.ouv'
#endif
      stop
      end subroutine lectes

!---------------------------------------------------------
      subroutine impdon(iouv,ibouv)

      use module_tableaux, only: zfa,nobmax,noumax,noemax,ntrmax&
     &,zc,zp,alp,alc,z0,d50,rho,phi,db0,zb0,eta,ka,c1,c2,dt2,&
     &zb,db,it,ie1,ie2,ia1,ia2,j1,j2,nouv,nbouv,zfm,g,largmail,trect&
     &,tini,elap,suivan,mb,mblineaire,id_res
      use module_precision

      implicit none
! impression des donnees
      integer ibouv,iamont

      integer iouv

      integer imp(nobmax)

      real(wp) k1

      !common/digue/zc,zp,alp,alc,z0,d50,rho,phi,db0,zb0
      !common/conrup/eta,ka,c1,c2,dt2
      !common/breche/zb,db,it
      !common/iouvra/ie1,ie2,ia1,ia2,j1,j2,nouv,nbouv
      !common/zare/zfa
      !common/zbas/zfm
      !common/grav/g
      !common/maxbre/largmail,trect
      !common/tinib/tini
      !common/elappr/elap,suivan

      imp(ibouv)=id_res+ibouv
! on suppose que l'amont etait bien l'amont de l'ouvrage
      iamont=ia1(iouv)
      z0(ibouv)=zfa(iamont)

!  impression et test des donnees
      write(imp(ibouv),'(a,i2)')' it=',it(ibouv)
      if (it(ibouv).lt.0) write(imp(ibouv),'(a)')&
#if ENG
     &' OVERTOPPING BREACHING'
#else
     &' RUPTURE PAR SUBMERSION'
#endif
      if (it(ibouv).ge.0) write(imp(ibouv),'(a)')&
#if ENG
     &' PIPING'
#else
     &' RUPTURE PAR RENARD'
#endif
      if(elap(ibouv))then
#if ENG
        write(imp(ibouv),'(a)')'WITH REDUCED STRESS'
#else
        write(imp(ibouv),'(a)')'AVEC CONTRAINTE REDUITE'
#endif
      else
#if ENG
        write(imp(ibouv),'(a)')'WITH SIMPLE METHOD'
#else
        write(imp(ibouv),'(a)')'PAR METHODE SIMPLE'
#endif
      endif
      if(mblineaire(ibouv))then
#if ENG
      write(imp(ibouv),'(a,f10.8)')'WITH LINEAR EROSION COEFFICIENT '&
#else
      write(imp(ibouv),'(a,f10.8)')'AVEC EROSION LINEAIRE COEFFICIENT '&
#endif
     & ,mb(ibouv)
      else
#if ENG
        write(imp(ibouv),'(a)')'WITH EROSION MEYERPETERMULLER'
#else
        write(imp(ibouv),'(a)')'AVEC EROSION MEYERPETERMULLER'
#endif
      endif
      write(imp(ibouv),*)
#if ENG
      write(imp(ibouv),'(a,f10.2)')'  CREST ELEVATION =', zc(ibouv)
      write(imp(ibouv),'(a,f10.2)')'  CREST WIDTH =',alc(ibouv)
      write(imp(ibouv),'(a,f10.2)')'  BASEMENT ELEVATION =',zp(ibouv)
      write(imp(ibouv),'(a,f10.2)')'  BASEMENT WIDTH =',alp(ibouv)
      k1=21/d50(ibouv)**(1./6.)
      write(imp(ibouv),'(a,f10.8)')'  GRAIN DIAMETER GRAINS (d50) =',d50(ibouv)
      write(imp(ibouv),'(a,f4.2)')'  POROSITY =',phi(ibouv)
      write(imp(ibouv),'(a,f5.0)')'  VOLUMIC MASS =',rho(ibouv)
      write(imp(ibouv),'(a,f5.1)')'  STRICKLER SET =',ka(ibouv)
      write(imp(ibouv),'(a,f5.1)')'  STRICKLER ESTIMATED =',k1
      write(imp(ibouv),*)'MAXIMUM BREACH WIDTH = ', largmail(ibouv)
      write(imp(ibouv),*)'COEF UPSTREAM HEAD LOSS = ',eta(ibouv)
      write(imp(ibouv),*)'TIME BREACHING STARTS = ', tini(ibouv), ' s'
      write(imp(ibouv),*)
      write(imp(ibouv),'(a,f8.2)')'  BED ELEVATION =',Z0(IBOUV)

      write(imp(ibouv),'(a,f8.2)')'  BREACH ELEVATION =',zb0(ibouv)
      write(imp(ibouv),*)
      if (it(ibouv).lt.0) then
      write(imp(ibouv),'(a,f11.5)')'  BREACH WIDTH =',db0(ibouv)
      elseif (it(ibouv).ge.0)then
        write(imp(ibouv),'(a,f11.5)')'  PIPING RADIUS =',db0(ibouv)
      endif
      if ((zc(ibouv).le.zp(ibouv)).or.(alp(ibouv).le.alc(ibouv)))&
     & write(imp(ibouv),'(a)')' MISTAKE ABOUT LEVEE'
      if ((zb0(ibouv).ge.zc(ibouv)).or.(zb0(ibouv).lt.zp(ibouv)))&
     &write(imp(ibouv),'(a)')' MISTAKE ABOUT FIRST BREACH ELEVATION'
#else
      write(imp(ibouv),'(a,f10.2)')'  COTE EN CRETE =', zc(ibouv)
!      write(imp(ibouv),'(a,f10.2)')'  cote en crete =', zc(ibouv)+zfm
      write(imp(ibouv),'(a,f10.2)')'  LARGEUR EN CRETE =',alc(ibouv)
      write(imp(ibouv),'(a,f10.2)')'  COTE EN PIED =',zp(ibouv)
!      write(imp(ibouv),'(a,f10.2)')'  cote en pied =',zp(ibouv)+zfm
      write(imp(ibouv),'(a,f10.2)')'  LARGEUR EN PIED =',alp(ibouv)
      k1=21/d50(ibouv)**(1./6.)
      write(imp(ibouv),'(a,f10.8)')'  DIAMETRE DES GRAINS (d50) =',d50(ibouv)
      write(imp(ibouv),'(a,f4.2)')'  POROSITE =',phi(ibouv)
      write(imp(ibouv),'(a,f5.0)')'  MASSE VOLUMIQUE =',rho(ibouv)
      write(imp(ibouv),'(a,f5.1)')'  STRICKLER DONNE =',ka(ibouv)
      write(imp(ibouv),'(a,f5.1)')'  STRICKLER CALCULE =',k1
      write(imp(ibouv),*)'LARGEUR MAXIMALE DE BRECHE = ', largmail(ibouv)
      write(imp(ibouv),*)'COEF PERTE DE CHARGE AMONT = ',eta(ibouv)
      write(imp(ibouv),*)'TEMPS DE DEBUT DE RUPTURE = ', tini(ibouv), ' s'
      write(imp(ibouv),*)
      write(imp(ibouv),'(a,f8.2)')'  COTE DU TERRAIN NATUREL =',z0(ibouv)
!     :'  cote du terrain naturel =',z0(ibouv)+zfm

      write(imp(ibouv),'(a,f8.2)')'  COTE DE LA BRECHE =',zb0(ibouv)
!     :'  cote de la breche =',zb0(ibouv)+zfm
      write(imp(ibouv),*)
      if (it(ibouv).lt.0) then
      write(imp(ibouv),'(a,f11.5)')'  DIMENSION DE LA BRECHE =',db0(ibouv)
      elseif (it(ibouv).ge.0)then
        write(imp(ibouv),'(a,f11.5)')'  RAYON DU RENARD =',db0(ibouv)
      endif
      if ((zc(ibouv).le.zp(ibouv)).or.(alp(ibouv).le.alc(ibouv)))&
     &write(imp(ibouv),'(a)')' ANOMALIE SUR LA DIGUE'
      if ((zb0(ibouv).ge.zc(ibouv)).or.(zb0(ibouv).lt.zp(ibouv)))&
     &write(imp(ibouv),'(a)')' ANOMALIE SUR LA COTE INITIALE DE LA BRECHE'
#endif
      write(imp(ibouv),*)
      write(imp(ibouv),*)
      end subroutine impdon

!---------------------------------------------------------
      subroutine ouver(ibouv)

      use module_tableaux,only:etude,nobmax,id_res

      implicit none

      integer ibouv
      character*40 xlis,xbouv*3
      integer imp(nobmax)

      !common/etud/etude

!   ouverture du fichier de resultat en rupture progressive
      imp(ibouv)=id_res+ibouv
      if (ibouv.eq.1) then
        xlis=trim(etude)//'.res'
      else
        if (ibouv.lt.10) then
          write(xbouv,'(i1)')ibouv
        elseif (ibouv.lt.100) then
          write(xbouv,'(i2)')ibouv
        elseif (ibouv.lt.1000) then
          write(xbouv,'(i3)')ibouv
        endif
        xlis=trim(etude)//'.res'//xbouv
      endif
      open(imp(ibouv),file=xlis,status='unknown')
      end subroutine ouver

!---------------------------------------------------------
      subroutine calvolp(volp)

      use module_precision, only:wp
      use module_tableaux, only:napp,nchro,nbchro,se,la,nmchro,nmcoap&
     &,ne,na,nn,appchr,tappch,tm,tr,tinit,tmax

      implicit none
! calcule le volume total des apports pendant la simulation


      real(wp) volp
! variables relatives aux apports de pluie
! nombre maximal de chroniques et de nombre de couples par chronique
      integer ichro,ncouap,ie
      logical fin
      real(wp) vol (nmchro),apptm,appti
      integer no,nf


! variables relatives aux apports de pluie
      !common/nappor/napp,nchro,nbchro
      !common/apport/appchr,tappch
      !common/dose/se,la
      !common/tenv/tm,tr,tinit,tmax
      !common/nomb/ne,na,nn

! calcul du volume correspondant a chaque chronique en m�tres
      do ichro =1,nchro
        fin = .false.
        vol(ichro)=0.
        if (tappch(ichro,1) .gt. tinit) then
          no=0
        else
          no=1
          do ncouap=2,nbchro(ichro)
            if (tappch(ichro, ncouap).lt.tinit) then
              no=ncouap
            endif
          enddo
        endif
        if (tappch(ichro,1) .gt. tmax) then
          nf=0
        else
          nf=1
          do ncouap=2,nbchro(ichro)
            if (tappch(ichro, ncouap).lt.tinit) then
              nf=ncouap
            endif
          enddo
        endif
        if (nf.eq.0) then
            vol(ichro)=(tmax-tinit)*appchr(ichro,1)
!       write(*,*)'00 ',vol(ichro)
        elseif (no.eq.nbchro(ichro)) then
            vol(ichro)=(tmax-tinit)*appchr(ichro,nbchro(ichro))
!       write(*,*)'0 ',vol(ichro)
! cas ou les deux temps sont  dans le meme intervalle
       elseif (no.eq.nf) then
          if (no.eq.0) then
                appti=appchr(ichro,1)
          else
                appti=appchr(ichro,no+1)+&
     &(tinit-tappch(ichro,no+1))*&
     &(appchr(ichro,no)-appchr(ichro,no+1))&
     &/(tappch(ichro,no)-tappch(ichro,no+1))
          endif
          if (nf.eq.nbchro(ichro)) then
                apptm=appchr(ichro,nf)
          else
                apptm=appchr(ichro,nf+1)+&
     &(tmax-tappch(ichro,nf+1))*&
     &(appchr(ichro,nf)-appchr(ichro,nf+1))&
     &/(tappch(ichro,nf)-tappch(ichro,nf+1))
          endif
            vol(ichro)=0.5*(tmax-tinit)*(apptm+appti)

! cas normal
       else
          do ncouap= no+2,nf
              vol(ichro)=vol(ichro)+0.5*&
     &(tappch(ichro,ncouap)-tappch(ichro,ncouap-1))*&
     &(appchr(ichro,ncouap)+appchr(ichro,ncouap-1))
          enddo
          if (no.eq.0) then
                appti=appchr(ichro,1)
          else
                appti=appchr(ichro,no+1)+&
     &(tinit-tappch(ichro,no+1))*&
     &(appchr(ichro,no)-appchr(ichro,no+1))&
     &/(tappch(ichro,no)-tappch(ichro,no+1))
          endif
            vol(ichro)=vol(ichro)-0.5*&
     &(tinit-tappch(ichro,no+1))*(appti+appchr(ichro,no+1))
          if (nf.eq.nbchro(ichro)) then
                apptm=appchr(ichro,nf)
          else
                apptm=appchr(ichro,nf+1)+&
     &(tmax-tappch(ichro,nf+1))*&
     &(appchr(ichro,nf)-appchr(ichro,nf+1))&
     &/(tappch(ichro,nf)-tappch(ichro,nf+1))
          endif
            vol(ichro)=vol(ichro)+0.5*&
     &(tmax-tappch(ichro,nf))*(apptm+appchr(ichro,nf))
          endif
!        write(*,*)'ichro=',ichro,vol(ichro),appchr(ichro,1)
! fin de boucle sur les chroniques
      enddo
! calcul du volume sur tout le domaine
      volp=0.
      do ie=1,ne
        volp=volp+vol(napp(ie))*se(ie)
      enddo
!      s=0.
!      do ie=1,ne
!        s=s+se(ie)
!      enddo
!      write(*,*)'volp=',volp,s,napp(1)
      end subroutine calvolp

!*****************************************************************************
      subroutine wrnu(t)
!******************************************************************************
!     edition resultats vf2
      use module_tableaux, only: xa,ya,ncvi,etude,ne,na,nn,debut,id_nua&
     &,nouveau_format
      use module_precision

      implicit none
!      implicit logical (a-z)
      integer lu,ia
      real(wp) t

      !common/etud/etude
      !common/nomb/ne,na,nn
      !common/nuqe/ncvi
      !common/cooa/xa,ya
      !common/debcal/debut

      lu=id_nua

      if (debut) then
         open (lu,file=trim(etude)//'.nua',status='unknown')
      else
#if ENG
      write (lu,*)'TIME=',t
      write (lu,*) 'EDGE     X edge     Y edge     NU edge '
#else
      write (lu,*)'TEMPS=',t
      write (lu,*) 'ARETE    X arete    Y arete    NU arete'
#endif
      do ia = 1,na
        if(nouveau_format)then
           write (lu,'(i9,2f15.4,f12.4)') ia,xa(ia),ya(ia),ncvi(ia)
        else
           write (lu,'(i6,2f15.4,f12.4)') ia,xa(ia),ya(ia),ncvi(ia)
        endif
      enddo
      endif
      end subroutine wrnu

!******************************************************************************
      subroutine calgrd
!     calcul des variables gauche et droite sur les aretes
! hg1 est en common
!******************************************************************************
      use module_tableaux,only:xna,yna,xta,yta,xe,ye,xa,ya,zfa,zfe,&
     &nrefa,ieva,fra,fre,hal,qnal,qtal,het,quet,qvet,alpha,pxzfe,&
     &pyzfe,pxque,pyque,pxqve,pyqve,pxhe,pyhe,hae,fha,fqua,fqva,&
     &pxze,pyze,se,la,houv,qouv,xae,yae,hg1,qug1,qvg1,eau,&
     &smbu,smbv,smbu2,smbv2,smbu3,smbv3,g,eps1,eps2,paray,&
     &ne,na,nn
      use module_precision
      use module_mpi,only:naloc,aretes_loc
#if TRANSPORT_SOLIDE
      use module_ts,only:hcal,cet,pxhce,pyhce,smbhc,fhca,hcg1
#endif
!$    use omp_lib

      implicit none

      integer ieg,ied,ia,ie,je,ialoc
      real(wp) hg,qug,qvg,hd,qud,qvd&
!     &,qng,qtg,qnd,qtd&
     &,delh,delu,delv,delx,dely
      real(wp) zeg,zed,hmin,hmax,qna,qta
!     :,heg,hed
      integer ind
      real(wp) delx2,dely2

! en premier lieu, on calcule au centre h,qu,qv a t+1/2 sans frottements
!  et les frottements correspondants
!$omp parallel private(ind,ieg,ied,ia,hg,qug,qvg,hd,qud,je,ie,
!$omp1 qvd,delh,delu,delv,delx,dely,zeg,zed,hmin,hmax,qna,qta,delx2,
!$omp2 dely2)
!$omp do schedule(runtime)
      zeg=0.
      zed=0.
      do ialoc = 1,naloc
        ia=aretes_loc(ialoc)
        ieg=ieva(ia,1)
        ied=ieva(ia,2)
        if (ieg.eq.0) then
            ind=1
        elseif (ied.eq.0) then
            ind=2
        elseif (nrefa(ia).eq.-2) then
            ind=3
        elseif (.not.eau(ieg)) then
            ind=11
        elseif (.not.eau(ied)) then
            ind=12
        else
            ind=4
            zeg=zfe(ieg)+het(ieg)
            zed=zfe(ied)+het(ied)
            if (zfa(ia).ge.zeg) then
              if (zfa(ia).ge.zed) then
                ind=0
                hg1(ia,1)=0.
                hg1(ia,2)=0.
              endif
            endif
        endif

        if (ind.eq.1) then
          if (eau(ied)) then
            delx2=xae(ia,2)
            dely2=yae(ia,2)
!            if (nrefa(ia).eq.2) then
!              hg1(ia,2)=het(ied)+zfe(ied)-zfa(ia)
!            else
! si nrefa ne vaut pas 2
              delh = pxze(ied) * delx2 + pyze(ied) * dely2
              hd=het(ied)+zfe(ied)-zfa(ia)
              hg1(ia,2)=hd+delh
! fin du if sur nrefa
!            endif

!            delh = pxhe(ied) * delx2 + pyhe(ied) * dely2
!            hed=het(ied)+delh
!            hmin=min(hd,hed)
!            if (hg1(ia,2).lt.hmin) then
!              hg1(ia,2)=hmin
!            endif
!            hmax=max(hd,hed)
!            if (hg1(ia,2) .gt. hmax) then
!              hg1(ia,2)=hmax
!            endif
            if (hg1(ia,2) .gt. paray) then
              delu = pxque(ied) * delx2 + pyque(ied) * dely2
              delv = pxqve(ied) * delx2 + pyqve(ied) * dely2
              qug1(ia,2)=quet(ied)+delu
              qvg1(ia,2)=qvet(ied)+delv
              if (nrefa(ia).eq.2) then
                qta=qug1(ia,2)*xta(ia)+qvg1(ia,2)*yta(ia)
! qna=0
                qug1(ia,2)=qta*xta(ia)
                qvg1(ia,2)=qta*yta(ia)
              endif
#if TRANSPORT_SOLIDE
              hcg1(ia,2)=cet(ied)
!necessaire si on definit entree a partir conc interieure
!              else
!                hcg1(ia,2)=0.
#endif
            endif
! fin du if sur eau
          endif

          elseif (ind.eq.2) then
            if (eau(ieg)) then
             delx=xae(ia,1)
             dely=yae(ia,1)
!             if (nrefa(ia).eq.2) then
!               hg1(ia,1)=het(ieg)+zfe(ieg)-zfa(ia)
!             else
! si nrefa ne vaut pas 2
              delh = pxze(ieg) * delx + pyze(ieg) * dely
              hg=het(ieg)+zfe(ieg)-zfa(ia)
              hg1(ia,1)=hg+delh
! fin du if sur nrefa
!             endif
!            delh = pxhe(ieg) * delx + pyhe(ieg) * dely
!            heg=het(ieg)+delh
!            hmin=min(hg,heg)
!            if (hg1(ia,1).lt.hmin) then
!              hg1(ia,1)=hmin
!            endif
!            hmax=max(hg,heg)
!            if (hg1(ia,1) .gt. hmax) then
!              hg1(ia,1)=hmax
!            endif
              if (hg1(ia,1) .gt. paray) then
                delu = pxque(ieg) * delx + pyque(ieg) * dely
                delv = pxqve(ieg) * delx + pyqve(ieg) * dely
                qug1(ia,1)=quet(ieg)+delu
                qvg1(ia,1)=qvet(ieg)+delv
              if (nrefa(ia).eq.2) then
                qta=qug1(ia,1)*xta(ia)+qvg1(ia,1)*yta(ia)
! qna=0
                qug1(ia,1)=qta*xta(ia)
                qvg1(ia,1)=qta*yta(ia)
              endif
#if TRANSPORT_SOLIDE
              hcg1(ia,1)=cet(ieg)
!necessaire si on definit entree a partir conc interieure
!              else
!                hcg1(ia,2)=0.
#endif
! fin du if sur hg1=0
            endif
! fin du if sur he=0
          endif

        elseif (ind.eq.11) then
          if (eau(ied)) then
            delx2=xae(ia,2)
            dely2=yae(ia,2)
            delh = pxze(ied) * delx2 + pyze(ied) * dely2
            hd=het(ied)+zfe(ied)-zfa(ia)
            hg1(ia,2)=hd+delh
!            hg1(ia,2)=hd
!            delh = pxhe(ied) * delx2 + pyhe(ied) * dely2
!            hed=het(ied)+delh
!            hmin=min(hd,hed)
!            if (hg1(ia,2).lt.hmin) then
!              hg1(ia,2)=hmin
!            endif
!            hmax=max(hd,hed)
!            if (hg1(ia,2) .gt. hmax) then
!              hg1(ia,2)=hmax
!            endif
            if (hg1(ia,2) .gt. paray) then
              delu = pxque(ied) * delx2 + pyque(ied) * dely2
              delv = pxqve(ied) * delx2 + pyqve(ied) * dely2
              qug1(ia,2)=quet(ied)+delu
              qvg1(ia,2)=qvet(ied)+delv
#if TRANSPORT_SOLIDE
              hcg1(ia,2)=cet(ied)
#endif
            endif
          endif

        elseif (ind.eq.12) then
          if (eau(ieg)) then
            delx=xae(ia,1)
            dely=yae(ia,1)
            delh = pxze(ieg) * delx + pyze(ieg) * dely
              hg=het(ieg)+zfe(ieg)-zfa(ia)
!              hg1(ia,1)=hg
              hg1(ia,1)=hg+delh
!           delh = pxhe(ieg) * delx + pyhe(ieg) * dely
!            heg=het(ieg)+delh
!            hmin=min(hg,heg)
!            if (hg1(ia,1).lt.hmin) then
!              hg1(ia,1)=hmin
!            endif
!            hmax=max(hg,heg)
!            if (hg1(ia,1) .gt. hmax) then
!              hg1(ia,1)=hmax
!            endif
            if (hg1(ia,1) .gt. paray) then
              delu = pxque(ieg) * delx + pyque(ieg) * dely
              delv = pxqve(ieg) * delx + pyqve(ieg) * dely
              qug1(ia,1)=quet(ieg)+delu
              qvg1(ia,1)=qvet(ieg)+delv
#if TRANSPORT_SOLIDE
              hcg1(ia,1)=cet(ieg)
#endif

            endif
          endif

       elseif (ind.eq.3) then
! calcul des 2 cotes sans limitation
         if (eau(ieg)) then
            delx=xae(ia,1)
            dely=yae(ia,1)
            delh = pxze(ieg) * delx + pyze(ieg) * dely
!            hg1(ia,1)=het(ieg)+delh+zfe(ieg)-zfa(ia)
            hg1(ia,1)=het(ieg)+zfe(ieg)-zfa(ia)
            if (hg1(ia,1) .gt. paray) then
!              delu = pxque(ieg) * delx + pyque(ieg) * dely
!              delv = pxqve(ieg) * delx + pyqve(ieg) * dely
!              qug1(ia,1)=quet(ieg)+delu
!              qvg1(ia,1)=qvet(ieg)+delv
              qug1(ia,1)=quet(ieg)
              qvg1(ia,1)=qvet(ieg)
#if TRANSPORT_SOLIDE
              hcg1(ia,1)=cet(ieg)
#endif
            endif
         endif
         if (eau(ied)) then
            delx=xae(ia,2)
            dely=yae(ia,2)
            delh = pxze(ied) * delx + pyze(ied) * dely
!            hg1(ia,2)=het(ied)+delh+zfe(ied)-zfa(ia)
            hg1(ia,2)=het(ied)+zfe(ied)-zfa(ia)
            if (hg1(ia,2) .gt. paray) then
!              delu = pxque(ied) * delx + pyque(ied) * dely
!              delv = pxqve(ied) * delx + pyqve(ied) * dely
!              qug1(ia,2)=quet(ied)+delu
!              qvg1(ia,2)=qvet(ied)+delv
              qug1(ia,2)=quet(ied)
              qvg1(ia,2)=qvet(ied)
#if TRANSPORT_SOLIDE
              hcg1(ia,2)=cet(ied)
#endif
            endif
          endif

       elseif (ind.eq.4) then
! calcul des 2 cotes avec limitation de la difference
            delx=xae(ia,1)
            dely=yae(ia,1)
            delh = pxze(ieg) * delx + pyze(ieg) * dely
            hg1(ia,1)=het(ieg)+delh+zfe(ieg)-zfa(ia)
            delx2=xae(ia,2)
            dely2=yae(ia,2)
            delh = pxze(ied) * delx2 + pyze(ied) * dely2
            hg1(ia,2)=het(ied)+delh+zfe(ied)-zfa(ia)
! a mon avis cette limitation ne joue jamais mais ...
            zeg=zeg-zfa(ia)
            zed=zed-zfa(ia)
!            delh = pxhe(ieg) * delx + pyhe(ieg) * dely
!            heg=het(ieg)+delh
!            delh = pxhe(ied) * delx2 + pyhe(ied) * dely2
!            hed=het(ied)+delh
            if (zeg .gt. zed) then
               hmin=zed
               hmax=zeg
!              if (heg.lt.hed) then
!                hmin=min(zeg,heg)
!                hmax=max(zed,hed)
!              else
!                hmin=min(zeg,hed)
!                hmax=max(zed,heg)
!              endif
                   if (hg1(ia,1).lt.hmin) then
                             hg1(ia,1)=hmin
                   elseif (hg1(ia,1) .gt. hmax) then
                      hg1(ia,1)=hmax
                   endif
                   if (hg1(ia,2).lt.hmin) then
                             hg1(ia,2)=hmin
                   elseif (hg1(ia,2) .gt. hmax) then
                      hg1(ia,2)=hmax
                   endif
            else
               hmin=zeg
               hmax=zed
!              if (heg.lt.hed) then
!                hmin=min(zed,heg)
!                hmax=max(zeg,hed)
!              else
!                hmin=min(zed,hed)
!                hmax=max(zeg,heg)
!              endif
                   if (hg1(ia,1).lt.hmin) then
                             hg1(ia,1)=hmin
                   elseif (hg1(ia,1) .gt. hmax) then
                      hg1(ia,1)=hmax
                   endif
                   if (hg1(ia,2).lt.hmin) then
                             hg1(ia,2)=hmin
                   elseif (hg1(ia,2) .gt. hmax) then
                      hg1(ia,2)=hmax
                   endif
!            else
!c zeg et zed sont egaux : on choisit la moyenne
!              hg1(ia,1)=0.5*(zeg+zed)
!              hg1(ia,2)=hg1(ia,1)
            endif

            if (hg1(ia,1) .gt. paray) then
              delu = pxque(ieg) * delx + pyque(ieg) * dely
              delv = pxqve(ieg) * delx + pyqve(ieg) * dely
              qug=quet(ieg)
              qvg=qvet(ieg)
              qug1(ia,1)=qug+delu
              qvg1(ia,1)=qvg+delv
#if TRANSPORT_SOLIDE
              hcg1(ia,1)=cet(ieg)
#endif

              if (hg1(ia,2) .gt. paray) then
                delu = pxque(ied) * delx2 + pyque(ied) * dely2
                delv = pxqve(ied) * delx2 + pyqve(ied) * dely2
                qud=quet(ied)
                qvd=qvet(ied)
                qug1(ia,2)=qud+delu
                qvg1(ia,2)=qvd+delv
#if TRANSPORT_SOLIDE
                hcg1(ia,2)=cet(ied)
#endif

             endif
           elseif (hg1(ia,2) .gt. paray) then
              delu = pxque(ied) * delx2 + pyque(ied) * dely2
              delv = pxqve(ied) * delx2 + pyqve(ied) * dely2
              qud=quet(ied)
              qvd=qvet(ied)
              qug1(ia,2)=qud+delu
              qvg1(ia,2)=qvd+delv
#if TRANSPORT_SOLIDE
              hcg1(ia,2)=cet(ied)
#endif
            endif
! fin du if sur ind
        endif
! fin boucle sur ia
      end do
!$omp end do
!$omp do schedule(runtime)
      do ialoc = 1,naloc
       ia=aretes_loc(ialoc)
       do je=1,2
        ie=ieva(ia,je)
        if (ie.ne.0) then
          if (hg1(ia,je).le.paray) then
             hg1(ia,je)=0.
             qug1(ia,je)=0.
             qvg1(ia,je)=0.
#if TRANSPORT_SOLIDE
            hcg1(ia,je)=0.
#endif
          endif
! fin du if si ie=0
        endif
         end do
         end do
!$omp end do
!$omp end parallel

      return
      end subroutine calgrd
!onc

!************************************************************
      subroutine tevent!(xvent,yvent)
! calcul des termes de vent a t
!************************************************************

      use module_tableaux, only:nven,nchrov,nbchrv,nmchro,&
     &nmcoap,ventx,venty,tvent,dt,cfl,dt0,t,ne,na,nn
      use module_precision
      use module_mpi,only:ne_loc,me,mailles_loc
      use module_tevent
!$    use omp_lib

      implicit none
!
! nombre maximal de chroniques et de nombre de couples par chronique
      integer ichro,ncouap,ie ,ieloc
      real(wp) venx(nmchro),veny(nmchro),ventt
      !real(wp),dimension(:),allocatable::xvent,yvent
      logical cherche
!
      !common/nvent/nven,nchrov,nbchrv
      !common/vents/ventx,venty,tvent
      !common/cflr/dt,cfl,dt0,t
      !common/nomb/ne,na,nn

! boucle sur les chroniques
!$omp parallel private(ncouap,cherche)
!$omp do schedule(runtime)
      do ichro=1,nchrov
        cherche=.true.
        ncouap=1
        if (t.lt.tvent(ichro,ncouap)) then
          venx(ichro)=ventx(ichro,ncouap)
          veny(ichro)=venty(ichro,ncouap)
!          ventt=(venx(ichro)**2+veny(ichro)**2)**0.75
!          venx(ichro)=0.6e-06*ventt*venx(ichro)
!          veny(ichro)=0.6e-06*ventt*veny(ichro)
          ventt=sqrt(venx(ichro)**2+veny(ichro)**2)
          venx(ichro)=3.4e-06*ventt*venx(ichro)
          veny(ichro)=3.4e-06*ventt*veny(ichro)
          cherche=.false.
        endif
        if (cherche) then
          do ncouap=2,nbchrv(ichro)
            if (cherche) then
              if (t.lt.tvent(ichro,ncouap)) then
                venx(ichro)=ventx(ichro,ncouap)+&
     &(ventx(ichro,ncouap)-ventx(ichro,ncouap-1))&
     &/(tvent(ichro,ncouap)-tvent(ichro,ncouap-1))&
     & *(t-tvent(ichro,ncouap))
                veny(ichro)=venty(ichro,ncouap)+&
     &(venty(ichro,ncouap)-venty(ichro,ncouap-1))&
     &/(tvent(ichro,ncouap)-tvent(ichro,ncouap-1))&
     & *(t-tvent(ichro,ncouap))
!          ventt=(venx(ichro)**2+veny(ichro)**2)**0.75
!          venx(ichro)=0.6e-06*ventt*venx(ichro)
!          veny(ichro)=0.6e-06*ventt*veny(ichro)
          ventt=sqrt(venx(ichro)**2+veny(ichro)**2)
          venx(ichro)=3.4e-06*ventt*venx(ichro)
          veny(ichro)=3.4e-06*ventt*veny(ichro)
                cherche=.false.
              endif
            endif
! fin de boucle sur ncouap
          enddo
! on est arrive au couple maxi
          if (cherche) then
          venx(ichro)=ventx(ichro,nbchrv(ichro))
          veny(ichro)=venty(ichro,nbchrv(ichro))
!          ventt=(venx(ichro)**2+veny(ichro)**2)**0.75
!          venx(ichro)=0.6e-06*ventt*venx(ichro)
!          veny(ichro)=0.6e-06*ventt*veny(ichro)
          ventt=sqrt(venx(ichro)**2+veny(ichro)**2)
          venx(ichro)=3.4e-06*ventt*venx(ichro)
          veny(ichro)=3.4e-06*ventt*veny(ichro)
          endif
        endif
! fin de boucle sur les chroniques
      enddo
!$omp end do
! repartition sur les mailles
!$omp do schedule(runtime)
      do ieloc=1,ne_loc(me)
         ie=mailles_loc(ieloc)
        xvent(ie) = venx(nven(ie))
        yvent(ie) = veny(nven(ie))
      end do
!$omp end do
!$omp end parallel

      end subroutine tevent

!************************************************************
       subroutine vent(xvent,yvent)
! calcul du vent a t sur toutes les mailles
!************************************************************

!
      use module_precision

      use module_tableaux,only : nven,nchrov,nbchrv,ventx,venty&
      &,tvent,dt,cfl,dt0,t,ne,na,nn,nmchro,nmcoap

       implicit none

! nombre maximal de chroniques et de nombre de couples par chronique

       integer ichro,ncouap,ie

       real(wp) venx(nmchro),veny(nmchro),xvent(ne),yvent(ne)
       logical cherche


 ! boucle sur les chroniques
       do ichro=1,nchrov
         cherche=.true.
         ncouap=1
         if (t.lt.tvent(ichro,ncouap)) then
           venx(ichro)=ventx(ichro,ncouap)
           veny(ichro)=venty(ichro,ncouap)
 !          ventt=(venx(ichro)**2+veny(ichro)**2)**0.75
 !          venx(ichro)=0.6e-06*ventt*venx(ichro)
 !          veny(ichro)=0.6e-06*ventt*veny(ichro)
           cherche=.false.
         endif
         if (cherche) then
           do ncouap=2,nbchrv(ichro)
             if (cherche) then
               if (t.lt.tvent(ichro,ncouap)) then
                 venx(ichro)=ventx(ichro,ncouap)+&
      &(ventx(ichro,ncouap)-ventx(ichro,ncouap-1))&
      &/(tvent(ichro,ncouap)-tvent(ichro,ncouap-1))&
      & *(t-tvent(ichro,ncouap))
                 veny(ichro)=venty(ichro,ncouap)+&
      &(venty(ichro,ncouap)-venty(ichro,ncouap-1))&
      &/(tvent(ichro,ncouap)-tvent(ichro,ncouap-1))&
      & *(t-tvent(ichro,ncouap))
 !          ventt=(venx(ichro)**2+veny(ichro)**2)**0.75
 !          venx(ichro)=0.6e-06*ventt*venx(ichro)
 !          veny(ichro)=0.6e-06*ventt*veny(ichro)
                 cherche=.false.
               endif
             endif
 ! fin de boucle sur ncouap
           enddo
 ! on est arrive au couple maxi
           if (cherche) then
           venx(ichro)=ventx(ichro,nbchrv(ichro))
           veny(ichro)=venty(ichro,nbchrv(ichro))
 !          ventt=(venx(ichro)**2+veny(ichro)**2)**0.75
 !          venx(ichro)=0.6e-06*ventt*venx(ichro)
 !          veny(ichro)=0.6e-06*ventt*veny(ichro)
           endif
         endif
 ! fin de boucle sur les chroniques
       enddo
 ! repartition sur les mailles
       do 5 ie =1,ne
         xvent(ie) = venx(nven(ie))
         yvent(ie) = veny(nven(ie))
  5    continue

       end subroutine vent

!************************************************************
       subroutine venta(xvent,yvent,ia)
! calcul du vent a t sur une arete
!************************************************************
!
!
      use module_precision
      use module_tableaux,only:na,nn,ne,nbchrv,nven,nchrov&
     &,ieva,fvix0,fviy0,pven,dt,cfl,dt0,t,ventx,venty,tvent


      implicit none

      integer ichro,ncouap,ie,ia

      real(wp) xvent,yvent
      logical cherche

      if (.not.pven) then
           xvent=fvix0
           yvent=fviy0
 ! en premier recherche de la chronique si chronique
      else
           ie=ieva(ia,1)
           if (ie.eq.0) then
             ie=ieva(ia,2)
           endif
           ichro=nven(ie)
           cherche=.true.
           ncouap=1
           if (t.lt.tvent(ichro,ncouap)) then
             xvent=ventx(ichro,ncouap)
             yvent=venty(ichro,ncouap)
             cherche=.false.
           endif
           if (cherche) then
             do ncouap=2,nbchrv(ichro)
             if (cherche) then
               if (t.lt.tvent(ichro,ncouap)) then
                 xvent=ventx(ichro,ncouap)+&
      &(ventx(ichro,ncouap)-ventx(ichro,ncouap-1))&
      &/(tvent(ichro,ncouap)-tvent(ichro,ncouap-1))&
      & *(t-tvent(ichro,ncouap))
                 yvent=venty(ichro,ncouap)+&
      &(venty(ichro,ncouap)-venty(ichro,ncouap-1))&
      &/(tvent(ichro,ncouap)-tvent(ichro,ncouap-1))&
      & *(t-tvent(ichro,ncouap))
                 cherche=.false.
               endif
             endif
 ! fin de boucle sur ncouap
             enddo
 ! on est arrive au couple maxi
             if (cherche) then
               xvent=ventx(ichro,nbchrv(ichro))
               yvent=venty(ichro,nbchrv(ichro))
             endif
           endif
 ! fin du if sur pven
         endif
       end subroutine venta

#if TRANSPORT_SOLIDE
!******************************************************************************
      subroutine vitsur
! calcul la valeur du rapport vitesse de surface sur vitesse moyenne
! et ecrit la vitesse de surface dans vitfro
!******************************************************************************
!
!
      use module_precision

      use module_tableaux,only : ne,nn,na,pven,fvix0,fviy0,fra,fre&
      &,eps1,eps2,paray,he,que,qve

      use module_ts,only : derive,cvsurf,alfv,betav,vitfro,dhe

      implicit none

      integer ie

      real(wp) alfven,rap,xvent(ne),yvent(ne),ue(ne),ve(ne)

      if (pven) then
         call vent(xvent,yvent)
      else
         do ie=1,ne
           xvent(ie)=fvix0
           yvent(ie)=fviy0
         enddo
      endif
      do ie=1,ne
        if (he(ie) .gt. paray) then
          ue(ie)=que(ie)/he(ie)
          ve(ie)=qve(ie)/he(ie)
          if (.not.cvsurf) then
            alfven=alfv
          else
! rap est le rapport des vitesses de frottement surface et fond
            if ((xvent(ie)**2+yvent(ie)**2) .gt. eps2) then
              rap=1000./1.3*(ue(ie)**2+ve(ie)**2)&
     &/(xvent(ie)**2+yvent(ie)**2)/he(ie)**0.333/fra(ie)
              if ((xvent(ie)*ue(ie)+yvent(ie)*ve(ie)).lt.0.) then
! cas ou vent oppose a courant
                rap=-rap
              endif
! limitation de rap pour eviter denominateur nul
              if (rap .gt. 2.)rap=2.
              alfven=alfv*(1.-rap)/(1.-rap+alfv)
! cas de vent nul
            else
              alfven=alfv
            endif
! fin du if sur cvsurf
          endif
          dhe(ie)=alfven*ue(ie)+betav*xvent(ie)
          vitfro(ie)=alfven*ve(ie)+betav*yvent(ie)
! cas ou pas d'eau
        else
          dhe(ie)=0.
          vitfro(ie)=0.
        endif
! fin boucle sur ie
      enddo
      return
      end subroutine vitsur
#endif

!******************************************************************************
      subroutine veriftarglo
!******************************************************************************
!     verification egalite lois de tarage si nref>60

      use module_tableaux, only: nrefa,qlt,zlt,ialt,nblt,iltmax,&
     &na60,eps1,eps2,paray,nltmax,nplmax
      use module_mpi,only:machine_aretes60,me
!$    use omp_lib

      implicit none

      integer nref0,i0,j,i

      !common/narf/nrefa
      !common/qzlt/qlt,zlt
      !common/nbplt/ialt,nblt,iltmax
      !common/na60/na60
      !common/param/eps1,eps2,paray

!$omp parallel
!$omp do schedule(runtime)
      do nref0=61,60+na60
        if(machine_aretes60(nref0)/=me)cycle
    i0=0
      do i=1,iltmax
      if (nrefa(ialt(i)).eq.nref0) then
      if (i0.eq.0) then
        i0=i
!       write(*,*)'i0',i0,qlt(i0,1),zlt(i0,1)
        else
        if (nblt(i).ne.nblt(i0)) then
     write(*,*)'changement valeurs loi de tarage arete ',ialt(i)
                 nblt(i)=nblt(i0)
        do j=1,nblt(i)
          qlt(i,j)=qlt(i0,j)
      zlt(i,j)=zlt(i0,j)
        end do
! si meme nombre de valeurs
              else
        do j=1,nblt(i)
          if (abs(qlt(i,j)-qlt(i0,j)).gt.eps1) then
       write(*,*)'changement valeur ',j,' loi de tarage arete ',ialt(i)
          qlt(i,j)=qlt(i0,j)
      zlt(i,j)=zlt(i0,j)
          elseif (abs(zlt(i,j)-zlt(i0,j)).gt.eps1) then
      write(*,*)'changement valeur ',j,' loi de tarage arete ',ialt(i)
          qlt(i,j)=qlt(i0,j)
      zlt(i,j)=zlt(i0,j)
      endif
        end do
! endif sur nombre de valeurs
         endif
! endif sur i0
         endif
! endif sur nref0
         endif

      end do
! fin boucle sur nref0
      enddo
!$omp end do
!$omp end parallel

      return
      end subroutine veriftarglo

!*****************************************************************************
      subroutine wrmaillespenal(if)
!      subroutine cflmax(icfl,tnou,vole,vols)
!*****************************************************************************
!   ecriture des 10 mailles les plus penalisantes dans etude.edm
!
!   entree :
!          he : hauteur d eau
!          que : debit selon x
!          qve : debit selon y
!          dt0 : pas de temps initial

!*****************************************************************************

      use module_tableaux,only:g,dxe,dxen,ne,na,nn,he,que,qve,dt,cfl,dt0,t,tm,tr,tinit,tmax,&
     &xna,yna,xta,yta,iae,nne,eps1,eps2,paray,nrefa,hae,fha,fqua,fqva,eau,se,la,ieva,tnou
      use module_precision

      implicit none

      real(wp) :: dt1,c,cflc,sgn
      integer :: if,ie,id,id2,ja,ia,i
      integer, parameter :: necr=10
      real(wp) :: ui,ci
      logical :: test
      integer :: iecr,ieecr(necr)
      real(wp) :: cflecr(necr),vecr(necr),dxecr(necr)
      external c,sgn

      !common/grav/g
      !common/dxdy/dxe,dxen
      !common/nomb/ne,na,nn
!      common/tlig/het,quet,qvet
      !common/tpli/he,que,qve
      !common/cflr/dt,cfl,dt0,t
      !common/tenv/tm,tr,tinit,tmax
      !common/vare/xna,yna,xta,yta
      !common/nrel/iae
      !common/nvois/nne
      !common/param/eps1,eps2,paray
      !common/narf/nrefa
      !common/hali/hae
      !common/flux/fha,fqua,fqva
      !common/preso/eau
      !common/dose/se,la
      !common/iela/ieva
      !common/tsuiva/tnou

      save id,id2
      data id,id2/0,0/

!
!  test si trace de calcul o/n
!*****************************
        do iecr=1,necr
          cflecr(iecr)=0.
          vecr(iecr)=0.
          dxecr(iecr)=0.
          ieecr(iecr)=0
        enddo
! calcul de la cfl
              cflc=0.
              ui=0.
              do 1 ie = 1,ne
                 if (eau(ie)) then
                    ci=c(he(ie))
                   do 51 ja=1,nne(ie)

                      ia=iae(ie,ja)
                     if(nrefa(ia).eq.-2)then
                       dt1=0.
                     elseif(hae(ia).gt.paray)then
                      ui=fha(ia)/hae(ia)
                      dt1=(abs(ui)+ci)/dxe(ie,ja)
                     else
                       dt1=0.
                     endif
                   cflc=max(cflc,dt1)
                   test=.true.
                   do iecr=1,necr
                    if (test)then
                     if(cflecr(iecr).lt.dt1)then
                       test=.false.
                       do i=necr,iecr+1,-1
                           cflecr(i)=cflecr(i-1)
                           vecr(i)=vecr(i-1)
                           dxecr(i)=dxecr(i-1)
                           ieecr(i)=ieecr(i-1)
                       enddo
                       cflecr(iecr)=dt1
                       vecr(iecr)=abs(ui)
                       dxecr(iecr)=dxe(ie,ja)
                       ieecr(iecr)=ie
! fin du if sur cflecr
                     endif
! fin du if sur test
                    endif
! fin du do sur iecr
                  enddo
 51                continue
! fin du if sur he=0
                 endif
 1           continue
        write(if,*)
        write(if,*) 'temps = ',tm
        write(if,*)'maille    v+c/dx        v          dx'
        do iecr=1,necr
      write(if,'(i9,3f12.4)') ieecr(iecr),cflecr(iecr),vecr(iecr),dxecr(iecr)
        enddo
        return
        end subroutine

!******************************************************************************
      subroutine deallocate_tabs
!=======================================================================
! d�sallocation des tableaux dynamiques pour �viter les fuites de m�moire
!=======================================================================
      use module_tableaux
      use module_laplacien
      use module_tevent
      use module_loc
      use module_mpi
      implicit none

      !desallocation des tableaux du module_tableaux
      deallocate(iae,nne,ine,ieve)
      deallocate(dxe,dxen,xe,ye,zfe)
      deallocate(fra,fre,het,quet,qvet)
      deallocate(he,que,qve,pxzfe,pyzfe,pxque,pyque,pxqve,pyqve)
      deallocate(pxhe,pyhe,pxze,pyze,se,cofr)
      deallocate(px2que,py2que,px2qve,py2qve)
      deallocate(smbu,smbv,smbu2,smbv2,smbu3,smbv3)
      deallocate(pxqu,pxqv,pyqu,pyqv)
      deallocate(napp,smbh,nven,kse)
      deallocate(eau)
      deallocate(ina,ieva,nrefa,neva)
      deallocate(xna,yna,xta,yta,xa,ya,zfa)
      deallocate(hal,qnal,qtal,la,hae)
      deallocate(fha,fqua,fqva)
      deallocate(houv,qouv,difqouvp)
      deallocate(xae,yae)
      deallocate(hg1,qug1,qvg1)
      deallocate(fluu,flvv,cvia,ncvi)
      deallocate(flgu,flgv)
      deallocate(vouv,vtouv)
      deallocate(nevn,ievn)
      deallocate(xn,yn,zfn)
      !desllocation des tableaux du module_laplacien
      deallocate(a11,a22,a12,deta)
      deallocate(xbb,ybb,xybb)
      !desallocation des tableaux du module_tevent
      deallocate(xvent,yvent)
      !desallocation des tableaux du module_loc
      deallocate(hem,qum,qvm)
      deallocate(hem2,qum2,qvm2)
      deallocate(th,tu)
      deallocate(xaa,yaa)
      deallocate(a110,a220,a120,deta0)
      deallocate(sse)
      deallocate(hezer)
      deallocate(ustar)
      deallocate(fxuu,fyuu,fxvv,fyvv,pxuu,pyuu,pxvv,pyvv,ue,ve)
      deallocate(dh,dqu,dqv)
      deallocate(qouvp)
      deallocate(hed)
      !desallocation des tableaux du module_mpi
      deallocate(aretes_loc)
      deallocate(message_scribe,message_scribe2,message_scribe3)
      deallocate(message_scribe4,message_scribe4int)
      deallocate(ouvragesb_loc)
      deallocate(nb_elt_send_proc,nb_elt_recv_proc)
      if (nob>0) deallocate(procouvb)
      !if (me/=scribe)then
        deallocate(aretes_bord)
        deallocate(ouvrages_loc,ouvrages_bord)
        deallocate(elt_voisins,vecteur_message)
        deallocate(elt_bord,vecteur_reception)
        deallocate(deplacement_send_proc,deplacement_recv_proc,message,message2)
        deallocate(message_logical,message_logical2)
        deallocate(machine_ias)
        deallocate(deplacement_message_c,deplacement_reception_c)
        deallocate(nb_elt_message_c,nb_elt_reception_c)
        deallocate(vecteur_message_c,vecteur_reception_c)
        deallocate(message_c,reception_c)
      !end if ! me/=scribe
#ifdef WITH_MPI
        deallocate(aretes_loc_all,mailles_loc_all,noeuds_loc_all)
        deallocate(reception_scribe,reception_scribe2,reception_scribe3,reception_scribe4,reception_scribe4int)
        deallocate(deplacements,deplacements2,deplacements3)
        deallocate(nb_aretes_recues,nb_mailles_recues,nb_noeuds_recus)
#endif /* WITH_MPI */

      end subroutine deallocate_tabs

#if TRANSPORT_SOLIDE
      subroutine deallocate_tabs_ts
!=======================================================================
! d�sallocation des tableaux dynamiques propres au transport solide
!=======================================================================
      use module_ts
      deallocate(diamn,epcoun,sigman,taummn)
      deallocate(diamn2,epcoun2,taummn2,sigman2,diamsmb,sigmasmb&
      &,diamdhn,sigmadhn,sigmaa,diama)
      deallocate(sigmae,diame,sigmae2,diame2,diamnt,sigmant)
      deallocate(sigmaouv,diamouv)
      deallocate(nbcoun,nbcoun2)
      deallocate(eseldt,dhe,hcal,hcet,hce,smbhc,fhca,pxhc&
     &,pxhce,pyhce,pyhc,smbhc2,flhc,ceq,rapsmbhc,zfe0,cet,vitfro,zfndur)
      deallocate(hcg1,coouv)
      deallocate(dzfn)
      deallocate(rapva)
      deallocate(zfn2,zfa2,zfe2,cofr2)
      deallocate(imailczero)
      deallocate(taun,taue)

      end subroutine deallocate_tabs_ts
!cou1d      deallocate(ccouplts,dcouplts,scouplts)

#endif

!!! section transport solide !!!
#if TRANSPORT_SOLIDE

!******************************************************************************
       subroutine semba_loc!(zfn2,zfa2,zfe2,cofr2)
!     on calcule la contribution des termes de sedimentation et erosion
!******************************************************************************
!  definition des donnees diverses servant aux calculs
! niux    = la diffusion liquide selon ox
! niuy    = la diffusion liquide selon oy
! sigm    = le nombre de schmidt
! dens    = la densite des sediments
! den     = la densite d'eau
! m       = le coefficient empirique pour le transport de sediments
! ikds    = 1 ou 0 : si calcul avec kds constante ou non
! kds     = la constante donnee par le diagramme de shields
! kaa      = constante intervenant dans le calcul du taucr si la maille
!           est en pente
! alprep  = le coefficient de vitesse de depot
! d       = d50
! re      = d* parametre adimensionnel
! vstar   = module de la vitesse
! vch     = la vitesse de chute
! vch2 vitesse de chute coorigee
! vfr     = vitesse de frottement
! uprime  = vitesse de frottement relative aux grains
! zed     = parametre de suspension (force fluide/ forces gravitationnelles)
! consnu  = la diffusion lique moyenne constante
! dix     = le coefficient de diffusion selon ox
! diy     = le coefficient de diffusion selon oy
! diagsh  = false ou true : si kds est donne par l'utilisateur ou calculer par le diagramme de shields
! vanri   = false ou true : si la concentration d'equilibre est calculee par van rijn
! corvch  = false ou true : si il y a correction sur le vitesse de chute pour le terme de sedimentation
! beta    = coefficient....
! ppente  = prise en compte de la pente pour determiner tau critique
! meyer =calcul erosion par meyer peter muller
! zeta  rapport vitesse sediments charries utilisee a vitesse theorique
!**************************************************************************

    use module_precision

     use module_tableaux,only:ne,na,nn,eau,het,quet,qvet,fra,fre,cofr&
     &,ieva,iae,nne,g,smbh,zfn,dt,cfl,dt0,t,eps1,eps2,paray,idl
      use module_precision,only:wp

     use module_ts,only : efficace,sigma0,rapsmbhc,nbcoun,epcoun,diamn,sigman&
     &,taummn,diame2,sigmae2,exposantaucr,camenen,hansen,exptaucr,taue,rapva,smbhc&
     &,solute,vitfro,dhe,hcet,cet,sg,rzmax,diams,fi,fi2,al,ksi,gdd,eseldt&
     &,diagsh,modes,consed,vanri,corvch,meyer,ppente,d90,re,alprep,consnu&
#if SEDVAR
     &,sedvar&
!end if sedvar
#endif
     &,d,den,dens,kds,m,poro,zeta,tk,vch,ceq,zfn2,zfa2,zfe2,cofr2

#ifdef WITH_MPI
      use module_messages
#endif /* WITH_MPI */
      use module_mpi,only:ne_loc,me,mailles_loc,naloc,aretes_loc

      implicit none

!!!!!     real(WP),intent(inout) :: cofr2(ne),zfn2(nn),zfa2(na),zfe2(ne)!,ceq(ne)
     real(WP) :: rapve(ne),vitchut2
     real(wp) :: ero,sed,tau,taucs,taucr,vfr,kaa,chez2,transp&
     &,uprime,ucr,afond,cafond,beta,phi,zed,frep,cee,co1,co2
     real(WP) :: fra2,frcor,hcmin,q,rz,vch2,vmin,vstar,vsed
     integer :: ia,ie,in,ie2
     integer :: ieloc,ialoc
!     integer :: idl = 0
! compte de messages avertissement
     ! data idl/0/
     ! save idl

#if SEDVAR
       external vitchut2
!end if sedvar
#endif

!write(*,*) 'On passe par le calcul local'
      if(.not.solute)then
!        hmin=max(d,paray)
!        sg=sqrt(g)
! rzmax fixe a  25 mm/s soit 90 m en 1 heure
!        rzmax=0.025
! si on utilise van rijn
!        if(vanri)then
! diam�tre adimensionnel
! le cas dens=den est exclu par .not.solute
!          diams=d*((dens/den-1.)*g)**0.333333*consnu**(-0.666667)
!        endif
! calcul du coefficient de pente a chaque pas de temps pour le calcul
! de la contrainte critique d erosion taucr
!        if(ppente)then
! calcul des constantes pour le calcul du coefficient de pente dont dependra taucr
! fi est la tangente de l'angle de frottement interne des sediments que
! l'on donne ici en radians (30 degres = 0.52333 radians)
! al=alpha un coefficient empirique fix� � 0.85
!          fi=tan(0.52333)
!          fi2=tan(0.52333)*tan(0.52333)
!          fi=0.576951981
!          fi2=0.332873583
!          al=0.85
!        endif
        do ieloc=1,ne_loc(me)
          ie = mailles_loc(ieloc)
          ceq(ie)=0.
            taue(ie)=0.
            rapve(ie)=0.
#if SEDVAR
          if(sedvar)then
            d=diame2(ie)
          endif
!end if sedvar
#endif

! si on a de l'eau
!! d suppose plus grand que paray
!          if(he(ie).gt.d)then
! 16/12/2010 on remet paray et on utilise hmin apres
         if(het(ie).gt.paray)then
!          if(het(ie).gt.hmin)then
!          if(eau(ie))then
            q=sqrt(quet(ie)**2+qvet(ie)**2)
            vstar=q/het(ie)
!! si le module de  la vitesse est tr�s faible
!            if(vstar.lt.eps2)then
!              tau=0.
!              ero=0.
!              sed=alprep*vch*cet(ie)
! si la vitesse est suffisante
!            else
! on limite la vitesse
              if(vstar.gt.10.)vstar=10.
! calcul du coefficient de chezy
              fra2=max(fra(ie),fre(ie)*het(ie)**0.3333333)
! on divise par cofr car la surface est integree dans fra ou fre
              fra2=fra2/cofr(ie)
! calcul de la vitesse de frottement
              vfr= sg*vstar/sqrt(fra2)
              vitfro(ie)=vfr
              tau=den*vfr*vfr
              if(diagsh)then
! courbe de shields nouvelle
                re=d*vfr/consnu
                if(re.le.0.2)then
                  kds=0.575
                elseif(re.lt.2.)then
                  kds=0.115/re
                elseif(re.lt.5.)then
                  kds=0.081317279*re**(-0.5)
                elseif(re.lt.10.)then
                  kds=0.050175514*re**(-0.2)
                elseif(re.lt.40.)then
                  kds=0.025147327*re**(0.1)
                elseif(re.lt.490.)then
                  kds=0.017389449*re**(0.2)
                else
                  kds=0.060
                endif
! fin du if sur diagsh
              endif
              taucr=kds*gdd*d
              if(exptaucr)then
! exposantaucr a  ete divise par 2 dans ledd
                 taucr=taucr*(1./cofr(ie)**2-1.)**exposantaucr
!              endif
! calcul du frottement critique ave! introduction du
! coefficient kaa traduisant la dependance a la pente
              elseif(ppente)then
! le 27 aout 2012 : en 1d le minimum est zero
!                taucrmin=0.5*taucr
!                taucrmin=0.d0
                co1=-al*fi2*cofr(ie)
                co2=fi2*cofr(ie)*cofr(ie)+al*al*fi2*(1.-cofr(ie)*cofr(ie))-(1.-cofr(ie)*cofr(ie))
                if(co2.lt.eps2)then
! 10/4/2014   si co2 negatif, le calcul est invalide
! on annule la contrainte critique car une raison =forte pente
! modif aussi apres supposant taucrmin=0
                  taucr=0.
                elseif((co1+sqrt(co2)).lt.eps2) then
                  taucr=0.
                else
!                  kaa=(co1+sqrt(co2))/((1.-al*fi)*fi)
                  kaa=(1.-al*fi)*fi
          if(kaa.gt.eps1)then
            kaa=(co1+sqrt(co2))/kaa
              if(kaa.lt.1.)then
                         taucr=kaa*taucr
!                     else
! sinon taucr inchange
! fin du if sur kaa<1
                      endif
! else du if sur kaa>0
                else
                    taucr=0.
! fin du if sur kaa>0
                endif
! fin du if sur co2>0
                endif
! fin du if sur ppente
              endif
! si on apporte le conrrection sur la vitesse de chute
              if(corvch)then
! la correction vaut vmin pour particule en autousspension
                if(vstar.gt.eps2)then
                  vmin=vstar**3/het(ie)/fra2
                else
                  vmin=0.
                endif
#if SEDVAR
                vch=vitchut2(d)
!end if sedvar
#endif
                vch2=max(vch-vmin,0.d0)
!                if(vch.gt.vmin)then
!                  vch2=vch-vmin
!                else
!                  vch2=0.
!                endif
              else
#if SEDVAR
                vch=vitchut2(d)
!end if sedvar
#endif
                vch2=vch
! fin du if sur la correction
              endif
! si on utilise van rijn
              if(vanri)then
! si il y a du sediment
                if(cet(ie).gt.eps2)then
                  cee=cet(ie)
                else
                  cee=0.
                endif
! si la vitesse de chute est trop grande
                if(vch2.gt.vfr)then
                  ceq(ie)=0.
!                  frep=1.
! si la vitesse est suffisante
                else
#if SEDVAR
                  if(sedvar)then
                    d90=d*sigmae2(ie)**1.15
                  endif
!end if sedvar
#endif
! introduction de fra2 pour eviter les valeurs trop petites si he petit
                  chez2=max(18.*log(4.*het(ie)/d90)/log(10.),sqrt(fra2))
! calcul de la vitesse de frottement relative aux grains
                  uprime=sg*vstar/chez2
! calcul de la vitesse de frottement critique relative au frottement critique
                  ucr=sqrt(taucr/den)
! si la vitesse de frottement relative aux grains est suffisante
                  if(uprime.gt.ucr)then
! calcul du parametre de transport
                    transp=(uprime**2-ucr**2)/ucr**2
! calcul des asperites
                    afond=max(0.01*het(ie),3.*d90)
! calcul de la concentration du fond
! cafond modifie suite a modif diams
                    cafond=0.015*d**0.7*transp**1.5/(afond*diams**0.3)
!                    cafond=0.015*d*transp**1.5/afond/diams**0.3
!                     cafond=0.015*d*transp**1.5/afond/re**0.3
                    afond=afond/het(ie)
!                diams=d*(1.+0.011*(sigs-1.)*(transp-25.)
! calcul du parametre de suspension
                    beta=1.+2.*(vch2/vfr)**2
                    phi=2.5*(vch2/vfr)**0.8*(cafond/0.65)**0.4
                    zed=2.5*vch2/vfr/beta
                    zed=zed+phi
! si ce parametre est trop grand
                    if(zed.gt.1.)zed=1.
! calcul de la concentratino d'equilibre en m3/m3
! si les asperites sont suffisamment petites
                    if(afond.lt.0.1)then
                      frep=(afond**zed-afond**1.2)/(1.-afond)**zed/(1.2-zed)
! si les asperites sont trop importantes
                    else
                      frep=(0.1**zed-0.0631)/0.9**zed/(1.2-zed)
                    endif
! cafond est en m3/m3
                    ceq(ie)=frep*cafond
!  if sur uprime par rapport a ucr
                  else
                    ceq(ie)=0.
                  endif
! fin du if sur vitesse de chute
                endif
! si pas van rijn
              elseif(.not.meyer)then
! si il y a du sediment
                if(cet(ie).gt.eps2)then
! calcul de la concentration en sediments en m3/m3
                  cee=cet(ie)
! calcul de la concentration d'equilibre
                  if(consed)then
                    if(q.gt.eps2)then
            taucs=(dens-den)*g*het(ie)*vch2*hcet(ie)/(tk*q)
                    else
                      taucs=taucr
                    endif
                  else
                    taucs=taucr
                  endif
!      limitation de tau/taucr a 10
                  ceq(ie)=cee*min(tau/taucs,10.d0)
! si pas de sediment arrivant
                else
                  cee=0.
                  ceq(ie)=0.
                endif
! fin du if sur van rijn
              endif
! si on ne s'est pas donne le coefficient alprep on ne le lie
! qu'� la repartition des concentrations
! calcul de la sedimentation
! calcul a partir de meyer peter muller, camenen ou engelund hansen
              if(meyer)then
                if(efficace)then
! on corrige pour obtenir tau efficace modifie ave! ksi
!                frcor=fra2**0.75*ksi*het(ie)**(-0.25)
                frcor=fra2**0.75*ksi*(d/het(ie))**0.25
! on ne corrige que pour trouver un tau efficace inf�rieur a 1
                if(frcor.lt.1.)then
                  tau=tau*frcor
                  vfr=sqrt(tau/den)
                  vitfro(ie)=vfr
                endif
! fin du if sur efficace
                endif
! m contient le coefficient de meyer peter et alprep inverse distance chargement
! si meyer ceq=0 partout gard� et pas de taucs
                if(tau.lt.eps2)then
                  ero=0.0
                elseif(tau.le.taucr) then
                  if(camenen)then
                    if(tau.gt.0.1*taucr)then
                      ero=m*tau**1.5*exp(-4.5*taucr/tau)
                    else
                      ero=0.0
                    endif
                  else
                    ero=0.0
                  endif
!      limitation de tau/taucr a 10
!      limitation de tau/taucr a 10 supprimee car taucr=0 si forte pente
                else
!                   if(tau.gt.10.*taucr)then
!                     tau=10.*taucr
!                   endif
                  if(camenen)then
                    ero=m*tau**1.5*exp(-4.5*taucr/tau)
                  elseif(hansen)then
                    ero=m*vstar*vstar*tau**1.5/d
                  else
                    ero=m*(tau-taucr)**1.5
                  endif
! fin du if sur tau inferieur a taucr
                endif
                if(modes)then
! calcul de la vitesse de frottement critique relative au frottement critique
                  ucr=sqrt(taucr/den)
! formule de engelund et fredsoe pour la vitesse de charriage
                  if(vfr.gt.0.7*ucr)then
                    vsed=10.*zeta*vfr*(1.-0.7*ucr/vfr)
                    if(vsed.gt.vstar)then
                      vsed=vstar
                    endif
                    if(vstar.gt.eps2)then
                      rapve(ie)=vsed/vstar
!                    else
!                      rapve(ie)=0.
                    endif
                    sed=alprep*hcet(ie)*max(vsed,vch2)
!                  if(vfr.gt.ucr)then
!                    sed=alprep*hcet(ie)*8.5*sqrt((vfr-ucr)*vfr)
! rien ne peut �tre transporte don! tout se depose
                  else
!                    rapve(ie)=0.
!                    sed=hcet(ie)/dt
! pour continuite si vsed=0.
                     sed=alprep*hcet(ie)*vch2
!                    sed=min(hcet(ie)/dt,vch2)
                  endif
                else
! modes faux indique ici que la vitesse des s�diments est la vitesse liquide multipliee par zeta
                  sed=alprep*hcet(ie)*max(vstar*zeta,vch2)
! inutile car rapva mis a zeta plus loin
!                  rapve(ie)=zeta
                endif
! deplace pour que taue toujours defini
!                taue(ie)=tau/gdd/d
! si pas meyer peter muller 2 cas ave! ou sans calculs differents erosion/depot
              elseif(modes)then
! si la concentration en sediments est superieure � la concentration d'equilibre
                if(cee.gt.ceq(ie))then
                  sed=alprep*vch2*(cee-ceq(ie))
! si la concentration est inferieure � la concentration d'equilibre
                else
                  sed=0.
                endif
                if (tau.lt.taucr) then
                  ero=0.0
!      limitation de tau/taucr a 10
                elseif(tau.gt.10.*taucr)then
                  ero=m*9.
                else
                  ero=m*(tau/taucr-1.)
                endif
! si tout calcule a partir concentration equilibre
              else
                sed=alprep*vch2*(cee-ceq(ie))
                ero=0.0
                if(sed.lt.0.)then
                  ero=-sed
                  sed=0.
                endif
! fin du if sur modes/meyer
              endif
!! fin du if sur q=0
!             endif
! on rajoute une condition car on ne peut deposer plus que present
!              if(sed.gt.hce(ie)/dt)sed=hce(ie)/dt
            eseldt(ie)=dt*(sed-ero)
! en un pas de temps, on ne peut deposer plus de 1% du sediment present
! ni eroder sur plus que 1% de la hauteur d'eau
! le 24/1/2014: on peut tout d�poser
            if(eseldt(ie).gt.hcet(ie))then
              eseldt(ie)=hcet(ie)
!            if(eseldt(ie).gt.0.01*hcet(ie))then
!              eseldt(ie)=0.01*hcet(ie)
            else
             hcmin=-0.01*het(ie)
!             if(cet(ie).gt.eps2)then
!               hcmin=max(hcmin,-0.01*hcet(ie))
!             endif
               if(eseldt(ie).lt.hcmin)then
                 eseldt(ie)=hcmin
               endif
!             endif
            endif
! introduction pour limiter erosion ou depot sur un pas de temps
            rz=eseldt(ie)/dt
            if(rz.gt.vch2)then
              eseldt(ie)=vch2*dt
              if(idl.lt.1000)then
              write(*,*)'depot limite maille',ie
              idl=idl+1
              endif
            elseif(rz.lt.-rzmax)then
              eseldt(ie)=-rzmax*dt
!              if(eseldt(ie).lt.-0.01*het(ie))then
!                eseldt(ie)=-0.01*het(ie)
!              endif
              if(idl.lt.1000)then
              write(*,*)'erosion limite maille',ie
!              write(*,*)'depot limite maille',ie
              idl=idl+1
              endif
            endif
! pour les faibles hauteurs, on autorise depot seulement
! hmin abandonne pour d
!            if(het(ie).lt.hmin)then
            if(het(ie).lt.0.5*d)then
              eseldt(ie)=max(0.d0,eseldt(ie))
!              eseldt(ie)=min(max(0.d0,eseldt(ie)),hcet(ie))
            endif
            eseldt(ie)=poro*eseldt(ie)
            taue(ie)=tau
!  si he<paray
          else
            eseldt(ie)=0.
!            eseldt(ie)=poro*min(hcet(ie),vch*dt)
            vitfro(ie)=0.
!            taue(ie)=0.
!            rapve(ie)=0.
! fin du if sur he
          endif
!          if(eseldt(ie).gt.0.)then
! si depot on fait une correction en fonction dt precedent
! rapsmbh! est la correction pour non fourniture de depot
!            eseldt(ie)=eseldt(ie)*rapsmbhc(ie)
!          else
!            rapsmbhc(ie)=1.
!          endif
    end do

        call ctopo_local!(zfn2,zfa2,zfe2,cofr2)
        do ieloc=1,ne_loc(me)
          ie=mailles_loc(ieloc)
          eseldt(ie)=dhe(ie)
          smbhc(ie)=-dhe(ie)/poro
! on met dans dhe le taux de d�p�t
          dhe(ie)=dhe(ie)/dt

        enddo
        if(meyer)then
        if(modes)then
#ifdef WITH_MPI
         call message_group_calcul(rapve)
#endif
         do ialoc = 1,naloc
          ia = aretes_loc(ialoc)
          ie=ieva(ia,1)
          ie2=ieva(ia,2)
          if(ie.gt.0)then
            if(ie2.gt.0)then
              rapva(ia)=0.5*(rapve(ie)+rapve(ie2))
            else
              rapva(ia)=rapve(ie)
            endif
          else
              rapva(ia)=rapve(ie2)
          endif
        enddo
        else
        do ialoc = 1,naloc
          ia = aretes_loc(ialoc)
          rapva(ia)=zeta
        enddo
        endif
! fin du if sur meyer
        endif
! fin du if sur sediments/solute
      endif
      end

!******************************************************************************
      subroutine ctopo_local!(zfn2,zfa2,zfe2,cofr2)
!******************************************************************************
! sous-programme de modification de la topographie par rapport
! a la cote  moyenne zfm sur le sous-domaine local au processeur
! entree  :
!          eseldt : variation de cotes des elements
!  sortie :
!          zfe : cotes des elements
!          zfa : cotes des aretes
!          zfn : cotes des noeuds
!          dhe : variation de cotes des elements
!          cofr2: nouvelle pente maille
!
!******************************************************************************
      use module_precision

      use module_tableaux,only:eau,ne,na,nn,zfa,zfe,zfm,iae,nne&
      &,eps1,eps2,paray,xn,yn,se,la,dt,cfl,dt0,t,zfn,ina,ine,ievn,nevn

      use module_ts,only:eseldt,diame2,sigmae2,epcoun,diamn,sigman,taummn&
      &,nbcoun,sigma0,taue,dhe,zfndur,cofr2,zfn2,zfa2,zfe2

      use module_mpi,only:ne_loc,me,mailles_loc,naloc,aretes_loc,nnloc&
      &,noeuds_loc,nmv,elt_voisins

#ifdef WITH_MPI
      use module_messages
#endif /* WITH_MPI */

#if SEDVAR
     use module_ts,only:sedvar,diamdhn,sigmadhn,camenen,exptaucr,meyer,modes&
     &,hcet,diamsmb,sigmasmb,hansen
!end if sedvar
#endif


      implicit none

      real(WP) :: dhn(nn)
      integer :: ien(nn)
!!!!!      real(WP),intent(inout) :: cofr2(ne),zfn2(nn),zfa2(na),zfe2(ne)
      integer :: ie,in,jn,ia,j,ieloc,k
      integer :: ialoc
      integer :: ie1,ie2,ie3,ie4,nen(nn)
#if SEDVAR
     real(wp) :: hcn(nn),m,dm,sm
     logical :: erosion
!end if sedvar
#endif

!  calcul des hauteurs aux noeuds
!**************************************************************

     do jn=1,nnloc
       in=noeuds_loc(jn)
       dhn(in)=0.
       ien(in)=0
     enddo
! initialisation pour que ne pas avoir de valeur aberrante
! mais inutile car dhn=0
#if SEDVAR
      if(sedvar)then
         do jn=1,nnloc
           in=noeuds_loc(jn)
!             hcn(in)=0.
           diamdhn(in)=diamn(in,1)
           sigmadhn(in)=sigman(in,1)
         enddo
      endif
!end if sedvar
#ifdef WITH_MPI
      call message_group_calcul(taue)
      call message_group_calcul(diame2)
      call message_group_calcul(sigmae2)
      call message_group_calcul(hcet)
#endif
#endif
#ifdef WITH_MPI
      call message_group_calcul(eseldt)
#endif

      do jn=1,nnloc
        in=noeuds_loc(jn)
        do ieloc=1,nevn(in)
          ie=ievn(in,ieloc)
          ien(in)=ien(in)+1

#if SEDVAR
          if(sedvar)then
! si erosion
             if(eseldt(ie).lt.-eps2)then
                erosion=.true.
                if(taue(ie).lt.taummn(in,1))then
                   if(modes.and.(.not.camenen))then
                      erosion=.false.
                   elseif(meyer.and.(.not.camenen))then
                      erosion=.false.
                   endif
                endif
!
! si tau superieur a taummn
!             if(taue(ie).gt.taummn(in,1))then
                if(erosion)then
                   if(dhn(in).lt.-eps2)then
                      dhn(in)=-dhn(in)
                      call melangeds(dhn(in),-eseldt(ie)&
                      &,diamdhn(in),sigmadhn(in),diame2(ie),sigmae2(ie))
                      dhn(in)=-dhn(in)
! si dhn positif
                   elseif(dhn(in).gt.eps2)then
                      if(dhn(in).gt.-eseldt(ie))then
                         call dmelangeds(dhn(in),-eseldt(ie),diamdhn(in)&
                         &,sigmadhn(in),diame2(ie),sigmae2(ie),ie)
                      else
! dhn positif mais plus petit que -eseldt
                         m=-eseldt(ie)
                         dm=diame2(ie)
                         sm=sigmae2(ie)
                         call dmelangeds(m,dhn(in)&
                         &,dm,sm,diamdhn(in),sigmadhn(in),ie)
                         dhn(in)=dhn(in)-m
                         diamdhn(in)=dm
                         sigmadhn(in)=sm
! fin du if sur dhn/eseldt
                      endif
! le else veut dire taue inferieur a taummn
! pour les cas ou cela compte
                   else
! dhn=0
                      dhn(in)=dhn(in)+eseldt(ie)
                      diamdhn(in)=diame2(ie)
                      sigmadhn(in)=sigmae2(ie)
! fin du if sur dhn
                   endif
!                dhn(in)=dhn(in)+eseldt(ie)
! dans le cas contraire dhn additionnel=0
! fin du if sur tau
                endif
! si depot
             elseif(eseldt(ie).gt.eps2)then
                if(dhn(in).gt.eps2)then
                   call melangeds(dhn(in),eseldt(ie),diamdhn(in)&
                   &,sigmadhn(in),diame2(ie),sigmae2(ie))
                elseif(dhn(in).lt.-eps2)then
                   if(dhn(in).lt.-eseldt(ie))then
                      m=-dhn(in)
                      call dmelangeds(m,eseldt(ie),diamdhn(in)&
                      &,sigmadhn(in),diame2(ie),sigmae2(ie),ie)
                      dhn(in)=dhn(in)-m
! dhn plus petit que esleldt
                   else
                      m=eseldt(ie)
                      dm=diame2(ie)
                      sm=sigmae2(ie)
                      call dmelangeds(m,-dhn(in)&
                      &,dm,sm,diamdhn(in),sigmadhn(in),ie)
                      dhn(in)=dhn(in)-m
                      diamdhn(in)=dm
                      sigmadhn(in)=sm
! fin du if sur eseldt/dhn
                   endif
                else
! dhn=0
                   dhn(in)=dhn(in)+eseldt(ie)
                   diamdhn(in)=diame2(ie)
                   sigmadhn(in)=sigmae2(ie)
! fin du if sur dhn
                endif
! fin du if sur erosion
             endif
             hcn(in)=hcn(in)+hcet(ie)
           else
#endif
             dhn(in)=dhn(in)+eseldt(ie)
#if SEDVAR
          endif
!end if sedvar
#endif

        enddo
      enddo


       do jn=1,nnloc
         in=noeuds_loc(jn)
         dhn(in)=dhn(in)/float(ien(in))
       enddo
!  calcul des cotes des noeuds par rapport a la moyenne
!******************************************************
#if SEDVAR
      if(sedvar)then
       do jn=1,nnloc
         in = noeuds_loc(jn)
!        hcn(in)=hcn(in)/float(ien(in))
! erosion
          if(dhn(in).lt.-eps2)then
! on limite erosion a epaisseur premiere couche
             if(-dhn(in).gt.epcoun(in,1))then
               dhn(in)=-epcoun(in,1)
               call supcouchn(in)
              diamdhn(in)=diamn(in,1)
              sigmadhn(in)=sigman(in,1)
             else
               call erocouchn(in,dhn(in),diamdhn(in),sigmadhn(in))
             endif
! depot
          elseif(dhn(in).gt.eps2)then
            diamdhn(in)=0.d0
            sigmadhn(in)=0.d0
            nen(in)=0
!            call depcouchn(in,dhn(in),hcn(in),diamdhn(in),sigmadhn(in))
          else
            dhn(in)=0.
            diamdhn(in)=diamn(in,1)
            sigmadhn(in)=sigman(in,1)
            call zerocouchn(in)
          endif
          zfn2(in)=zfn(in)+dhn(in)
! fin boucle sur in
        enddo
! else sur sedvar
      else

!end if sedvar
#endif
       do jn=1,nnloc
          in = noeuds_loc(jn)
          zfn2(in)=zfn(in)+dhn(in)
          if ((zfn2(in)).lt.zfndur(in)) then
             zfn2(in)=zfndur(in)
! dhn corrige pour fond dur(toujours negative)
             dhn(in)=min(zfndur(in)-zfn(in),0.d0)
!        else
!           dhn(in)=0.
          endif
       enddo
#if SEDVAR
! fin du if sur sedvar
       endif
!end if sedvar
#endif
!  calcul des cotes des aretes
!******************************************************
      do ialoc = 1,naloc
        ia = aretes_loc(ialoc)

        zfa2(ia) =.5*(zfn2(ina(ia,1))+zfn2(ina(ia,2)))
!        dha(ia) =.5*(dhn(ina(ia,1))+dhn(ina(ia,2)))
      enddo

!  calcul des cotes des elements
!*******************************
      do ieloc = 1,ne_loc(me)
        ie = mailles_loc(ieloc)
        zfe2(ie)=zfa2(iae(ie,1))
!        dhe2(ie)=dha(iae(ie,1))
        do j=2,nne(ie)
          zfe2(ie)=zfe2(ie)+zfa2(iae(ie,j))
!          dhe2(ie)=dhe2(ie)+dha(iae(ie,j))
         enddo

        zfe2(ie)=zfe2(ie)/float(nne(ie))
      enddo
      do ieloc = 1,ne_loc(me)
        ie = mailles_loc(ieloc)
!        dhe2(ie)=dhe2(ie)/float(nne(ie))
!       calcul de la variation de cote du fond
        dhe(ie)=zfe2(ie)-zfe(ie)
        ie1=ine(ie,1)
        ie2=ine(ie,2)
        ie3=ine(ie,3)
        if(abs(dhe(ie)).lt.eps2)then
            zfe2(ie)=zfe(ie)
            dhe(ie)=0.
#if SEDVAR
          if(sedvar)then
            diamsmb(ie)=diamn(ie1,1)
            sigmasmb(ie)=sigman(ie1,1)
! fin du if sur sedvar
          endif
!end if sedvar
#endif

! else sur dhe=0
        else
#if SEDVAR
        if(sedvar)then
! on melange les dhn de la maille pour obtenir diamsmb
          if(nne(ie).eq.3)then
          call melangemaille(ie,-dhn(ie1)&
     &,-dhn(ie2),-dhn(ie3),0.d0,nen(ie1),nen(ie2),nen(ie3),0)
          else
          call melangemaille(ie&
     &,-dhn(ie1),-dhn(ie2),-dhn(ie3),-dhn(ine(ie,4))&
     &,nen(ie1),nen(ie2),nen(ie3),nen(ine(ie,4)))
          endif
! fin du if sur sedvar
        endif
!end if sedvar
#endif
! endif sur dhe=0
        endif
!        calcul surface horizontale de l'element
          if(nne(ie).eq.4)then
            ie4=ine(ie,4)
          endif

!        calcul surface de l'element en 3d pour pente
! surface du triangle 123
          cofr2(ie)=sqrt(((xn(ie3)-xn(ie2))*(yn(ie1)-yn(ie3))-&
     &(yn(ie3)-yn(ie2))*(xn(ie1)-xn(ie3)))**2+&
     &((xn(ie3)-xn(ie2))*(zfn2(ie1)-zfn2(ie3))-&
     &(zfn2(ie3)-zfn2(ie2))*(xn(ie1)-xn(ie3)))**2+&
     &((yn(ie3)-yn(ie2))*(zfn2(ie1)-zfn2(ie3))-&
     &(zfn2(ie3)-zfn2(ie2))*(yn(ie1)-yn(ie3)))**2 )
   !cofr2(ie) = xn(ie2)
!      print *,"Hello , ",ie ," ... ",ie2

          if(nne(ie).eq.4)then
! on ajoute la surface du triangle 143
            cofr2(ie)=cofr2(ie)+sqrt(((xn(ie3)-xn(ie4))*&
     &(yn(ie1)-yn(ie3))-(yn(ie3)-yn(ie4))*(xn(ie1)-xn(ie3)))**2+&
     &((xn(ie3)-xn(ie4))*(zfn2(ie1)-zfn2(ie3))-&
     &(zfn2(ie3)-zfn2(ie4))*(xn(ie1)-xn(ie3)))**2+&
     &((yn(ie3)-yn(ie4))*(zfn2(ie1)-zfn2(ie3))-&
     &(zfn2(ie3)-zfn2(ie4))*(yn(ie1)-yn(ie3)))**2 )
         endif
! cofr2 contient deux fois la surafce en 3d
         cofr2(ie) = 2.*abs(se(ie)/cofr2(ie))
!         cofr2(ie) = abs(se(ie)/cofr2(ie))
!         se(ie) = 0.5*abs(se(ie))
 enddo

! on applique  le depot aux sommets concern�s
#if SEDVAR
      if(sedvar)then
         do jn=1,nnloc
          in = noeuds_loc(jn)
          if(dhn(in).gt.eps2)then
            if(nen(in).gt.0)then
! sigmadhn contient la somme des d84 et diamdhn la somme des d50
              sigmadhn(in)=sigmadhn(in)/diamdhn(in)
              diamdhn(in)=diamdhn(in)/nen(in)
              call depcouchn(in,dhn(in)&
     &,diamdhn(in),sigmadhn(in))
             else
! si nen(in)=0 pas de depot
               dhn(in)=0.
               diamdhn(in)=diamn(in,1)
               sigmadhn(in)=sigman(in,1)
             endif
          endif
        enddo
      endif
!end if sedvar
#endif
      end






!*****************************************************************************
      subroutine erc(if,t)
!******************************************************************************
!     edition resultats dans etude.mas


!cou1d       logical couplage1d(namax)
      use module_precision

      use module_tableaux, only : ie1,ie2,ia1,ia2,j1,j2,nouv,nbouv,debut&
      &,eps1,eps2,paray,houv,qouv,ieva,fha,fqua,fqva,hae,se,la,zfm,nrefa&
      &,zfa,xa,ya,ne,na,nn,vols,debut,id_mas,nouveau_format

      use module_ts, only : diama,sigmaa,diamouv,sigmaouv,conckg,coouv,fhca&
      &,esdt,dens,volas

#if SEDVAR
     use module_ts,only:sedvar
!end if sedvar
#endif


      implicit none

!cou1d      common/couplage1d/couplage1d
      integer if,ia,iouv
      real(WP) :: fq
      real(wp),intent(in) :: t

      logical :: ecrit

      save ecrit

      if(debut)then
        if (if.gt.0)then
           ecrit=.true.
        endif
      else
        if(ecrit)then
      write (id_mas,*)
      write (id_mas,*) 'temps  : ',t
      write (id_mas,*)
      write (id_mas,*) 'arete   h arete    conc arete    diametre',&
     &             '     etendue     x arete     y arete  ref'
      write (id_mas,*) '--------------------------------------',&
     &             '--------------------------------'
! si concentrations en kg/m3
      if(conckg)then
           do ia = 1,na
             if(nrefa(ia).gt.0)then
               if(nrefa(ia).ne.2&
!cou1d     :.or.couplage1d(ia) &
     &)then
!       if(nrefa(ia).gt.0.and.nrefa(ia).ne.2)then
                 if(abs(fha(ia)).gt.eps2)then
                    fq=fhca(ia)*dens/fha(ia)
                 else
                   fq=0.
                 endif
                 write(id_mas,1)ia,hae(ia),fq,diama(ia),sigmaa(ia)&
!         write(id_mas,1)ia,hae(ia),fq,la(ia),zfa(ia)+zfm,xa(ia),ya(ia),
     &,xa(ia),ya(ia),nrefa(ia)
! fin du if sur nrefa=2
               endif
! fin du if sur nrefa=0
             endif
           enddo
! on ecrit la concentration de l'ouvrage iouv sur arete ia1
#if SEDVAR
          if(sedvar)then
        if(nouveau_format)then
                do iouv=1,nbouv
                if(nouv(iouv).ne.0)then
                  ia=ia1(iouv)
                  write(id_mas,2)ia,houv(ia,j1(iouv)),coouv(ia,j1(iouv))*dens&
     &,diamouv(ia,j1(iouv)),sigmaouv(ia,j1(iouv))&
     &,xa(ia),ya(ia),nrefa(ia)
! fin if sur nouv
                endif
! fin boucle sur iouv
              enddo
       else ! else if nouveau_format
                do iouv=1,nbouv
                if(nouv(iouv).ne.0)then
                  ia=ia1(iouv)
                  write(id_mas,1)ia,houv(ia,j1(iouv)),coouv(ia,j1(iouv))*dens&
     &,diamouv(ia,j1(iouv)),sigmaouv(ia,j1(iouv))&
     &,xa(ia),ya(ia),nrefa(ia)
! fin if sur nouv
                endif
! fin boucle sur iouv
              enddo
        endif ! fin if nouveau_format
! pas sedvar
         else
!end if sedvar
#endif
        if(nouveau_format)then
             do iouv=1,nbouv
               if(nouv(iouv).ne.0)then
                 ia=ia1(iouv)
                 write(id_mas,2)ia,houv(ia,j1(iouv)),coouv(ia,j1(iouv))*dens&
     &,diama(ia),sigmaa(ia)&
     &,xa(ia),ya(ia),nrefa(ia)
! fin if sur nouv
               endif
! fin boucle sur iouv
              enddo
       else ! else if nouveau_format
             do iouv=1,nbouv
               if(nouv(iouv).ne.0)then
                 ia=ia1(iouv)
                 write(id_mas,1)ia,houv(ia,j1(iouv)),coouv(ia,j1(iouv))*dens&
     &,diama(ia),sigmaa(ia)&
     &,xa(ia),ya(ia),nrefa(ia)
! fin if sur nouv
               endif
! fin boucle sur iouv
              enddo
        endif ! fin if nouveau_format


#if SEDVAR
! fin du if sur sedvar
          endif
!end if sedvar
#endif

! si concentrations adimensionnelles
      else
        if(nouveau_format)then
          do ia = 1,na
            if(nrefa(ia).gt.0.and.nrefa(ia).ne.2)then
              if(abs(fha(ia)).gt.eps2)then
                fq=fhca(ia)/fha(ia)
              else
                fq=0.
              endif
              write(id_mas,2)ia,hae(ia),fq,diama(ia),sigmaa(ia)&
!         write(id_mas,1)ia,hae(ia),fq,la(ia),zfa(ia)+zfm,xa(ia),ya(ia),
     &,xa(ia),ya(ia),nrefa(ia)
            endif
          enddo
        else ! else if nouveau_format
          do ia = 1,na
            if(nrefa(ia).gt.0.and.nrefa(ia).ne.2)then
              if(abs(fha(ia)).gt.eps2)then
                fq=fhca(ia)/fha(ia)
              else
                fq=0.
              endif
              write(id_mas,1)ia,hae(ia),fq,diama(ia),sigmaa(ia)&
!         write(id_mas,1)ia,hae(ia),fq,la(ia),zfa(ia)+zfm,xa(ia),ya(ia),
     &,xa(ia),ya(ia),nrefa(ia)
            endif
          enddo
        endif ! fin if nouveau_format

#if SEDVAR
! on ecrit la concentration de l'ouvrage iouv sur arete ia1
          if(sedvar)then
           if(nouveau_format)then
            do iouv=1,nbouv
              if(nouv(iouv).ne.0)then
                ia=ia1(iouv)
                write(id_mas,2)ia,houv(ia,j1(iouv)),coouv(ia,j1(iouv))&
     &,diamouv(ia,j1(iouv)),sigmaouv(ia,j1(iouv))&
     &,xa(ia),ya(ia),nrefa(ia)
! fin if sur nouv
              endif
! fin boucle sur iouv
            enddo
           else ! else if nouveau_format
            do iouv=1,nbouv
              if(nouv(iouv).ne.0)then
                ia=ia1(iouv)
                write(id_mas,1)ia,houv(ia,j1(iouv)),coouv(ia,j1(iouv))&
     &,diamouv(ia,j1(iouv)),sigmaouv(ia,j1(iouv))&
     &,xa(ia),ya(ia),nrefa(ia)
! fin if sur nouv
              endif
! fin boucle sur iouv
            enddo
           endif ! fin if nouveau_format
! si pas sedvar
          else
!end if sedvar
#endif
            if(nouveau_format)then
             do iouv=1,nbouv
               if(nouv(iouv).ne.0)then
                 ia=ia1(iouv)
                 write(id_mas,2)ia,houv(ia,j1(iouv)),coouv(ia,j1(iouv))&
     &,diama(ia),sigmaa(ia)&
     &,xa(ia),ya(ia),nrefa(ia)
! fin if sur nouv
               endif
! fin boucle sur iouv
             enddo
            else ! else if nouveau_format
             do iouv=1,nbouv
               if(nouv(iouv).ne.0)then
                 ia=ia1(iouv)
                 write(id_mas,1)ia,houv(ia,j1(iouv)),coouv(ia,j1(iouv))&
     &,diama(ia),sigmaa(ia)&
     &,xa(ia),ya(ia),nrefa(ia)
! fin if sur nouv
               endif
! fin boucle sur iouv
             enddo
            endif ! fin if nouveau_format

#if SEDVAR
! fin du if sur sedvar
         endif
!end if sedvar
#endif

! fin du if sur conckg
      endif
   1  format(i6,6f12.5,i3)
   2  format(i9,6f12.5,i3)
! ecriture globale
      write (id_mas,*) '--------------------------------------',&
     &             '--------------------------------'
      write(id_mas,*)'temps ',' volume d eau sorti ',&
     &' masse de sediments sortie '&
     &,' esdt (bilan d�pot/�rosion)'
      write(id_mas,*) t,vols,volas,esdt
! fin du if sur ecrit
      endif
! fin du if sur debut (on n'ecrit pas la prmiere fois)
      endif
      end




!*****************************************************************************
      subroutine etc(if,t)
!******************************************************************************
!     edition concentrations en certains points  dans etude.hyc
!
!      implicit logical (a-z)


      use module_precision

      use module_tableaux, only : eps1,eps2,paray,het,quet,qvet,nbpt,ienb&
      &,nlimmax,id_hyc

      use module_ts, only : cet,vitfro,dhe,conckg,dens

!#if SEDVAR
!     use module_ts,only:sedvar,diame,sigmae
!end if sedvar
!#endif

      implicit none


      real(wp) dh(nlimmax),vf(nlimmax),co(nlimmax)
      real(wp),intent(in) :: t

!#if SEDVAR
!     real(wp) :: diam(nlimmax),sigma(nlimmax)
!end if sedvar
!#endif

      integer if,i

      if(if.gt.0)then
      do 1 i=1,nbpt
       if(het(ienb(i)).gt.paray)then
         co(i)=cet(ienb(i))
! pour ecrire des valeurs lisibles on met dh en mm/h au lieu de m/s
         dh(i)=dhe(ienb(i))*3600000.
         vf(i)=vitfro(ienb(i))
       else
         co(i)=0.
         dh(i)=0.
         vf(i)=0.
       endif
!var!       if(sedvar)then
!var!         diam(i)=diame(ienb(i))
!var!         sigma(i)=sigmae(ienb(i))
!var!       endif
 1    continue
      write (id_hyc,*) t
      if(conckg)then
        write (id_hyc,'(5f15.7)')(min(co(i)*dens,999999.999d0),i=1,nbpt)
      else
        write (id_hyc,'(5f15.7)')(min(co(i),999999.999d0),i=1,nbpt)
      endif
!var!      if(sedvar)then
!var!            write(id_hyc,'(5f15.7)')(diam(i),i=1,nbpt)
!var!            write(id_hyc,'(5f15.7)')(sigma(i),i=1,nbpt)
!var!      else
      write (id_hyc,'(5f15.7)')&
     &(max(-99999.999d0,min(dh(i),999999.999d0)),i=1,nbpt)
      write (id_hyc,'(5f15.7)')&
     &(max(-99999.999d0,min(vf(i),999999.999d0)),i=1,nbpt)
! fin du if sur sedvar
!var!      endif
      endif
      return
      end

!******************************************************************************
      subroutine correccharriage(ia,ie,fhc,fha,ha)
!******************************************************************************
! calcul de la modification du flux charrie pour tenir compte courbure et pente du fond
      use module_precision

      use module_tableaux,only:ne,na,nn,eps1,eps2,paray,het,quet,qvet&
      &,alpha,pxzfe,pyzfe,pxque,pyque,pxqve,pyqve,xna,yna,xta,yta,ina

      use module_ts,only:rapva,taun,taue,constantea,constanteb,modifz&
      &,chzn,diame,sigmae,sg,rzmax,diams,fi,fi2,al,ksi,gdd&
      &,niux,niuy,dens,den,m,kds,d,tk,consnu,vch,zeta

#if SEDVAR
     use module_ts,only:sedvar
!end if sedvar
#endif

      implicit none

      integer ia,ie

      double precision ha,fhc,tau,fm1,qm1,courbure,gradienfon,tangentepsi&
     &,vsednormale,fha

! modif juillet 2019
!      vsedx(ie)=0.
!      vsedy(ie)=0.
      if(rapva(ia).gt.eps2)then
       if(het(ie).gt.paray)then
        qm1=sqrt(quet(ie)**2+qvet(ie)**2)
        if(qm1.gt.eps2)then
          if(chzn)then
            tau=max(taun(ina(ia,1)),taun(ina(ia,2)))
          else
            tau=taue(ie)
          endif
          if(tau.gt.eps2)then
! tau mis en adimensionnel

#if SEDVAR
            if(sedvar)then
               tau=tau/(gdd*diame(ie))
            else
!end if sedvar
#endif

               tau=tau/(gdd*d)
#if SEDVAR
            endif
!end if sedvar
#endif
            fm1=1./(constanteb*sqrt(tau))
          else
            fm1=0.
          endif
          qm1=1./qm1
          courbure=(quet(ie)*(quet(ie)*pxqve(ie)-qvet(ie)*pxque(ie))+&
     &    qvet(ie)*(quet(ie)*pyqve(ie)-qvet(ie)*pyque(ie)))*qm1**3
          gradienfon=(quet(ie)*pyzfe(ie)-qvet(ie)*pxzfe(ie))*qm1
          tangentepsi=-(constantea*ha*courbure+fm1*gradienfon)
!      modvsed=rapve(ie)/(HET(IE)*sqrt(1.+TANGENTEPSI**2))
!      vsedx(ie)=(quet(ie)-qvet(ie)*TANGENTEPSI)*modvsed
!          vsedy(ie)=(quet(ie)*TANGENTEPSI+qvet(ie))*modvsed
! composante normale de la vitesse sedimentaire multipliee par h
          vsednormale=(quet(ie)-qvet(ie)*tangentepsi)*xna(ia)+&
     &    (quet(ie)*tangentepsi+qvet(ie))*yna(ia)
          vsednormale=vsednormale/sqrt(1.+tangentepsi**2)
          if(fha.gt.eps2)then
            fhc=fhc*min(abs(vsednormale*rapva(ia)/fha),1.d0)
          else
            fhc=0.
          endif
! on met a 0 car pas de raison de transfert si vitesse amont nulle
! si qm1 nul
        else
            taue(ie)=0.
!            vsedx(ie)=0.
!            vsedy(ie)=0.
            fhc=0.
        endif
! on met a 0 car pas de raison de transfert si pas d'eau amont
! si he nul
       else
            taue(ie)=0.
!            vsedx(ie)=0.
!            vsedy(ie)=0.
            fhc=0.
       endif
! cas ou rapva est nul
      else
        fhc=0.
      endif
      return
end

!**********************************************************************
      subroutine lecsed
!---------------------------------------------------------------------c
!     lecture du fichier sed des couches sedimentaires
!=====================================================================c


      use module_tableaux,only:ne,na,nn,tm,etude,tr,tinit,tmax,eps1, eps2, paray
#if TRANSPORT_SOLIDE
      use module_tableaux,only:ifen,irep
#endif

      use module_ts,only:epcoun,diamn,sigman,taummn,diame,sigmae,diama,sigmaa&
      &,sedvar,nbcoun,niux,niuy,dens,den,m,kds,d,tk,consnu,vch,zeta&
      &,sigma0,ncousedmax

        implicit none

      integer in,jn,ia,ie
      logical encore,encore2

      character*470 ligne, ligne2*46
      integer nltotal
      double precision t



      ligne2='                                              '
      encore=.true.
! on compte le nombre de lignes lues
      nltotal=0
      open (37,file=trim(etude)//'.sed',status='old',err=10)
      do while(encore)
      in=0
      read(37,'(f15.3)',end=23,err=21)t
      nltotal=nltotal+1
      if(abs(tinit-t).gt..01)then
!iq pas le bon temps
        do in=1,nn
          read(37,*,end=23,err=21)ligne
        enddo
        nltotal=nltotal+nn
      else
        nltotal=nltotal+nn
        encore=.false.
        do in=1,nn
          read(37,'(a470)')ligne
          read(ligne(1:47),'(f12.6,2f10.7,f15.5)',err=21)&
     &epcoun(in,1),diamn(in,1),sigman(in,1),taummn(in,1)
          nbcoun(in)=1
          encore2=.true.
          do jn=2,ncousedmax
              if(encore2)then
            if(ligne((jn-1)*47+2:jn*47).ne.ligne2)then
          read(ligne((jn-1)*47+1:jn*47),'(f12.6,2f10.7,f15.5)',err=22)&
     &epcoun(in,jn),diamn(in,jn),sigman(in,jn),taummn(in,jn)
              nbcoun(in)=jn
            else
              encore2=.false.
            endif
 22         encore2=.false.
! fin du if sur encore2
              endif
! fin boucle sur jn
          enddo
! fin boucle sur in
        enddo
      endif
! fin boucle encore
      enddo
      do in=1,nn
        do jn=nbcoun(in),1,-1
           if(diamn(in,jn).lt.eps2)then
             if(jn.eq.1)then
!                sedvar=.false.
                go to 10
             else
               nbcoun(in)=jn-1
             endif
           endif
         enddo
        do jn=1,nbcoun(in)
           if(sigman(in,jn).lt.1.)then
               sigman(in,jn)=1.
           endif
           if(epcoun(in,jn).lt.eps2)then
               epcoun(in,jn)=0.1*eps2
           endif
           if(taummn(in,jn).lt.eps2)then
             call recalcultaumm(taummn(in,jn)&
     &,diamn(in,jn),sigman(in,jn))
            endif
         enddo
      enddo
      sedvar=.true.
#if ENG
      write(*,*)'WITH SEDIMENT LAYERS'
#else
      write(*,*)'COUCHES SEDIMENTAIRES PRISES EN COMPTE'
#endif
! on ferme le fichier et on le reouvre jusqu'a la ligne
! correspondant au temps initial
      close (37)
      open (37,file=trim(etude)//'.sed',status='old')
      do in=1,nltotal-nn-1
          read(37,*)ligne
      enddo
! si reprise, sed pas ecrit ailleurs au temps initial
! donc necessite de relire ce temps initial aussi
! pour qu il soit present dans sed
#if TRANSPORT_SOLIDE
      if(irep.eq.1)then
        do in=1,nn+1
          read(37,*)ligne
        enddo
      endif
#endif
      return
 23  write(*,*) 'fin fichier sed sans temps initial trouve'
      stop
      return
 21   write(*,*) 'erreur fichier sed : noeud ',in
      stop
      return
  10  sedvar=.false.
      do ia=1,na
        diama(ia)=d
        sigmaa(ia)=sigma0
      enddo
      do ie=1,ne
        diame(ie)=d
        sigmae(ie)=sigma0
      enddo
      return
      end

!**********************************************************************
      subroutine ecrsed(t)
!---------------------------------------------------------------------c
!     ecriture du fichier sed des couches sedimentaires
!     le fichier sed reste ouvert pendant tout le calcul
!=====================================================================c


      use module_tableaux,only:ne,na,nn
      use module_ts,only:epcoun,sigman,diamn,taummn,nbcoun

      implicit none

      integer in,jn
      double precision t

! TODO send nodes to scribe

!      fichier 37 est etude//'.sed'
      write(37,'(f15.3)')t
      do in=1,nn
!          write(37,'(ncousedmax(f12.6,2f10.7,f15.5))')
          write(37,'(10(f12.6,f10.7,f10.5,f15.5))')&
     &(epcoun(in,jn),diamn(in,jn),min(sigman(in,jn),9999.d0)&
     &,taummn(in,jn),jn=1,nbcoun(in))
! fin boucle sur in
        enddo
      return
      end


!*****************************************************************************
      subroutine recalcultaumm(tau,diam,sigma)
!*****************************************************************************
!   recalcul de taumm en fonction diametre et etendue
!

!*****************************************************************************


      use module_ts,only:niux,niuy,dens,den,m,kds,d,tk,consnu,vch,zeta&
      &,diagsh,modes,consed,vanri,corvch,ppente,meyer&
      &,sg,rzmax,diams,fi,fi2,al,ksi,gdd

      implicit none

      double precision tau,diam,sigma,re
!     :,vfr


!              if(diagsh)then
!                vfr=0.1
! on prend arbitrairement uen vitesse de frottement de 0.1 m/s
! courbe de shields nouvelle
!                re=diam*vfr/consnu
!                if(re.le.0.2)then
!                  kds=0.575
!                elseif(re.lt.2.)then
!                  kds=0.115/re
!                elseif(re.lt.5.)then
!                  kds=0.081317279*re**(-0.5)
!                elseif(re.lt.10.)then
!                  kds=0.050175514*re**(-0.2)
!                elseif(re.lt.40.)then
!                  kds=0.025147327*re**(0.1)
!                elseif(re.lt.490.)then
!                  kds=0.017389449*re**(0.2)
!                else
!                  kds=0.060
!                endif
! fin du if sur diagsh
!              endif
! ancienne courbe shields sans vitfro
      if (diagsh) then
!           re=diam*((dens/den-1.)*g/consnu**2)**0.333333
           re=diam*(gdd/(den*consnu**2))**0.333333
           if (re.lt.4.) then
                 kds=0.24/re
           elseif (re.lt.10.) then
                kds=0.14*re**(-0.64)
           elseif (re.lt.20.) then
                kds=0.04*re**(-0.1)
           elseif (re.lt.150.) then
                kds=0.013*re**0.29
           else
                 kds=0.055
           endif
! fin du if sur diagsh
      endif
              tau=kds*gdd*diam

        return
        end

!*****************************************************************************
      subroutine melangecouches
!*****************************************************************************
!   ajout smbhc a hce
!   calcul nouvelles couches aux noeuds
!  calcul nouveau diametre transporte
!

!*****************************************************************************
      use module_tableaux,only:ne,na,nn,eps1,eps2,paray,he,que,qve,ine,nne


      use module_ts,only:smbhc,hce,diame,sigmae,diame2,sigmae2&
      &,nbcoun,nbcoun2,diamsmb,diamn,sigman,epcoun,diamn2,sigman2&
      &,taummn2,epcoun2,taummn,sigmasmb,modifz


      use module_mpi,only:ne_loc,me,mailles_loc,nnloc,noeuds_loc,nmv,elt_voisins
      implicit none

      integer ie,in,jn,kn
      integer :: ieloc
      double precision :: hceie


! modification definitive couches
       if(modifz)then
         do kn=1,nnloc
             in = noeuds_loc(kn)
             if (nbcoun2(in).gt.nbcoun(in))then
! ajout d'une couche
                nbcoun(in)=nbcoun2(in)
                do jn=nbcoun2(in),2,-1
                  diamn(in,jn)=diamn(in,jn-1)
                  sigman(in,jn)=sigman(in,jn-1)
                  taummn(in,jn)=taummn(in,jn-1)
                  epcoun(in,jn)=epcoun(in,jn-1)
                enddo
                diamn(in,1)=diamn2(in)
                sigman(in,1)=sigman2(in)
                taummn(in,1)=taummn2(in)
                epcoun(in,1)=epcoun2(in)
             elseif(nbcoun2(in).lt.nbcoun(in))then
! suppression couche
                nbcoun(in)=nbcoun2(in)
                do jn=1,nbcoun2(in)
                  diamn(in,jn)=diamn(in,jn+1)
                  sigman(in,jn)=sigman(in,jn+1)
                  taummn(in,jn)=taummn(in,jn+1)
                  epcoun(in,jn)=epcoun(in,jn+1)
                enddo
             else
! seules les caracteristiques de la couche de surface ont varie
                diamn(in,1)=diamn2(in)
                sigman(in,1)=sigman2(in)
                taummn(in,1)=taummn2(in)
                epcoun(in,1)=epcoun2(in)
             endif
! fin boucle sur in
         enddo

! fin du if sur modifz
       endif
! smbhc a un diametre et une etendue calcule
! dans ctopo ou ctopo2 lors des depots erosions
      do ieloc=1,ne_loc(me)
        ie = mailles_loc(ieloc)
         if(he(ie).gt.paray)then

! hce a pour diametre diame2,sigmae2
        hceie=hce(ie)
!    on calcule hceie mais on ne le fait pas remonter
! si apport
        if(smbhc(ie).gt.eps2)then
          call melangeds(hceie,smbhc(ie),diame2(ie)&
     &,sigmae2(ie),diamsmb(ie),sigmasmb(ie))
! si retrait
         elseif(smbhc(ie).lt.-eps2)then
           if(hceie.gt.-smbhc(ie))then
! normalement dans ce cas diamsmb deja calcule dans demixn
! est le diametre de hce+smbhc avec smbhc negatif
            diame2(ie)=diamsmb(ie)
            sigmae2(ie)=sigmasmb(ie)
!        call dmelangeds(hceie,-smbhc(ie),diame2(ie),sigmae2(ie)
!     :,diamsmb(ie),sigmasmb(ie),ie)
            endif
! rien ajoute ou hce = presque zero
! on garde hce,diame2,sigmae2
           endif
! on passe au temps suivant
           diame(ie)=diame2(ie)
           sigmae(ie)=sigmae2(ie)

! fin if sur he<paray
          endif
! fin boucle sur ie
        enddo
        return
        end

!-----------------------------------------------------------------------
      subroutine melangeds(m1,m2,d1,s1,d2,s2)
!-----------------------------------------------------------------------
! mixe les s�diments des compartiments 1 et 2
! et met le r�sultat dans le compartiment 1
!-----------------------------------------------------------------------
      use module_tableaux,only:eps1,eps2,paray

      implicit none

      double precision m1,d1,s1,m2,d2,s2
      double precision m,d,s


! contr�le des entr�es
!-----------------------------------------------------------------------
! passage de param�tres non physiques
        if(m1.lt.eps2)then
         if(m1.lt.-eps2)then
           write(*,*)' '
           write(*,*)'probleme lors de l''appel a melangeds('&
     &    ,m1,',',d1,',',s1,',',m2,',',d2,',',s2,')'
           write(*,*)' '
!           m1=0.
! on met m2 dans m1
          if(m2.gt.eps2)then
              if(d2.gt.eps2)then
                if(s2.ge.1.)then
           m1=m2
             d1=d2
             s1=s2
             endif
             endif
             endif
             return
!           stop
         else
!           m1=0.
! on met m2 dans m1
          if(m2.gt.eps2)then
              if(d2.gt.eps2)then
                if(s2.ge.1.)then
           m1=m2
             d1=d2
             s1=s2
             endif
             endif
             endif
             return
         endif
       endif
       if(m2.lt.eps2)then
         if(m2.lt.-eps2)then
           write(*,*)' '
           write(*,*)'probleme lors de l''appel a melangeds('&
     &    ,m1,',',d1,',',s1,',',m2,',',d2,',',s2,')'
           write(*,*)' '
           m2=0.
! m1 inchange
           return
!           stop
         else
           m2=0.
! m1 inchange
           return
         endif
       endif
       if(d1.lt.eps2)then
!           if(m1.lt.eps2)then
!             d1=d2
!             s1=s2
!           else
           write(*,*)' '
           write(*,*)'probleme lors de l''appel a melangeds('&
     &    ,m1,',',d1,',',s1,',',m2,',',d2,',',s2,')'
           write(*,*)' '
             if(m1.lt.0.01*m2)then
               m1=m1+m2
             d1=d2
             s1=s2
             return
      else
              stop
               endif
           endif
!       endif
       if(d2.lt.eps2)then
!           if(m2.lt.eps2)then
!             d2=d1
!             s2=s1
!           else
           write(*,*)' '
           write(*,*)'probleme lors de l''appel a melangeds('&
     &    ,m1,',',d1,',',s1,',',m2,',',d2,',',s2,')'
           write(*,*)' '
             if(m2.lt.0.01*m1)then
             m1=m1+m2
             m2=0.
             d2=d1
             s2=s1
             return
      else
              stop
               endif
           endif
!       endif
       if(s1.lt.1.)then
         if(s1.lt.1.-eps2)then
           write(*,*)' '
           write(*,*)'probleme lors de l''appel a melangeds('&
     &    ,m1,',',d1,',',s1,',',m2,',',d2,',',s2,')'
           write(*,*)' '
           s1=1.
!           stop
         else
           s1=1.
         endif
       endif
       if(s2.lt.1.)then
         if(s2.lt.1.-eps2)then
           write(*,*)' '
           write(*,*)'probleme lors de l''appel a melangeds('&
     &    ,m1,',',d1,',',s1,',',m2,',',d2,',',s2,')'
           write(*,*)' '
           s2=1.
!           stop
         else
           s2=1.
         endif
       endif
! compartiment 2 n�gligeable
      if(m2.le.eps2*m1)then
        m1=m1+m2
        return
! compartiment 2 n�gligeable (mais on ne perd pas sa masse)
! approximation lineaire
!      elseif(m2.le.eps1*m1)then
      elseif(m2.le.0.1*m1)then
        m=m1+m2
        d1=(d1*m1+d2*m2)/m
        s1=(s1*m1+s2*m2)/m
        m1=m
        return
      endif
! compartiment 1 n�gligeable
      if(m1.le.eps2*m2)then
        m1=m1+m2
!        m1=m2
        d1=d2
        s1=s2
        return
! compartiment 1 n�gligeable (mais on ne perd pas sa masse)
! approximation lineaire
!      elseif(m1.le.eps1*m2)then
      elseif(m1.le.0.1*m2)then
        m=m1+m2
        d1=(d1*m1+d2*m2)/m
        s1=(s1*m1+s2*m2)/m
        m1=m
        return
      endif

! calcul du compartiment mix�
!-----------------------------------------------------------------------
! moyenne g�om�trique simple: approximation au premier ordre
      m=m1+m2
      d=d1*(d2/d1)**(m2/m)
      s=s1*(s2/s1)**(m2/m)
      if(s.lt.1.)then
         s=1.
      endif

! affectation au compartiment 1
!-----------------------------------------------------------------------
      m1=m
      d1=d
      s1=s

      return
      end

!*****************************************************************************
      subroutine melangeiaouv(ia,ie,jouv,hceie,ahceie)
!*****************************************************************************
!   ajout flux ouvrage a hce
!  calcul nouveau diametre transporte
!
!*****************************************************************************
      use module_tableaux,only:eps1,eps2,paray

      use module_ts,only:diame,diame2,sigmae,sigmae2,diamouv,sigmaouv
!cou1d      use module_ts,only:ccouplts,dcouplts,scouplts

      implicit none

      integer ia,ie,jouv
      double precision hceie,ahceie

! si apport
      if(ahceie.gt.eps2)then
!cou1d        if(couplage1d(ia))then
!cou1d          diamouv(ia,jouv)=dcouplts(ia)
!cou1d          sigmaouv(ia,jouv)=scouplts(ia)
!cou1d        endif
        call melangeds(hceie,ahceie,diame2(ie),sigmae2(ie)&
     &,diamouv(ia,jouv),sigmaouv(ia,jouv))
! si retrait (avec ancien materiau)
       elseif(ahceie.lt.-eps2)then
          if(hceie.gt.-ahceie)then
        call dmelangeds(hceie,-ahceie,diame2(ie),sigmae2(ie)&
     &,diame(ie),sigmae(ie),ie)
           else
             hceie=0.
! on garde diame2,sigmae2
           endif
! rien ajoute
! on garde hceie,diame2,sigmae2
       endif

        return
        end

!-----------------------------------------------------------------------
      subroutine dmelangeds(m,m1,d,s,d1,s1,ie)
!-----------------------------------------------------------------------
! enleve les s�diments du compartiment 1 depuis 0 et donne ce qui reste dans 0
! on rentre avec les caracteristiques des deux compartiments
! on sort les caracteristiques du reste
! on passe ie en parametre mais inutilise en version 0
!-----------------------------------------------------------------------
      use module_tableaux,only:eps1,eps2,paray

      implicit none

      double precision m1,d1,s1,m2,d2,s2
      integer ie
! compartiment d'origine
      double precision m,d,s


! contr�le des entr�es
!-----------------------------------------------------------------------
      if(m.lt.eps2)then
        if(m.lt.-eps1)then
        write(*,*)'probleme lors de l''appel a dmelangeds'&
     &    ,m,d,s,m1,d1,s1
!        write(*,*)' '
        stop
        else
          m=0.
!          m1=0.
!          m2=0.
!          d2=d
!          s2=s
!          d1=d
!          s1=s
          return
        endif
      elseif(m1.lt.eps2)then
        if(m1.lt.-eps1)then
        write(*,*)'probleme lors de l''appel a dmelangeds'&
     &    ,m,d,s,m1,d1,s1
!        write(*,*)' '
!        stop
         return
        else
          m1=0.
!          m2=m
          return
        endif
      else
        if(d.lt.eps2)then
        write(*,*)'probleme lors de l''appel a dmelangeds'&
     &    ,m,d,s,m1,d1,s1
        write(*,*)' '
        stop
        endif
        if(d1.lt.eps2)then
        write(*,*)'probleme lors de l''appel a dmelangeds'&
     &    ,m,d,s,m1,d1,s1
!        write(*,*)' '
        stop
        endif
         m2=m-m1
        if(m2.lt.eps2)then
        if(m2.lt.-eps1)then
        write(*,*)'probleme lors de l''appel a dmelangeds'&
     &    ,m,d,s,m1,d1,s1
!        write(*,*)' '
          m=0.
!          m1=m
          return
!        stop
        else
          m=0.
!          m1=m
          return
! fin des deux if sur m2
        endif
        endif
! fin du if sur m
              endif
! compartiment 1 n�gligeable
      if(m1.lt.eps2*m2)then
        m=m2
! on garde d et s
        return
      elseif(abs((d-d1)/d1).lt.eps1)then
! diametre peu different : on garde d et s
          m=m2
          return
      elseif(m2.lt.eps1*m)then
! m2 est petit : on suppose operation impossible si diametres differents
        m=m2
        return
!        d2=d
!        s2=s
!      elseif(m1.lt.0.1*m)then
!04      elseif(m1*d1.lt.0.1*m*d)then
! m1 est petit
!04        d2=(m*d-d1*m1)/m2
!04        if(d2.lt.0.5*(d/s+d))then
!04           d2=0.5*(d/s+d)
!04           s2=s/d*d2
!04        else
!04           s2=(s*m-s1*m1)/m2
!04    endif
      else
! moyenne g�om�trique simple: approximation au premier ordre
! on suppose que m et m2 sont du meme ordre
        d2=d1*(d/d1)**(m/m2)
        s2=s1*(s/s1)**(m/m2)
! pour le cas ou cela donnerait n'importe quoi
        if(d2.lt.0.5*(d/s+d))then
           d2=0.5*(d/s+d)
           s2=s/d*d2
        elseif(d2.gt.0.5*(d*s+d))then
           d2=0.5*(d*s+d)
           s2=s*d/d2
    else
! on limite le d84
          s2=min(s2,d*s/d2)
        endif
      endif
      if(s2.lt.1.)then
         s2=1.
      endif

! affectation au compartiment 0
!-----------------------------------------------------------------------
      m=m2
      if(d2.lt.eps2)then
!        write(*,*)' '
        write(*,*)'probleme en sortie de dmelangeds'&
     &    ,m,d2,s2,m1,d1,s1
! dans ce cas on garde d et s pour le reste
!      write(*,*)' '
      else
        d=d2
        s=s2
      endif

      return
      end

!*****************************************************************************
      subroutine melangeia(ia,ie,iev,hceie,ahceie)
!*****************************************************************************
!   ajout flux arete a hce
!  calcul nouveau diametre transporte diame2,sigmae2
!
!*****************************************************************************
      use module_tableaux,only:eps1,eps2,paray

      use module_ts,only:diame,sigmae,diame2,sigmae2,diama,sigmaa
!cou1d      use module_ts,only:ccouplts,dcouplts,scouplt

      implicit none

      double precision hceie,ahceie
      integer ie,ia,iev


! si apport
      if(ahceie.gt.eps2)then
        if(iev.gt.0)then
          diama(ia)=diame(iev)
          sigmaa(ia)=sigmae(iev)
!cou1d        elseif(couplage1d(ia))then
!cou1d          diama(ia)=dcouplts(ia)
!cou1d          sigmaa(ia)=scouplts(ia)
!        else
! valeurs limites calculees dans lecl
        endif
        call melangeds(hceie,ahceie,diame2(ie)&
     &,sigmae2(ie),diama(ia),sigmaa(ia))
! si retrait (avec ancien materiau)
       elseif(ahceie.lt.-eps2)then
!c a priori 2 lignes suivantes inutiles sauf  si couplage vers le 1d
!cou1d          diama(ia)=diame(ie)
!cou1d          sigmaa(ia)=sigmae(ie)
          if(hceie.gt.-ahceie)then
        call dmelangeds(hceie,-ahceie,diame2(ie),sigmae2(ie)&
     &,diame(ie),sigmae(ie),ie)
           else
             hceie=0.
! on garde diame2,sigmae2
           endif
! rien ajoute
        else
! flux quasi nul mais on definit diametre tout de meme
          diama(ia)=diame(ie)
          sigmaa(ia)=sigmae(ie)
! on garde hceie,diame2,sigmae2
       endif

        return
        end


!-----------------------------------------------------------------------
      real(wp) function vitchut2(d)
!-----------------------------------------------------------------------
! calcule la vitesse de chute (m/s) en fonction du diam�tre d
!-----------------------------------------------------------------------
      use module_tableaux,only:g
      use module_precision
      use module_ts,only:niux,niuy,dens,den,m,kds,tk,consnu,vch,zeta&
      &,vchfixe


      implicit none
!      double precision d,dgr
!      double precision chezy,grav,eps,epsy,epsm
!      double precision halfa,mucaso,visc,tcadim
!!      double precision poro,alf,rograv,dcharg,ro,ros,coef0,coef1
!      double precision rom1
      real(wp) d


!      common/parnum/chezy,grav,eps,epsy,epsm
!      common/abn/halfa,mucaso,visc,tcadim
!!      common/condep/poro,alf,rograv,dcharg,ro,ros,coef0,coef1
!      common/rosro/rom1

      if(vchfixe)then
         vitchut2=vch
       else
! van rijn, 1990
          if(d.lt.0.0001)then
            vitchut2=(dens/den-1.)*g*d*d/(18.*consnu)
          elseif(d.lt.0.001)then
            vitchut2=10.*consnu/d*&
     &  (sqrt(1.+0.01*(dens/den-1.)*g*d**3/(consnu**2))-1.)
          else
            vitchut2=1.1*sqrt((dens/den-1.)*g*d)
          endif
! fin du if sur vchfixe
       endif
!              if(d.lt.0.0001)then !
!        vitchut=rom1*grav*d**2/(18.*visc)
!        elseif(d.lt.0.001)then
!        vitchut=10.*visc/d*
!     & (sqrt(1.+0.01*rom1*grav*d**3/(visc**2))-1.)
!          else
!          vitchut=1.1*sqrt(rom1*grav*d)
!        endif

! fisher, 1995
!          dgr=d**3*grav*rom1/visc**2
!              if(dgr.lt.16.187)then !
!        vitchut=rom1*grav*d**2/(18.*visc)
!        elseif(dgr.lt.16187)then
!        vitchut=10.*visc/d*
!     & (sqrt(1.+0.01*rom1*grav*d**3/(visc**2))-1.)
!          else
!          vitchut=1.1*sqrt(rom1*grav*d)
!        endif

! loi de stockes (�coulement laminaire # d < 0,06 mm)
!       vitchut=((ros-ro)*grav*d**2)/(18.*0.0000013*ro)
!      vitchut=((ros-ro)*grav*d**2)/(18.*visc*ro)

! formule de lefort (0,04 < d < 4 mm)
!      vitchut=1.837*sqrt(delta*grav*d)
!     &  *(1-exp(-(d*(delta*grav/visc**2)**(1./3.))/10.33))**1.5

      return
      end

!*****************************************************************************
      subroutine supcouchn(in)
!*****************************************************************************
!   suppression une couche a un noeud
!
!*****************************************************************************
      use module_tableaux,only : eps1,eps2,paray

      use module_ts,only : epcoun,diamn,sigman,taummn,nbcoun&
      &,epcoun2,diamn2,sigman2,taummn2,nbcoun2

      implicit none

      integer in

      nbcoun2(in)=nbcoun(in)-1
        if(nbcoun2(in).eq.0)then
          nbcoun2(in)=1
          epcoun2(in)=0.1*eps2
          diamn2(in)=diamn(in,1)
          sigman2(in)=sigman(in,1)
          taummn2(in)=999.999
         endif
        return
        end

!*****************************************************************************
      subroutine erocouchn(in,dh,diamdh,sigmadh)
!*****************************************************************************
!   erosion une couche a un noeud
!    dh est negatif
!
!*****************************************************************************
      use module_tableaux,only:eps1,eps2,paray,dxe,dxen

      use module_precision

      use module_ts,only:epcoun,diamn,epcoun,sigman,taummn&
      &,diamn2,epcoun2,sigman2,taummn2,nbcoun,nbcoun2,idemix

      implicit none

      integer in

      real(wp) :: dh,diamdh,sigmadh


       nbcoun2(in)=nbcoun(in)
      epcoun2(in)=epcoun(in,1)+dh
       if(idemix)then
        diamn2(in)=diamn(in,1)
        sigman2(in)=sigman(in,1)
        diamdh=diamn(in,1)
        sigmadh=sigman(in,1)
    if(diamn(in,1).lt.eps2)then
             write(*,*)'erocouchn',in,epcoun(in,1),diamn(in,1)
             epcoun(in,1)=0.
             diamn(in,1)=0.001
             sigman(in,1)=1.
           else
       call demixn(epcoun(in,1),diamn(in,1),sigman(in,1),-dh,diamdh&
     &,sigmadh,epcoun2(in),diamn2(in),sigman2(in),dxen(in))
       call recalcultaumm(taummn2(in),diamn2(in),sigman2(in))
          endif
! si pas demixage
      else
        diamn2(in)=diamn(in,1)
        sigman2(in)=sigman(in,1)
        taummn2(in)=taummn(in,1)
        diamdh=diamn(in,1)
        sigmadh=sigman(in,1)
      endif
      return
      end


!-----------------------------------------------------------------------
      subroutine demixn(m,d,s,m1,d1,s1,m2,d2,s2,dx)
!-----------------------------------------------------------------------
! d�mixe les s�diments des compartiments 1 et 2:
! on rentre avec les maasses des deux compartiments
! et les autres carcteristiques identisques et egales a celles du tout
! on sort les carcteristiques des deux compartiments
! modif rubarbe du 23/01/12
!-----------------------------------------------------------------------
      use module_precision

      use module_tableaux,only:eps1,eps2,paray

      use module_ts,only:dchardiam,dcharsigma

      implicit none
      real(wp) m1,d1,s1,m2,d2,s2
      real(wp) dx,a,a2,b,b2
! compartiment d'origine
      real(wp) m,d,s

! contr�le des entr�es
!-----------------------------------------------------------------------
      if(m.lt.0.)then
        if(m.lt.-eps2)then
        write(*,*)'probleme sur m lors de l''appel a demixn'&
     &    ,m,d,s,m1,d1,s1,m2,d2,s2
        write(*,*)' '
!        pause
        stop
        else
          m=0.
          m1=0.
          m2=0.
! inutile car on rentre ave! meme d et s pour les 3 compartiments
!          d2=d
!          s2=s
!          d1=d
!          s1=s
          return
        endif
      elseif(m1.lt.0.)then
        if(m1.lt.-eps2)then
        write(*,*)'probleme sur m1 lors de l''appel a demixn'&
     &    ,m,d,s,m1,d1,s1,m2,d2,s2
        write(*,*)' '
!        pause
        stop
        else
          m1=0.
          m2=m
        endif
      elseif(m2.lt.0.)then
        if(m2.lt.-eps2)then
        write(*,*)'probleme sur m2 lors de l''appel a demixn'&
     &    ,m,d,s,m1,d1,s1,m2,d2,s2
        write(*,*)' '
!        pause
        stop
        else
          m2=0.
          m1=m
        endif
! fin du if sur m
              endif
! dpas de demixage = diam�tre identique pour les deux compartiments 1 et 2
!      if(.not.idemix)then
!        d1=d
!        s1=s
!        d2=d
!        s2=s
!!      si demixage
!     else
! cas ou la masse du compartiment m2 est n�gligeable devant la masse de m1
        if(m2.le.eps1*m1) then
! inutile car on rentre avec meme d et s pour les 3 compartiments
!          d1=d
!          s1=s
! on suppose que le compartiment 2 garde les caract�ristiques du compartiment d'origine
! c'est uniquement pour des raisons num�riques, la masse m2 est nulle
!          d2=d
!          s2=s
! cas ou la masse du compartiment m1 est n�gligeable devant la masse de m2
        elseif(m1.le.eps1*m2) then
! inutile car on rentre avec meme d et s pour les 3 compartiments
!          d2=d
!          s2=s
! on suppose que le compartiment 1 garde les caract�ristiques du compartiment d'origine
! c'est uniquement pour des raisons num�riques, la masse m1 est nulle
!          d1=d
!          s1=s
! cas ou le materiau est deja uniforme
        elseif(s.le.1.+eps1) then
! inutile car on rentre avec meme d et s pour les 3 compartiments
!          d2=d
!          s2=s
!          d1=d
!          s1=s
! on traite le cas ou les deux masses ne sont pas nulles
        else
! les coefficients dchard et dchars ne doivent pas �tre petits
        if(dchardiam.le.dx)then
          a=(s-1.)/s*m2/m
          a2=(s-1.)/s*m1/m
        else
          a=dx/dchardiam*(s-1.)/s*m2/m
          a2=dx/dchardiam*(s-1.)/s*m1/m
        endif
        if(dcharsigma.le.dx)then
          b=(s-1.)/s*m2/m
          b2=(s-1.)/s*m1/m
        else
          b=dx/dcharsigma*(s-1.)/s*m2/m
          b2=dx/dcharsigma*(s-1.)/s*m1/m
        endif

! le passage de la forme exponentielle a la forme lineaire est mise � 0,1 (5% erreur)
        if(a.lt.0.1)then
!        if(a.lt.0.32)then
          d1=d*(1.-a)
        else
          d1=d*exp(-a)
        endif
        if(a2.lt.0.1)then
!        if(a2.lt.0.32)then
          d2=d*(1.+a2)
        else
          d2=d*exp(a2)
        endif
! on inverse s grand pour d petit
! modif du 23/01/12
! on a s petit pour d petit donc meme formule
         if(b.lt.0.1)then
!         if(b.lt.0.32)then
            s1=s*(1.-b)
          else
            s1=s*exp(-b)
          endif
! on inverse s petit pour d grand
! modif du 23/1/12 :s grand pour d grand
          if(b2.lt.0.1)then
            s2=s*(1.+b2)
          else
            s2=s*exp(b2)
          endif
! a voir si ces limitations sont utiles
! 23/1/12 : on garde les limitations qui limitent le d84 au d84
        if(d2.gt.d*s)then
           d2=d*s
           s2=1.d0
        elseif(d2*s2.gt.d*s)then
            s2=(d*s)/d2
!              if(s2.lt.1.)then
!              s2=1.d0
!               endif
! modif du 23/1/12 : s2 est plus grand que s donc inutile
!        elseif(s2.lt.1.)then
!            s2=1.
        endif
! a priori d1 et s1 plus petits que d et s don! inutile
!       if(d1*s1.gt.d*s)then
!            s1=(d*s)/d1
!! d1 inferieur a d donc s1 superieur a s
!!            if (s1.lt.1.)then
!!               d1=d*s
!!               s1=1.
!!            endif
! ajout du 23/1/12
!        if(s1.lt.1.)then
!            s1=1.d0
!        endif
! fin du if sur masses trop petites
      endif
! fin du if sur demix=0
!      endif

      if((d2.lt.eps2).or.(d1.lt.eps2))then
        write(*,*)'probleme sur d lors de l''appel a demixn'&
     &    ,m,d,s,m1,d1,s1,m2,d2,s2
!     &    ,m1,',',d1,',',s1,',',m2,',',d2,',',s2,')'
        stop
      endif
      if(s2.lt.1.)then
        s2=1.d0
       endif
      if(s1.lt.1.)then
        s1=1.d0
      endif


! affectation au compartiment 1
!-----------------------------------------------------------------------

! 100  continue

      return
      end


!*****************************************************************************
      subroutine zerocouchn(in)
!*****************************************************************************
!   erosion nulle actualisation une couche a un noeud
!
!*****************************************************************************
      use module_ts,only:epcoun,diamn,sigman,taummn,nbcoun&
      &,epcoun2,diamn2,sigman2,taummn2,nbcoun2

      implicit none

      integer in


        nbcoun2(in)=nbcoun(in)
          epcoun2(in)=epcoun(in,1)
          diamn2(in)=diamn(in,1)
          sigman2(in)=sigman(in,1)
          taummn2(in)=taummn(in,1)
        return
        end


!*****************************************************************************
      subroutine melangemaille(ie,dh1,dh2,dh3,dh4,ne1,ne2,ne3,ne4)
!*****************************************************************************
!   ajout dhn pour obtenir diametre smbhc
!

!*****************************************************************************
      use module_tableaux,only:eps1,eps2,paray,ine,nne,dxe,dxen

      use module_precision

      use module_ts,only:diamsmb,sigmasmb,diamdhn,sigmadhn&
      &,diame2,sigmae2,hcet,idemix

      implicit none

      integer ie
      real(wp) :: dh(4),diampos,sigmapos,mpos,mneg,diamneg,sigmaneg&
     &,dh1,dh2,dh3,dh4,mtr,diamtr,sigmatr
      integer i,nbpos,nbneg,npos(4),nneg(4),ne1,ne2,ne3,ne4



     logical depot



    depot=.true.
! depot vrai indique qu'il y a depot dans la maille

! on ajoute d'abord les positifs puis les negatifs et on retranche ensuite
      nbpos=0
      nbneg=0
      mneg=0.
      if(dh1.gt.eps2)then
          nbpos=nbpos+1
          npos(nbpos)=1
          dh(1)=dh1
      elseif(dh1.lt.-eps2)then
          nbneg=nbneg+1
          nneg(nbneg)=1
          dh(1)=-dh1
            mneg=mneg+dh(1)
      endif
      if(dh2.gt.eps2)then
          nbpos=nbpos+1
          npos(nbpos)=2
          dh(2)=dh2
      elseif(dh2.lt.-eps2)then
          nbneg=nbneg+1
          nneg(nbneg)=2
          dh(2)=-dh2
            mneg=mneg+dh(2)
      endif
      if(dh3.gt.eps2)then
          nbpos=nbpos+1
          npos(nbpos)=3
          dh(3)=dh3
      elseif(dh3.lt.-eps2)then
          nbneg=nbneg+1
          nneg(nbneg)=3
          dh(3)=-dh3
            mneg=mneg+dh(3)
      endif
      if(dh4.gt.eps2)then
          nbpos=nbpos+1
          npos(nbpos)=4
          dh(4)=dh4
      elseif(dh4.lt.-eps2)then
          nbneg=nbneg+1
          nneg(nbneg)=4
          dh(4)=-dh4
            mneg=mneg+dh(4)
      endif
       mpos=float(nne(ie))*hcet(ie)
       diampos=diame2(ie)
       sigmapos=sigmae2(ie)
      if(nbpos.gt.0)then
!        mpos=dh(npos(1))
!        diampos=diamdhn(ine(ie,npos(1)))
!        sigmapos=sigmadhn(ine(ie,npos(1)))
        do i=1,nbpos
           call melangeds(mpos,dh(npos(i)),diampos,sigmapos&
     &,diamdhn(ine(ie,npos(i))),sigmadhn(ine(ie,npos(i))))
        enddo
!      else
!        mpos=0.
!        diampos=diame(ie)
!        sigmapos=sigmae(ie)
      endif

      if(nbneg.gt.0)then
            if(mpos.gt.mneg)then
            if(idemix)then
              mtr=mpos-mneg
                 if(mtr.gt.eps2)then
                   diamtr=diampos
                    sigmatr=sigmapos
                   diamneg=diampos
                     sigmaneg=sigmapos
! dans le cas de demixage, on revient avec un nouveau diamtr et diamneg
                   if(diampos.lt.eps2)then
      write(*,*)'mel',mpos,diampos,diame2(ie),diamdhn(ine(ie,npos(1)))
!    write(*,*)nbpos,diamdhn(ine(ie,npos(2))),diamdhn(ine(ie,npos(3)))
                        mpos=0.
                  mtr=0.
                  mneg=0.
                     else
        call demixn(mpos,diampos,sigmapos,mtr,diamtr,sigmatr,mneg,&
     &diamneg,sigmaneg,dxen(ie))
                    endif
! si mtr nul on garde le diamtre initial pour toute la masse
               else
                 diamneg=diampos
                   sigmaneg=sigmapos
                diamtr=diampos
                 sigmatr=sigmapos
               endif
! si pas de demixage
           else
             diamneg=diampos
              sigmaneg=sigmapos
             diamtr=diampos
              sigmatr=sigmapos
! fin du if sur demixage
           endif
          diamsmb(ie)=diamtr
          sigmasmb(ie)=sigmatr
! si mpos inferieur a mneg
        else
           if(mpos.gt.eps2)then
! on suppose que mpos correspond a la masse precedente et donc
! a ce qui s'est depose
           diamneg=diampos
            sigmaneg=sigmapos
! si mpos =0 diampos n'a pas de sens
! rien ne peut se deposer
      else
       depot=.false.
! fin du else sur mpos =0
          endif
! a priori ne sert a rien car masse nulle
          diamsmb(ie)=diampos
          sigmasmb(ie)=sigmapos
! fin du if sur mpos superieur a mneg
        endif
! si nbneg=0
    else
          depot=.false.
          diamsmb(ie)=diampos
          sigmasmb(ie)=sigmapos
        endif
    if(depot)then
! on fera ensuite la moyenne des diametres en divisant diamdhn par ne
! et idem pour d84 (au lieu de sigma)
        if(dh1.lt.-eps2)then
          ne1=ne1+1
          diamdhn(ine(ie,1))=diamdhn(ine(ie,1))+diamneg
          sigmadhn(ine(ie,1))=sigmadhn(ine(ie,1))+sigmaneg*diamneg
        endif
      if(dh2.lt.-eps2)then
          ne2=ne2+1
          diamdhn(ine(ie,2))=diamdhn(ine(ie,2))+diamneg
          sigmadhn(ine(ie,2))=sigmadhn(ine(ie,2))+sigmaneg*diamneg
      endif
      if(dh3.lt.-eps2)then
          ne3=ne3+1
          diamdhn(ine(ie,3))=diamdhn(ine(ie,3))+diamneg
          sigmadhn(ine(ie,3))=sigmadhn(ine(ie,3))+sigmaneg*diamneg
      endif
      if(dh4.lt.-eps2)then
          ne4=ne4+1
          diamdhn(ine(ie,4))=diamdhn(ine(ie,4))+diamneg
          sigmadhn(ine(ie,4))=sigmadhn(ine(ie,4))+sigmaneg*diamneg
      endif
! fin du if sur depot
      endif
        return
        end

!*****************************************************************************
      subroutine depcouchn(in,dh,diamdh,sigmadh)
!*****************************************************************************
!   depot une couche a un noeud
!
!*****************************************************************************
      use module_tableaux,only:eps1,eps2,paray

      use module_precision

      use module_ts,only:epcoun,diamn,sigman,taummn,nbcoun&
      &,epcoun2,diamn2,sigman2,taummn2,nbcoun2,diamnt,sigmant&
      &,ncousedmax

      implicit none

      integer in
      real(wp) :: dh,diamdh,sigmadh,taudh,ep
      logical proche,prochecouches

      external prochecouches

      ep=epcoun(in,1)
      call recalcultaumm(taudh,diamdh,sigmadh)
      proche=prochecouches(diamdh,sigmadh&
     &,diamn(in,1),sigman(in,1),taudh,taummn(in,1))
      if(proche)then
! modification couche
          taummn2(in)=(taudh*dh+taummn(in,1)*ep)/(dh+ep)
          nbcoun2(in)=nbcoun(in)
          epcoun2(in)=ep
          diamn2(in)=diamn(in,1)
          sigman2(in)=sigman(in,1)
         call melangeds(epcoun2(in),dh,diamn2(in), sigman2(in)&
     &,diamdh,sigmadh)
!         call recalcultaumm(taummn2(in),diamn2(in),sigman2(in))
! si la contrainte est plus forte, on la ramene a la pr�c�dente
!         if(taummn2(in).gt.taummn(in,1))taummn2(in)=taummn(in,1)
      elseif(nbcoun(in).lt.ncousedmax.and.&
      &(ep.gt.diamn(in,1)).or.&
     &(taummn(in,1).gt.999.))then
!     :and.epcoun(in,1).gt.diamn(in,1))then
! ajout une couche
! si nb maxi couches pas atteint et si
! couche superficielle d'�paisseur sup�rieure a diametre
! et si couche erodable
        nbcoun2(in)=nbcoun(in)+1
          epcoun2(in)=dh
          diamn2(in)=diamdh
          sigman2(in)=sigmadh
          taummn2(in)=taudh
!          call recalcultaumm(taummn2(in),diamn2(in),sigman2(in))
      else
! pas proche mais trop de couches donc comme proche
          taummn2(in)=(taudh*dh+taummn(in,1)*ep)/(dh+ep)
          nbcoun2(in)=nbcoun(in)
          epcoun2(in)=ep
          diamn2(in)=diamn(in,1)
          sigman2(in)=sigman(in,1)
         call melangeds(epcoun2(in),dh,diamn2(in), sigman2(in)&
     &,diamdh,sigmadh)
!         call recalcultaumm(taummn2(in),diamn2(in),sigman2(in))
! si la contrainte est plus forte, on la ramene a la pr�c�dente
!         if(taummn2(in).gt.taummn(in,1))taummn2(in)=taummn(in,1)
      endif
        return
        end

!-----------------------------------------------------------------------
      logical function prochecouches(d1,s1,d2,s2,tau1,tau2)
!-----------------------------------------------------------------------
! indique si les caract�ristiques des sediments 1 et 2 sont proches
!-----------------------------------------------------------------------
      use module_precision

      implicit none
      real(wp) :: d1,s1,d2,s2,dl1,dl2,dl3,dl4,eps,tau1,tau2
!      double precision coefprochec
!      common/cprochec/coefprochec
!      double precision eps1,eps2,paray
!      common/param/eps1,eps2,paray

! version provisoire: on pourrait tester
! les contraintes de mise en mouvement
      eps=0.1
      prochecouches=.true.
      if(tau1.gt.tau2*(1.+eps)) then
        prochecouches=.false.
      elseif(tau1.lt.tau2*(1.-eps))then
        prochecouches=.false.
      else

!      dl1=d2*(1.+coefprochec*(s2-1.))*(1.+eps2)
!      dl2=d1/(1.+coefprochec*(s1-1.))*(1.-eps2)
!      dl3=d2/(1.+coefprochec*(s2-1.))*(1.-eps2)
!      dl4=d1*(1.+coefprochec*(s1-1.))*(1.+eps2)
        if(d1.gt.d2*s2*(1.+eps).and.(d2.lt.d1/s1*(1.-eps))) then
!      if(d1.gt.dl1.and.d2.lt.dl2) then
          prochecouches=.false.
        elseif(d1.lt.d2/s2*(1.-eps).and.(d2.gt.d1*s1*(1.+eps)))then
!      elseif(d1.lt.dl3.and.d2.gt.dl4)then
          prochecouches=.false.
        endif
      endif

      return
      end

!******************************************************************************
      subroutine semba2!(zfn2,zfa2,zfe2,cofr2)
!     on calcule la contribution des termes de sedimentation et erosion
!******************************************************************************
!  definition des donnees diverses servant aux calculs
! niux    = la diffusion liquide selon ox
! niuy    = la diffusion liquide selon oy
! sigm    = le nombre de schmidt
! dens    = la densite des sediments
! den     = la densite d'eau
! m       = le coefficient emperique pour le transport de sediments
! ikds    = 1 ou 0 : si calcul avec kds constante ou non
! kds     = la constante donnee par le diagramme de shields
! kaa      = constante intervenant dans le calcul du taucr si la maille
!           est en pente
! alprep  = le coefficient de vitesse de depot
! d       = d50
! re      = d* parametre adimensionnel
! vstar   = module de la vitesse
! vch     = la vitesse de chute
! vch2 vitesse de chute coorigee
! vfr     = vitesse de frottement
! uprime  = vitesse de frottement relative aux grains
! zed     = parametre de suspension (force fluide/ forces gravitationnelles)
! consnu  = la diffusion lique moyenne constante
! dix     = le coefficient de diffusion selon ox
! diy     = le coefficient de diffusion selon oy
! diagsh  = false ou true : si kds est donne par l'utilisateur ou calculer par le diagramme de shields
! vanri   = false ou true : si la concentration d'equilibre est calculee par van rijn
! corvch  = false ou true : si il y a correction sur le vitesse de chute pour le terme de sedimentation
! beta    = coefficient....
! ppente  = prise en compte de la pente pour determiner tau critique
! meyer =calcul erosion par meyer peter muller
! zeta  rapport vitesse sediments charries utilisee a vitesse theorique


!**************************************************************************


      use module_precision

      use module_mpi,only:np,mailles_loc,ne_loc,me,noeuds_loc,nnb,nnloc,naloc&
     &,aretes_loc,elt_voisins,nmv
      use module_tableaux,only:ne,na,nn,eau,quet,qvet,het,fra,fre,cofr&
     &,ieva,iae,nne,g,smbh,zfn,dt,cfl,dt0,t,eps1,eps2,paray,zfe,ine,ina,ievn,nevn,idl

      use module_ts,only : efficace,sigma0,rapsmbhc,nbcoun,epcoun,diamn,sigman&
     &,taummn,diame2,sigmae2,exposantaucr,camenen,hansen,exptaucr,taue,rapva,smbhc&
     &,solute,vitfro,dhe,hcet,cet,sg,rzmax,diams,fi,fi2,al,ksi,gdd,eseldt&
     &,diagsh,modes,consed,vanri,corvch,meyer,ppente,d90,re,alprep,consnu&
     &,d,den,dens,kds,m,poro,zeta,tk,vch,ceq,diamnt,sigmant,taun,zfn2,cofr2,zfa2,zfe2

#ifdef WITH_MPI
      use module_messages
#endif /* WITH_MPI */

#if SEDVAR
      use module_ts,only:sedvar,eron
!end if sedvar
#endif

      implicit none

!!!!!     real(WP),intent(inout) :: cofr2(ne),zfn2(nn),zfa2(na),zfe2(ne)!,ceq(ne)
      real(WP) :: rapve(ne),hn(nn),qun(nn),qvn(nn),fran(nn)&
     &,fren(nn),cofrn(nn),un(nn),vn(nn),cn(nn),hcn(nn),rapvn(nn),ceqn(nn)&
     &,vitfrn(nn),eseldtn(nn)
      real(wp) :: ero,sed,tau,taucs,taucr,vfr,kaa,chez2,transp&
     &,uprime,ucr,afond,cafond,beta,phi,zed,frep,cee,co1,co2
      real(WP) :: fra2,frcor,frint,hcmin,q,rz,vch2,vmin,vstar,vsed
      integer :: ien(nn)
      integer :: i,ia,ja,ie,je,jn,in
!      integer :: idl = 0
#if SEDVAR
      !logical eron(nn)
      real(wp) vitchut2

      !eron = .FALSE.
     ! datA eron/nn*.FALSE./
!end if sedvar
#endif

!     save idl

#if SEDVAR
      !save eron
      external vitchut2
!end if sedvar
#endif

      if(.not.solute)then
!        hmin=max(d,paray)
!        sg=sqrt(g)
! rzmax fixe a  25 mm/s soit 90 m en 1 heure
!        rzmax=0.025
! si on utilise van rijn
!        if(vanri)then
! diam�tre adimensionnel
!!         diams=d*((dens/den-1.)*g)**(1./3.)*consnu**(-2./3.)
! le cas dens=den est exclu par .not.solute
!          diams=d*((dens/den-1.)*g)**0.333333*consnu**(-0.666667)
!        endif
! calcul du coefficient de pente a chaque pas de temps pour le calcul
! de la contrainte critique d erosion taucr
!       write(*,*)'point1'
!        if(ppente)then
! constantes pour le calcul du coefficient de pente dont dependra taucr
! fi est la tangente de l'angle de frottement interne des sediments
! fi2 carr� de fi
! al=alpha un coefficient empirique fix� � 0.85
!        endif

! phase d interpolation des variables hydrauliques du centre de maille
! aux noeuds
!*********************************************************************

! interpolation de he

      do i=1,nnloc
        in = noeuds_loc(i)
        hn(in)=0.
        ien(in)=0
        fran(in)=0.1
        fren(in)=0.1
        cofrn(in)=1.
        un(in)=0.
        vn(in)=0.
        qun(in)=0.
        qvn(in)=0.
        cn(in)=0.
        hcn(in)=0.
        rapvn(in)=0.
        ceqn(in)=0.
        diamnt(in)=0.
        sigmant(in)=0.
!        rapsmbhcn(in)=0.
        taun(in)=0.
        vitfrn(in)=0.
      end do
!       do i=1,ne_loc(me)
!         ie=mailles_loc(i)
!         do jn=1,nne(ie)
!           in=ine(ie,jn)
#ifdef WITH_MPI
      call message_group_calcul(dhe)
      call message_group_calcul(zfe)
      call message_group_calcul(cofr)
      call message_group_calcul(fra)
      call message_group_calcul(fre)
      call message_group_calcul_logical(eau)
#if SEDVAR
      if(sedvar)then
        call message_group_calcul(diame2)
        call message_group_calcul(sigmae2)
      endif
!end if sedvar
#endif
#endif
      do jn=1,nnloc
        in = noeuds_loc(jn)
        do je=1,nevn(in)
          ie = ievn(in,je)
          if(eau(ie))then
            if(cofr(ie).lt.cofrn(in))cofrn(in)=cofr(ie)
! on met dans fran et fren le coefficient de frottement pur sans correction de surface
            frint=fra(ie)/cofr(ie)
            if(frint.gt.fran(in))fran(in)=frint
            frint=fre(ie)/cofr(ie)
            if(frint.gt.fren(in))fren(in)=frint
            hn(in)=hn(in)+max(eps2,(het(ie)+zfe(ie)-zfn(in)))
            un(in)=un(in)+quet(ie)/het(ie)
            vn(in)=vn(in)+qvet(ie)/het(ie)
            qun(in)=qun(in)+quet(ie)
            qvn(in)=qvn(in)+qvet(ie)
            cn(in)=cn(in)+cet(ie)
            hcn(in)=hcn(in)+hcet(ie)
!            rapsmbhcn(in)=rapsmbhcn(in)+rapsmbhc(ie)
#if SEDVAR
            if(sedvar)then
              diamnt(in)=diamnt(in)+diame2(ie)*hcet(ie)
              sigmant(in)=sigmant(in)+sigmae2(ie)*hcet(ie)
            endif
!end if sedvar
#endif

            ien(in)=ien(in)+1
          endif
        end do
      end do
      do i=1,nnloc
        in = noeuds_loc(i)
! si un des elements a une hauteur non nulle
        if(ien(in).eq.0)then
!          hn(in)=0.0
!          un(in)=0.
!          vn(in)=0.
!          cn(in)=0.
              diamnt(in)=d
              sigmant(in)=sigma0
!              rapsmbhcn(in)=1.
        else
          hn(in)=hn(in)/float(ien(in))
          if(hn(in).lt.eps2)then
              hn(in)=0.0
              un(in)=0.
              vn(in)=0.
              cn(in)=0.
              diamnt(in)=d
              sigmant(in)=sigma0
!              rapsmbhcn(in)=1.
          else
            un(in)=un(in)/float(ien(in))
            vn(in)=vn(in)/float(ien(in))
            qun(in)=qun(in)/float(ien(in))
            qvn(in)=qvn(in)/float(ien(in))
            un(in)=min(un(in),qun(in)/hn(in))
            vn(in)=min(vn(in),qvn(in)/hn(in))
!            rapsmbhcn(in)=rapsmbhcn(in)/float(ien(in))
            cn(in)=cn(in)/float(ien(in))
            hcn(in)=hcn(in)/float(ien(in))
            cn(in)=min(cn(in),hcn(in)/hn(in))

#if SEDVAR
            if(sedvar)then
              if(hcn(in).gt.eps2)then
              diamnt(in)=diamnt(in)/(hcn(in)*float(ien(in)))
              sigmant(in)=sigmant(in)/(hcn(in)*float(ien(in)))
              else
              hcn(in)=0.
              diamnt(in)=diamn(in,1)
              sigmant(in)=sigman(in,1)
              endif
            endif
!end if sedvar
#endif

          endif
        endif
      end do
      do i=1,nnloc
        in = noeuds_loc(i)
! si on a de l'eau
!!d suppose plus grand que paray
!          if(he(ie).gt.d)then
         if(hn(in).gt.paray)then

#if SEDVAR
           if(sedvar)then
             if(eron(in))then
               d=diamn(in,1)
               sigma0=sigman(in,1)
             else
               d=diamnt(in)
               sigma0=sigmant(in)
             endif
           endif
!end if sedvar
#endif

!          if(hn(in).gt.hmin)then
!          if(eau(ie))then
!            q=sqrt(qun(in)**2+qvn(in)**2)
!            vstar=q/het(ie)
             vstar=sqrt(un(in)**2+vn(in)**2)
!!si le module de  la vitesse est tr�s faible
!            if(vstar.lt.eps2)then
!              tau=0.
!              ero=0.
!              sed=alprep*vch*cet(ie)
! si la vitesse est suffisante
!            else
! on limite la vitesse
              if(vstar.gt.10.)vstar=10.
! calcul du coefficient de chezy
              fra2=max(fran(in),fren(in)*hn(in)**0.3333333)
! calcul de la vitesse de frottement
              vfr= sg*vstar/sqrt(fra2)
              vitfrn(in)=vfr
              tau=den*vfr*vfr
              if(diagsh)then
! courbe de shields nouvelle
                re=d*vfr/consnu
                if(re.le.0.2)then
                  kds=0.575
                elseif(re.lt.2.)then
                  kds=0.115/re
                elseif(re.lt.5.)then
                  kds=0.081317279*re**(-0.5)
                elseif(re.lt.10.)then
                  kds=0.050175514*re**(-0.2)
                elseif(re.lt.40.)then
                  kds=0.025147327*re**(0.1)
                elseif(re.lt.490.)then
                  kds=0.017389449*re**(0.2)
                else
                  kds=0.060
                endif
! fin du if sur diagsh
              endif
              taucr=kds*gdd*d
              if(exptaucr)then
! exposantaucr a ete divise par 2 dans ledd
                 taucr=taucr*(1./cofrn(in)**2-1.)**exposantaucr
!              endif
! calcul du frottement critique avec introduction du
! coefficient kaa traduisant la dependance a la pente
              elseif(ppente)then
! le 27 aout 2012 : en 1d le minimum est zero
!                taucrmin=0.5*taucr
!                taucrmin=0.d0
                co1=-al*fi2*cofrn(in)
                co2=fi2*cofrn(in)*cofrn(in)+al*al*fi2*&
        &(1.-cofrn(in)*cofrn(in))-(1.-cofrn(in)*cofrn(in))
                if(co2.lt.eps2)then
! 10/4/2014   si co2 negatif, le calcul est invalide
! on annule la contrainte critique car une raison =forte pente
! modif aussi apres supposant taucrmin=0
                  taucr=0.
                elseif((co1+sqrt(co2)).lt.eps2) then
                  taucr=0.
                else
!                  kaa=(co1+sqrt(co2))/((1.-al*fi)*fi)
                  kaa=(1.-al*fi)*fi
          if(kaa.gt.eps1)then
            kaa=(co1+sqrt(co2))/kaa
              if(kaa.lt.1.)then
                         taucr=kaa*taucr
!                     else
! sinon taucr inchange
! fin du if sur kaa<1
                      endif
! else du if sur kaa>0
                else
                    taucr=0.
! fin du if sur kaa>0
                endif
! fin du if sur co2>0
                endif
! fin du if sur ppente
              endif
! si on apporte le conrrection sur la vitesse de chute
              if(corvch)then
! la correction vaut vmin pour particule en autousspension
                if(vstar.gt.eps2)then
                  vmin=vstar**3/hn(in)/fra2
                else
                  vmin=0.
                endif

#if SEDVAR
        vch=vitchut2(d)
!end if sedvar
#endif

        vch2=max(vch-vmin,0.d0)
!                if(vch.gt.vmin)then
!                  vch2=vch-vmin
!                else
!                  vch2=0.
!                endif
              else
#if SEDVAR
                vch=vitchut2(d)
!end if sedvar
#endif

                vch2=vch
! fin du if sur la correction
              endif
! si on utilise van rijn
              if(vanri)then
! si il y a du sediment
                if(cn(in).gt.eps2)then
                  cee=cn(in)
                else
                  cee=0.
                endif
! si la vitesse de chute est trop grande
                if(vch2.gt.vfr)then
                  ceqn(in)=0.
!                  frep=1.
! si la vitesse est suffisante
                else
! introduction de fra2 pour eviter les valeurs trop petites si hn petit

#if SEDVAR
                   if(sedvar)then
                     d90=d*sigma0**1.15
                   endif

!end if sedvar
#endif
                   chez2=max(18.*log(4.*hn(in)/d90)&
     &/log(10.),sqrt(fra2))
! calcul de la vitesse de frottement relative aux grains
                  uprime=sg*vstar/chez2
! calcul de la vitesse de frottement critique relative au frottement critique
                  ucr=sqrt(taucr/den)
! si la vitesse de frottement relative aux grains est suffisante
                  if(uprime.gt.ucr)then
! calcul du parametre de transport
                    transp=(uprime**2-ucr**2)/ucr**2
! calcul des asperites
                    afond=max(0.01*hn(in),3.*d90)
! calcul de la concentration du fond
! cafond modifie suite a modif diams
                    cafond=0.015*d**0.7*transp**1.5/(afond*diams**0.3)
!                    cafond=0.015*d*transp**1.5/afond/diams**0.3
!                     cafond=0.015*d*transp**1.5/afond/re**0.3
                    afond=afond/hn(in)
!                diams=d*(1.+0.011*(sigs-1.)*(transp-25.)
! calcul du parametre de suspension
                    beta=1.+2.*(vch2/vfr)**2
                    phi=2.5*(vch2/vfr)**0.8*(cafond/0.65)**0.4
                    zed=2.5*vch2/vfr/beta
                    zed=zed+phi
! si ce parametre est trop grand
                    if(zed.gt.1.)zed=1.
! calcul de la concentratino d'equilibre en m3/m3
! si les asperites sont suffisamment petites
                    if(afond.lt.0.1)then
                      frep=(afond**zed-afond**1.2)/(1.-afond)**zed&
                      &/(1.2-zed)
! si les asperites sont trop importantes
                    else
                      frep=(0.1**zed-0.0631)/0.9**zed&
                       &/(1.2-zed)
                    endif
! cafond est en m3/m3
                    ceqn(in)=frep*cafond
!  if sur uprime par rapport a ucr
                  else
                    ceqn(in)=0.
                  endif
! fin du if sur vitesse de chute
                endif
! si pas van rijn
              elseif(.not.meyer)then
! si il y a du sediment
                if(cn(in).gt.eps2)then
! calcul de la concentration en sediments en m3/m3
                  cee=cn(in)
! calcul de la concentration d'equilibre
                  if(consed)then
                    if(vstar.gt.eps2)then
            taucs=(dens-den)*g*hn(in)*vch2*cn(in)/(tk*vstar)
! cette valeur ne devrait jamais etre utilisee
                    else
                      taucs=taucr
                    endif
                  else
                    taucs=taucr
                  endif
!      limitation de tau/taucr a 10
                  ceqn(in)=cee*min(tau/taucs,10.d0)
! si pas de sediment arrivant
                else
                  cee=0.
                  ceqn(in)=0.
                endif
! fin du if sur van rijn
              endif
! si on ne s'est pas donne le coefficient alprep on ne le lie
! qu'� la repartition des concentrations
! calcul de la sedimentation
! calcula a partir de meyer peter muller
              if(meyer)then
          if(efficace)then
! on corrige pour obtenir tau efficace
!                tau=tau*fra2**0.75*ksi*hn(in)**(-0.25)
!                frcor=fra2**0.75*ksi*hn(in)**(-0.25)
                frcor=fra2**0.75*ksi*(d/hn(in))**0.25
! on ne corrige que pour trouver un tau efficace inf�rieur a 1
                if(frcor.lt.1.)then
                  tau=tau*frcor
                  vfr=sqrt(tau/den)
                  vitfrn(in)=vfr
                endif
! fin du if si efficace
              endif
! m contient le coefficient de meyer peter et alprep inverse distance chargement
! si meyer ceq=0 partout gard� et pas de taucs
                if(tau.lt.eps2)then
                  ero=0.0
                elseif(tau.le.taucr) then
                  if(camenen)then
                    if(tau.gt.0.1*taucr)then
                      ero=m*tau**1.5*exp(-4.5*taucr/tau)
                    else
                      ero=0.0
                    endif
                  else
                    ero=0.0
                  endif
!  si tau superieur a taucr    limitation de tau/taucr a 10
! supprimee car taucr = 0 en pente
                else
!                   if(tau.gt.10.*taucr)then
!                     tau=10.*taucr
!                   endif
                  if(camenen)then
                    ero=m*tau**1.5*exp(-4.5*taucr/tau)
                  elseif(hansen)then
                    ero=m*vstar*vstar*tau**1.5/d
                  else
                    ero=m*(tau-taucr)**1.5
                  endif
! fin du if sur tau inferieur a taucr
                endif
! si on calcule la vitesse des sediments a partir vfr
                if(modes)then
! calcul de la vitesse de frottement critique relative au frottement critique
                  ucr=sqrt(taucr/den)
! formule de engelund et fredsoe pour la vitesse de charriage
                  if(vfr.gt.0.7*ucr)then
                    vsed=10.*zeta*vfr*(1.-0.7*ucr/vfr)
                    if(vsed.gt.vstar)then
                      vsed=vstar
                    endif
                    if(vstar.gt.eps2)then
                      rapvn(in)=vsed/vstar
                    else
                      rapvn(in)=0.
                    endif
                    sed=alprep*hn(in)*cn(in)*max(vsed,vch2)
! rien ne peut �tre transporte donc tout se depose
                  else
! rapvn initialise a zero
!                    rapvn(in)=0.
! tout se depose car vsed=0
!                    sed=cn(in)*hn(in)/dt
! pour continuite si vsed=0 le 13 janvier 2014.
                     sed=alprep*hn(in)*cn(in)*vch2
                  endif
                else
! modes faux indique ici que la vitesse des s�diments est la vitesse liquide
!                  sed=alprep*cn(in)*hn(in)*vstar*zeta
                  sed=alprep*cn(in)*hn(in)*max(vstar*zeta,vch2)
! inutile car dans ce cas rapva mis a zeta
!                  rapvn(in)=zeta
                endif
! deplace pour que taun toujours defini
!                taun(in)=tau/gdd/d
! si pas meyer peter muller 2 cas avec ou sans calculs differents erosion/depot
              elseif(modes)then
! si la concentration en sediments est superieure � la concentration d'equilibre
                if(cee.gt.ceqn(in))then
                  sed=alprep*vch2*(cee-ceqn(in))
! si la concentration est inferieure � la concentration d'equilibre
                else
                  sed=0.
                endif
                if (tau.lt.taucr) then
                  ero=0.0
!      limitation de tau/taucr a 10
!                 else
!                   if(tau.gt.10.*taucr)then
!                     tau=10.*taucr
!                   endif
                  ero=m*(tau/taucr-1.)
                endif
! si tout calcule a partir concentration equilibre
              else
                sed=alprep*vch2*(cee-ceqn(in))
                ero=0.0
! inutile car sed et ero non utilises separement
!                if(sed.lt.0.)then
!                  ero=-sed
!                  sed=0.
!                endif
! fin du if sur modes/meyer
              endif
!!fin du if sur q=0
!             endif
! on rajoute une condition car on ne peut deposer plus que present
!              if(sed.gt.hce(ie)/dt)sed=hce(ie)/dt
            eseldtn(in)=dt*(sed-ero)
! en un pas de temps, on ne peut deposer plus de 1% du sediment present
! ni eroder sur plus que 1% de la hauteur d'eau
! le 24/1/2014: on va jusqu'�100%depot
            if(eseldtn(in).gt.hn(in)*cn(in))then
              eseldtn(in)=hn(in)*cn(in)
!            if(eseldtn(in).gt.0.01*hn(in)*cn(in))then
!              eseldtn(in)=0.01*hn(in)*cn(in)
            else
              hcmin=-0.01*hn(in)
!              if(cn(in).gt.eps2)then
!                hcmin=max(hcmin,-0.01*hn(in)*cn(in))
!              endif
                if(eseldtn(in).lt.hcmin)then
                  eseldtn(in)=hcmin
                endif
!              endif
            endif
! introduction pour limiter erosion ou depot sur un pas de temps
            rz=eseldtn(in)/dt
            if(rz.gt.vch2)then
              eseldtn(in)=vch2*dt
              if(idl.lt.1000)then
              write(*,*)'depot limite noeud',in
              idl=idl+1
              endif
             elseif(rz.lt.-rzmax)then
              eseldtn(in)=-rzmax*dt
              if(idl.lt.1000)then
              write(*,*)'erosion limite noeud',in
!              write(*,*)'depot limite maille',ie
              idl=idl+1
              endif
            endif
! modif du 16/12/2010 on traite ici hn entre paray et hmin
! en prenant le minimum des depots et 0 pour erosion
!            if(hn(in).lt.hmin)then
! hmin abandonne
            if(hn(in).lt.0.5*d)then
              eseldtn(in)=max(0.d0,eseldtn(in))
!              eseldtn(in)=min(max(0.d0,eseldtn(in)),hn(in)*cn(in))
            endif
            eseldtn(in)=poro*eseldtn(in)
            taun(in)=tau
!  si hn<paray
          else
            eseldtn(in)=0.
!            eseldtn(in)=poro*min(hn(in)*cn(in),vch*dt)
! taun=0 mis en debut de sp
!            taun(in)=0.
!! si hn=0
!          else
!            eseldtn(in)=0.
! fin du if sur hn
          endif
      end do
!       do in=1,nn
      do jn=1,nnloc
        in=noeuds_loc(jn)
        if(eseldtn(in).gt.0.)then
!            eseldtn(in)=rapsmbhcn(in)*eseldtn(in)
#if SEDVAR
          if(sedvar)then
             eron(in)=.false.
          endif
!end if sedvar
#endif
        elseif(eseldtn(in).lt.0.)then
#if SEDVAR
          if(sedvar)then
             eron(in)=.true.
          endif
!end if sedvar
#endif
        endif
      enddo
      call ctopo2(eseldtn)
      ceq(:)=0.
      vitfro(:)=0.
      do i=1,ne_loc(me)
          ie = mailles_loc(i)

          eseldt(ie)=dhe(ie)
          smbhc(ie)=-eseldt(ie)/poro
!          smbhc(ie)=-dhe(ie)/poro
! on met dans dhe le taux de d�p�t
          dhe(ie)=dhe(ie)/dt
      enddo
      do jn=1,nnloc
        in=noeuds_loc(jn)
        do i=1,nevn(in)
          ie=ievn(in,i)
            ceq(ie)=ceq(ie)+ceqn(in)
            vitfro(ie)=vitfro(ie)+vitfrn(in)
          enddo
      enddo
      do i=1,ne_loc(me)
          vitfro(ie)=vitfro(ie)/float(nne(ie))
          ceq(ie)=ceq(ie)/float(nne(ie))
      enddo
      if(meyer)then
        if(modes)then
          do ja=1,naloc
            ia=aretes_loc(ja)
            rapva(ia)=0.5*(rapvn(ina(ia,1))+rapvn(ina(ia,2)))
          enddo
        else
          do ja=1,naloc
            ia=aretes_loc(ja)
            rapva(ia)=zeta
          enddo
        endif
      endif
! fin du if sur sediments/solute
      endif
      end subroutine semba2

!******************************************************************************
      subroutine ctopo2(dhn)
!******************************************************************************
! sous-programme de modification de la topographie par rapport
! a la cote  moyenne zfm
! entree  :
!          dhn : variation de cotes des noeuds
!  sortie :
!          zfe2 : cotes des elements
!          zfa2 : cotes des aretes
!          zfn2 : cotes des noeuds
!          dhe : variation de cotes des elements
!          cofr2: nouvelle pente maille
!
!******************************************************************************
use module_precision

      use module_tableaux,only:eau,ne,na,nn,zfa,zfe,zfm,iae,nne&
     &,eps1,eps2,paray,xn,yn,se,la,dt,cfl,dt0,t,zfn,ina,ine

      use module_ts,only:eseldt,epcoun,diamn,sigman,taummn&
     &,nbcoun,sigma0,taun,dhe,zfndur&
     &,cofr2,zfn2,zfa2,zfe2&
     &,diamnt,sigmant,nbcoun2
      use module_mpi, only:noeuds_loc,aretes_loc,mailles_loc,naloc,ne_loc&
     &,nnloc,me,nmv,elt_voisins
#ifdef WITH_MPI
     use module_messages
#endif

#if SEDVAR
      use module_ts,only:sedvar,diamdhn,sigmadhn,diamsmb,sigmasmb&
     &,diagsh,modes,consed,vanri,corvch,ppente,meyer&
     &,exposantaucr,exptaucr,camenen,hansen
!end if sedvar
#endif

      implicit none

      real(WP) :: dhn(nn)
      integer :: nen(nn)
!!!!!      real(WP),intent(inout) :: cofr2(ne),zfn2(nn),zfa2(na),zfe2(ne)
      integer :: ie,je,in,jn,ia,ja,j
      integer :: ie1,ie2,ie3,ie4

#if SEDVAR
     logical erosion
!end if sedvar
#endif

! write(*,*) 'Allo?'

#if SEDVAR
!  calcul des cotes des noeuds par rapport a la moyenne
!******************************************************
      if(sedvar)then
      do 4 jn=1,nnloc
          in=noeuds_loc(jn)
!           nbcoun2(in)=nbcoun(in)
!          dhn(in)=eseldtn(in)
! erosion
          if(dhn(in).lt.-eps2)then
! on limite erosion a epaisseur premiere couche
                erosion=.true.
             if(taun(in).lt.taummn(in,1))then
                if(modes.and.(.not.camenen))then
                   erosion=.false.
                elseif(meyer.and.(.not.camenen))then
                   erosion=.false.
                endif
             endif
!
              if(.not.erosion)then
! si tau inferieur a taummn
!             if(taun(in).lt.taummn(in,1))then
               dhn(in)=0.
               diamdhn(in)=diamn(in,1)
               sigmadhn(in)=sigman(in,1)
               call zerocouchn(in)
             elseif(-dhn(in).gt.epcoun(in,1))then
               dhn(in)=-epcoun(in,1)
               diamdhn(in)=diamn(in,1)
               sigmadhn(in)=sigman(in,1)
               call supcouchn(in)
             else
                call erocouchn(in,dhn(in),diamdhn(in),sigmadhn(in))
             endif
! depot
          elseif(dhn(in).gt.eps2)then
! depcouchn reporte en fin de sousprogramme
!            call depcouchn(in,dhn(in),hcn(in),diamdhn(in),sigmadhn(in))
            diamdhn(in)=0.d0
            sigmadhn(in)=0.d0
            nen(in)=0
          else
            dhn(in)=0.
            diamdhn(in)=diamn(in,1)
            sigmadhn(in)=sigman(in,1)
            call zerocouchn(in)
          endif
          zfn2(in)=zfn(in)+dhn(in)
 4    continue
! else sur sedvar
      else
!end if sedvar
#endif
      do jn=1,nnloc
        in=noeuds_loc(jn)
!        dhn(in)=eseldtn(in)
        zfn2(in)=zfn(in)+dhn(in)
!        zfn2(in)=zfn(in)+eseldtn(in)
        if ((zfn2(in)).lt.zfndur(in)) then
           zfn2(in)=zfndur(in)
! dhn corrige pour fond dur(toujours negative)
           dhn(in)=min(zfndur(in)-zfn(in),0.d0)
!        else
!           dhn(in)=0.
        endif
      end do
#if SEDVAR
! fin du if sur sedvar
       endif
!end if sedvar
#endif
!  calcul des cotes des aretes
!******************************************************
      do 5 ja = 1,naloc
        ia=aretes_loc(ja)
        zfa2(ia) =.5*(zfn2(ina(ia,1))+zfn2(ina(ia,2)))
!        dha(ia) =.5*(dhn(ina(ia,1))+dhn(ina(ia,2)))
 5    continue
!  calcul des cotes des elements
!*******************************
      do 14 je = 1,ne_loc(me)
        ie = mailles_loc(je)
        zfe2(ie)=zfa2(iae(ie,1))
!        dhe2(ie)=dha(iae(ie,1))
        do 41 j=2,nne(ie)
          zfe2(ie)=zfe2(ie)+zfa2(iae(ie,j))
!          dhe2(ie)=dhe2(ie)+dha(iae(ie,j))
 41     continue
        zfe2(ie)=zfe2(ie)/float(nne(ie))
!        dhe2(ie)=dhe2(ie)/float(nne(ie))
!       calcul de la variation de cote du fond
        dhe(ie)=zfe2(ie)-zfe(ie)
        ie1=ine(ie,1)
        ie2=ine(ie,2)
        ie3=ine(ie,3)
        if(abs(dhe(ie)).lt.eps2)then
            zfe2(ie)=zfe(ie)
            dhe(ie)=0.
#if SEDVAR
        if(sedvar)then
            diamsmb(ie)=diamn(ie1,1)
            sigmasmb(ie)=sigman(ie1,1)
#ifdef WITH_MPI
!             call message_group_calcul(diamsmb)
!             call message_group_calcul(sigmasmb)
#endif
! fin du if sur sedvar
        endif
!end if sedvar
#endif

! else sur dhe=0
        else
#if SEDVAR
        if(sedvar)then
 !TODO check melangemaille
! on melange les dhn de la maille pour obtenir diamsmb
          if(nne(ie).eq.3)then
          call melangemaille(ie,-dhn(ie1)&
     &,-dhn(ie2),-dhn(ie3),0.d0,nen(ie1),nen(ie2),nen(ie3),0)
          else
          call melangemaille(ie&
     &,-dhn(ie1),-dhn(ie2),-dhn(ie3),-dhn(ine(ie,4))&
     &,nen(ie1),nen(ie2),nen(ie3),nen(ine(ie,4)))
          endif
! fin du if sur sedvar
        endif
!end if sedvar
#endif
! endif sur dhe=0
        endif
!        calcul surface horizontale de l'element
          if(nne(ie).eq.4)then
            ie4=ine(ie,4)
          endif

!        calcul surface de l'element en 3d pour pente
! surface du triangle 123
          cofr2(ie)=sqrt(((xn(ie3)-xn(ie2))*(yn(ie1)-yn(ie3))-&
     & (yn(ie3)-yn(ie2))*(xn(ie1)-xn(ie3)))**2+&
     &((xn(ie3)-xn(ie2))*(zfn2(ie1)-zfn2(ie3))-&
     & (zfn2(ie3)-zfn2(ie2))*(xn(ie1)-xn(ie3)))**2+&
     &((yn(ie3)-yn(ie2))*(zfn2(ie1)-zfn2(ie3))-&
     & (zfn2(ie3)-zfn2(ie2))*(yn(ie1)-yn(ie3)))**2 )
          if(nne(ie).eq.4)then
! on ajoute la surface du triangle 143
            cofr2(ie)=cofr2(ie)+sqrt(((xn(ie3)-xn(ie4))*&
     & (yn(ie1)-yn(ie3))-(yn(ie3)-yn(ie4))*(xn(ie1)-xn(ie3)))**2+&
     &((xn(ie3)-xn(ie4))*(zfn2(ie1)-zfn2(ie3))-&
     & (zfn2(ie3)-zfn2(ie4))*(xn(ie1)-xn(ie3)))**2+&
     &((yn(ie3)-yn(ie4))*(zfn2(ie1)-zfn2(ie3))-&
     & (zfn2(ie3)-zfn2(ie4))*(yn(ie1)-yn(ie3)))**2 )
         endif
! cofr2 contient deux fois la surafce en 3d
         cofr2(ie) = 2.*abs(se(ie)/cofr2(ie))
 14    continue
! on applique  le depot aux sommets concern�s

#if SEDVAR
      if(sedvar)then
      do jn=1,nnloc
          in = noeuds_loc(jn)
          if(dhn(in).gt.eps2)then
              if(nen(in).gt.0.)then
! sigmadhn contient la somme des d84 et diamdhn la somme des d50
              sigmadhn(in)=sigmadhn(in)/diamdhn(in)
              diamdhn(in)=diamdhn(in)/nen(in)
              call depcouchn(in,dhn(in)&
     &,diamdhn(in),sigmadhn(in))
              else
                 dhn(in)=0.
               diamdhn(in)=diamn(in,1)
               sigmadhn(in)=sigman(in,1)
            endif
        endif
      enddo
      endif
!end if sedvar
#endif

      end
#endif /* fin section transport solide */

!       subroutine write_contrainte(id_tps)
!       use module_tableaux,only:ne,het,fra,fre,quet,qvet,paray,g,cofr
!       use module_precision,only:wp
!       implicit none
!       real(WP) :: q,vstar,fra2,vfr
!       integer,intent(in) :: id_tps
!       integer :: ie
!       real(WP), dimension(:),allocatable :: vitfro
!
!       allocate(vitfro(ne))
!       do ie=1,ne
!         if(het(ie).gt.paray)then
!           q=sqrt(quet(ie)**2+qvet(ie)**2)
!           vstar=q/het(ie)
! ! on limite la vitesse
!           if(vstar.gt.10.)vstar=10.
! ! calcul du coefficient de chezy
!           fra2=max(fra(ie),fre(ie)*het(ie)**0.3333333)
! ! on divise par cofr car la surface est integree dans fra ou fre
!           fra2=fra2/cofr(ie)
! ! calcul de la vitesse de frottement
!           vfr= sqrt(g)*vstar/sqrt(fra2)
!           vitfro(ie)=vfr
!         else
!           vitfro(ie)=0.
!         endif
!       enddo
!       deallocate(vitfro)
!       write(id_tps,'(8f10.5)') (vitfro(ie),ie=1,ne)
!       end subroutine write_contrainte


