! Copyright (c) 2021, Institut National de Recherche pour l'Agriculture, l'Alimentation et l'Environnement (INRAE)
! All rights reserved.
!
! Redistribution and use in source and binary forms, with or without
! modification, are permitted provided that the following conditions are met:
!
! * Redistributions of source code must retain the above copyright notice, this
!   list of conditions and the following disclaimer.
!
! * Redistributions in binary form must reproduce the above copyright notice,
!   this list of conditions and the following disclaimer in the documentation
!   and/or other materials provided with the distribution.
!
! * Neither the name of the copyright holder nor the names of its
!   contributors may be used to endorse or promote products derived from
!   this software without specific prior written permission.
!
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
! DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
! FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
! DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
! SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
! CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
! OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
! OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

module module_ts
use module_precision
use module_tableaux,only : noumax,noemax

implicit none
!onc
! nombre de couches maximal
      integer :: ncousedmax

      parameter (ncousedmax=10)
! parametres lus dans sed


!     double precision :: diamn(nnmax,ncousedmax)&
!    &,epcoun(nnmax,ncousedmax),sigman(nnmax,ncousedmax)&
!    &,taummn(nnmax,ncousedmax)
!    double precision :: diamn2(nnmax),epcoun2(nnmax)&
!    &,taummn2(nnmax),sigman2(nnmax)&
!    &,diamsmb(nemax),sigmasmb(nemax)&
!    &,diamdhn(nnmax),sigmadhn(nnmax)&


    real(WP),dimension(:,:),allocatable :: diamn&
    &,epcoun,sigman&
    &,taummn
    real(WP),dimension(:),allocatable :: diamn2,epcoun2&
    &,taummn2,sigman2&
    &,diamsmb,sigmasmb&
    &,diamdhn,sigmadhn&


! pour flux entre mailles


!    &,diama(namax),sigmaa(namax)&
!    &,sigma0&
!    &,sigmae(nemax),diame(nemax)&
!    &,sigmae2(nemax),diame2(nemax)&
!    &,sigmaouv(0:namax,nevamx),diamouv(0:namax,nevamx)&
!    &,diamnt(nnmax),sigmant(nnmax)

    &,diama,sigmaa
    real(WP) :: sigma0
    real(WP),dimension(:),allocatable::sigmae,diame&
    &,sigmae2,diame2&
    &,diamnt,sigmant
    real(WP),dimension(:,:),allocatable::sigmaouv,diamouv
! nombre reel de couches du noeud
! integer :: nbcoun(nnmax),nbcoun2(nnmax)
    integer,dimension(:),allocatable :: nbcoun,nbcoun2


! dhe depot par maille, eseldt depot par maille par dt, esdt depot total



!     double precision :: niux,niuy,dens,den,m,kds,d&
!     &,esdt,eseldt(nemax),dhe(nemax)&
!     &,hcal(namax),hcet(nemax),hce(nemax)&
!     &,smbhc(nemax),fhca(namax),pxhc(nemax)&
!     &,pxhce(nemax),pyhce(nemax),hcg1(namax,nevamx)&
!     &,pyhc(nemax),smbhc2(nemax),tk,consnu,vch,zeta,sigm,dix,diy&
!     &,zfe0(nemax),cet(nemax),vitfro(nemax)&
!     &,zfn2(nnmax),zfndur(nnmax)&
!     &,volae,volas,volast,volai,poro&
!     &,coouv(0:namax,nevamx),voloa(noumax),voaouv(noumax)&
!     &,flhc(namax),ceq(nemax)&
!     &,rapsmbhc(nemax)

     real(WP) :: niux,niuy,dens,den,m,kds,d&
     &,esdt,tk,consnu,vch,zeta,sigm,dix,diy&
     &,volae,volas,volai,poro


     real(WP),dimension(:),allocatable :: eseldt,dhe&
     &,hcal,hcet,hce,smbhc,fhca,pxhc&
     &,pxhce,pyhce,pyhc,smbhc2,flhc,ceq&
     &,rapsmbhc,zfe0,cet,vitfro,zfndur

     real(WP) :: voloa(noumax),voaouv(noumax)

     real(WP),dimension(:,:),allocatable :: hcg1,coouv

! ceq : concentration equilibre
!     double precision :: d90,re,alprep,dzfn(nnmax),volsed
     real(WP) :: d90,re,alprep
!     real(WP) :: volsed
     real(WP),dimension(:),allocatable :: dzfn
! ccoup contient les concentrations entrees par les ouvrages
     real(WP) :: ccoup(noumax*noemax*noemax)
     real(WP) :: ccoup2(noumax*noemax*noemax)
     real(WP) :: diamcoup2(noumax*noemax*noemax)
     real(WP) :: sigmacoup2(noumax*noemax*noemax)
     real(WP) :: diamcoup(noumax*noemax*noemax)
     real(WP) :: sigmacoup(noumax*noemax*noemax)
     logical :: diagsh,modes,consed,vanri,corvch,solute,ppente,meyer
     logical :: diffus,conckg,modifz&
! variable logique pour ouverture du fichier dzf
     &,opendz&
! variable logique pour calcul chnagement topo aux noeuds ou aux centres
     &,chzn
     real(WP) :: sg,rzmax,diams,fi,fi2,al,ksi,gdd
     real(WP),dimension(:),allocatable :: rapva

!variables utiles dans les sous-routines semba et ctopo
     real(WP),dimension(:),allocatable :: zfn2,zfa2,zfe2,cofr2

!      real(wp),dimension(:),allocatable :: hae
!!!!!!!!!!!!!
! commentaire car présent dans la sous-routine ledm de rubar20_init.f90
! A voir s'il y a des conflits...
! integer :: je,in
!!!!!!!!!!!!!




     logical :: lmailczero
     integer :: nmailczero
     integer,dimension(:),allocatable :: imailczero
     logical :: avects
     real(WP),dimension(:),allocatable :: taun,taue
     real(WP) :: constantea,constanteb,exposantaucr
     logical :: devch,exptaucr,camenen,hansen
     logical :: derive,cvsurf
     real(WP) :: alfv,betav
     logical :: efficace,idemix

!variables utiles dans l'option sedvar
     logical vchfixe
! indique que diametre variable
     logical sedvar
     real(wp) dchardiam,dcharsigma
     logical,dimension(:),allocatable :: eron

     ! couplage 1D avec TS
     real(wp),dimension(:),allocatable :: ccouplts,dcouplts,scouplts

end module module_ts
