! Copyright (c) 2021, Institut National de Recherche pour l'Agriculture, l'Alimentation et l'Environnement (INRAE)
! All rights reserved.
!
! Redistribution and use in source and binary forms, with or without
! modification, are permitted provided that the following conditions are met:
!
! * Redistributions of source code must retain the above copyright notice, this
!   list of conditions and the following disclaimer.
!
! * Redistributions in binary form must reproduce the above copyright notice,
!   this list of conditions and the following disclaimer in the documentation
!   and/or other materials provided with the distribution.
!
! * Neither the name of the copyright holder nor the names of its
!   contributors may be used to endorse or promote products derived from
!   this software without specific prior written permission.
!
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
! DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
! FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
! DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
! SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
! CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
! OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
! OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

      module comptage

      use module_precision
#ifdef WITH_MPI
      use mpi
#endif /* WITH_MPI */

      implicit none

      interface

        subroutine METIS_PartMeshDual(ne,nn,eptr,eind,vwgt,vsize,ncommon,&
       &np,tpwgts,opts,objval,epart,nparts) bind(C, name="METIS_PartMeshDual")

            ! use binding module
            use iso_c_binding
            ! declare variables with C++ types semantics
            integer(kind=c_int)               :: ne, nn, ncommon, objval, np
            integer(kind=c_int), dimension(*) :: eptr, eind
            integer(kind=c_int), dimension(*) :: epart, nparts
            type(c_ptr), value                :: vwgt, vsize, tpwgts, opts
!             integer(kind=c_int)               :: opts(0:40)

        end subroutine METIS_PartMeshDual

    end interface

      contains


      subroutine aretes
!--------------------------------------------------------------------
!      trouve et compte les aretes du domaine local
!      associe a chaque arete sortante le numero du processeur
!      qui traite la maille voisine
!      idem pour les ouvrages
!--------------------------------------------------------------------
      use module_tableaux, only:ieva,iae,nae,na,neva,ne,nrefa,nbouv,ie1,ie2,typouv,nas,iac,nn,ine,nne&
      &,ievn,nevn
      use module_mpi, only:noeuds_loc,noeuds_bord,aretes_loc,aretes_bord,nb_elt_send_proc,&
      &nb_elt_recv_proc,me,elt_voisins,vecteur_message,np,ne_loc,&
      &nab,naloc,elt_bord,vecteur_reception,nbouvloc,nouvb,ouvrages_loc,&
      &ouvrages_bord,nbouv_b_loc,ouvragesb_loc,nnb,nnloc&
      &,machine,deplacement_recv_proc,deplacement_send_proc,message,message2&
      &,message_logical,nmv,message_scribe,nploc,&
      &message_scribe2,message_scribe3,machine_ias,message_logical2,&
      &message_scribe4,message_scribe4int

      implicit none
      ! np: nombre de processeurs, ou de machines
      ! ja,k: compteurs
      ! nab: nombre d'aretes du bord donnant sur un autre domaine
      ! naloc: nombre d'aretes totales du domaine local
      ! ialoc: indice local de l'arete du domaine local
      ! nouvloc: nombre d'ouvrages dont au moins une maille est dans le domaine local
      ! iouvloc: indice des ouvrages de nouvloc
      ! nouvb: nombre d'ouvrages ayant une seule maille dans le domaine local
      integer::ja,i,k,iab,ialoc,iouvloc,jouv,iouvb,ias
      integer::ie,je,evn_loc,ieb,iev,in,inb,inloc,jn,nmb
      ! elt_voisins: tableau des éléments voisins du domaine local
      ! aretes_loc: tableau des indices globaux des aretes du domaine local
      ! aretres_bord: tableau des aretes du bord du domaine local
      !               hors aretes limites du domaine global
      ! nb_elt_proc: nombre d'éléments voisins calcules par tel processeur
      !             ou le numéro du process est son indice dans le tableau
!      integer,dimension(:),allocatable::aretes_loc,aretes_bord,nb_elt_proc
!      integer,dimension(:),allocatable::elt_voisins,vecteur_message,vecteur_reception

      ! on réserve le dernier processus pour les lectures/écritures
      integer::iouv_b,nbouvbproc
      integer,dimension(:),allocatable ::recv_from
      integer,dimension(:,:),allocatable ::send_to
      nploc=np

      nbouv_b_loc=0
      nab=0
      naloc=0
      nbouvloc=0
      nbouvbproc=0
      allocate(nb_elt_send_proc(0:nploc-1))
      allocate(nb_elt_recv_proc(0:nploc-1))
      nb_elt_send_proc=0
      nb_elt_recv_proc=0
      nouvb=0
      ! boucle sur les aretes du domaine global
      ! on commence par compter avant de stocker
      do ja=1,na
        ! conditions sur les references des aretes
        ! si l'arete n'a qu'un element voisin, elle est au bord du domaine global
        if (ieva(ja,1)==0 .or. ieva(ja,2)==0 .or. neva(ja)==1) then
          if (machine(ieva(ja,1))==me .or. machine(ieva(ja,2))==me) then
            naloc=naloc+1
          end if
        ! on regarde si le premier element de l'arete est dans le domaine local
        elseif (machine(ieva(ja,1))==me) then
          naloc=naloc+1
          ! si oui, on regarde si le second element est aussi dans le domaine local
          if (machine(ieva(ja,2))==me) then
            ! si oui, l'arete est dans le domaine intérieur
          else
            ! sinon, c'est une arete du bord du domaine local
            nab=nab+1
          end if
        ! si le premier elt n'est pas dans le domaine local
        ! on regarde si le second y est
        elseif(machine(ieva(ja,2))==me) then
          ! si oui, c'est une arete du bord du domaine local
          naloc=naloc+1
          nab=nab+1
        else
          ! on passe a l'arete suivante sans rien faire
        end if
      end do

      ! boucle sur les ouvrages
      do jouv=1,nbouv
        if (machine(ie1(jouv))==me) then
        ! si la première maille de l'ouvrage est dans le domaine local:
           nbouvloc=nbouvloc+1
           if(typouv(jouv,1)=='b' .or. typouv(jouv,2)=='b' .or. typouv(jouv,3)=='b' .or. typouv(jouv,4)=='b' .or. &
          &typouv(jouv,5)=='b')then
             nbouv_b_loc=nbouv_b_loc+1
             nbouvbproc=nbouvbproc+1
           end if
           if (machine(ie2(jouv))==me) then
           ! si la seconde maille de l'ouvrage est dans le domaine local:
           ! on ne fait rien
           elseif (ie2(jouv)<=0) then
           ! si la seconde maille de l'ouvrage sort du maillage:
           ! on ne fait rien
           else
             nouvb=nouvb+1
             !nb_elt_proc(machine(ie2(jouv),nploc,ne))=nb_elt_proc(machine(ie2(jouv),nploc,ne))+1
           end if
         elseif (machine(ie2(jouv))==me .and. ie1(jouv) > 0) then
         ! si la première maille de l'ouvrage n'est pas dans le domaine local
         ! si la seconde maille de l'ouvrage est dans le domaine local:
           !nbouvloc=nbouvloc+1
           nouvb=nouvb+1
!            if(typouv(jouv,1)=='b' .or. typouv(jouv,2)=='b' .or. typouv(jouv,3)=='b' .or. typouv(jouv,4)=='b' .or. &
!           &typouv(jouv,5)=='b') then
!              nbouv_b_loc=nbouv_b_loc+1
!              nouvb=nouvb+1
!            end if
           !nb_elt_proc(machine(ie1(jouv),nploc,ne))=nb_elt_proc(machine(ie1(jouv),nploc,ne))+1
         else
         ! on passe a l'ouvrage suivant sans rien faire
         end if
      end do

      ! on a assez d'informations pour allouer les tableaux
      allocate(aretes_loc(naloc),aretes_bord(nab))
      allocate(ouvrages_loc(nbouvloc),ouvrages_bord(nouvb))
      ! ouvragesb_loc ne sert plus à rien
      allocate(ouvragesb_loc(nbouv_b_loc))
      nmv=nab!+nouvb ! nmv = nombre de mailles voisines
!       allocate(elt_voisins(nmv),vecteur_message(nmv))
!       allocate(elt_bord(nmv),vecteur_reception(nmv))

      ! on remplis les tableaux
      ialoc=1
      iab=1
      do ja=1,na
        ! si l'arete est au bord du domaine global:
        if (ieva(ja,1)==0 .or. ieva(ja,2)==0 .or. neva(ja)==1) then
          ! si elle est dans le domaine local
          if (machine(ieva(ja,1))==me .or. machine(ieva(ja,2))==me) then
            ! elle ne peux pas etre une interface entre deux machines
            aretes_loc(ialoc)=ja
            ialoc=ialoc+1
          end if
        ! si l'arete a au moins un élément dans le domaine local:
        elseif (machine(ieva(ja,1))==me .or. machine(ieva(ja,2))==me) then
          aretes_loc(ialoc)=ja
          ! si elle n'en a qu'un seul dans le domaine local:
          if (machine(ieva(ja,2))/=me) then
            aretes_bord(iab)=ja
            ! elle a un element voisin traite par une autre machine
!             elt_voisins(iab)=ieva(ja,2)
!             elt_bord(iab)=ieva(ja,1)
            iab=iab+1
          elseif(machine(ieva(ja,1))/=me) then
            aretes_bord(iab)=ja
!             elt_voisins(iab)=ieva(ja,1)
!             elt_bord(iab)=ieva(ja,2)
            iab=iab+1
          end if
          ialoc=ialoc+1
        end if
      end do

      !!!!!!!!!!
      ! noeuds !
      !!!!!!!!!!

      inloc=0
      inb=0
      do in=1,nn
        evn_loc=0
        do ie=1,nevn(in)
          if (machine(ievn(in,ie))==me .and. ievn(in,ie)>0) then
            evn_loc=evn_loc+1
          end if
        end do
        ! si le noeud a tous ses elements dans le domaine local:
        if (evn_loc == nevn(in)) then
          inloc=inloc+1
        ! si le noeud n'a pas tous ses elements dans le domaine local mais au moins un:
        elseif (evn_loc > 0) then
          inloc=inloc+1
          inb=inb+1
        ! sinon, autre domaine, on laisse passer
        end if
      end do
      nnloc=inloc
      nnb=inb

      ! on a assez d'informations pour allouer les tableaux
      allocate(noeuds_loc(nnloc),noeuds_bord(nnb))

      inloc=1
      inb=1
      do in=1,nn
        evn_loc=0
        do ie=1,nevn(in)
          if (machine(ievn(in,ie))==me .and. ievn(in,ie)>0) then
            evn_loc=evn_loc+1
          end if
        end do
        ! si le noeud a tous ses elements dans le domaine local:
        if (evn_loc == nevn(in)) then
          noeuds_loc(inloc)=in
          inloc=inloc+1
        ! si le noeud n'a pas tous ses elements dans le domaine local mais au moins un:
        elseif (evn_loc > 0) then
          noeuds_loc(inloc)=in
          noeuds_bord(inb)=in
          inloc=inloc+1
          inb=inb+1
        ! sinon, autre domaine, on laisse passer
        end if
      end do

      nmv=0 ! nombre de mailles voisines
      nmb=0 ! nombre de mailles du bord
      ! pour chaque element du maillage
      do ie=1,ne
        if (machine(ie)==me) then
          ! pour chaque noeud de l'element
          inloop1: do in=1,nne(ie)
            do jn=1,size(noeuds_bord)
              if (noeuds_bord(jn)==ine(ie,in)) then
                nmb=nmb+1
                exit inloop1
              end if
            end do
          end do inloop1
        else
          ! pour chaque noeud de l'element
          inloop2: do in=1,nne(ie)
            do jn=1,size(noeuds_bord)
              if (noeuds_bord(jn)==ine(ie,in)) then
                nmv=nmv+1
                exit inloop2
              end if
            end do
          end do inloop2
        end if
      end do
      allocate(elt_voisins(nmv))
      allocate(recv_from(nmv))
      allocate(elt_bord(nmb))
      allocate(send_to(nmb,0:nploc-1))
!       allocate(vecteur_message(nmb))
!       allocate(vecteur_reception(nmv))
      send_to(:,:)=0

      iev=0
      ieb=0
      ! pour chaque element du maillage
      do ie=1,ne
        if (machine(ie)==me) then
          ! pour chaque noeud de l'element
          inloop3: do in=1,nne(ie)
            do jn=1,size(noeuds_bord)
              if (noeuds_bord(jn)==ine(ie,in)) then
                ieb=ieb+1
                elt_bord(ieb)=ie
                exit inloop3
              end if
            end do
          end do inloop3
        else
          evn_loc=0
          ! pour chaque noeud de l'element
          inloop4: do in=1,nne(ie)
            do jn=1,size(noeuds_bord)
              if (noeuds_bord(jn)==ine(ie,in)) then
                iev=iev+1
                elt_voisins(iev)=ie
                exit inloop4
              end if
            end do
          end do inloop4
        end if
      end do

      ! pour chaque element du bord du domaine local
      do ieb=1,nmb
        ie=elt_bord(ieb)
        do jn=1,nne(ie)
          in=ine(ie,jn)
          do iev=1,nevn(in)
            je=ievn(in,iev)
            if (machine(je).ne.me) then
              send_to(ieb,machine(je))=1
            end if
          end do
        end do
      end do

      ! pour chaque element voisin du domaine local
      do iev=1,nmv
        ie=elt_voisins(iev)
        recv_from(iev)=machine(ie)
      end do

      ! les vecteurs a envoyer seront tries par processeurs,
      ! puis numero croissant d'aretes et d'ouvrages
      k=0
      do i=0,nploc-1
        do ieb=1,nmb
          if (send_to(ieb,i)==1) then
            k=k+1
          end if
        end do
      end do
      allocate(vecteur_message(k))
      k=0
      do i=0,nploc-1
        do ieb=1,nmb
          if (send_to(ieb,i)==1) then
            k=k+1
            vecteur_message(k)=elt_bord(ieb)
            nb_elt_send_proc(i)=nb_elt_send_proc(i)+1
          end if
        end do
      end do
      k=0
      do i=0,nploc-1
        do iev=1,nmv
          if (recv_from(iev)==i) then
            k=k+1
          end if
        end do
      end do
      allocate(vecteur_reception(k))
      k=0
      do i=0,nploc-1
        do iev=1,nmv
          if (recv_from(iev)==i) then
            k=k+1
            vecteur_reception(k)=elt_voisins(iev)
            nb_elt_recv_proc(i)=nb_elt_recv_proc(i)+1
          end if
        end do
      end do

      iouvloc=1
      iouvb=1 ! ouvrages du bord
      iouv_b=1 ! ouvrages b
      do jouv=1,nbouv
        if (machine(ie1(jouv))==me) then
        ! si la première maille de l'ouvrage est dans le domaine local:
           ouvrages_loc(iouvloc)=jouv
           iouvloc=iouvloc+1
           if(typouv(jouv,1)=='b' .or. typouv(jouv,2)=='b' .or. typouv(jouv,3)=='b' .or. typouv(jouv,4)=='b' .or. &
           typouv(jouv,5)=='b') then
             ! ouvragesb_loc ne sert plus à rien
             ouvragesb_loc(iouv_b)=jouv
             iouv_b=iouv_b+1
           end if
           if (machine(ie2(jouv))==me) then
           ! si la seconde maille de l'ouvrage est dans le domaine local:
           ! on ne fait rien
           elseif (ie2(jouv)<=0) then
           ! si la seconde maille de l'ouvrage sort du maillage:
           ! on ne fait rien
           else
             ouvrages_bord(iouvb)=jouv
!              elt_voisins(nab+iouvb)=ie2(jouv)
!              elt_bord(nab+iouvb)=ie1(jouv)
             iouvb=iouvb+1
           end if
         elseif (machine(ie2(jouv))==me .and. ie2(jouv)>0) then
         ! si la première maille de l'ouvrage n'est pas dans le domaine local
         ! si la seconde maille de l'ouvrage est dans le domaine local:
           !ouvrages_loc(iouvloc)=jouv
           !iouvloc=iouvloc+1
!            if(typouv(jouv,1)=='b' .or. typouv(jouv,2)=='b' .or. typouv(jouv,3)=='b' .or. typouv(jouv,4)=='b' .or.&
!            typouv(jouv,5)=='b') then
!              ouvragesb_loc(iouv_b)=jouv
!              iouv_b=iouv_b+1
!            end if
           ouvrages_bord(iouvb)=jouv
!            elt_voisins(nab+iouvb)=ie1(jouv)
!            elt_bord(nab+iouvb)=ie2(jouv)
           iouvb=iouvb+1
         else
         ! on passe a l'ouvrage suivant sans rien faire
         end if
      end do

      ! boucle sur les aretes rentrantes
      allocate(machine_ias(nas))
      do ias=1,nas
        if(ieva(iac(ias),1)/=0)then
          machine_ias(ias)=machine(ieva(iac(ias),1))
        else
          machine_ias(ias)=machine(ieva(iac(ias),2))
        end if
      end do

      ! les vecteurs a envoyer seront tries par processeurs,
      ! puis numero croissant d'aretes et d'ouvrages
!       k=0
!       do i=0,nploc-1
!         do iab=1,nab
!           if (machine(elt_voisins(iab))==i) then
!             k=k+1
!             vecteur_message(k)=elt_bord(iab)
!             vecteur_reception(k)=elt_voisins(iab)
!             nb_elt_proc(i)=nb_elt_proc(i)+1
!           end if
!         end do
! !         do iouvb=1,nouvb
! !           if (machine(elt_voisins(nab+iouvb))==i) then
! !             k=k+1
! !             vecteur_message(k)=elt_bord(nab+iouvb)
! !             vecteur_reception(k)=elt_voisins(nab+iouvb)
! !             nb_elt_proc(i)=nb_elt_proc(i)+1
! !           end if
! !         end do
!       end do

! allocation des tableaux utilisés dans la subroutine message_group_calcul
! et dans l'envoi au scribe
      allocate(deplacement_send_proc(0:nploc-1))
      allocate(deplacement_recv_proc(0:nploc-1))
      allocate(message_scribe(naloc))
      allocate(message_scribe2(ne_loc(me)))
      allocate(message_scribe3(ne_loc(me)))
      allocate(message_scribe4(nnloc))
      allocate(message_scribe4int(nnloc))
      allocate(message(size(vecteur_message)))
      allocate(message2(size(vecteur_reception)))
      allocate(message_logical(size(vecteur_message)))
      allocate(message_logical2(size(vecteur_reception)))
!       allocate(message_tableau_aretes(size(vecteur_message),4))
      deplacement_send_proc(0)=0
      deplacement_recv_proc(0)=0
      do i=1,nploc-1
        deplacement_send_proc(i)=deplacement_send_proc(i-1)+nb_elt_send_proc(i-1)
        deplacement_recv_proc(i)=deplacement_recv_proc(i-1)+nb_elt_recv_proc(i-1)
      end do

      call ouvrages_c

      end subroutine aretes


!       subroutine charge(me, ne, np, e1loc, enloc)
! !--------------------------------------------------------------------
! !      calcule la repartition des charges entre les machines
! !--------------------------------------------------------------------
!
!       use module_mpi,only:machine,e1_loc,en_loc,ne_loc
!
!       implicit none
!       ! ne: nombre total de mailles
!       ! np: nombre de processeurs, ou de machines
!       ! me: numero de la machine (de 0 a np-1)
!       ! e1loc: premier indice global du domaine local
!       ! enloc: dernier indice global du domaine local
!       integer :: me, ne, np, e1loc, enloc
!       integer :: card, surplus, i,j,k
!
!       allocate(e1_loc(0:np-1))
!       allocate(en_loc(0:np-1))
!       allocate(ne_loc(0:np-1))
!
!       card = ne/np
!       surplus = mod(ne, np)
!
!
!       if(me < surplus) then
!         e1loc = me*card+me+1
!         enloc = e1loc+card
!       else
!         e1loc = me*card+surplus+1
!         enloc = e1loc+card-1
!       end if
!
!
!       k=1
!       do i=0,np-1
!         if (i<surplus) then
!           e1_loc(i) = i*card+i+1
!           en_loc(i) = e1_loc(i)+card
!           do j=1,card+1
!             machine(k)=i
!             k=k+1
!           end do
!         else
!           e1_loc(i) = i*card+surplus+1
!           en_loc(i) = e1_loc(i)+card-1
!           do j=1,card
!             machine(k)=i
!             k=k+1
!           end do
!         end if
!       end do
!
!       ne_loc=en_loc-e1_loc+1
!
!       end subroutine charge


      subroutine charge2(me, ne, np)
!--------------------------------------------------------------------
!      calcule la repartition des charges entre les machines
!--------------------------------------------------------------------

      use module_mpi,only:machine,ne_loc,mailles_loc,scribe,statinfo
      use module_tableaux,only:debglo,debtar,nob,xe,ye,nn,nne,ine,nbouv
      use iso_c_binding

      implicit none
      ! ne: nombre total de mailles
      ! np: nombre de processeurs, ou de machines
      ! me: numero de la machine (de 0 a np-1)
      integer :: me, ne, np
      integer :: card, surplus, i,j,k
#ifdef WITH_MPI
      integer,dimension(mpi_status_size)::status
#endif /* WITH_MPI */
#if WITH_METIS
      integer,dimension(:),allocatable :: eptr, nodes, npart
      integer(kind=c_int) :: nntot, ncommon
      type(c_ptr)    :: vwgt, vsize, tpwgts, mopts
#else /* WITH_METIS */
      integer,dimension(:),allocatable::itri
#endif /* WITH_METIS */

      allocate(ne_loc(0:np-1))
      ne_loc(:)=0

      card = ne/np
      surplus = modulo(ne, np)

#if WITH_METIS
      nntot = 0
      do i=1,ne
        nntot = nntot+nne(i)
      end do
      allocate(eptr(ne+1), nodes(nntot), npart(nn))
      eptr(1) = 0
      do i=1,ne
        eptr(i+1) = eptr(i)+nne(i)
        do j=1,nne(i)
          nodes(eptr(i)+j)=ine(i,j)-1
        end do
      end do
      vwgt   = c_null_ptr
      vsize  = c_null_ptr
      tpwgts = c_null_ptr
      mopts = c_null_ptr
!       mopts(0)   = 1
!       mopts(7)   = 1
!       if (me==scribe) print*, "eptr",eptr
!       if (me==scribe) print*, "nodes",nodes
      ncommon = 2
      call METIS_PartMeshDual(ne,nn,eptr,nodes,vwgt,vsize,ncommon,np,tpwgts,mopts,i,machine,npart)
!       call METIS_PartMeshNodal(ne,nn,eptr,nodes,vwgt,vsize,np,tpwgts,mopts,i,machine,npart)
      do i=1,ne
        ne_loc(machine(i))=ne_loc(machine(i))+1
      end do
!       print*, ne_loc
      deallocate(eptr, nodes, npart)
#else /* WITH_METIS */
!       allocate(itri(ne))
!       xe(:)=xe(:)*(-1)
!       call indexArrayReal(ne, xe, itri)
!       call test_tri(ne, ye, itri)
!       xe(:)=xe(:)*(-1)
      k=1 ! compteur des mailles globales
!       do i=np-1,0,-1
      do i=0,np-1
        if (i<surplus) then
          do j=1,card+1
!             machine(itri(k))=i ! numero de la machine qui traite la maille k
            machine(k)=i ! numero de la machine qui traite la maille k
            ne_loc(i)=ne_loc(i)+1 ! nombre de mailles traitées par la machine i
            k=k+1
          end do
        else
          do j=1,card
!             machine(itri(k))=i
            machine(k)=i
            ne_loc(i)=ne_loc(i)+1
            k=k+1
          end do
        end if
      end do
!       call test_machine(ne, machine, np)
! !       if (np.eq.2) then
! !         machine(:)=0
! !         ne_loc(0)=ne
! ! !         machine(ne)=0
! !         ne_loc(1)=0
! !       end if
!       deallocate(itri)
#endif /* WITH_METIS */

!       machine(:)=0
!       machine(501:504)=1
!       machine(509:512)=1
!       machine(492:494)=1
!       machine(485:487)=1
!       machine(469:471)=3
!       machine(474:476)=3
!       machine(452:459)=3
!       machine(450:451)=2
!       machine(460:461)=2
!       machine(419:424)=3
!       machine(433:435)=2
!       machine(442:444)=2
!       machine(436:441)=3
!       machine(416:417)=2
!       machine(426:427)=2
!       machine(420)=2
!       machine(423)=2
!       machine(418)=1
!       machine(425)=1
!       machine(399:402)=1
!       machine(403:406)=3
!       machine(407:410)=1
!       machine(383:392)=1
!       machine(386)=3
!       machine(389)=3
!       machine(367:372)=1
!       machine(369)=3
!       machine(351:357)=2
!       machine(332:333)=1
!       machine(334:337)=2
!       machine(338:341)=1
!       machine(315)=1
!       machine(316)=2
!       machine(317:318)=1
!       machine(319:321)=2
!       machine(322)=1
!       machine(323:325)=2
!       machine(298)=1
!       machine(299)=2
!       machine(300)=1
!       machine(301:303)=2
!       machine(304)=1
!       machine(305:307)=2
!       machine(282:284)=1
!       machine(285:286)=2
!       machine(287)=1
!       machine(288)=2
!       machine(265:273)=3
!       machine(249:253)=3




!       allocate(plop(ne))
!
!       if(me==scribe)then
!         do i=1,ne
!  567      k=int(rand(0)*(np))
!           if (k==np) goto 567
!           plop(i)=k
!         end do
!       endif
!       call mpi_barrier(mpi_comm_world,statinfo)
!       call mpi_bcast(plop,ne,mpi_integer,scribe,mpi_comm_world,statinfo)
!
!       machine(1:ne)=plop(1:ne)
!
!       do i=1,ne
!         ne_loc(machine(i))=ne_loc(machine(i))+1
!       end do

!       do i=1,ne,np
!         do j=0,np-1
!           if(i+j<=ne)then
!             machine(i+j)=j
!             ne_loc(j)=ne_loc(j)+1
!           end if
!         end do
!       end do

!       do i=1,ne
!         do j=0,np-1
!           if(xe(i)<=(j+1)*(29./np).and.xe(i)>(j)*(29./np))then
!             machine(i)=j
!             ne_loc(j)=ne_loc(j)+1
!           end if
!         end do
!       end do

      if(debglo.or.debtar) call groupes_aretes
      if(nbouv>0) call repouv
      if(nob>1) call repouvb
      if((debglo.or.debtar).and.nob>1) call verif_groupes_aretes

      allocate(mailles_loc(ne_loc(me)))

      j=1 ! compteur des mailles locales
        do k=1,ne
          if(machine(k)==me)then
            mailles_loc(j)=k ! tableau de conversion local/global
            j=j+1
          end if
        end do

      end subroutine charge2


#ifdef WITH_MPI
      subroutine init_gatherv
!--------------------------------------------------------------------
!      initialise les valeurs pour la communication au scribe
!      subroutine reservee au scribe
!--------------------------------------------------------------------
      use module_mpi,only:deplacements,me,scribe,np,nb_aretes_recues,nb_noeuds_recus,&
     &deplacements2,statinfo,noeuds_loc_all,aretes_loc_all,ouvragesb_loc,&
     &message_scribe,mailles_loc_all,message_scribe2,message_scribe3,ne_loc,&
     &nb_mailles_recues,reception_scribe,reception_scribe2,reception_scribe3,&
     &naloc,nnloc,nbouv_b_loc,deplacements3,reception_scribe4,&
     &reception_scribe4int

      use module_tableaux, only:ntrmax

      implicit none

      integer::i,reste,plop
      integer,dimension(mpi_status_size)::status

      call mpi_send(naloc,1,mpi_integer,scribe,200+me,mpi_comm_world,statinfo)
      call mpi_send(nnloc,1,mpi_integer,scribe,200+2*np+me,mpi_comm_world,statinfo)
!       call mpi_send(nbouv_b_loc,1,mpi_integer,scribe,200+np+me,mpi_comm_world,statinfo)
      if(me==scribe)then
          allocate(nb_mailles_recues(np),deplacements(np))
          allocate(nb_aretes_recues(np),deplacements2(np))
          allocate(nb_noeuds_recus(np),deplacements3(np))

          call mpi_recv(plop,1,mpi_integer,0,200,mpi_comm_world,status,statinfo)
          nb_aretes_recues(1)=plop
          nb_mailles_recues(1)=ne_loc(0)
          deplacements(1) = 0
          deplacements2(1) = 0
          call mpi_recv(plop,1,mpi_integer,0,200+2*np,mpi_comm_world,status,statinfo)
          nb_noeuds_recus(1)=plop
          deplacements3(1) = 0
      ! ouvragesb_loc ne sert plus à rien
          do i=2,np
            deplacements(i)=deplacements(i-1)+ne_loc(i-2)
            nb_mailles_recues(i)=ne_loc(i-1)
            call mpi_recv(plop,1,mpi_integer,i-1,199+i,mpi_comm_world,status,statinfo)
            nb_aretes_recues(i)=plop
            deplacements2(i) = deplacements2(i-1)+nb_aretes_recues(i-1)
            call mpi_recv(plop,1,mpi_integer,i-1,199+2*np+i,mpi_comm_world,status,statinfo)
            nb_noeuds_recus(i)=plop
            deplacements3(i) = deplacements3(i-1)+nb_noeuds_recus(i-1)
          end do
!           deplacements(np)=deplacements(np-1)+ne_loc(np-2)
!           nb_mailles_recues(np)=ne_loc(np-1)
!           deplacements2(np)=deplacements2(np-1)+nb_aretes_recues(np-1)
!           nb_aretes_recues(np)=1
!           deplacements3(np)=deplacements3(np-1)+nb_ouvb_recus(np-1)*ntrmax
!           nb_ouvb_recus(np)=1

          allocate(noeuds_loc_all(deplacements3(np)+nb_noeuds_recus(np)))
          allocate(aretes_loc_all(deplacements2(np)+nb_aretes_recues(np)))
          allocate(mailles_loc_all(deplacements(np)+ne_loc(np-1)))

          allocate(reception_scribe(deplacements2(np)+nb_aretes_recues(np)))
          allocate(reception_scribe2(deplacements(np)+ne_loc(np-1)))
          allocate(reception_scribe3(deplacements(np)+ne_loc(np-1)))
          allocate(reception_scribe4(deplacements3(np)+nb_noeuds_recus(np)))
          allocate(reception_scribe4int(deplacements3(np)+nb_noeuds_recus(np)))
      else
          allocate(nb_mailles_recues(0),deplacements(0))
          allocate(nb_aretes_recues(0),deplacements2(0))
          allocate(nb_noeuds_recus(0),deplacements3(0))

          allocate(noeuds_loc_all(0))
          allocate(aretes_loc_all(0))
          allocate(mailles_loc_all(0))

          allocate(reception_scribe(0))
          allocate(reception_scribe2(0))
          allocate(reception_scribe3(0))
          allocate(reception_scribe4(0))
          allocate(reception_scribe4int(0))
      endif

      ! exemple:
      !call mpi_gatherv(valeurs,longueur_tranche,mpi_real,donnees,nb_elts_recus,&
      !deplacements,mpi_real,2,mpi_comm_world,statinfo)
      !if (rang == 2) print *, ’moi, processus 2 je recois’, donnees(1:ne)

      end subroutine init_gatherv
#endif /* WITH_MPI */


      subroutine groupes_aretes
!--------------------------------------------------------------------
!      Les aretes d'un groupe doivent etre traitees par 1 seul processus
!      On donne toutes les mailles voisines des aretes au processus
!      qui traite la maille voisine de la premiere arete du groupe
!--------------------------------------------------------------------

      use module_tableaux,only:na60,nas,iac,ieva,nrefa
      use module_lecl,only:na30
      use module_mpi,only:machine,ne_loc,machine_aretes30,machine_aretes60

      implicit none

      integer::i,ia,ie,k,ias
      logical::premiere

      k=0
      do i=31,30+na30
        premiere=.true.
        do ias=1,nas
          if (nrefa(iac(ias)).eq.i ) then
            ia=iac(ias)
            ie=ieva(ia,1)
            if (ie.eq.0) then
              ie=ieva(ia,2)
            end if
            if (premiere) then
              premiere=.false.
              k=machine(ie)
              machine_aretes30(i)=k
            else
              if(machine(ie)/=k)then
                ne_loc(machine(ie))=ne_loc(machine(ie))-1
                machine(ie)=k
                ne_loc(k)=ne_loc(k)+1
              end if
            end if
          end if
        end do
      end do

      do i=61,60+na30
        premiere=.true.
        do ias=1,nas
          if (nrefa(iac(ias)).eq.i ) then
            ia=iac(ias)
            ie=ieva(ia,1)
            if (ie.eq.0) then
              ie=ieva(ia,2)
            end if
            if (premiere) then
              premiere=.false.
              k=machine(ie)
              machine_aretes60(i)=k
            else
              if(machine(ie)/=k)then
                ne_loc(machine(ie))=ne_loc(machine(ie))-1
                machine(ie)=k
                ne_loc(k)=ne_loc(k)+1
              end if
            end if
          end if
        end do
      end do

      end subroutine groupes_aretes


      subroutine repouvb
!--------------------------------------------------------------------
!      boucle sur les ouvrages B:
!      l'ouvrage B suivant est donné au process qui traite l'ouvrage B courant
!--------------------------------------------------------------------

      use module_tableaux,only:nbouv,nouv,ouv,ie1,ie2,suivan,nob
      use module_mpi,only:machine,ne_loc

      implicit none

      integer::ibouv,ie,iee

      do ibouv=1,nob-1
        if (suivan(ibouv))then
          ie=ie1(ouv(ibouv))
          iee=ie1(ouv(ibouv+1))
          if (machine(iee)/=machine(ie))then
            ne_loc(machine(iee))=ne_loc(machine(iee))-1
            machine(iee)=machine(ie)
            ne_loc(machine(ie))=ne_loc(machine(ie))+1
          end if
        end if
        ! on donne toutes les breches au proc 0
!         ne_loc(machine(ie1(ouv(ibouv))))=ne_loc(machine(ie1(ouv(ibouv))))-1
!         machine(ie1(ouv(ibouv)))=0
!         ne_loc(0)=ne_loc(0)+1
!         ne_loc(machine(ie2(ouv(ibouv))))=ne_loc(machine(ie2(ouv(ibouv))))-1
!         machine(ie2(ouv(ibouv)))=0
!         ne_loc(0)=ne_loc(0)+1
      end do

      end subroutine repouvb


      subroutine repouv
!--------------------------------------------------------------------
!      boucle sur les ouvrages:
!      ouvrage traité par un seul proc
!--------------------------------------------------------------------

      use module_tableaux,only:nbouv,nouv,ouv,ie1,ie2,suivan,nob
      use module_mpi,only:machine,ne_loc

      implicit none

      integer::ibouv,i1,iee,i2

      do ibouv=1,nbouv
          i1=ie1(ouv(ibouv))
          i2=ie2(ouv(ibouv))
          if (machine(i1)/=machine(i2))then
            ne_loc(machine(i2))=ne_loc(machine(i2))-1
            machine(i2)=machine(i1)
            ne_loc(machine(i1))=ne_loc(machine(i1))+1
          end if
      end do

      end subroutine repouv


      subroutine verif_groupes_aretes
!--------------------------------------------------------------------
!      Vérification que toutes les aretes d'un groupe sont traitées par la même machine
!      Si "non", c'est qu'un ouvrage B interfère.
!--------------------------------------------------------------------

      use module_tableaux,only:na60,nas,iac,ieva,nrefa
      use module_lecl,only:na30
      use module_mpi,only:machine,ne_loc,machine_aretes30,machine_aretes60,statinfo
#ifdef WITH_MPI
      use mpi
#endif /* WITH_MPI */

      implicit none

      integer::i,ia,ie,k,ias
      logical::premiere

      do i=31,30+na30
        do ias=1,nas
          if (nrefa(iac(ias)).eq.i ) then
            ia=iac(ias)
            ie=ieva(ia,1)
            if (ie.eq.0) then
              ie=ieva(ia,2)
            end if
            if (machine(ie)/=machine_aretes30(i)) then
              print*,'-----------------------------------------------'
              print*,' un ouvrage interfere dans un groupe d''aretes '
              print*,'-----------------------------------------------'
#ifdef WITH_MPI
              call mpi_finalize(statinfo)
#endif /* mpi */
              stop
            else
            end if
          end if
        end do
      end do

      do i=61,60+na60
        do ias=1,nas
          if (nrefa(iac(ias)).eq.i ) then
            ia=iac(ias)
            ie=ieva(ia,1)
            if (ie.eq.0) then
              ie=ieva(ia,2)
            end if
            if (machine(ie)/=machine_aretes60(i)) then
              print*,'------------------------------------------------'
              print*,' 000   00   00   000  0000'
              print*,' 0  0 0  0 0      0  0 0'
              print*,' 000  0000  00    0  0 000'
              print*,' 0    0  0    0   0  0 0'
              print*,' 0    0  0 000    000  0000'
              print*,' '
              print*,'  000 0  0  00  0  0  000 0000  0'
              print*,' 0    0  0 0  0 00 0 0    0     0'
              print*,' 0    0000 0000 0 00 0    0000  0'
              print*,' 0    0  0 0  0 0  0 0    0    '
              print*,'  000 0  0 0  0 0  0  000 0000  @'
              print*,' '
              print*,' un ouvrage interfere dans un groupe d''aretes'
              print*,' choisissez une autre répartition du maillage'
              print*,' puis relancez les calculs'
              print*,'------------------------------------------------'
#ifdef WITH_MPI
              call mpi_finalize(statinfo)
#endif /* mpi */
              stop
            else
            end if
          end if
        end do
      end do

      end subroutine verif_groupes_aretes


      subroutine ouvrages_c
!--------------------------------------------------------------------
!      création du vecteur message pour les mailles de contrôle
!--------------------------------------------------------------------

      use module_tableaux,only:nbouv,typouv,ieamont,ieaval,noemax,ie1
      use module_mpi,only:machine,nploc,me,deplacement_message_c,deplacement_reception_c&
     &,nb_elt_message_c,nb_elt_reception_c,vecteur_message_c,vecteur_reception_c&
     &,message_c,reception_c

      implicit none

      integer :: k,iouv,j,compteur_message,compteur_reception

      compteur_message=0
      compteur_reception=0
      allocate(deplacement_message_c(0:nploc))
      allocate(deplacement_reception_c(0:nploc))
      allocate(nb_elt_message_c(0:nploc-1))
      allocate(nb_elt_reception_c(0:nploc-1))

      !comptage des mailles de contrôles qui sont traitées
      !par un process différent de celui qui traite ie1
      !do k=0,nploc-1
        do iouv=1,nbouv
          do j=1,noemax
            if (typouv(iouv,j)=='c')then
              if (ieamont(iouv,j)/=0)then
                if (machine(ieamont(iouv,j))/=machine(ie1(iouv)))then
                  if(machine(ie1(iouv))==me)compteur_reception=compteur_reception+1
                  if(machine(ieamont(iouv,j))==me)compteur_message=compteur_message+1
                end if
              end if
              if (ieaval(iouv,j)/=0)then
                if (machine(ieaval(iouv,j))/=machine(ie1(iouv)))then
                  if(machine(ie1(iouv))==me)compteur_reception=compteur_reception+1
                  if(machine(ieaval(iouv,j))==me)compteur_message=compteur_message+1
                end if
              end if
            end if
          end do
        end do
      !end do

      allocate(vecteur_message_c(compteur_message))
      allocate(vecteur_reception_c(compteur_reception))

      compteur_message=0
      compteur_reception=0
      nb_elt_message_c(:)=0
      nb_elt_reception_c(:)=0


      !envoi/reception tries par numero de processeur, puis d'ouvrage
      do k=0,nploc-1
        do iouv=1,nbouv
          if (machine(ie1(iouv))==k)then
          do j=1,noemax
            if (typouv(iouv,j)=='c')then
              if (ieamont(iouv,j)/=0)then
                if (machine(ieamont(iouv,j))/=machine(ie1(iouv)))then
                  if(machine(ie1(iouv))==me)then
                    compteur_reception=compteur_reception+1
                    nb_elt_reception_c(machine(ieamont(iouv,j)))=nb_elt_reception_c(machine(ieamont(iouv,j)))+1
                    vecteur_reception_c(compteur_reception)=ieamont(iouv,j)
                  elseif(machine(ieamont(iouv,j))==me)then
                    compteur_message=compteur_message+1
                    nb_elt_message_c(machine(ie1(iouv)))=nb_elt_message_c(machine(ie1(iouv)))+1
                    vecteur_message_c(compteur_message)=ieamont(iouv,j)
                  end if
                end if
              end if
              if (ieaval(iouv,j)/=0)then
                if (machine(ieaval(iouv,j))/=machine(ie1(iouv)))then
                  if(machine(ie1(iouv))==me)then
                    compteur_reception=compteur_reception+1
                    nb_elt_reception_c(machine(ieaval(iouv,j)))=nb_elt_reception_c(machine(ieaval(iouv,j)))+1
                    vecteur_reception_c(compteur_reception)=ieaval(iouv,j)
                  elseif(machine(ieaval(iouv,j))==me)then
                    compteur_message=compteur_message+1
                    nb_elt_message_c(machine(ie1(iouv)))=nb_elt_message_c(machine(ie1(iouv)))+1
                    vecteur_message_c(compteur_message)=ieaval(iouv,j)
                  end if
                end if
              end if
            end if
          end do
          end if
        end do
      end do

      deplacement_message_c(0)=0
      deplacement_reception_c(0)=0
      do k=1,nploc
        deplacement_message_c(k)=deplacement_message_c(k-1)+nb_elt_message_c(k-1)
        deplacement_reception_c(k)=deplacement_reception_c(k-1)+nb_elt_reception_c(k-1)
      end do
      allocate(message_c(size(vecteur_message_c)))
      allocate(reception_c(size(vecteur_reception_c)))

      end subroutine ouvrages_c

      subroutine indexArrayReal(n,Array,Index)
        implicit none
        integer, intent(in)  :: n
        real(wp), intent(in), allocatable :: Array(:)
        integer, intent(inout), allocatable :: Index(:)
        integer, parameter   :: nn=15, nstack=50
        integer              :: k,i,j,indext,jstack,l,r
        integer              :: istack(nstack)
        real(wp)                 :: a
        do j = 1,n
            Index(j) = j
        end do
        jstack=0
        l=1
        r=n
        do
            if (r-l < nn) then
                do j=l+1,r
                    indext=Index(j)
                    a=Array(indext)
                    do i=j-1,l,-1
                        if (Array(Index(i)) <= a) exit
                        Index(i+1)=Index(i)
                    end do
                    Index(i+1)=indext
                end do
                if (jstack == 0) return
                r=istack(jstack)
                l=istack(jstack-1)
                jstack=jstack-2
            else
                k=(l+r)/2
                call swap(Index(k),Index(l+1))
                call exchangeIndex(Index(l),Index(r))
                call exchangeIndex(Index(l+1),Index(r))
                call exchangeIndex(Index(l),Index(l+1))
                i=l+1
                j=r
                indext=Index(l+1)
                a=Array(indext)
                do
                    do
                        i=i+1
                        if (Array(Index(i)) >= a) exit
                    end do
                    do
                        j=j-1
                        if (Array(Index(j)) <= a) exit
                    end do
                    if (j < i) exit
                    call swap(Index(i),Index(j))
                end do
                Index(l+1)=Index(j)
                Index(j)=indext
                jstack=jstack+2
                if (jstack > nstack) then
                    write(*,*) 'NSTACK too small in indexArrayReal()'   ! xxx
                    error stop
                end if
                if (r-i+1 >= j-l) then
                    istack(jstack)=r
                    istack(jstack-1)=i
                    r=j-1
                else
                    istack(jstack)=j-1
                    istack(jstack-1)=l
                    l=i
                end if
            end if
        end do
    contains
        subroutine exchangeIndex(i,j)
            integer, intent(inout) :: i,j
            integer :: swp
            if (Array(j) < Array(i)) then
                swp=i
                i=j
                j=swp
            end if
        end subroutine exchangeIndex
        pure elemental subroutine swap(a,b)
            implicit none
            integer, intent(inout) :: a,b
            integer :: dum
            dum=a
            a=b
            b=dum
        end subroutine swap
      end subroutine indexArrayReal

!       subroutine test_tri(n,Array,Index)
!         integer, intent(in)  :: n
!         real(wp), intent(in),allocatable :: Array(:)
!         integer, intent(in),allocatable :: Index(:)
!         integer :: i, j, npb
!         npb = 0
!         print*,"size(itri)",size(Index),"ne",n
!         do i=1,n
!           if (index(i) < 1 .or. index(i) > n) then
!             print*,"*** probleme tri ",npb
!             print*,"itri(",i,") = ",index(i)
!           end if
!         enddo
!         do i=2,n
!         do j=1,i-1
!           if (Array(index(i)) < Array(index(j))) then
!             npb = npb + 1
!             print*,"*** probleme tri ",npb
!           end if
!         enddo
!         enddo
!         npb = 0
!         do i=1,n
!         do j=1,n
!           if (i .ne. j) then
!           if (index(i) == index(j)) then
!             npb = npb + 1
!             print*,"*** probleme tri, deux indices identiques ",npb
!           end if
!           end if
!         enddo
!         enddo
!       end subroutine test_tri

!       subroutine test_machine(n,Array,np)
!         integer, intent(in)  :: n,np
!         integer, intent(in),allocatable :: Array(:)
!         integer :: i, npb
!         npb = 0
!         do i=1,n
!           if (Array(i) >= np .or. Array(i)<0) then
!             npb = npb + 1
!             print*,"*** probleme machine ",npb
!             print*,"*** maille ",i," to machine ",Array(i)
!           end if
!         enddo
!       end subroutine test_machine


      end module
