! Copyright (c) 2021, Institut National de Recherche pour l'Agriculture, l'Alimentation et l'Environnement (INRAE)
! All rights reserved.
!
! Redistribution and use in source and binary forms, with or without
! modification, are permitted provided that the following conditions are met:
!
! * Redistributions of source code must retain the above copyright notice, this
!   list of conditions and the following disclaimer.
!
! * Redistributions in binary form must reproduce the above copyright notice,
!   this list of conditions and the following disclaimer in the documentation
!   and/or other materials provided with the distribution.
!
! * Neither the name of the copyright holder nor the names of its
!   contributors may be used to endorse or promote products derived from
!   this software without specific prior written permission.
!
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
! DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
! FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
! DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
! SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
! CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
! OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
! OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifdef WITH_MPI
module module_messages

      use module_precision

      contains


      subroutine message_gatherv(vecteur)
!--------------------------------------------------------------------
!      envoie les valeurs sur les aretes au scribe
!--------------------------------------------------------------------
      use module_mpi,only:np,naloc,aretes_loc,nb_aretes_recues,deplacements2,&
     &me,scribe,statinfo,message_scribe,aretes_loc_all,reception_scribe

      use mpi

      implicit none

      real(wp),dimension(:),intent(inout)::vecteur
      integer::ia,ja

   !--creation du vecteur message--!
      do ja=1,naloc
        ia=aretes_loc(ja)
        message_scribe(ja)=vecteur(ia)
      end do

   !--envoie/reception--!
      call mpi_gatherv(message_scribe(1:naloc),naloc,mpi_real8,reception_scribe,nb_aretes_recues&
     &,deplacements2,mpi_real8,scribe,mpi_comm_world,statinfo)

   !--tri--!
      if(me==scribe)then
        do ja=1,size(aretes_loc_all)
          ia=aretes_loc_all(ja)
          vecteur(ia)=reception_scribe(ja)
        end do
      end if

      end subroutine message_gatherv


      subroutine message_gatherv2(vecteur)
!--------------------------------------------------------------------
!      envoie les valeurs sur les mailles au scribe
!--------------------------------------------------------------------
      use module_mpi,only:np,mailles_loc,ne_loc,deplacements,&
     &me,scribe,statinfo,message_scribe2,me,nb_mailles_recues,mailles_loc_all,&
     &reception_scribe2

      use mpi

      implicit none

      real(wp),dimension(:),intent(inout)::vecteur
      integer::ie,je

   !--creation du vecteur message--!
      do je=1,ne_loc(me)
        ie=mailles_loc(je)
        message_scribe2(je)=vecteur(ie)
      end do

   !--envoie/reception--!
      call mpi_gatherv(message_scribe2(1:ne_loc(me)),ne_loc(me),mpi_real8,reception_scribe2,nb_mailles_recues&
     &,deplacements,mpi_real8,scribe,mpi_comm_world,statinfo)

   !--tri--!
      if(me==scribe)then
        do je=1,size(mailles_loc_all)
          ie=mailles_loc_all(je)
          vecteur(ie)=reception_scribe2(je)
        end do
      end if

      end subroutine message_gatherv2


      subroutine message_gatherv3(vecteur)
!--------------------------------------------------------------------
!      envoie les valeurs sur les noeuds au scribe
!--------------------------------------------------------------------
      use module_mpi,only:np,nnloc,noeuds_loc,nb_noeuds_recus,deplacements3,&
     &me,scribe,statinfo,message_scribe4,noeuds_loc_all,reception_scribe4

      use mpi

      implicit none

      real(wp),dimension(:),intent(inout)::vecteur
      integer::in,jn

   !--creation du vecteur message--!
      do jn=1,nnloc
        in=noeuds_loc(jn)
        message_scribe4(jn)=vecteur(in)
      end do

   !--envoie/reception--!
      call mpi_gatherv(message_scribe4(1:nnloc),nnloc,mpi_real8,reception_scribe4,&
     &nb_noeuds_recus,deplacements3,mpi_real8,scribe,mpi_comm_world,statinfo)

   !--tri--!
      if(me==scribe)then
        do jn=1,size(noeuds_loc_all)
          in=noeuds_loc_all(jn)
          vecteur(in)=reception_scribe4(jn)
        end do
      end if

      end subroutine message_gatherv3

      subroutine message_gatherv_nbcou(vecteur)
!--------------------------------------------------------------------
!      envoie les valeurs sur les noeuds au scribe
!--------------------------------------------------------------------
      use module_mpi,only:np,nnloc,noeuds_loc,nb_noeuds_recus,deplacements3,&
     &me,scribe,statinfo,message_scribe4int,noeuds_loc_all,reception_scribe4int

      use mpi

      implicit none

      integer,dimension(:),intent(inout)::vecteur
      integer::in,jn

   !--creation du vecteur message--!
      do jn=1,nnloc
        in=noeuds_loc(jn)
        message_scribe4int(jn)=vecteur(in)
      end do

   !--envoie/reception--!
      call mpi_gatherv(message_scribe4int(1:nnloc),nnloc,mpi_int,reception_scribe4int,&
     &nb_noeuds_recus,deplacements3,mpi_int,scribe,mpi_comm_world,statinfo)

   !--tri--!
      if(me==scribe)then
        do jn=1,size(noeuds_loc_all)
          in=noeuds_loc_all(jn)
          vecteur(in)=reception_scribe4int(jn)
        end do
      end if

      end subroutine message_gatherv_nbcou


      subroutine message_gatherv_eau(vecteur)
!--------------------------------------------------------------------
!      envoie les valeurs sur les mailles au scribe
!--------------------------------------------------------------------
      use module_mpi,only:np,mailles_loc,ne_loc,deplacements,&
     &me,scribe,statinfo,message_scribe3,me,nb_mailles_recues,mailles_loc_all,&
     &reception_scribe3

      use mpi

      implicit none

      logical,dimension(:),intent(inout)::vecteur
      integer::ie,je

   !--creation du vecteur message--!
      do je=1,ne_loc(me)
        ie=mailles_loc(je)
        message_scribe3(je)=vecteur(ie)
      end do

   !--envoie/reception--!
      call mpi_gatherv(message_scribe3(1:ne_loc(me)),ne_loc(me),mpi_logical,reception_scribe3,nb_mailles_recues&
     &,deplacements,mpi_logical,scribe,mpi_comm_world,statinfo)

   !--tri--!
      if(me==scribe)then
        do je=1,size(mailles_loc_all)
          ie=mailles_loc_all(je)
          vecteur(ie)=reception_scribe3(je)
        end do
      end if

      end subroutine message_gatherv_eau


      subroutine message_group_calcul(vecteur)
!--------------------------------------------------------------------
!      échange de messages portant sur les mailles du bord
!--------------------------------------------------------------------
      use module_mpi,only:me,np,nb_elt_send_proc,nb_elt_recv_proc,&
     &vecteur_message,vecteur_reception,group_calcul,comm_calcul,&
     &message,message2,nploc,deplacement_send_proc,deplacement_recv_proc
      use mpi

      implicit none

      real(wp),dimension(:),intent(inout)::vecteur
      integer::i!,j,req
      integer,dimension(mpi_status_size)::status
      integer::statinfo

      do i=1,size(vecteur_message)
        message(i)=vecteur(vecteur_message(i))
      end do

      call mpi_alltoallv(message,nb_elt_send_proc,deplacement_send_proc,mpi_real8,&
     &message2,nb_elt_recv_proc,deplacement_recv_proc,mpi_real8,comm_calcul,statinfo)

      do i=1,size(vecteur_reception)
        vecteur(vecteur_reception(i))=message2(i)
      end do

      end subroutine message_group_calcul


      subroutine message_group_calcul_logical(vecteur)
!--------------------------------------------------------------------
!      échange de messages portant sur les mailles du bord dans le cas d'un vecteur booléen
!--------------------------------------------------------------------
      use module_mpi,only:me,np,nb_elt_recv_proc,nb_elt_send_proc,vecteur_message,nploc,&
     &vecteur_reception,group_calcul,comm_calcul,deplacement_send_proc,deplacement_recv_proc,message_logical,message_logical2
      use mpi

      implicit none

      logical,dimension(:),intent(inout)::vecteur
      integer::i
      integer,dimension(mpi_status_size)::status
      integer::statinfo

      do i=1,size(vecteur_message)
        message_logical(i)=vecteur(vecteur_message(i))
      end do
!
!       do i=0,nploc-1
!         if(nb_elt_proc(i)>0)then
!           call mpi_send(message_logical(deplacement_elt_proc(i)+1:deplacement_elt_proc(i+1))&
!          &,nb_elt_proc(i),mpi_logical,i,3000+me,comm_calcul,statinfo)
!         end if
!       end do
!
!       do i=0,nploc-1
!         if(nb_elt_proc(i)>0)then
!           call mpi_recv(message_logical(deplacement_elt_proc(i)+1:deplacement_elt_proc(i+1))&
!          &,nb_elt_proc(i),mpi_logical,i,3000+i,comm_calcul,status,statinfo)
!         end if
!       end do

      call mpi_alltoallv(message_logical,nb_elt_send_proc,deplacement_send_proc,mpi_logical,&
     &message_logical2,nb_elt_recv_proc,deplacement_recv_proc,mpi_logical,comm_calcul,statinfo)

      do i=1,size(vecteur_reception)
        vecteur(vecteur_reception(i))=message_logical2(i)
      end do

      end subroutine message_group_calcul_logical


      subroutine message_group_calcul_c(vecteur)
!--------------------------------------------------------------------
!      échange de messages portant sur les mailles de contrôle des ouvrages c
!--------------------------------------------------------------------
      use module_mpi,only:me,nploc,deplacement_message_c,deplacement_reception_c&
     &,nb_elt_message_c,nb_elt_reception_c,vecteur_message_c,vecteur_reception_c&
     &,message_c,reception_c,comm_calcul
      use mpi

      implicit none

      real(wp),dimension(:),intent(inout)::vecteur
      integer::i!,j,req
      integer,dimension(mpi_status_size)::status
      integer::statinfo

      do i=1,size(vecteur_message_c)
        message_c(i)=vecteur(vecteur_message_c(i))
      end do

      call mpi_alltoallv(message_c,nb_elt_message_c,deplacement_message_c,mpi_real8,&
     &reception_c,nb_elt_reception_c,deplacement_reception_c,mpi_real8,comm_calcul,statinfo)

      do i=1,size(vecteur_reception_c)
        vecteur(vecteur_reception_c(i))=reception_c(i)
      end do

      end subroutine message_group_calcul_c


end module
#endif /* WITH_MPI */
